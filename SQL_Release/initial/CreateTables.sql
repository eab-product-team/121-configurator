drop table CONFIG_REPO;
create table CONFIG_REPO (
  CONFIG_ID number,
  CONFIG_TYPE varchar2(30),
  CONFIG_NAME varchar2(50),
  CONFIG_DESC varchar2(100),
  CONFIG_VALUE clob,
  CREATE_DATE date,
  CREATE_BY varchar2(50),
  UPD_DATE date,
  UPD_BY varchar2(50)
);
drop sequence CONFIG_REPO_SEQ;
CREATE SEQUENCE CONFIG_REPO_SEQ INCREMENT BY 50;


drop table CONFIG_MST;
create table CONFIG_MST (
  ID number,
  module varchar2(20),
  type varchar2(20),
  CONFIG_NAME varchar2(50),
  CONFIG_DESC varchar2(100),
  CREATE_DATE date,
  CREATE_BY varchar2(50),
  UPD_DATE date,
  UPD_BY varchar2(50),
  constraint CONFIG_MST primary key (ID)
);
drop sequence CONFIG_SEQ;
create sequence CONFIG_SEQ increment by 50;


drop table CONFIG_VERSION;
create table CONFIG_VERSION (
  ID number,
  CONFIG_ID number,
  VERSION_NUM number,
  VERSION_DESC varchar2(100),
  CREATE_DATE date,
  CREATE_BY varchar2(50),
  UPD_DATE date,
  UPD_BY varchar2(50),
  constraint CONFIG_VERSION_PK primary key (ID),
  constraint CONFIG_VERSION_FK foreign key (CONFIG_ID) references CONFIG_MST(ID) on delete cascade
);
drop sequence CONFIG_VERSION_SEQ;
create sequence CONFIG_VERSION_SEQ increment by 50;


drop table CONFIG_SECTION;
create table CONFIG_SECTION (
  ID number,
  VERSION_ID number,
  SECTION_CODE varchar2(50),
  JSON_FILE clob,
  CREATE_DATE date,
  CREATE_BY varchar2(50),
  UPD_DATE date,
  UPD_BY varchar2(50),
  constraint CONFIG_SECTION_PK primary key (ID),
  constraint CONFIG_SECTION_FK foreign key (VERSION_ID) references CONFIG_VERSION(ID) on delete cascade,
  constraint CONFIG_SECTION_UNIQUE unique (VERSION_ID, SECTION_CODE)
);
drop sequence CONFIG_SECTION_SEQ;
create sequence CONFIG_SECTION_SEQ increment by 50;

drop table FUND_DETAIL_MST;
create table FUND_DETAIL_MST (	
  FUND_ID number, 
  COMP_CODE varchar2(10 BYTE) not null enable, 
  FUND_CODE varchar2(50 BYTE) not null enable, 
  FUND_NAME varchar2(500 BYTE), 
  ASSET_CLASS varchar2(50 BYTE) not null enable, 
  PAYMENT_METHOD varchar2(50 BYTE) not null enable, 
  RISK_RATING varchar2(50 BYTE) not null enable, 
  CREATE_DATE date, 
  CREATE_BY varchar2(50 BYTE), 
  UPDATE_DATE date, 
  UPDATE_BY varchar2(50 BYTE), 
  CCY varchar2(10 BYTE), 
  IS_MIXED_ASSET varchar2(10 BYTE), 
  ANNUAL_MANAGE_FEE varchar2(50 BYTE), 
  constraint "constrain_fundCode" unique ("FUND_CODE"),
  constraint "PK_fundID" unique ("FUND_ID")
);
drop sequence SEQ_FUND;
create sequence SEQ_FUND increment by 50;


CREATE OR REPLACE VIEW v_product (cov_code, cov_desc, plan_type, create_date, create_by, upd_date, upd_by) AS 
select config_desc, config_name || ' (' || config_desc || ')', type, create_date, create_by, upd_date, upd_by from config_mst where module = 'product' order by config_name;

CREATE OR REPLACE VIEW v_pdf (pdf_code, pdf_desc, create_date, create_by, upd_date, upd_by) AS 
select config_desc, config_name || ' (' || config_desc || ')', create_date, create_by, upd_date, upd_by from config_mst where module = 'pdf' order by config_name;

CREATE OR REPLACE VIEW v_fund_detail_mst (fund_name, fund_code, fund_desc, create_date, create_by, update_date, update_by) AS
select fund_name, fund_code, fund_name || ' (' || fund_code || ')', create_date, create_by, update_date, update_by from fund_detail_mst;

package com.eab.model.environment.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class EnvKey implements Serializable {

    @Column(name = "COMP_CODE")
    private String compCode;

    @Column(name = "ENV_ID")
    private String envId;

    public String getCompCode() {
        return compCode;
    }

    public void setCompCode(String compCode) {
        this.compCode = compCode;
    }

    public String getEnvId() {
        return envId;
    }

    public void setEnvId(String envId) {
        this.envId = envId;
    }
}

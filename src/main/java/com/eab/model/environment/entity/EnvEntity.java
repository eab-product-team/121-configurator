package com.eab.model.environment.entity;

import com.eab.model.environment.Environment;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "SYS_ENV_MST")
public class EnvEntity {

    @EmbeddedId
    private EnvKey key;

    @Column(name = "ENV_NAME", nullable = false)
    private String name;

    @Column(name = "HOST")
    private String host;

    @Column(name = "PORT")
    private String port;

    @Column(name = "DB_NAME")
    private String dbName;

    @Column(name = "DB_LOGIN")
    private String userName;

    @Column(name = "DB_PASSWORD")
    private String password;

    public EnvKey getKey() {
        return key;
    }

    public void setKey(EnvKey key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Environment toModel() {
        Environment model = new Environment();
        model.setCompCode(this.key.getCompCode());
        model.setEnvId(this.key.getEnvId());
        model.setName(this.name);
        model.setHost(this.host);
        model.setPort(this.port);
        model.setDbName(this.dbName);
        model.setUserName(this.userName);
        model.setPassword(this.password);
        return model;
    }

}

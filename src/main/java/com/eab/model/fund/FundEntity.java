package com.eab.model.fund;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "FUND_DETAIL_MST")
public class FundEntity {

    @Id
    @GeneratedValue(generator = "fundSeq")
    @SequenceGenerator(name = "fundSeq", sequenceName = "SEQ_FUND")
    @Column(name = "FUND_ID", nullable = false)
    private int id;

    @Column(name = "COMP_CODE", nullable = false)
    private String compCode;

    @Column(name = "FUND_CODE", nullable = false)
    private String fundCode;

    @Column(name = "FUND_NAME", nullable = false)
    private String fundName;

    @Column(name = "ASSET_CLASS", nullable = false)
    private String assetClass;

    @Column(name = "ANNUAL_MANAGE_FEE", nullable = false)
    private String annualManageFee;

    @Column(name = "PAYMENT_METHOD", nullable = false)
    private String paymentMethod;

    @Column(name = "RISK_RATING", nullable = false)
    private String riskRating;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "CREATE_BY")
    private String createBy;

    @Column(name = "UPDATE_DATE")
    private Date updateDate;

    @Column(name = "UPDATE_BY")
    private String updateBy;

    @Column(name = "CCY")
    private String ccy;

    @Column(name = "IS_MIXED_ASSET")
    private String isMixedAsset;

    public FundEntity() {
    }

    public FundEntity(Fund fund) {
        this.id = fund.getId();
        this.compCode = fund.getCompCode();
        this.fundCode = fund.getFundCode();
        this.fundName = fund.getFundName();
        this.assetClass = fund.getAssetClass();
        this.annualManageFee = fund.getAnnualManageFee();
        this.paymentMethod = fund.getPaymentMethod();
        this.riskRating = fund.getRiskRating() != null ? fund.getRiskRating().toString() : null;
        this.createDate = fund.getCreateDate();
        this.createBy = fund.getCreateBy();
        this.updateDate = fund.getUpdateDate();
        this.updateBy = fund.getUpdateBy();
        this.ccy = fund.getCcy();
        this.isMixedAsset = fund.getIsMixedAsset() ? "Y" : "N";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCompCode() {
        return compCode;
    }

    public void setCompCode(String compCode) {
        this.compCode = compCode;
    }

    public String getFundCode() {
        return fundCode;
    }

    public void setFundCode(String fundCode) {
        this.fundCode = fundCode;
    }

    public String getFundName() {
        return fundName;
    }

    public void setFundName(String fundName) {
        this.fundName = fundName;
    }

    public String getAssetClass() {
        return assetClass;
    }

    public void setAssetClass(String assetClass) {
        this.assetClass = assetClass;
    }

    public String getAnnualManageFee() {
        return annualManageFee;
    }

    public void setAnnualManageFee(String annualManageFee) {
        this.annualManageFee = annualManageFee;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getRiskRating() {
        return riskRating;
    }

    public void setRiskRating(String riskRating) {
        this.riskRating = riskRating;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public String getCcy() {
        return ccy;
    }

    public void setCcy(String ccy) {
        this.ccy = ccy;
    }

    public String getIsMixedAsset() {
        return isMixedAsset;
    }

    public void setIsMixedAsset(String isMixedAsset) {
        this.isMixedAsset = isMixedAsset;
    }

    public Fund toModel() {
        Fund fund = new Fund();
        fund.setId(this.id);
        fund.setCompCode(this.compCode);
        fund.setFundCode(this.fundCode);
        fund.setFundName(this.fundName);
        fund.setAssetClass(this.assetClass);
        fund.setAnnualManageFee(this.annualManageFee);
        fund.setPaymentMethod(this.paymentMethod);
        try {
            fund.setRiskRating(this.riskRating != null ? Integer.parseInt(this.riskRating) : null);
        } catch (NumberFormatException e) {
            fund.setRiskRating(null);
        }
        fund.setCreateDate(this.createDate);
        fund.setCreateBy(this.createBy);
        fund.setUpdateDate(this.updateDate);
        fund.setUpdateBy(this.updateBy);
        fund.setCcy(this.ccy);
        fund.setIsMixedAsset("Y".equalsIgnoreCase(this.isMixedAsset));
        return fund;
    }
}

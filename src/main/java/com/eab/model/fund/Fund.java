package com.eab.model.fund;

import java.util.Date;

public class Fund {

    private int id;

    private String compCode;

    private String fundCode;

    private String fundName;

    private String assetClass;

    private String annualManageFee;

    private String paymentMethod;

    private Integer riskRating;

    private Date createDate;

    private String createBy;

    private Date updateDate;

    private String updateBy;

    private String ccy;

    private boolean isMixedAsset;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCompCode() {
        return compCode;
    }

    public void setCompCode(String compCode) {
        this.compCode = compCode;
    }

    public String getFundCode() {
        return fundCode;
    }

    public void setFundCode(String fundCode) {
        this.fundCode = fundCode;
    }

    public String getFundName() {
        return fundName;
    }

    public void setFundName(String fundName) {
        this.fundName = fundName;
    }

    public String getAssetClass() {
        return assetClass;
    }

    public void setAssetClass(String assetClass) {
        this.assetClass = assetClass;
    }

    public String getAnnualManageFee() {
        return annualManageFee;
    }

    public void setAnnualManageFee(String annualManageFee) {
        this.annualManageFee = annualManageFee;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public Integer getRiskRating() {
        return riskRating;
    }

    public void setRiskRating(Integer riskRating) {
        this.riskRating = riskRating;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public String getCcy() {
        return ccy;
    }

    public void setCcy(String ccy) {
        this.ccy = ccy;
    }

    public boolean getIsMixedAsset() {
        return isMixedAsset;
    }

    public void setIsMixedAsset(boolean isMixedAsset) {
        this.isMixedAsset = isMixedAsset;
    }
}

package com.eab.model.fund;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class FundId implements Serializable {

    @Column(name = "COMP_CODE", nullable = false)
    private String compCode;

    @Column(name = "FUND_CODE", nullable = false)
    private String fundCode;

    public FundId() {

    }

    public FundId(String compCode, String fundCode) {
        this.compCode = compCode;
        this.fundCode = fundCode;
    }

    public String getCompCode() {
        return compCode;
    }

    public String getFundCode() {
        return fundCode;
    }
}
package com.eab.model.configuration.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class AttachmentKey implements Serializable {

    @Column(name = "COMP_CODE")
    private String compCode;

    @Column(name = "MODULE")
    private String module;

    @Column(name = "DOC_CODE")
    private String docCode;

    @Column(name = "ATT_ID")
    private String attId;

    @Column(name = "VERSION")
    private Integer version = 1;

    public AttachmentKey() {
    }

    public AttachmentKey(String compCode, String module, String docCode, String attId, Integer version) {
        this.compCode = compCode;
        this.module = module;
        this.docCode = docCode;
        this.attId = attId;
        this.version = version;
    }

    public String getCompCode() {
        return compCode;
    }

    public void setCompCode(String compCode) {
        this.compCode = compCode;
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public String getDocCode() {
        return docCode;
    }

    public void setDocCode(String docCode) {
        this.docCode = docCode;
    }

    public String getAttId() {
        return attId;
    }

    public void setAttId(String attId) {
        this.attId = attId;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }
}

package com.eab.model.configuration.entity;

import com.eab.model.configuration.ConfigSection;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "CONFIG_SECTION")
public class ConfigSectionEntity {

    @Id
    @GeneratedValue(generator = "configSectionSeq")
    @SequenceGenerator(name = "configSectionSeq", sequenceName = "CONFIG_SECTION_SEQ")
    @Column(name = "ID", nullable = false)
    private Integer id;

    @Column(name = "SECTION_CODE", nullable = false)
    private String sectionCode;

    @Column(name = "JSON_FILE")
    private String json;

    @Column(name = "CREATE_DATE", nullable = false)
    private Date createDate;

    @Column(name = "CREATE_BY", nullable = false)
    private String createBy;

    @Column(name = "UPD_DATE", nullable = false)
    private Date updateDate;

    @Column(name = "UPD_BY", nullable = false)
    private String updateBy;

    @ManyToOne
    @JoinColumn(name = "VERSION_ID", nullable = false)
    private ConfigVersionEntity version;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSectionCode() {
        return sectionCode;
    }

    public void setSectionCode(String sectionCode) {
        this.sectionCode = sectionCode;
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public ConfigVersionEntity getVersion() {
        return version;
    }

    public void setVersion(ConfigVersionEntity version) {
        this.version = version;
    }

    public ConfigSection toModel() {
        ConfigSection section = new ConfigSection();
        section.setId(this.id);
        section.setSectionCode(this.sectionCode);
        section.setJson(this.json);
        section.setCreateDate(this.createDate);
        section.setCreateBy(this.createBy);
        section.setUpdateDate(this.updateDate);
        section.setUpdateBy(this.updateBy);
        return section;
    }

}

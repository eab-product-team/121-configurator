package com.eab.model.configuration.entity;

import com.eab.model.configuration.Attachment;
import sun.misc.BASE64Decoder;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.IOException;

@Entity
@Table(name = "DYN_ATTACHMENT_MST")
public class AttachmentEntity {

    @EmbeddedId
    private AttachmentKey key;

    @Column(name = "ATT_FILE")
    private byte[] data;

    public AttachmentKey getKey() {
        return key;
    }

    public void setKey(AttachmentKey key) {
        this.key = key;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public Attachment toModel() throws IOException {
        Attachment model = new Attachment();
        model.setAttId(this.key.getAttId());
        if (this.data != null) {
            String b64String = new String(this.data);
            String contentType = b64String.split(";base64,")[0].replace("data:", "");
            model.setContentType(contentType);
            model.setData(new BASE64Decoder().decodeBuffer(b64String.split(";base64,")[1]));
        }
        return model;
    }
}

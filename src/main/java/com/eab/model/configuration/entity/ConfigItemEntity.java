package com.eab.model.configuration.entity;

import com.eab.model.configuration.ConfigItem;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "CONFIG_REPO")
public class ConfigItemEntity {

    @Id
    @GeneratedValue(generator = "itemSeq")
    @SequenceGenerator(name = "itemSeq", sequenceName = "CONFIG_REPO_SEQ")
    @Column(name = "CONFIG_ID", nullable = false)
    private Integer id;

    @Column(name = "CONFIG_TYPE", nullable = false)
    private String configType;

    @Column(name = "CONFIG_NAME", nullable = false)
    private String name;

    @Column(name = "CONFIG_DESC")
    private String description;

    @Column(name = "CONFIG_VALUE")
    private String value;

    @Column(name = "CREATE_DATE", nullable = false)
    private Date createDate;

    @Column(name = "CREATE_BY", nullable = false)
    private String createBy;

    @Column(name = "UPD_DATE", nullable = false)
    private Date updateDate;

    @Column(name = "UPD_BY", nullable = false)
    private String updateBy;

    public ConfigItemEntity() {
    }

    public ConfigItemEntity(ConfigItem configItem) {
        this.id = configItem.getId();
        this.configType = configItem.getConfigType();
        this.name = configItem.getName();
        this.description = configItem.getDescription();
        this.value = configItem.getValue();
        this.createBy = configItem.getCreateBy();
        this.createDate = configItem.getCreateDate();
        this.updateBy = configItem.getUpdateBy();
        this.updateDate = configItem.getUpdateDate();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getConfigType() {
        return configType;
    }

    public void setConfigType(String configType) {
        this.configType = configType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public ConfigItem toModel() {
        ConfigItem configItem = new ConfigItem();
        configItem.setId(this.id);
        configItem.setConfigType(this.configType);
        configItem.setValue(this.value);
        configItem.setName(this.name);
        configItem.setDescription(this.description);
        configItem.setCreateBy(this.createBy);
        configItem.setCreateDate(this.createDate);
        configItem.setUpdateBy(this.updateBy);
        configItem.setUpdateDate(this.updateDate);
        return configItem;
    }
}

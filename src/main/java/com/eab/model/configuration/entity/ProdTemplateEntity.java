package com.eab.model.configuration.entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "PROD_TEMPLATE")
public class ProdTemplateEntity {

    @EmbeddedId
    private ProdTemplateKey key;

    @Column(name = "TEMPLATE_FILE")
    private String template;

    public ProdTemplateKey getKey() {
        return key;
    }

    public void setKey(ProdTemplateKey key) {
        this.key = key;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

}

package com.eab.model.configuration.entity;

import com.eab.model.configuration.ConfigSummary;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "CONFIG_MST")
public class ConfigSummaryEntity {

    @Id
    @GeneratedValue(generator = "configSummarySeq")
    @SequenceGenerator(name = "configSummarySeq", sequenceName = "CONFIG_SEQ")
    @Column(name = "ID", nullable = false)
    private Integer id;

    @Column(name = "MODULE", nullable = false)
    private String module;

    @Column(name = "TYPE")
    private String type;

    @Column(name = "CONFIG_NAME", nullable = false)
    private String name;

    @Column(name = "CONFIG_DESC")
    private String description;

    @Column(name = "CREATE_DATE", nullable = false)
    private Date createDate;

    @Column(name = "CREATE_BY", nullable = false)
    private String createBy;

    @Column(name = "UPD_DATE", nullable = false)
    private Date updateDate;

    @Column(name = "UPD_BY", nullable = false)
    private String updateBy;

    public ConfigSummaryEntity() {
    }

    public ConfigSummaryEntity(ConfigSummary summary) {
        this.id = summary.getId();
        this.module = summary.getModule();
        this.type = summary.getType();
        this.name = summary.getName();
        this.description = summary.getDescription();
        this.createDate = summary.getCreateDate();
        this.createBy = summary.getCreateBy();
        this.updateDate = summary.getUpdateDate();
        this.updateBy = summary.getUpdateBy();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public ConfigSummary toModel() {
        ConfigSummary summary = new ConfigSummary();
        summary.setId(this.id);
        summary.setModule(this.module);
        summary.setType(this.type);
        summary.setName(this.name);
        summary.setDescription(this.description);
        summary.setCreateDate(this.createDate);
        summary.setCreateBy(this.createBy);
        summary.setUpdateDate(this.updateDate);
        summary.setUpdateBy(this.updateBy);
        return summary;
    }
}

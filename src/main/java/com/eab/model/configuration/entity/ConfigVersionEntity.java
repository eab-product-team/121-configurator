package com.eab.model.configuration.entity;

import com.eab.model.configuration.ConfigVersion;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "CONFIG_VERSION")
public class ConfigVersionEntity {

    @Id
    @GeneratedValue(generator = "versionSeq")
    @SequenceGenerator(name = "versionSeq", sequenceName = "CONFIG_VERSION_SEQ")
    @Column(name = "ID", nullable = false)
    private Integer id;

    @Column(name = "VERSION_NUM", nullable = false)
    private int versionNum;

    @Column(name = "VERSION_DESC")
    private String description;

    @Column(name = "CREATE_DATE", nullable = false)
    private Date createDate;

    @Column(name = "CREATE_BY", nullable = false)
    private String createBy;

    @Column(name = "UPD_DATE", nullable = false)
    private Date updateDate;

    @Column(name = "UPD_BY", nullable = false)
    private String updateBy;

    @ManyToOne
    @JoinColumn(name = "CONFIG_ID")
    private ConfigSummaryEntity summary;

    public ConfigVersionEntity() {
    }

    public ConfigVersionEntity(ConfigVersion version, ConfigSummaryEntity summary) {
        this.id = version.getId();
        this.versionNum = version.getVersionNum();
        this.description = version.getDescription();
        this.createDate = version.getCreateDate();
        this.createBy = version.getCreateBy();
        this.updateDate = version.getUpdateDate();
        this.updateBy = version.getUpdateBy();
        this.summary = summary;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getVersionNum() {
        return versionNum;
    }

    public void setVersionNum(int versionNum) {
        this.versionNum = versionNum;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public ConfigSummaryEntity getSummary() {
        return summary;
    }

    public void setSummary(ConfigSummaryEntity summary) {
        this.summary = summary;
    }

    public ConfigVersion toModel() {
        ConfigVersion model = new ConfigVersion();
        model.setId(this.id);
        model.setVersionNum(this.versionNum);
        model.setDescription(this.description);
        model.setCreateDate(this.createDate);
        model.setCreateBy(this.createBy);
        model.setUpdateDate(this.updateDate);
        model.setUpdateBy(this.updateBy);
        return model;
    }

}

package com.eab.model.configuration.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class ProdTemplateKey implements Serializable {

    @Column(name = "COMP_CODE")
    private String compCode;

    @Column(name = "TYPE")
    private String type;

    @Column(name = "TEMPLATE_CODE")
    private String templateCode;

    @Column(name = "VERSION")
    private int version;

    public ProdTemplateKey() {
    }

    public ProdTemplateKey(String compCode, String type, String templateCode, int version) {
        this.compCode = compCode;
        this.type = type;
        this.templateCode = templateCode;
        this.version = version;
    }

    public String getCompCode() {
        return compCode;
    }

    public void setCompCode(String compCode) {
        this.compCode = compCode;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTemplateCode() {
        return templateCode;
    }

    public void setTemplateCode(String templateCode) {
        this.templateCode = templateCode;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }
}

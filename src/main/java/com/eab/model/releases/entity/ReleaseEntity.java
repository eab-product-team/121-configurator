package com.eab.model.releases.entity;

import com.eab.model.configuration.entity.ConfigVersionEntity;
import com.eab.model.releases.Release;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(name = "CONFIG_RELEASE")
public class ReleaseEntity {

    @Id
    @GeneratedValue(generator = "releaseSeq")
    @SequenceGenerator(name = "releaseSeq", sequenceName = "CONFIG_RELEASE_SEQ")
    @Column(name = "ID", nullable = false)
    private Integer id;

    @Column(name = "NAME", nullable = false)
    private String name;

    @Column(name = "STATUS")
    private String status;

    @Column(name = "CREATE_DATE", nullable = false)
    private Date createDate;

    @Column(name = "CREATE_BY", nullable = false)
    private String createBy;

    @Column(name = "UPD_DATE", nullable = false)
    private Date updateDate;

    @Column(name = "UPD_BY", nullable = false)
    private String updateBy;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "CONFIG_RELEASE_VERSION",
            joinColumns = {@JoinColumn(name = "RELEASE_ID")},
            inverseJoinColumns = {@JoinColumn(name = "VERSION_ID")})
    private List<ConfigVersionEntity> versions;

    public ReleaseEntity() {
    }

    public ReleaseEntity(Release release) {
        this.id = release.getId();
        this.name = release.getName();
        this.status = release.getStatus();
        this.createDate = release.getCreateDate();
        this.createBy = release.getCreateBy();
        this.updateDate = release.getUpdateDate();
        this.updateBy = release.getUpdateBy();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public List<ConfigVersionEntity> getVersions() {
        return versions;
    }

    public void setVersions(List<ConfigVersionEntity> versions) {
        this.versions = versions;
    }

    public Release toModel() {
        Release model = new Release();
        model.setId(this.id);
        model.setName(this.name);
        model.setStatus(this.status);
        model.setCreateDate(this.createDate);
        model.setCreateBy(this.createBy);
        model.setUpdateDate(this.updateDate);
        model.setUpdateBy(this.updateBy);
        return model;
    }
}

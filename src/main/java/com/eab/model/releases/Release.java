package com.eab.model.releases;

import com.eab.model.configuration.ConfigVersion;

import java.util.Date;
import java.util.List;

public class Release {

    private Integer id;

    private String name;

    private String status;

    private Date createDate;

    private String createBy;

    private Date updateDate;

    private String updateBy;

    private List<ConfigVersion> versions;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public List<ConfigVersion> getVersions() {
        return versions;
    }

    public void setVersions(List<ConfigVersion> versions) {
        this.versions = versions;
    }
}

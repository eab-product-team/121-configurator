package com.eab.controller;

import com.eab.controller.common.EABUser;
import com.eab.model.configuration.ConfigSummary;
import com.eab.model.configuration.ConfigVersion;
import com.eab.model.profile.UserPrincipalBean;
import com.eab.service.ConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/rest/configs")
public class ConfigSummaryController {

    @Autowired
    private ConfigService configService;

    @RequestMapping(method = RequestMethod.GET)
    public List<ConfigSummary> getSummaries(@EABUser UserPrincipalBean user, @RequestParam(required = false) String module, @RequestParam(required = false) String type) {
        return this.configService.getSummaries(user, module, type);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public ConfigSummary getSummary(@EABUser UserPrincipalBean user, @PathVariable Integer id) {
        return this.configService.getSummary(user, id);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ConfigSummary createSummary(@EABUser UserPrincipalBean user, @RequestBody ConfigSummary summary) {
        return this.configService.createSummary(user, summary);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public ConfigSummary updateSummary(@EABUser UserPrincipalBean user, @PathVariable Integer id, @RequestBody ConfigSummary summary) {
        if (!id.equals(summary.getId())) {
            throw new IllegalArgumentException("id is not consistent");
        }
        return this.configService.updateSummary(user, summary);
    }

    @RequestMapping(method = RequestMethod.DELETE)
    public void deleteSummaries(@EABUser UserPrincipalBean user, @RequestParam List<Integer> ids) {
        configService.deleteSummaries(user, ids);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/{id}/versions")
    public ConfigVersion createVersion(@EABUser UserPrincipalBean user, @RequestBody ConfigVersion version, @PathVariable Integer id) {
        return configService.createVersion(user, id, version);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{id}/versions/{versionId}")
    public ConfigVersion updateVersion(@EABUser UserPrincipalBean user, @PathVariable Integer id, @PathVariable Integer versionId, @RequestBody ConfigVersion version) {
        if (!versionId.equals(version.getId())) {
            throw new IllegalArgumentException("versionId is not consistent");
        }
        return configService.updateVersion(user, id, version);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}/versions")
    public void deleteVersions(@EABUser UserPrincipalBean user, @RequestParam List<Integer> versionIds) {
        configService.deleteVersions(user, versionIds);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/{id}/versions/{versionId}/clone")
    public ConfigVersion cloneVersion(@EABUser UserPrincipalBean user, @PathVariable Integer id, @PathVariable Integer versionId,
                                      @RequestParam(required = false) String description, @RequestParam(required = false) Integer targetId) {
        if (targetId == null) {
            return configService.cloneVersion(user, versionId, description);
        } else {
            return configService.cloneVersion(user, versionId, targetId, description);
        }
    }

}

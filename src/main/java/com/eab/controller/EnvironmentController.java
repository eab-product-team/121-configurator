package com.eab.controller;

import com.eab.controller.common.EABUser;
import com.eab.model.environment.Environment;
import com.eab.model.profile.UserPrincipalBean;
import com.eab.service.EnvironmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/rest/environments")
public class EnvironmentController {

    @Autowired
    private EnvironmentService envService;

    @RequestMapping(method = RequestMethod.GET)
    public List<Environment> getEnvironments(@EABUser UserPrincipalBean user) {
        return envService.getEnvironments(user);
    }
}

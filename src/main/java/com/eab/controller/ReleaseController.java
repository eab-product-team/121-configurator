package com.eab.controller;

import com.eab.biz.dynTemplate.SaveMgr;
import com.eab.controller.common.EABUser;
import com.eab.controller.exception.ResourceNotFoundException;
import com.eab.couchbase.CBServer;
import com.eab.helper.DocHelper;
import com.eab.helper.TemplateHelper;
import com.eab.model.configuration.Attachment;
import com.eab.model.configuration.ConfigVersion;
import com.eab.model.environment.Environment;
import com.eab.model.profile.UserPrincipalBean;
import com.eab.model.releases.Release;
import com.eab.service.AttachmentService;
import com.eab.service.ConfigService;
import com.eab.service.EnvironmentService;
import com.eab.service.ReleaseService;
import org.apache.tika.exception.TikaException;
import org.apache.tika.mime.MimeTypes;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@RestController
@RequestMapping(value = "/rest/releases")
public class ReleaseController {

    @Autowired
    private ReleaseService releaseService;

    @Autowired
    private ConfigService configService;

    @Autowired
    private TemplateHelper templateHelper;

    @Autowired
    private EnvironmentService envService;

    @Autowired
    private AttachmentService attService;

    @RequestMapping(method = RequestMethod.GET)
    public List<Release> getReleases(@EABUser UserPrincipalBean user) {
        return releaseService.getReleases(user);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public Release getRelease(@EABUser UserPrincipalBean user, @PathVariable Integer id) {
        return releaseService.getRelease(user, id);
    }

    @RequestMapping(method = RequestMethod.POST)
    public Release createRelease(@EABUser UserPrincipalBean user, @RequestBody Release release) {
        return releaseService.createRelease(user, release);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public Release updateRelease(@EABUser UserPrincipalBean user, @PathVariable Integer id, @RequestBody Release release) {
        if (!id.equals(release.getId())) {
            throw new IllegalArgumentException("id is not consistent");
        }
        return releaseService.updateRelease(user, release);
    }

    @RequestMapping(method = RequestMethod.DELETE)
    public void deleteReleases(@EABUser UserPrincipalBean user, @RequestParam List<Integer> ids) {
        releaseService.deleteReleases(user, ids);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/{id}/versions/{versionId}")
    public Release addVersion(@EABUser UserPrincipalBean user, @PathVariable Integer id, @PathVariable Integer versionId) {
        return releaseService.addVersionToRelease(user, id, versionId);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}/versions")
    public Release removeVersions(@EABUser UserPrincipalBean user, @PathVariable Integer id, @RequestParam List<Integer> versionIds) {
        return releaseService.removeVersionFromRelease(user, id, versionIds);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}/export", produces = "application/zip")
    public byte[] exportRelease(HttpServletRequest request, @EABUser UserPrincipalBean user, @PathVariable Integer id) throws IOException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ZipOutputStream zos = new ZipOutputStream(bos);
        String compCode = user.getCompCode();
        Release release = releaseService.getRelease(user, id);
        for (ConfigVersion version : release.getVersions()) {
            String module = version.getSummary().getModule();
            String versionNum = version.getVersionNum() + "";

            JSONObject doc = getJsonDoc(request, user, version);
            String docName = DocHelper.getDocName(doc, module);
            String docId = DocHelper.getDocId(compCode, module, docName, versionNum);

            // write json doc
            zos.putNextEntry(new ZipEntry(docId + ".json"));
            zos.write(doc.toString().getBytes());
            zos.closeEntry();

            // write attachments
            List<Attachment> attachments = attService.getAttachments(user, version.getSummary().getModule(), "config-" + version.getId());
            for (Attachment att : attachments) {
                String extension;
                try {
                    extension = MimeTypes.getDefaultMimeTypes().forName(att.getContentType()).getExtension();
                } catch (TikaException e) {
                    extension = ".pdf";
                }
                zos.putNextEntry(new ZipEntry(String.format("%s_%s_%s%s", docName, versionNum, att.getAttId(), extension)));
                zos.write(att.getData());
                zos.closeEntry();
            }
        }
        zos.close();
        return bos.toByteArray();
    }

    @RequestMapping(method = RequestMethod.POST, value = "/{id}/publish")
    public void publishRelease(HttpServletRequest request, @EABUser UserPrincipalBean user, @PathVariable Integer id, @RequestParam String envId) throws ResourceNotFoundException, IOException {
        Environment env = envService.getEnvironmentByEnvId(user, envId);
        if (env == null) {
            throw new ResourceNotFoundException();
        }
        String compCode = user.getCompCode();
        CBServer cbServer = new CBServer(env.getHost(), env.getPort(), env.getDbName(), env.getUserName(), env.getPassword());
        Release release = releaseService.getRelease(user, id);
        for (ConfigVersion version : release.getVersions()) {
            String module = version.getSummary().getModule();
            String versionNum = version.getVersionNum() + "";

            JSONObject doc = getJsonDoc(request, user, version);
            String docName = DocHelper.getDocName(doc, module);
            String docId = DocHelper.getDocId(compCode, module, docName, versionNum);

            // create cb doc
            cbServer.CreateDoc(docId, doc.toString());

            // add cb attachments
            List<Attachment> attachments = attService.getAttachments(user, version.getSummary().getModule(), "config-" + version.getId());
            for (Attachment att : attachments) {
                cbServer.CreateAttach(docId, att.getAttId(), att.getContentType(), new ByteArrayInputStream(att.getData()));
            }
        }
    }

    private JSONObject getJsonDoc(HttpServletRequest request, UserPrincipalBean user, ConfigVersion version) {
        SaveMgr saveMgr = new SaveMgr(request);
        Map<String, String> sections = configService.getConfigSectionJsons(version.getId());
        Map<String, JSONObject> sectionJsonMap = new HashMap<>();
        for (Map.Entry<String, String> entry : sections.entrySet()) {
            sectionJsonMap.put(entry.getKey(), new JSONObject(entry.getValue()));
        }
        JSONObject template = templateHelper.getTemplateForParsing(request, user, version.getId());
        JSONObject doc = saveMgr.parseSections(sectionJsonMap, template);
        return DocHelper.appendDocInfo(doc, user.getCompCode(), version.getVersionNum(), version.getSummary().getModule());
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public Map<String, Object> handleRequestException(ResourceNotFoundException e) {
        Map<String, Object> response = new HashMap<>();
        response.put("error", e.getMessage());
        return response;
    }

}

package com.eab.controller;

import com.eab.controller.common.EABUser;
import com.eab.model.configuration.ConfigItem;
import com.eab.model.profile.UserPrincipalBean;
import com.eab.service.ConfigItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/rest/configItems")
public class ConfigItemController {

    @Autowired
    private ConfigItemService configService;

    @RequestMapping(method = RequestMethod.GET)
    public List<ConfigItem> getConfigurations(@EABUser UserPrincipalBean user, @RequestParam("configType") String configType, @RequestParam(value = "q", defaultValue = "") String query) {
        return this.configService.findConfigItems(user, configType, query);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public ConfigItem getConfiguration(@EABUser UserPrincipalBean user, @PathVariable Integer id) {
        return this.configService.getConfigItem(user, id);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ConfigItem createConfiguration(@EABUser UserPrincipalBean user, @RequestBody ConfigItem configItem) {
        return this.configService.createConfigItem(user, configItem);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public ConfigItem updateConfiguration(@EABUser UserPrincipalBean user, @PathVariable Integer id, @RequestBody ConfigItem configItem) {
        if (id != configItem.getId()) {
            throw new IllegalArgumentException("id is not consistent");
        }
        return this.configService.updateConfigItem(user, configItem);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public ConfigItem deleteConfiguration(@EABUser UserPrincipalBean user, @PathVariable Integer id) {
        return this.configService.deleteConfigItem(user, id);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public Map<String, Object> handleRequestException(IllegalArgumentException e) {
        Map<String, Object> response = new HashMap<>();
        response.put("error", e.getMessage());
        return response;
    }

}

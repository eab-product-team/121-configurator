package com.eab.controller;

import com.eab.biz.dynTemplate.DynMgr;
import com.eab.controller.common.EABUser;
import com.eab.helper.TemplateHelper;
import com.eab.model.configuration.ConfigSummary;
import com.eab.model.profile.UserPrincipalBean;
import com.eab.service.ConfigService;
import com.google.gson.Gson;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping(value = "/controller/configDetails")
public class ConfigDetailController {

    @Autowired
    private ConfigService configService;

    @Autowired
    private TemplateHelper templateHelper;

    @RequestMapping(method = RequestMethod.GET, value = "/init/{versionId}")
    @ResponseBody
    public Map initPage(HttpServletRequest request, @EABUser UserPrincipalBean user, @PathVariable int versionId) {
        Gson gson = new Gson();

        JSONObject templateObj = templateHelper.getTemplateForConfig(request, user, versionId);
        new DynMgr(request).fillTemplateOptions(templateObj);
        Map template = gson.fromJson(templateObj.toString(), Map.class);

        Map<String, Map> sections = new HashMap<>();
        ConfigSummary config = configService.getSummaryByVersion(user, versionId);
        Map<String, String> jsons = configService.getConfigSectionJsons(versionId);
        if (jsons.size() == 0 && "product".equalsIgnoreCase(config.getModule())) {
            JSONObject obj = new JSONObject();
            obj.put("planInd", config.getType());
            obj.put("covCode", config.getDescription());
            jsons.put("ProdDetail", obj.toString());
        }
        for (Map.Entry<String, String> entry : jsons.entrySet()) {
            sections.put(entry.getKey(), gson.fromJson(entry.getValue(), Map.class));
        }

        Map<String, Object> result = new HashMap<>();
        result.put("template", template);
        result.put("sections", sections);
        result.put("version", configService.getVersion(user, versionId));
        return result;
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{versionId}")
    @ResponseBody
    public Map setConfigSectionJsons(@EABUser UserPrincipalBean user, @PathVariable int versionId, @RequestBody Map<String, Object> sections) {
        Map<String, String> jsons = new HashMap<>();
        for (Map.Entry<String, Object> entry : sections.entrySet()) {
            jsons.put(entry.getKey(), new Gson().toJson(entry.getValue()));
        }
        configService.setConfigSectionJsons(user, versionId, jsons);
        return sections;
    }

}

package com.eab.controller;

import com.eab.controller.common.EABUser;
import com.eab.model.configuration.Attachment;
import com.eab.service.AttachmentService;
import com.eab.service.FundService;
import com.eab.helper.FundHelper;
import com.eab.model.fund.Fund;
import com.eab.model.profile.UserPrincipalBean;
import org.apache.tika.exception.TikaException;
import org.apache.tika.mime.MimeTypes;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@RestController
@RequestMapping(value = "/rest/funds")
public class FundController {

    @Autowired
    private FundService service;

    @Autowired
    private FundHelper fundHelper;

    @Autowired
    private AttachmentService attService;

    @RequestMapping(method = RequestMethod.GET)
    public List<Fund> getFunds(@EABUser UserPrincipalBean user) {
        return this.service.findFunds(user);
    }

    @RequestMapping(method = RequestMethod.POST)
    public Fund createFund(@EABUser UserPrincipalBean user, @RequestBody Fund fund) {
        return this.service.createFund(user, fund);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public Fund getFund(@EABUser UserPrincipalBean user, @PathVariable Integer id) {
        return this.service.getFund(user, id);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public Fund updateFund(@EABUser UserPrincipalBean user, @PathVariable Integer id, @RequestBody Fund fund) {
        if (id != fund.getId()) {
            throw new IllegalArgumentException("id is not consistent");
        }
        return this.service.updateFund(user, fund);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public Fund deleteFund(@EABUser UserPrincipalBean user, @PathVariable Integer id) {
        return this.service.deleteFund(user, id);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/export", produces = "application/zip")
    public byte[] exportFund(HttpServletRequest request, @EABUser UserPrincipalBean user, @RequestParam List<Integer> ids) throws IOException {
        String module = "fund";
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ZipOutputStream zos = new ZipOutputStream(bos);

        for (Integer id : ids) {
            Fund fund = this.service.getFund(user, id);
            String docCode = "fund_" + fund.getId();
            String docName = fund.getFundCode() + "_PHS";

            Map<String, JSONObject> docs = fundHelper.getExportJson(request, user, fund);
            for (Map.Entry<String, JSONObject> entry : docs.entrySet()) {

                // write json doc
                zos.putNextEntry(new ZipEntry(entry.getKey() + ".json"));
                zos.write(entry.getValue().toString().getBytes());
                zos.closeEntry();

                // write attachments
                List<Attachment> attachments = attService.getAttachments(user, module, docCode);
                for (Attachment att : attachments) {
                    String extension;
                    try {
                        extension = MimeTypes.getDefaultMimeTypes().forName(att.getContentType()).getExtension();
                    } catch (TikaException e) {
                        extension = ".pdf";
                    }
                    zos.putNextEntry(new ZipEntry(String.format("%s%s", docName, extension)));
                    zos.write(att.getData());
                    zos.closeEntry();
                }
            }
        }
        zos.close();
        return bos.toByteArray();
    }


    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public Map<String, Object> handleRequestException(IllegalArgumentException e) {
        Map<String, Object> response = new HashMap<>();
        response.put("error", e.getMessage());
        return response;
    }
}

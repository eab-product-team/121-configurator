package com.eab.controller.common;

import com.eab.common.Token;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class CsrfTokenInterceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        HttpSession session = request.getSession(false);
        response.setHeader("csrfId", Token.CsrfID(session));
        response.setHeader("csrfToken", Token.CsrfToken(session));
        return true;
    }

}

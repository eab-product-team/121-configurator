package com.eab.helper;

import com.eab.biz.dynTemplate.DynMgr;
import com.eab.model.configuration.ConfigSummary;
import com.eab.model.profile.UserPrincipalBean;
import com.eab.service.ConfigService;
import com.eab.service.TemplateService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service
public class TemplateHelper {

    private static final String TMPL_TYPE_PRODUCT = "product";
    private static final String TMPL_TYPE_PDF = "pdf";

    private static final String TMPL_CODE_BASIC_PLAN = "NEW_B";
    private static final String TMPL_CODE_RIDER = "NEW_R";
    private static final String TMPL_CODE_PDF = "NEW";

    private static final String SUMMARY_TYPE_BASIC_PLAN = "B";

    @Autowired
    private ConfigService configService;

    @Autowired
    private TemplateService templateService;

    private JSONObject getTemplate(UserPrincipalBean user, int versionId) {
        ConfigSummary summary = configService.getSummaryByVersion(user, versionId);
        String templateCode;
        if (TMPL_TYPE_PRODUCT.equals(summary.getModule())) {
            templateCode = SUMMARY_TYPE_BASIC_PLAN.equals(summary.getType()) ? TMPL_CODE_BASIC_PLAN : TMPL_CODE_RIDER;
        } else {
            templateCode = TMPL_CODE_PDF;
        }
        String templateJson = templateService.getTemplate(user.getCompCode(), summary.getModule(), templateCode, 1);
        return new JSONObject(templateJson);
    }

    public JSONObject getTemplateForConfig(HttpServletRequest request, UserPrincipalBean user, int versionId) {
        DynMgr dynMgr = new DynMgr(request);
        return new JSONObject(dynMgr.parseTemplate(getTemplate(user, versionId)));
    }

    public JSONObject getTemplateForParsing(HttpServletRequest request, UserPrincipalBean user, int versionId) {
        JSONObject templateJson = getTemplate(user, versionId);
        new DynMgr(request).fillTemplateOptions(templateJson);
        return templateJson;
    }
}

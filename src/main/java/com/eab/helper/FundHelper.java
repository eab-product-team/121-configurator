package com.eab.helper;

import com.eab.model.fund.Fund;
import com.eab.model.profile.UserPrincipalBean;
import com.eab.service.FundService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Service
public class FundHelper {

    @Autowired
    private FundService service;

    public Map<String, JSONObject> getExportJson(HttpServletRequest request, UserPrincipalBean user, Fund fund) {
        String module = "fund";
        int version = 1;
        String versionNum = "1";
        String effDate = "2017-07-01 00:00:00";
        String expDate = "2047-06-30 23:59:59";
        String compCode = user.getCompCode();

        Map<String, JSONObject> result = new HashMap<>();
        String fundCode = fund.getFundCode();
        String[] arrPaymentMethod = fund.getPaymentMethod().split("\\,", -1);
        JSONObject fundName = new JSONObject();
        fundName.put("en", fund.getFundName());
        fundName.put("zh-Hant", "");
        String create = fund.getCreateBy() + "  on " + fund.getCreateDate();
        String update = fund.getUpdateBy() + "  on " + fund.getUpdateDate();

        JSONObject json = new JSONObject();
        json.put("create", create);
        json.put("launchDate", fund.getCreateDate());
        json.put("upd", update);
        json.put("fundCode", fundCode);
        json.put("annualManageFee", fund.getAnnualManageFee());
        json.put("assetClass", fund.getAssetClass());
        json.put("riskRating", fund.getRiskRating());
        json.put("ref_no", fundCode);
        json.put("fundName", fundName);
        json.put("effDate", effDate);
        json.put("expDate", expDate);
        json.put("paymentMethod", arrPaymentMethod);
        json.put("version", version);
        json.put("ccy", fund.getCcy());
        json.put("isMixedAsset", fund.getIsMixedAsset() ? "Y" : "N");

        String docId = DocHelper.getDocId(compCode, module, fundCode, versionNum);
        result.put(docId, DocHelper.appendDocInfo(json, compCode, version, module));
        return result;
    }
}

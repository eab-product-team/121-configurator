package com.eab.helper;

import com.eab.common.Function;
import org.json.JSONObject;

public class DocHelper {

    public static String getDocId(String compCode, String module, String docCode, String version) {
        return compCode + "_" + module + "_" + docCode + "_" + version;
    }

    public static String getDocName(JSONObject obj, String module) {
        if ("pdf".equalsIgnoreCase(module)) {
            return obj.getString("pdfCode");
        } else {
            return obj.getString("covCode");
        }
    }

    public static JSONObject convertFinalJson(String compCode, JSONObject obj, String module){
        JSONObject finalJson = obj;
        try {
            String type = module;
            if("pdf".equalsIgnoreCase(type)){
                type = "pdfTemplate";
            }
            finalJson.put("type", type);
            finalJson.put("compCode", compCode);
            finalJson.put("status", "A");
            //releaseDate
            finalJson.put("releaseDate", Function.getToday());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return finalJson;
    }

    public static JSONObject appendDocInfo(JSONObject obj, String compCode, int version, String module) {
        obj.put("version", version);
        return convertFinalJson(compCode, obj, module);
    }

}

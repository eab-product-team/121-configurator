package com.eab.service;

import com.eab.dao.configuration.ConfigVersionRepo;
import com.eab.dao.releases.ReleaseRepo;
import com.eab.model.configuration.ConfigVersion;
import com.eab.model.configuration.entity.ConfigVersionEntity;
import com.eab.model.profile.UserPrincipalBean;
import com.eab.model.releases.Release;
import com.eab.model.releases.entity.ReleaseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class ReleaseService {

    @Autowired
    private ReleaseRepo releaseRepo;

    @Autowired
    private ConfigVersionRepo versionRepo;

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Release> getReleases(UserPrincipalBean user) {
        return StreamSupport.stream(releaseRepo.findAll().spliterator(), false)
                .map(ReleaseEntity::toModel)
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Release getRelease(UserPrincipalBean user, Integer releaseId) {
        ReleaseEntity entity = releaseRepo.findOne(releaseId);
        Release release = entity.toModel();
        List<ConfigVersion> versions = new ArrayList<>();
        for (ConfigVersionEntity versionEntity : entity.getVersions()) {
            ConfigVersion version = versionEntity.toModel();
            version.setSummary(versionEntity.getSummary().toModel());
            versions.add(version);
        }
        release.setVersions(versions);
        return release;
    }

    @Transactional
    public Release createRelease(UserPrincipalBean user, Release release) {
        if (release == null) {
            return null;
        }
        Date currentDate = new Date();
        release.setCreateBy(user.getUserCode());
        release.setCreateDate(currentDate);
        release.setUpdateBy(user.getUserCode());
        release.setUpdateDate(currentDate);
        return releaseRepo.save(new ReleaseEntity(release)).toModel();
    }

    @Transactional
    public Release updateRelease(UserPrincipalBean user, Release release) {
        if (release == null) {
            return null;
        }
        ReleaseEntity entity = releaseRepo.findOne(release.getId());
        ReleaseEntity newEntity = new ReleaseEntity(release);
        newEntity.setName(release.getName());
        newEntity.setVersions(entity.getVersions());
        newEntity.setCreateBy(entity.getCreateBy());
        newEntity.setCreateDate(entity.getCreateDate());
        newEntity.setUpdateBy(user.getUserCode());
        newEntity.setUpdateDate(new Date());
        releaseRepo.save(newEntity);
        return getRelease(user, release.getId());
    }

    @Transactional
    public void deleteReleases(UserPrincipalBean user, List<Integer> releaseIds) {
        releaseRepo.delete(releaseRepo.findAll(releaseIds));
    }

    @Transactional
    public Release addVersionToRelease(UserPrincipalBean user, Integer releaseId, Integer versionId) {
        ReleaseEntity entity = releaseRepo.findOne(releaseId);
        boolean found = false;
        for (ConfigVersionEntity version : entity.getVersions()) {
            if (version.getId().equals(versionId)) {
                found = true;
                break;
            }
        }
        if (!found) {
            entity.getVersions().add(versionRepo.findOne(versionId));
            entity.setUpdateBy(user.getUserCode());
            entity.setUpdateDate(new Date());
            releaseRepo.save(entity);
        }
        return getRelease(user, releaseId);
    }

    @Transactional
    public Release removeVersionFromRelease(UserPrincipalBean user, Integer releaseId, List<Integer> versionIds) {
        ReleaseEntity entity = releaseRepo.findOne(releaseId);
        List<ConfigVersionEntity> versions = new ArrayList<>();
        for (ConfigVersionEntity version : entity.getVersions()) {
            if (!versionIds.contains(version.getId())) {
                versions.add(version);
            }
        }
        entity.setVersions(versions);
        entity.setUpdateBy(user.getUserCode());
        entity.setUpdateDate(new Date());
        releaseRepo.save(entity);
        return getRelease(user, releaseId);
    }

}

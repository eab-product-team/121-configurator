package com.eab.service;

import com.eab.dao.fund.FundDao;
import com.eab.model.fund.Fund;
import com.eab.model.fund.FundEntity;
import com.eab.model.profile.UserPrincipalBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class FundService {

    @Autowired
    private FundDao dao;

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Fund> findFunds(UserPrincipalBean user) {
        return dao.findByCompCode(user.getCompCode(), new Sort("fundCode")).stream()
                .map(FundEntity::toModel)
                .collect(Collectors.toList());
    }

    @Transactional
    public Fund createFund(UserPrincipalBean user, Fund fund) {
        if (fund != null) {
            Date currentDate = new Date();
            fund.setCreateBy(user.getUserCode());
            fund.setCreateDate(currentDate);
            fund.setUpdateBy(user.getUserCode());
            fund.setUpdateDate(currentDate);
            FundEntity entity = dao.save(new FundEntity(fund));
            return entity.toModel();
        }
        return null;
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Fund getFund(UserPrincipalBean user, int id) {
        FundEntity result = dao.findOne(id);
        if (result != null) {
            return result.toModel();
        }
        return null;
    }

    @Transactional
    public Fund updateFund(UserPrincipalBean user, Fund fund) {
        if (fund == null) {
            return null;
        }
        fund.setUpdateBy(user.getUserCode());
        fund.setUpdateDate(new Date());
        FundEntity entity = dao.save(new FundEntity((fund)));
        return entity.toModel();
    }

    @Transactional
    public Fund deleteFund(UserPrincipalBean user, int id) {
        FundEntity result = dao.findOne(id);
        if (result != null) {
            dao.delete(result);
            return result.toModel();
        }
        return null;
    }

}

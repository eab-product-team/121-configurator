package com.eab.service;

import com.eab.dao.configuration.ConfigSectionRepo;
import com.eab.dao.configuration.ConfigSummaryRepo;
import com.eab.dao.configuration.ConfigVersionRepo;
import com.eab.model.configuration.ConfigSummary;
import com.eab.model.configuration.ConfigVersion;
import com.eab.model.configuration.entity.ConfigSectionEntity;
import com.eab.model.configuration.entity.ConfigSummaryEntity;
import com.eab.model.configuration.entity.ConfigVersionEntity;
import com.eab.model.profile.UserPrincipalBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class ConfigService {

    @Autowired
    private ConfigSummaryRepo summaryRepo;

    @Autowired
    private ConfigVersionRepo versionRepo;

    @Autowired
    private ConfigSectionRepo sectionRepo;

    @Autowired
    private AttachmentService attachmentService;

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<ConfigSummary> getSummaries(UserPrincipalBean user, String module, String type) {
        return summaryRepo.findByModuleAndType(module, type).stream()
                .map(ConfigSummaryEntity::toModel)
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public ConfigSummary getSummary(UserPrincipalBean user, int id) {
        ConfigSummaryEntity entity = summaryRepo.findOne(id);
        List<ConfigVersionEntity> versions = versionRepo.findBySummaryOrderByVersionNumDesc(entity);
        ConfigSummary summary = entity.toModel();
        summary.setVersions(versions.stream()
                .map(ConfigVersionEntity::toModel)
                .collect(Collectors.toList()));
        return summary;
    }

    @Transactional
    public ConfigSummary createSummary(UserPrincipalBean user, ConfigSummary summary) {
        if (summary == null) {
            return null;
        }
        Date currentDate = new Date();
        summary.setCreateBy(user.getUserCode());
        summary.setCreateDate(currentDate);
        summary.setUpdateBy(user.getUserCode());
        summary.setUpdateDate(currentDate);
        return summaryRepo.save(new ConfigSummaryEntity(summary)).toModel();
    }

    @Transactional
    public ConfigSummary updateSummary(UserPrincipalBean user, ConfigSummary summary) {
        if (summary == null) {
            return null;
        }
        summary.setUpdateBy(user.getUserCode());
        summary.setUpdateDate(new Date());
        summaryRepo.save(new ConfigSummaryEntity(summary));
        return getSummary(user, summary.getId());
    }

    @Transactional
    public void deleteSummaries(UserPrincipalBean user, List<Integer> ids) {
        summaryRepo.delete(summaryRepo.findAll(ids));
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public ConfigSummary getSummaryByVersion(UserPrincipalBean user, int versionId) {
        return versionRepo.findOne(versionId).getSummary().toModel();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public ConfigVersion getVersion(UserPrincipalBean user, int versionId) {
        ConfigVersionEntity entity = versionRepo.findOne(versionId);
        ConfigVersion version = entity.toModel();
        version.setSummary(summaryRepo.findOne(entity.getSummary().getId()).toModel());
        return version;
    }

    @Transactional
    public ConfigVersion createVersion(UserPrincipalBean user, int summaryId, ConfigVersion version) {
        if (version == null) {
            return null;
        }
        Date currentDate = new Date();
        Integer maxVersion = versionRepo.getMaxVersionNum(summaryId);
        version.setVersionNum(maxVersion != null ? maxVersion + 1 : 1);
        version.setCreateBy(user.getUserCode());
        version.setCreateDate(currentDate);
        version.setUpdateBy(user.getUserCode());
        version.setUpdateDate(currentDate);
        ConfigSummaryEntity summary = summaryRepo.findOne(summaryId);
        return versionRepo.save(new ConfigVersionEntity(version, summary)).toModel();
    }

    private ConfigVersion clone(UserPrincipalBean user, ConfigVersionEntity version, ConfigSummaryEntity targetSummary, String newDescription) {
        Date currentDate = new Date();

        Integer maxVersion = versionRepo.getMaxVersionNum(targetSummary.getId());
        ConfigVersionEntity newVersion = new ConfigVersionEntity();
        newVersion.setVersionNum(maxVersion == null ? 1 : maxVersion + 1);
        newVersion.setDescription(!newDescription.equals("") ? newDescription : version.getDescription());
        newVersion.setCreateBy(user.getUserCode());
        newVersion.setCreateDate(currentDate);
        newVersion.setUpdateBy(user.getUserCode());
        newVersion.setUpdateDate(currentDate);
        newVersion.setSummary(targetSummary);
        newVersion = versionRepo.save(newVersion);

        List<ConfigSectionEntity> sections = sectionRepo.findByVersion(version);
        for (ConfigSectionEntity section : sections) {
            ConfigSectionEntity newSection = new ConfigSectionEntity();
            newSection.setSectionCode(section.getSectionCode());
            newSection.setJson(section.getJson());
            newSection.setCreateBy(user.getUserCode());
            newSection.setCreateDate(currentDate);
            newSection.setUpdateBy(user.getUserCode());
            newSection.setUpdateDate(currentDate);
            newSection.setVersion(newVersion);
            sectionRepo.save(newSection);
        }
        attachmentService.cloneAttachments(user, targetSummary.getModule(), "config-" + version.getId(), "config-" + newVersion.getId());
        return newVersion.toModel();
    }

    @Transactional
    public ConfigVersion cloneVersion(UserPrincipalBean user, int versionId, String newDescription) {
        ConfigVersionEntity version = versionRepo.findOne(versionId);
        ConfigSummaryEntity summary = summaryRepo.findOne(version.getSummary().getId());
        return clone(user, version, summary, newDescription);
    }

    @Transactional
    public ConfigVersion cloneVersion(UserPrincipalBean user, int versionId, int targetSummaryId, String newDescription) {
        ConfigVersionEntity version = versionRepo.findOne(versionId);
        ConfigSummaryEntity summary = summaryRepo.findOne(targetSummaryId);
        return clone(user, version, summary, newDescription);
    }

    @Transactional
    public ConfigVersion updateVersion(UserPrincipalBean user, int summaryId, ConfigVersion version) {
        if (version == null) {
            return null;
        }
        ConfigSummaryEntity summary = summaryRepo.findOne(summaryId);
        version.setUpdateBy(user.getUserCode());
        version.setUpdateDate(new Date());
        return versionRepo.save(new ConfigVersionEntity(version, summary)).toModel();
    }

    @Transactional
    public void deleteVersions(UserPrincipalBean user, List<Integer> versionIds) {
        versionRepo.delete(versionRepo.findAll(versionIds));
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Map<String, String> getConfigSectionJsons(int versionId) {
        ConfigVersionEntity version = versionRepo.findOne(versionId);
        Map<String, String> jsonMap = new HashMap<>();
        for (ConfigSectionEntity section : sectionRepo.findByVersion(version)) {
            jsonMap.put(section.getSectionCode(), section.getJson());
        }
        return jsonMap;
    }

    @Transactional
    public Map<String, String> setConfigSectionJsons(UserPrincipalBean user, int versionId, Map<String, String> jsonMap) {
        if (jsonMap == null) {
            return null;
        }
        ConfigVersionEntity version = versionRepo.findOne(versionId);
        version.setUpdateBy(user.getUserCode());
        version.setUpdateDate(new Date());
        versionRepo.save(version);

        Date currentDate = new Date();
        Map<String, ConfigSectionEntity> existingSections = sectionRepo.findByVersion(version).stream()
                .collect(Collectors.toMap(ConfigSectionEntity::getSectionCode, Function.identity()));
        for (ConfigSectionEntity section : existingSections.values()) {
            String sectionCode = section.getSectionCode();
            if (jsonMap.containsKey(sectionCode)) {
                section.setJson(jsonMap.get(sectionCode));
                section.setUpdateBy(user.getUserCode());
                section.setUpdateDate(currentDate);
                sectionRepo.save(section); // update existing section
            } else {
                sectionRepo.delete(section); // remove old section
            }
        }
        for (Map.Entry<String, String> entry : jsonMap.entrySet()) {
            String sectionCode = entry.getKey();
            if (!existingSections.containsKey(sectionCode)) {
                ConfigSectionEntity section = new ConfigSectionEntity();
                section.setSectionCode(sectionCode);
                section.setJson(entry.getValue());
                section.setCreateBy(user.getUserCode());
                section.setCreateDate(currentDate);
                section.setUpdateBy(user.getUserCode());
                section.setUpdateDate(currentDate);
                section.setVersion(version);
                sectionRepo.save(section); // create new section
            }
        }
        return jsonMap;
    }

}

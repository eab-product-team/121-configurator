package com.eab.service;

import com.eab.dao.configuration.ConfigItemRepo;
import com.eab.model.configuration.ConfigItem;
import com.eab.model.configuration.entity.ConfigItemEntity;
import com.eab.model.profile.UserPrincipalBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ConfigItemService {

    @Autowired
    private ConfigItemRepo repo;

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<ConfigItem> findConfigItems(UserPrincipalBean user, String configType, String query) {
        return repo.findByConfigTypeAndQuery(configType, query).stream()
                .map(ConfigItemEntity::toModel)
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public ConfigItem getConfigItem(UserPrincipalBean user, int id) {
        ConfigItemEntity result = repo.findOne(id);
        if (result != null) {
            return result.toModel();
        }
        return null;
    }

    @Transactional
    public ConfigItem createConfigItem(UserPrincipalBean user, ConfigItem configItem) {
        if (configItem != null) {
            Date currentDate = new Date();
            configItem.setCreateBy(user.getUserCode());
            configItem.setCreateDate(currentDate);
            configItem.setUpdateBy(user.getUserCode());
            configItem.setUpdateDate(currentDate);
            ConfigItemEntity entity = repo.save(new ConfigItemEntity(configItem));
            return entity.toModel();
        }
        return null;
    }

    @Transactional
    public ConfigItem updateConfigItem(UserPrincipalBean user, ConfigItem configItem) {
        if (configItem == null) {
            return null;
        }
        configItem.setUpdateBy(user.getUserCode());
        configItem.setUpdateDate(new Date());
        ConfigItemEntity entity = repo.save(new ConfigItemEntity((configItem)));
        return entity.toModel();
    }

    @Transactional
    public ConfigItem deleteConfigItem(UserPrincipalBean user, int id) {
        ConfigItemEntity result = repo.findOne(id);
        if (result != null) {
            repo.delete(result);
            return result.toModel();
        }
        return null;
    }

}

package com.eab.service;

import com.eab.dao.environment.EnvironmentRepo;
import com.eab.model.environment.Environment;
import com.eab.model.environment.entity.EnvEntity;
import com.eab.model.profile.UserPrincipalBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class EnvironmentService {

    @Autowired
    private EnvironmentRepo envRepo;

    public List<Environment> getEnvironments(UserPrincipalBean user) {
        return envRepo.findByKeyCompCode(user.getCompCode()).stream()
                .map(EnvEntity::toModel)
                .collect(Collectors.toList());
    }

    public Environment getEnvironmentByEnvId(UserPrincipalBean user, String envId) {
        EnvEntity result = envRepo.findByKeyCompCodeAndKeyEnvId(user.getCompCode(), envId);
        if (result != null) {
            return result.toModel();
        }
        return null;
    }

}

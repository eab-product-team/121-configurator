package com.eab.service;

import com.eab.dao.configuration.AttachmentRepo;
import com.eab.model.configuration.Attachment;
import com.eab.model.configuration.entity.AttachmentEntity;
import com.eab.model.configuration.entity.AttachmentKey;
import com.eab.model.profile.UserPrincipalBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class AttachmentService {

    @Autowired
    private AttachmentRepo attRepo;

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Attachment> getAttachments(UserPrincipalBean user, String module, String docCode) throws IOException {
        List<AttachmentEntity> atts = attRepo.findAllByKeyCompCodeAndKeyModuleAndKeyDocCode(user.getCompCode(), module, docCode);
        if (atts != null) {
            List<Attachment> result = new ArrayList<>();
            for (AttachmentEntity att : atts) {
                result.add(att.toModel());
            }
            return result;
        }
        return null;
    }

    @Transactional
    public void cloneAttachments(UserPrincipalBean user, String module, String srcDocCode, String targetDocCode) {
        List<AttachmentEntity> atts = attRepo.findAllByKeyCompCodeAndKeyModuleAndKeyDocCode(user.getCompCode(), module, srcDocCode);
        if (atts != null) {
            for (AttachmentEntity att : atts) {
                AttachmentEntity newAtt = new AttachmentEntity();
                newAtt.setKey(new AttachmentKey(user.getCompCode(), module, targetDocCode, att.getKey().getAttId(), 1));
                newAtt.setData(att.getData());
                attRepo.save(newAtt);
            }
        }
    }
}

package com.eab.service;

import com.eab.dao.configuration.ProdTemplateRepo;
import com.eab.model.configuration.entity.ProdTemplateKey;
import com.eab.model.configuration.entity.ProdTemplateEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TemplateService {

    @Autowired
    private ProdTemplateRepo templateRepo;

    public String getTemplate(String compCode, String type, String templateCode, int version) {
        ProdTemplateEntity templateEntity = templateRepo.findOne(new ProdTemplateKey(compCode, type, templateCode, version));
        if (templateEntity != null) {
            return templateEntity.getTemplate();
        }
        return null;
    }
}

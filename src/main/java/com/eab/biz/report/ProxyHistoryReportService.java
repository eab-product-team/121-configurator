package com.eab.biz.report;

import java.util.ArrayList;

import org.json.JSONArray;

import com.eab.common.Constants;
import com.eab.common.Function;
import com.eab.common.Log;
import com.eab.dao.report.ProxyHistoryDao;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class ProxyHistoryReportService {
	
	public JSONArray getReportData(String startDateString, String endDateString) {
		Log.info("Goto ProxyHistoryReportService.getReportData");
		
		JSONArray result = null;
		
		ProxyHistoryDao proxyHistoryDao = new ProxyHistoryDao();
		
		try {
			
			java.sql.Date startSqlDate = null;
			java.sql.Date endSqlDate = null;

			if (startDateString != null) {
				startSqlDate = Function.sqlDateFromString(startDateString, Constants.DATETIME_PATTERN_AS_ADMIN_DATE, Constants.TIMEZONE_ID);
			}

			if (endDateString != null) {
				endSqlDate = Function.sqlDateFromString(endDateString, Constants.DATETIME_PATTERN_AS_ADMIN_DATE, Constants.TIMEZONE_ID);
			}
			
			result = proxyHistoryDao.getProxyHistory(startSqlDate, endSqlDate);
		}catch(Exception ex) {
			Log.error(ex);
		}
		
		return result;
	}
	
	public String[][] parseToExcelDataArray(JsonArray dataJsonArray) {
		Log.info("Goto ProxyHistoryReportService.parseToExcelDataArray");

		ArrayList<String[]> rowArrayList = new ArrayList<String[]>();
		for (JsonElement item : dataJsonArray) {

			JsonObject dataJsonObject = item.getAsJsonObject();
			
			String P_NAME = dataJsonObject.has("P_NAME")?dataJsonObject.get("P_NAME").getAsString():null;
			String PROXY1 = dataJsonObject.has("PROXY1")?dataJsonObject.get("PROXY1").getAsString():null;
			String PROXY2 = dataJsonObject.has("PROXY2")?dataJsonObject.get("PROXY2").getAsString():null;
			String TOP = dataJsonObject.has("TOP")?dataJsonObject.get("TOP").getAsString():null;
			String START_DT = dataJsonObject.has("START_DT")?dataJsonObject.get("START_DT").getAsString():null;
			String END_DT = dataJsonObject.has("END_DT")?dataJsonObject.get("END_DT").getAsString():null;
			String AUTH_GROUP = dataJsonObject.has("AUTH_GROUP")?dataJsonObject.get("AUTH_GROUP").getAsString():null;
			
			if(AUTH_GROUP != null)
			{
				AUTH_GROUP = mapChannelDisplayName(AUTH_GROUP);
			}
			
			String[] rowArray = createRowArray(P_NAME,PROXY1,PROXY2,TOP,START_DT,END_DT,AUTH_GROUP);
			rowArrayList.add(rowArray);

		}

		return convertArrayListTo2DStringArray(rowArrayList);
	}
	
	private String[] createRowArray(String ...items) {
		Log.info("Goto ProxyHistoryReportService.createRowArray");
		
		ArrayList<String> rowList = new ArrayList<String>();
		
		
		for(String item : items) {
			rowList.add(item);
		}

		return rowList.toArray(new String[rowList.size()]);
	}

	private String[][] convertArrayListTo2DStringArray(ArrayList<String[]> rowArrayList) {
		Log.info("Goto ProxyHistoryReportService.convertArrayListTo2DStringArray");

		String[][] array = new String[rowArrayList.size()][];
		for (int i = 0; i < rowArrayList.size(); i++) {
			String[] stringArray = rowArrayList.get(i);
			array[i] = stringArray;
		}

		return array;
	}
	
	private String mapChannelDisplayName(String auth_group) {
		Log.info("Goto ProxyHistoryReportService.mapChannelDisplayName");

		switch(auth_group) {
		case "AG001":
			auth_group = "Agency";
			break;
		case "FU004":
			auth_group = "IAM/JPK";
			break;
		case "WS005":
			auth_group = "Direct";
			break;
		case "PO006":
			auth_group = "AXA@Post";
			break;
		case "SY002":
			auth_group = "Synergy";
			break;
		case "BF003":
			auth_group = "Broker";
			break;
		case "GI001":
			auth_group = "GI Agents";
			break;
		case "BA001":
			auth_group = "Banca";
			break;
		
		}

		return auth_group;
	}
	

}

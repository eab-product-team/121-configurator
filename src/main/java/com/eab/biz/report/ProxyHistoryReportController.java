package com.eab.biz.report;

import org.json.JSONArray;

import com.eab.common.Function;
import com.eab.common.Log;
import com.google.gson.JsonArray;

public class ProxyHistoryReportController {
	
	private final ProxyHistoryReportProvider reportProvider = new ProxyHistoryReportProvider();
	private final ProxyHistoryReportService proxyHistoryReportService = new ProxyHistoryReportService();
	
	public String generateReport(String startDateString, String endDateString, String password ) throws Exception{
		Log.info("Goto ProxyHistoryReportController.generateReport");
		
		String base64String = "";
		
		Log.debug("*** startDateString="+startDateString+",endDateString="+endDateString);
		
		JSONArray dataJsonArray = proxyHistoryReportService.getReportData(startDateString, endDateString);
		
		if(dataJsonArray != null) {
			JsonArray gsonArray = Function.validateJsonArrayFormatForString(dataJsonArray.toString());
			
			if(gsonArray != null) {
				String[][] excelData = proxyHistoryReportService.parseToExcelDataArray(gsonArray);
				
				if(excelData != null) {
					base64String = reportProvider.createExcelReportInBase64Format(excelData, password);
				}else {
					Log.error("ERROR - excelData is null");
				}
			}else {
				Log.error("ERROR - parsing dataJsonArray fail");
			}
			
		}else {
			base64String = reportProvider.createEmptyReport(password);
		}

        return base64String;
    }
}

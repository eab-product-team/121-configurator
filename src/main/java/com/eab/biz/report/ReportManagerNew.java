package com.eab.biz.report;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.eab.biz.bcp.BcpRequest;
import com.eab.common.*;
import com.eab.couchbase.CBServer;

import org.json.JSONArray;
import org.json.JSONObject;

import com.eab.json.model.AppBar;
import com.eab.json.model.AppBarTitle;
import com.eab.json.model.Content;
import com.eab.json.model.Field;
import com.eab.json.model.Option;
import com.eab.json.model.Page;
import com.eab.json.model.Response;
import com.eab.json.model.Template;
import com.eab.model.MenuList;
import com.eab.model.profile.UserPrincipalBean;
import com.google.gson.JsonObject;

import static com.eab.dao.report.Report.getPickerOptions;
import static com.eab.dao.report.Report.getChannels;

public class ReportManagerNew {
	HttpServletRequest request;
	
	private final String USER_AGENT = "Mozilla/5.0";
	public ReportManagerNew(HttpServletRequest request) {
		this.request = request;
	}
	
	public Response getAllChannelReport(JSONObject paramJSON, boolean isInit) {
		UserPrincipalBean principal = Function.getPrincipal(request);
		JsonObject values = new JsonObject();
		Response resp = null;

		SimpleDateFormat sdf = new SimpleDateFormat(  Constants.DATETIME_PATTERN_AS_ADMIN_DATE);

		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);

		String endDate  = sdf.format(cal.getTime());
		String startDate=  sdf.format(cal.getTime());
		String channel = "all";
		String dateType = "appStart";
		boolean isGenExcel = false;

		// values
		try {

			if (paramJSON != null  && !isInit){
				if(paramJSON.has("channel") && paramJSON.get("channel") instanceof String)
					channel = paramJSON.getString("channel");
				if(paramJSON.has("dateType") && paramJSON.get("dateType") instanceof String)
					dateType = paramJSON.getString("dateType");
				if(paramJSON.has("startDate") && paramJSON.get("startDate") instanceof String)
					startDate = paramJSON.getString("startDate");
				if(paramJSON.has("endDate") && paramJSON.get("endDate") instanceof String)
					endDate = paramJSON.getString("endDate");
				if(paramJSON.has("channel")  && paramJSON.has("startDate")&& paramJSON.has("endDate")&& paramJSON.has("dateType"))
					isGenExcel = true;
			}

			values.addProperty("channel", channel);
			values.addProperty("dateType", dateType);
			values.addProperty("startDate", startDate);
			values.addProperty("endDate", endDate);

			// AppBar - Title
			AppBarTitle appBarTitle = new AppBarTitle(null, "label", "", "REPORT.ALLCHANNEL", null, null);
			// AppBar
			AppBar appBar = new AppBar(appBarTitle, null);
			Page pageObject = new Page("/Report", "");


			// create template
			List<Field> fields = new ArrayList<>();
			List<Option> dateTypeOptons = getPickerOptions("REPORT.DATETYPE");

			// Search fields
			Field startDateField = new Field("startDate", "datePicker", startDate, null, "REPORT.STARTDATE");
			Field endDateField = new Field("endDate", "datePicker", endDate, null, "REPORT.ENDDATE");

			List<Option> channelOptions = getChannels();
			for(int i = channelOptions.size()-1; i >= 0 ;i--){
				Option channelOption = channelOptions.get(i);
				if(channelOption.getValue().equals("GIAGENCY") || channelOption.getValue().equals("BANCA"))
					channelOptions.remove(i);
			}

			Field channelField = new Field("channel", "selectField", channel, channelOptions, "REPORT.CHANNEL");
			Field dateTypeField = new Field("dateType", "selectField", dateType, dateTypeOptons, "REPORT.DATETYPE");
			Field generateBtn = new Field("gen", "button", "BUTTON.GENERATE");

			fields.add(dateTypeField);
			fields.add(startDateField);
			fields.add(endDateField);
			fields.add(channelField);
			fields.add(generateBtn);
			// translation
			Template template = new Template("report", fields, "", "Reports");
			List<String> nameList = new ArrayList<String>();
			template.getNameCodes(nameList);
			appBar.getNameCodes(nameList);
			Map<String, String> nameMap = new HashMap<String, String>();
			nameMap = Function2.getTranslateMap(request, nameList);
			template.translate(nameMap);
			appBar.translate(nameMap);

			Content content = new Content(template, values, values);
			String tokenID = Token.CsrfID(request.getSession());
			String tokenKey = Token.CsrfToken(request.getSession());

			resp = new Response(Constants.ActionTypes.CHANGE_CONTENT, null, pageObject, tokenID, tokenKey, appBar, content);

			JsonObject error = ReportUtil.dateRangeValidation(sdf.parse(startDate),sdf.parse(endDate), sdf.parse(sdf.format(new Date())));
			if(error != null && error.has("title"))
				((JsonObject)resp.getContent().getValues()).add("error", error);
			else if(isGenExcel){
//				AllChannelReport allChannelReport = new AllChannelReport();
				
				
				
//				String b64 = allChannelReport.genAllChannelReport(principal.getUserName(), startDate, endDate,dateType, channel, principal.getUserCode());
				
				StringBuilder sb = new StringBuilder();
				String base_url = EnvVariable.get("INTERNAL_API_DOMAIN");
				
				
				
				String url 		= base_url + "/report/generator";
				int count = 0;
				sb.append("{\"UserName\":\"" + principal.getUserName() + "\", \"startDate\" : \"" + startDate + "\", \"endDate\" : \"" + endDate + "\", \"dateType\" : \"" + dateType + "\", \"channel\" : \"" + channel + "\", \"UserCode\" : " +principal.getUserCode() +"} ");
//				Log.info("sb !!!!!!!!!!!" + sb.toString());
				HttpURLConnection con;
				try{
					URL obj = new URL(url);
					con = (HttpURLConnection) obj.openConnection();
			 
				    // Setting basic post request
					con.setRequestMethod("POST");
					con.setRequestProperty("User-Agent", USER_AGENT);
					con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
					con.setRequestProperty("Content-Type","application/json");
				 
				  
					// Send post request
					con.setDoOutput(true);
					DataOutputStream wr = new DataOutputStream(con.getOutputStream());
					wr.writeBytes(sb.toString());
					wr.flush();
					wr.close();
		 
					int responseCode = con.getResponseCode();
					 
					BufferedReader in = null;
					if ((responseCode >= 200) && (responseCode < 400)){
						in = new BufferedReader(new InputStreamReader(con.getInputStream()));
					}
					else{
						in = new BufferedReader(new InputStreamReader(con.getErrorStream()));
					}
					
					String output;
					
					
					sb.delete(0,  sb.length());
					while ((output = in.readLine()) != null) {
						sb.append(output);
					}
					in.close();
		  
					String 		response_str = sb.toString();
					Log.info("response from api:: " + response_str);
					
					JsonObject status = new JsonObject();
					JSONObject 	response_json 	= new JSONObject(response_str);
					if(response_json.has("success") && response_json.get("success") instanceof Boolean) {
						if(response_json.getBoolean("success")) {
							status.addProperty("title", "All Channel Report");
							status.addProperty("message", "System is generating your report. You will receive an email shortly.");
//							status.addProperty("error", error1);
						} else {
							status.addProperty("title", "Generate Failed");
							status.addProperty("message", response_json.getString("msg"));
//							status.addProperty("error", response_json.getString("msg"));
						}
					} else {
						status.addProperty("title", "Generate Failed");
						status.addProperty("message", "Something went wrong");
					}
//					JSONArray	response_result	= response_json.getJSONArray("result");
//					Object result = response_result.get(0);
					
					
					((JsonObject)resp.getContent().getValues()).add("error", status);
					
					
				}
				catch (Exception e){
					
					sb.delete(0, sb.length());
					sb.append("ReportManager.getAllChannelReport(); error - ").append(e);
					
					Log.info(sb.toString());
					JsonObject error1 = new JsonObject();
					error1.addProperty("msg", e.toString());
//					error.addProperty("file", "data:application/vnd.ms-excel;base64,"+b64);
					((JsonObject)resp.getContent().getValues()).add("error", error1);
				}
				
//				String 64 = 
//				
//				
//				if(b64!= null && !b64.isEmpty()){
//					JsonObject report = new JsonObject();
//					report.addProperty("file", "data:application/vnd.ms-excel;base64,"+b64);
//					report.addProperty("fileName", "All_Channel_Report.xlsx");
//					((JsonObject)resp.getContent().getValues()).add("report", report);
//				}
			}
			// get template
		} catch (Exception e) {
			Log.error(e);
			resp = new Response(Constants.ActionTypes.PAGE_REDIRECT, "/Error", null);
		}

		return resp;
	}



}

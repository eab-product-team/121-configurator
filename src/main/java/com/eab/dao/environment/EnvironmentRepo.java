package com.eab.dao.environment;

import com.eab.model.environment.entity.EnvEntity;
import com.eab.model.environment.entity.EnvKey;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface EnvironmentRepo extends CrudRepository<EnvEntity, EnvKey> {

    List<EnvEntity> findByKeyCompCode(String compCode);

    EnvEntity findByKeyCompCodeAndKeyEnvId(String compCode, String envId);
}

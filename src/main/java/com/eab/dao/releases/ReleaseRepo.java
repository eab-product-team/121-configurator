package com.eab.dao.releases;

import com.eab.model.releases.entity.ReleaseEntity;
import org.springframework.data.repository.CrudRepository;

public interface ReleaseRepo extends CrudRepository<ReleaseEntity, Integer> {
}

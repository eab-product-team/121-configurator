package com.eab.dao.configuration;

import com.eab.model.configuration.entity.ConfigSectionEntity;
import com.eab.model.configuration.entity.ConfigVersionEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ConfigSectionRepo extends CrudRepository<ConfigSectionEntity, Integer> {

    List<ConfigSectionEntity> findByVersion(ConfigVersionEntity version);

}

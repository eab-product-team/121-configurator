package com.eab.dao.configuration;

import com.eab.model.configuration.entity.ConfigSummaryEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ConfigSummaryRepo extends CrudRepository<ConfigSummaryEntity, Integer> {

    @Query("select c from ConfigSummaryEntity c where (?1 is null or module = ?1) and (?2 is null or type = ?2)")
    List<ConfigSummaryEntity> findByModuleAndType(String module, String type);

}

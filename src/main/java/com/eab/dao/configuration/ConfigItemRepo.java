package com.eab.dao.configuration;

import com.eab.model.configuration.entity.ConfigItemEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ConfigItemRepo extends CrudRepository<ConfigItemEntity, Integer> {

    @Query("select c from ConfigItemEntity c where configType = ?1 and (name like %?2% or description like %?2% or id like %?2%)")
    List<ConfigItemEntity> findByConfigTypeAndQuery(String configType, String q);
}

package com.eab.dao.configuration;

import com.eab.model.configuration.entity.AttachmentEntity;
import com.eab.model.configuration.entity.AttachmentKey;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AttachmentRepo extends CrudRepository<AttachmentEntity, AttachmentKey> {

    List<AttachmentEntity> findAllByKeyCompCodeAndKeyModuleAndKeyDocCode(String compCode, String module, String docCode);

}

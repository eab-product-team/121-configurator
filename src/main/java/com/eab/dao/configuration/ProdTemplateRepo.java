package com.eab.dao.configuration;

import com.eab.model.configuration.entity.ProdTemplateKey;
import com.eab.model.configuration.entity.ProdTemplateEntity;
import org.springframework.data.repository.CrudRepository;

public interface ProdTemplateRepo extends CrudRepository<ProdTemplateEntity, ProdTemplateKey> {
}

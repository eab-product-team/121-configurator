package com.eab.dao.configuration;

import com.eab.model.configuration.entity.ConfigSummaryEntity;
import com.eab.model.configuration.entity.ConfigVersionEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ConfigVersionRepo extends CrudRepository<ConfigVersionEntity, Integer> {

    List<ConfigVersionEntity> findBySummaryOrderByVersionNumDesc(ConfigSummaryEntity summary);

    @Query("select max(c.versionNum) from ConfigVersionEntity c where summary.id = ?1")
    Integer getMaxVersionNum(int summaryId);

}

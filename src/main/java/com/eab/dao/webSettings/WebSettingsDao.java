package com.eab.dao.webSettings;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;

import com.eab.common.Function;
import com.eab.common.Log;
import com.eab.database.jdbc.DBAccess;
import com.eab.database.jdbc.DBManager;
import com.eab.database.jdbc.DataType;
import com.eab.model.profile.UserPrincipalBean;

public class WebSettingsDao {

	public JSONObject getSettings(UserPrincipalBean principal, String id) throws SQLException {
		Connection conn = null;
		DBManager dbm = null;
		ResultSet rs = null;
		JSONObject settings = null;

		try {	
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			
			String sql = " select setg_doc_id,"
					+ " setg_type,"
					+ " raw_data"
					+ " from web_settgs_mst"				   
					+ " where comp_code = " + dbm.param(principal.getCompCode(), DataType.TEXT)
					+ " and setg_doc_id = " + dbm.param(id, DataType.TEXT);

			rs = dbm.select(sql, conn);
					
//			return (rs);		
			if (rs.next()) {				
				settings = new JSONObject();
				settings.put("docId", rs.getString("setg_doc_id"));
				settings.put("raw",  Function.convertClobToJson(rs.getClob("raw_data")));
				settings.put("type", rs.getString("setg_type"));
			}
		} catch (Exception e) {
			Log.error("{ReleasePublish getENVData} ----------->");
			Log.error(e);
		} finally {
			dbm = null;
			rs = null;
			try {
				if (conn != null && !conn.isClosed())
					conn.close();
			} catch (Exception ex) {
				Log.error(ex);
			}
			conn = null;
		}	
		
		return settings;
	}
}

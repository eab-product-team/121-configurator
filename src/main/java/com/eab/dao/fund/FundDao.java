package com.eab.dao.fund;

import com.eab.model.fund.FundEntity;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface FundDao extends CrudRepository<FundEntity, Integer> {

    List<FundEntity> findByCompCode(String compCode, Sort sort);

}
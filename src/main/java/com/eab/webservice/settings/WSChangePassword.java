package com.eab.webservice.settings;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Base64;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import com.eab.biz.settings.SettingsManager;
import com.eab.common.Log;
import com.eab.json.model.Response;
import com.google.gson.Gson;

/**
 * Servlet implementation class ChangePassword
 */
@WebServlet("/Settings/ChangePassword")
public class WSChangePassword extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public WSChangePassword() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		Log.debug("servelet -> Change language");
		Response resp = null;
		String param = request.getParameter("p0");
		if (param != null && !param.isEmpty()) {
			String resultStr = new String(Base64.getDecoder().decode(param.replace(" ", "+")), "UTF-8");
			JSONObject resultJSON = new JSONObject(resultStr);
			SettingsManager sm = new SettingsManager(request);
			String curPass = resultJSON.getString("data_001");
			String newPass1 = resultJSON.getString("data_002");
			String newPass2 = resultJSON.getString("data_003");
			Log.debug("{ChangePassword} curPass = " + curPass);
			Log.debug("{ChangePassword} newPass1 = " + newPass1);
			Log.debug("{ChangePassword} newPass2 = " + newPass2);
			resp = sm.changePassword(curPass, newPass1, newPass2);
		}
		response.setCharacterEncoding("utf-8");
		response.setContentType("application/json; charset=utf-8");
		response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
		response.setDateHeader("Expires", 0);
		response.addHeader("Cache-Control", "post-check=0, pre-check=0");
		response.setHeader("Pragma", "no-cache");
		PrintWriter out = response.getWriter();
		Gson gson = new Gson();
		out.append(gson.toJson(resp));

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

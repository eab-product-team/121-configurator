package com.eab.webservice.dynamic;

import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.codec.binary.Base64;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.eab.common.Function2;
import com.eab.common.Log;
import com.eab.dao.dynamic.TemplateDAO;
import com.eab.database.jdbc.DBManager;
import com.eab.database.jdbc.DataType;
import com.eab.json.model.AppBar;
import com.eab.json.model.AppBarTitle;
import com.eab.json.model.Field;
import com.eab.json.model.Option;
import com.eab.json.model.Template;
import com.eab.model.dynamic.DynamicUI;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class DynamicFunction {

	public static JSONObject getParap0(HttpServletRequest request) {
		byte[] decoded = Base64.decodeBase64(request.getParameter("p0").replace(" ", "+"));
		String paraStr = new String(decoded, StandardCharsets.UTF_8);
		Log.debug("p0:::"+paraStr);
		JSONObject paraJson = new JSONObject(paraStr);
		return paraJson;
	}
	
	public static AppBar getAppBarFromJson(JSONObject barObject){

		AppBar bar = null;
		List<List<Field>> appBarActions = null;
		AppBarTitle appBarTitle = null;
		
		//tool bar Actions
		if(barObject.has("actions")){
			JSONArray actions = barObject.getJSONArray("actions");
			appBarActions = new ArrayList<>();
			for(int i=0; i<actions.length(); i++){
				JSONArray action = actions.getJSONArray(i);
				List<Field> actionFields = new ArrayList<>();
				for(int j=0; j<action.length(); j++){
					JSONObject fieldObject = action.getJSONObject(j);
					String id = fieldObject.has("id") ? fieldObject.getString("id"):"";
					String type = fieldObject.has("type") ? fieldObject.getString("type"):"";
					String title = fieldObject.has("title") ? fieldObject.getString("title"):"";
					String value = fieldObject.has("value") ? fieldObject.getString("value"):"";
					
					//right actions
					Field actionField = new Field(id, type, title);
					actionField.setValue(value);
					
					//right actions options
					if(fieldObject.has("options")){
						JSONArray optionsArray = fieldObject.getJSONArray("options");
						List<Option> options = new ArrayList<Option>();
						for(int optionIndex = 0; optionIndex<optionsArray.length(); optionIndex++){
							JSONObject optionObject = optionsArray.getJSONObject(optionIndex);
							Option option = new Option(optionObject.getString("title"), optionObject.getString("value"));
							options.add(option);
						}
						actionField.setOptions(options);							
						
					}
					actionFields.add(actionField);
				}
				appBarActions.add(actionFields);
			}
			
		}
		
		if(barObject.has("title")){
			JSONObject titleObject = barObject.getJSONObject("title");
			String titleId = titleObject.has("id")?titleObject.getString("id"):"";
			String titleType = titleObject.has("type")?titleObject.getString("type"):"";
			String titleValue = titleObject.has("value")?titleObject.getString("value"):"";
			String titlePrimary = titleObject.has("primary")?titleObject.getString("primary"):"";
								
			appBarTitle = new AppBarTitle(titleId, titleType, titleValue, titlePrimary, null, null);
			if(titleObject.has("options")){
				JSONArray optionsArray = titleObject.getJSONArray("options");
				List<Option> options = new ArrayList<Option>();
				for(int optionIndex = 0; optionIndex<optionsArray.length(); optionIndex++){
					JSONObject optionObject = optionsArray.getJSONObject(optionIndex);
					Option option = new Option(optionObject.getString("title"), optionObject.getString("value"));
					options.add(option);
				}
				appBarTitle.setOptions(options);											
			}
			if(titleObject.has("secondary")){
				appBarTitle.setSecondary(titleObject.getString("secondary"));
			}
			
		}
		JsonObject gsonValues = null;
		if(barObject.has("values")){
			 JsonParser jsonParser = new JsonParser();
			 gsonValues = (JsonObject)jsonParser.parse(barObject.getJSONObject("values").toString());
		}
		bar = new AppBar(appBarTitle,appBarActions, gsonValues);
		return bar;
	}

	public static Template getTemplateFromJson(HttpServletRequest request, JSONObject templateOj){		
		Gson gson = new Gson();
		Template template = gson.fromJson(templateOj.toString(), Template.class);
			
		try {
			List<String> names = new ArrayList<String>();
//			names.add(template.getTitle());
			template.getNameCodes(names);
			Map<String, String> lmap;
			lmap = Function2.getTranslateMap(request, names);
			template.translate(lmap);
		} catch (SQLException e) {
			Log.error(e);
			
		}
		
		return template;
	}
	
	public static JsonObject convertJsonToGson(JSONObject json){
	    JsonParser jsonParser = new JsonParser();
	    JsonObject gsonObject = (JsonObject)jsonParser.parse(json.toString());
	    return gsonObject;
	}
	
	public static ArrayList<Field> getSubFieldsByArray(String key, JSONObject item){
			JSONArray fieldSubItems = item.getJSONArray(key);
			ArrayList<Field> subFields = new ArrayList<Field>();
			for(int j = 0; j < fieldSubItems.length(); j++){
				JSONObject subItem = fieldSubItems.getJSONObject(j);
				Field subField = new Field();
				subField.setTitle(subItem.getString("title"));
				subField.setValue(subItem.getString("value"));
				subFields.add(subField);
			}
			return subFields;
	}
	
	public static ArrayList<Option> getOptionsByArray(String key, JSONObject item){
		JSONArray fieldSubItems = item.getJSONArray(key);
		ArrayList<Option> subFields = new ArrayList<Option>();
		for(int j = 0; j < fieldSubItems.length(); j++){
			JSONObject subItem = fieldSubItems.getJSONObject(j);
			Option subField = new Option(subItem.getString("title"), subItem.getString("value"));
			subFields.add(subField);
		}
		return subFields;
}

	public static JSONObject getViewFromTemplate(String viewID, JSONObject template) {
		JSONObject view = null;
		JSONArray items = template.getJSONArray("items");
		for (int i = 0; i < items.length(); i++) {
			JSONObject item = items.getJSONObject(i);
			if (item.getString("id").equals("viewID")) {
				view = item;
			}
		}
		return view;
	}

	public static String buildCondition(String condition, String newCon) {
		if (condition == null || condition.isEmpty()) {
			if (newCon == null || newCon.isEmpty())
				return "";
			else
				return " WHERE " + newCon;
		} else {
			if (newCon == null || newCon.isEmpty())
				return condition;
			else {
				return (condition + " and " + newCon);
			}
		}

	}
	
	public static String addCompanyFilter(DBManager dbm, String condition, String compCode) throws Exception{
		
		return addCompanyFilter(dbm, condition, null, compCode, null);
	}
	
	public static String addCompanyFilter(DBManager dbm, String condition, DynamicUI ui, String compCode, String table) throws Exception{
		try{
			if(ui == null || ui.getCompanyFilter()){
				// get all if compCode == 'SYS'
				String compFil = null;
				if(table != null){
					compFil = table+".comp_code = ?";
				}else{
					compFil = "comp_code = ?";
				}
				if(!isNotEmptyOrNull(condition)){
					condition = " where " + compFil;
				}
				else{
					condition += " and " + compFil;
				}
				dbm.param(compCode, DataType.TEXT);
			}
		}catch(Exception e){
			Log.error(e);
		}
		return condition;
	}
	
	public static String addLangFilter(DBManager dbm, String condition, String langCode) throws Exception{
		
		return addLangFilter(dbm, condition, null, langCode, null);
	}
	
	public static String addLangFilter(DBManager dbm, String condition, DynamicUI ui, String lang, String table) throws Exception{
		try{
			if(ui == null || ui.getLangFilter()){
				// get all if compCode == 'SYS'
				String langFil = null;
				if(table != null){
					langFil = table+".lang_code = ?";
				}else{
					langFil = "lang_code = ?";
				}
				if(!isNotEmptyOrNull(condition)){
					condition = " where " + langFil;
				}
				else{
					condition += " and " + langFil;
				}
				dbm.param(lang, DataType.TEXT);
			}
		}catch(Exception e){
			Log.error(e);
		}
		return condition;
	}
	
	public static String buildColStr(String col, String newCol) {
		if (col == null) {
			col = "(" + newCol;
		} else {
			col += ", " + newCol;
		}
		return col;
	}

	public static String endColStr(String col) {
		col += ")";
		return col;
	}

	// get All template ids, then get realted Data from data table
	public static String getSelectField(JSONArray templateItems) {
		String fieldStr = "";
		for (int i = 0; i < templateItems.length(); i++) {
			try {
				String key = null;
				JSONObject item = templateItems.getJSONObject(i);
				if (item.has("key")) {
					key = (item.getString("key")).toUpperCase();

				}
				if (key != null) {
					if (fieldStr == "")
						fieldStr = key;
					else
						fieldStr = fieldStr + ", " + key;
				}

			} catch (JSONException e) {
				Log.error(e);
			}
		}
		return fieldStr;
	}

	public static HashMap<String, String> getTemplateMap(JSONObject template) {
		HashMap<String, String> map = new HashMap<String, String>();
		JSONArray items = template.getJSONArray("items");
		int length = items.length();
		for (int i = 0; i < length; i++) {
			JSONObject item = items.getJSONObject(i);
			if(item.get("key") != null && item.get("id") != null)
				map.put(item.get("id").toString(), item.get("key").toString());
		}
		return map;
	}
	
	public static HashMap<String, String> getTemplateReverseMap(JSONObject template) {
		HashMap<String, String> map = new HashMap<String, String>();
		JSONArray items = template.getJSONArray("items");
		int length = items.length();
		for (int i = 0; i < length; i++) {
			JSONObject item = items.getJSONObject(i);
			if(item.get("key") != null && item.get("id") != null)
				map.put(item.get("key").toString(), item.get("id").toString());
		}
		return map;
	}
	
	public static String getTableColByTemplate(JSONObject template, String tableName) {
		String cols = null;
		JSONArray items = template.getJSONArray("items");
		int length = items.length();
		for (int i = 0; i < length; i++) {
			JSONObject item = items.getJSONObject(i);
			String col = item.getString("key");
			boolean seqTrue = false;	
			if(item.has("detailSeq")){
				if(item.get("detailSeq") != null && item.getInt("detailSeq") != 0){
					 seqTrue = true;
				}
			}
			if(item.getString("id").equals("compCode")){
				seqTrue = true;
			}
			
			if(isNotEmptyOrNull(col) && seqTrue && !item.getString("type").toUpperCase().equals("MULTTEXT")){
				String fieldWithTable = tableName+"."+col;
				String newCol = isNotEmptyOrNull(tableName) ? fieldWithTable : col;
				if(cols == null){					
					cols = newCol;
				}else{
					cols += ", " + newCol;
				}
			}
		}
		return cols;
		
	}
		
	public static JSONObject getTempalteWithDetailSEQ(String module,DynamicUI ui, Connection conn) {
		return getTemplateWithSEQ(module,ui, conn, "detailSeq", false);
	}

	static public JSONObject getTemplateWithListSEQ(String module,DynamicUI ui, Connection conn) {
		return getTemplateWithSEQ(module,ui, conn, "listSeq", false);
	}


	static public JSONObject getTemplateWithSEQ(String module, DynamicUI ui, Connection conn, String seq, boolean upload) {
		TemplateDAO tempalteDao =  new TemplateDAO();
		JSONObject template = tempalteDao.getTemplate(module, ui, conn);
		JSONArray items = template.getJSONArray("items");
		JSONArray itemsNew = new JSONArray();
		int size = items.length();
		for (int i = 0; i < size; i++) {
			JSONObject item = items.getJSONObject(i);
			boolean hasCompany = upload && item.getString("id").equals("compCode");
			if (strToInteger(item.getString(seq)) > 0 || hasCompany) {
				itemsNew.put(item);
			}
		}
		template.put("items", itemsNew);
		return template;
	}
	
	

	public static int strToInteger(String s) {
		int target = 0;
		try {
			target = Integer.parseInt(s);
		} catch (NumberFormatException e) {
			return 0;
		} catch (NullPointerException e) {
			return 0;
		}
		// only got here if we didn't return false
		return target;
	}


	static public JSONObject getDetailTemplate(JSONObject template){
		JSONObject detailTemplate = new JSONObject(template.toString());
		return detailTemplate;
	}
	
	public static boolean isNotEmptyOrNull(String str) {
		return str != null && !str.isEmpty();
	}

	public static String getUserPermission() {
		String permission = null;
		return permission;
	}

	public static boolean hasApproveRight() {
		return true;
	}
	

	public static boolean hasEditRight() {
		return true;
	}
	
	public static String dateNUserCol(String col, String table){
		if(table == null)
			col += ",CREATE_DATE, CREATE_BY, MODIFY_DATE, MODIFY_BY, APPROV_DATE, APPROV_BY";
		else{
			col += ","+table+".CREATE_DATE, "+table+".CREATE_BY ,"+table+".MODIFY_DATE,"+table+".MODIFY_BY,"+table+".APPROV_DATE,"+table+".APPROV_BY";
		};
		return col;
	}
	
}

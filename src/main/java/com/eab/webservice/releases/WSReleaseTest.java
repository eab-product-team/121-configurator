package com.eab.webservice.releases;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Base64;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;

import com.eab.biz.releases.ReleaseDetailMgr;
import com.eab.biz.releases.ReleaseMgr;
import com.eab.biz.releases.ReleasePublishMgr;
import com.eab.common.Constants;
import com.eab.common.Log;
import com.eab.dao.releases.ReleasePublish;
import com.eab.json.model.Response;
import com.eab.model.profile.UserPrincipalBean;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@WebServlet("/Releases/Test")
public class WSReleaseTest extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public WSReleaseTest() {
        super();
        // TODO Auto-generated constructor stub
    }
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Do not support GET method
		doPost(request, response);//TODO: to be removed after testing
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = null;
		boolean isMainPage = false;
		String releaseID = "";
		String envID = "";
		Response resp = null;
		Gson gson = new GsonBuilder().enableComplexMapKeySerialization().setPrettyPrinting().create();
		
	  	ReleaseDetailMgr releaseDetail = new ReleaseDetailMgr(request);
		
		try {			
			String param = request.getParameter("p0");
			
			if (param != null && !param.isEmpty()) {
				String resultStr = new String(Base64.getDecoder().decode(param.replace(" ", "+")), "UTF-8");
				JSONObject resultJSON = new JSONObject(resultStr);
									
				//Parameter from Release Detail Page
				if (resultJSON.has("keyList")) {
					releaseID = resultJSON.getString("keyList");
				} else if (resultJSON.has("values")) {
					releaseID = resultJSON.getJSONObject("values").getString("id");
				}
				
				if (resultJSON.has("radio_ENV_Detail"))
					envID = (String) resultJSON.get("radio_ENV_Detail");
				
				//Parameter from Release Main Page
				if (resultJSON.has("radio_ENV_Main")) {
					isMainPage = true;
					envID = (String) resultJSON.get("radio_ENV_Main");
				}
				
				//Common Parameter
				if (resultJSON.has("rows")) {
					if (resultJSON.getJSONArray("rows").length() > 0)
						releaseID = String.valueOf(resultJSON.getJSONArray("rows").get(0));
				}
			}	
				
			//Start to multi threading
			Thread t1 = new MyThread(request, releaseID, envID);
			t1.start();
			
			//Update release status to Locked
			releaseDetail.UpdateStatus(releaseID, "L");
			
			if (isMainPage) {
				ReleaseMgr release = new ReleaseMgr(request);
				resp = release.getJson();
			} else {
				resp = releaseDetail.getJson(releaseID);
			}
		} catch (Exception e) {
			Log.info("{WSReleaseTest} ---------- Exception 1");
			Log.error(e);
			
			if (releaseID != "") {
				try {
					releaseDetail.UpdateStatus(releaseID, "F");
				} catch(Exception e2) {
					Log.info("{WSReleaseTest} ---------- Exception 2");
					Log.error(e2);
				}
			}
		} finally {
			//Return Json
			response.setCharacterEncoding("utf-8");
	        response.setContentType("application/json; charset=utf-8");
			response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
			response.setDateHeader("Expires", 0);
			response.addHeader("Cache-Control", "post-check=0, pre-check=0");
			response.setHeader("Pragma", "no-cache");			
			
			out = response.getWriter();
			out.print(gson.toJson(resp));
			out.flush();
			out.close();			
		}
	}

	private static class MyThread extends Thread {
		private HttpServletRequest request;
		private String releaseID = "";
		private String envID = "";
		private String compCode;
		private String userCode;
		
		public MyThread(HttpServletRequest request, String releaseID, String envID) {
			this.request = request;
			this.releaseID = releaseID;
			this.envID = envID;
			
			HttpSession session = request.getSession();
			UserPrincipalBean principal = (UserPrincipalBean) session.getAttribute(Constants.USER_PRINCIPAL);
			
			compCode = principal.getCompCode();
			userCode = principal.getUserCode();
		}
		
	    @Override
	    public void run() {
	    	Log.info("----------------------Step 1.1 - Release publish - Start");
	    	
	    	ReleaseDetailMgr release = new ReleaseDetailMgr(request);
	    	
			if (!this.releaseID.equals("") && !this.envID.equals("")) {
				try {
					ReleasePublishMgr releasePublish = new ReleasePublishMgr(request);
					
					if (releasePublish.testRelease(releaseID, envID, compCode, userCode))
						release.UpdateStatus(releaseID, "T"); //Update release status to Testing
					
				} catch (Exception e) {
					Log.info("{WSReleaseTest run} ---------- Exception 1");
					Log.error(e);
					
					try {
						release.UpdateStatus(releaseID, "F"); //Update release status to Failed
						ReleasePublish publishdao = new ReleasePublish();
						publishdao.AddHistory(releaseID , envID, "N", e.toString(), ((UserPrincipalBean)request.getSession().getAttribute(Constants.USER_PRINCIPAL)).getUserCode());
					} catch(Exception e2) {
						Log.info("{WSReleaseTest run} ---------- Exception 2");
						Log.error(e2);
					}
				}
			} else {
				Log.info("{WSReleaseTest run} ---------- release or env id is empty");
			}
			
			Log.info("----------------------Step 1.2 - Release publish - End");
	    }
	}
	
}


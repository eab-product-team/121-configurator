package com.eab.webservice.password;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;

import com.eab.biz.settings.SettingsManager;
import com.eab.biz.system.PasswordPolicy;
import com.eab.biz.system.ResetPassword;
import com.eab.common.Constants;
import com.eab.common.CryptoUtil;
import com.eab.common.Function;
import com.eab.common.Language;
import com.eab.common.Log;
import com.eab.dao.system.Name;
import com.eab.dao.system.ResetPasswordTrx;
import com.eab.model.RSAKeyPair;
import com.eab.model.system.PasswordCheckType;

/**
 * Servlet implementation class WSResetAuth
 */
@WebServlet("/Password/ResetCheck")
public class WSResetCheck extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String RESET_PWD_TOKEN_NAME = "RESET_PWD_TOKEN";
	private static final String RESET_PWD_USERCODE_NAME = "RESET_PWD_USERCODE";
	private static final int TOKON_LENGTH = 30;   
    /**
     * @see HttpServlet#HttpServlet()
     */
    public WSResetCheck() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String userCode = null;
		HttpSession session = null;
		PrintWriter out = null;
		String pwd01 = null;
		String pwd02 = null;
		String reqCode = null;
		int checkResult = -1;
		ArrayList<Name> nameList = null;
		Name nameObj = null;
		Language langObj = null;
		RSAKeyPair keypair = null;
		Boolean hasError = false;
		ResetPassword resetBiz= null;
		try {

			session = request.getSession();
			userCode = (session.getAttribute(RESET_PWD_USERCODE_NAME) != null)? (String)session.getAttribute(RESET_PWD_USERCODE_NAME): null;
			
			//Get New Password Parameters.
			pwd01 = request.getParameter("pwd01");		// New Password
			pwd02 = request.getParameter("pwd02");		// Retype New Password
			reqCode = request.getParameter("p3");		// token
			
			//Parameter Validation
			if (reqCode == null || reqCode.length() != TOKON_LENGTH) {
				hasError = true;
			}

			//Verify Request Code (exist and non-expired).
			if (!hasError) {
				resetBiz = new ResetPassword();
				Map<String, String> resetMap = resetBiz.checkRequest(reqCode);
				if (resetMap != null) {
					userCode = resetMap.get("USER_CODE"); 
				}

				if (userCode == null || userCode.length() == 0)
					hasError = true;
			}

			if (!hasError) {
				if (session.getAttribute(Constants.CRYPTO_KEYPAIR_NAME) != null) {
					keypair = (RSAKeyPair) session.getAttribute(Constants.CRYPTO_KEYPAIR_NAME);
				}
				
				if (keypair != null) {
					pwd01 = Function.decryptJsText(keypair, pwd01);
					Log.debug("{LoginManager authenticate} Decrypted Password = " + pwd01);
				}
	
				langObj = new Language();
				nameList = new ArrayList<Name>();
			    HashMap<String, String> nameMap;
			    List<String> labelList;
				
				JSONObject json = new JSONObject();			
				
		    	labelList = Arrays.asList("PASSWORD.LABEL.TITLE", "PASSWORD.LABEL.SUBTITLE", "PASSWORD.FLOATLABEL.PASSWORD", "PASSWORD.HITS.PASSWORD", "PASSWORD.FLOATLABEL.REPASSWORD", "PASSWORD.HITS.REPASSWORD", "PASSWORD.BTN.RESET" ,"PASSWORD.POPMSG.LENGTH" ,"PASSWORD.POPMSG.MISS_CHAR" ,"PASSWORD.POPMSG.MISS_Num" ,"PASSWORD.POPMSG.HISTORY" ,"PASSWORD.POPMSG.USERCODE_PATTERN", "PASSWORD.POPMSG.MATCH");
				nameMap = new HashMap<String, String>();
				
				langObj = new Language();
				nameList = new ArrayList<Name>();
				
				for(String label: labelList) {
					nameObj = new Name();
					nameMap.put(label, "");
					nameObj.setCode(label);
					nameList.add(nameObj); 
				}
				
				if (langObj.getNameList(request, nameList) ) {
					for(Name dummy: nameList) {
						nameMap.replace(dummy.getCode(), dummy.getDesc());
					}
				}
				
				json.put("action", Constants.ActionTypes.CHANGE_CONTENT);
				Map<String , String>page = new HashMap<String, String>();
				
				page.put("id", "/Password/Reset");
				page.put("title", "Password");
				json.put("page", page);
				json.put("code", checkResult);
				json.put("pwLength", PasswordPolicy.checkLength(pwd01));
				json.put("pwLetter", PasswordPolicy.checkLetter(pwd01));
				json.put("pwNumeric", PasswordPolicy.checkNumeric(pwd01));
				json.put("pwHistory", PasswordPolicy.checkHistory(request, pwd01, userCode));
				json.put("pwUser", PasswordPolicy.checkUser(pwd01,userCode ));
				if(pwd01.equals(pwd02)){
					json.put("pwMatch", true);
				}else{
					json.put("pwMatch", false);
				}
				
				Map<String , String>template = new HashMap<String, String>();
				template.put("title", nameMap.get("PASSWORD.LABEL.TITLE"));
				template.put("subtitle", nameMap.get("PASSWORD.LABEL.SUBTITLE"));
				template.put("floatPW", nameMap.get("PASSWORD.FLOATLABEL.PASSWORD"));
				template.put("hintsPW", nameMap.get("PASSWORD.HITS.PASSWORD"));
				template.put("floatRPW", nameMap.get("PASSWORD.FLOATLABEL.REPASSWORD"));
				template.put("hintsRPW", nameMap.get("PASSWORD.HITS.REPASSWORD"));		
				template.put("btn", nameMap.get("PASSWORD.BTN.RESET"));		
				template.put("popLength", nameMap.get("PASSWORD.POPMSG.LENGTH"));		
				template.put("popChar", nameMap.get("PASSWORD.POPMSG.MISS_CHAR"));		
				template.put("popNum", nameMap.get("PASSWORD.POPMSG.MISS_Num"));		
				template.put("popHistory", nameMap.get("PASSWORD.POPMSG.HISTORY"));		
				template.put("popPattern", nameMap.get("PASSWORD.POPMSG.USERCODE_PATTERN"));
				template.put("popMatch", nameMap.get("PASSWORD.POPMSG.MATCH"));
				
				
				json.put("template", template);
	/*			jsonString = "{ \"action\" : \"" + Constants.ActionTypes.CHANGE_CONTENT+ "\","
						   + "  \"page\" : {\"id\" : \"/Password/Reset\", \"title\" : \"Password\"},"
						   + " \"code\":\"" + checkResult + "\", "
						   + "\"pwLength\": " + PasswordPolicy.checkLength(pwd01) + ", "
						   + "\"pwLetter\": " + PasswordPolicy.checkLetter(pwd01) + ", "
						   + "\"pwNumeric\": " + PasswordPolicy.checkNumeric(pwd01) + ", "
						   + "\"pwHistory\": " + PasswordPolicy.checkHistory(request, pwd01, userCode) + ", "
						   + "\"pwUser\": " + PasswordPolicy.checkUser(pwd01,userCode )
						   + "}";*/
				
				
				//Return JSON.
				response.setCharacterEncoding("utf-8");
		        response.setContentType("application/json; charset=utf-8");
				response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
				response.setDateHeader("Expires", 0);
				response.addHeader("Cache-Control", "post-check=0, pre-check=0");
				response.setHeader("Pragma", "no-cache");
				out = response.getWriter();
				out.print(json.toString());
				out.flush();
				out.close();
			}
		} catch(Exception e) {
			Log.error(e);
		} finally {
		}
		
		
	}

}

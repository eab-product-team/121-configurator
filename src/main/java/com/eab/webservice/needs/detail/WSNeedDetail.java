package com.eab.webservice.needs.detail;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Base64;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import com.eab.biz.dynamic.LockManager;
import com.eab.biz.needs.NeedDetailMgr;
import com.eab.common.Log;
import com.eab.dao.dynamic.RecordLock;
import com.eab.dao.needs.Needs;
import com.eab.json.model.Dialog;
import com.eab.model.dynamic.RecordLockBean;
import com.google.gson.Gson;

@WebServlet("/Needs/Detail")
public class WSNeedDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public WSNeedDetail() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Do not support GET method
		doPost(request, response);//TODO: to be removed after testing
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = null;
		String outputJSON = "";
		String needCode = "";
		String channelCode = "";
		String selectPageId = "";
		int version = -1;
		boolean isNewVersion = false;
				
		try {			
			try {
				String param = request.getParameter("p0");
				if (param != null && !param.isEmpty()) {
					String resultStr = new String(Base64.getDecoder().decode(param.replace(" ", "+")), "UTF-8");
					JSONObject resultJSON = new JSONObject(resultStr);
				
					//From create new version
					if (resultJSON.has("rows")){
						JSONArray jsonArray = (JSONArray)resultJSON.get("rows");
						
						for(Object obj : jsonArray){
							needCode = obj.toString();
						}				
						
						isNewVersion = true;	
					}
					
					//From need main page
					if (resultJSON.has("needCode"))
						needCode = resultJSON.get("needCode").toString();
					
					//From version selection
					if (resultJSON.has("id"))
						needCode = resultJSON.get("id").toString();
					
					if (resultJSON.has("version"))
						version = resultJSON.getInt("version");
								
					if (resultJSON.has("channelCode"))
						channelCode = resultJSON.get("channelCode").toString();
					
					if (resultJSON.has("selectPageId"))
						selectPageId = resultJSON.get("selectPageId").toString();
				}
			} catch (Exception e) {
				 Log.error(e);
			}
			
			
						
			//Get Json from need manager
			NeedDetailMgr need = new NeedDetailMgr(request);
			Gson gson = new Gson();
			outputJSON = gson.toJson(need.getNeedDetail(needCode, channelCode, version, selectPageId, isNewVersion));//TODO:version
						        
			//Return Json
			response.setCharacterEncoding("utf-8");
	        response.setContentType("application/json; charset=utf-8");
			response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
			response.setDateHeader("Expires", 0);
			response.addHeader("Cache-Control", "post-check=0, pre-check=0");
			response.setHeader("Pragma", "no-cache");			
			
			out = response.getWriter();
			out.print(outputJSON);
			out.flush();
			out.close();
		} catch(Exception e) {
			Log.error(e);
		} finally {
			
		}		
	}	
}

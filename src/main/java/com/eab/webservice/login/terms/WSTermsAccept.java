package com.eab.webservice.login.terms;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;

import com.eab.biz.system.Terms;
import com.eab.common.Constants;
import com.eab.common.Function;
import com.eab.common.Log;
import com.eab.common.LoginManager;
import com.eab.common.MainMenu;
import com.eab.common.Token;
import com.eab.dao.audit.AudUserLog;
import com.eab.model.profile.UserPrincipalBean;

/**
 * Servlet implementation class WSTermsAccept
 */
@WebServlet("/Login/Terms/Accept")
public class WSTermsAccept extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public WSTermsAccept() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		PrintWriter out = null;
		String jsonString = null;
		UserPrincipalBean principal = null;
		
		LoginManager loginMgr = new LoginManager(); 
		Terms terms = null;
		MainMenu menu = new MainMenu();
		try {
			Map<String, Object> json = new HashMap<String, Object>();
			principal = Function.getPrincipal(request);
			terms = new Terms();
			
			principal.getWorkflow().setTerms(true);
			
			// Update User Aceepted T&C
			terms.setTermsAccept(principal.getUserCode(), Function.getClientIp(request));
			
			String nextFlow =  principal.getWorkflow().nextFlow();
			
			if (nextFlow.equals("/Home")) {
				json.putAll(loginMgr.prepareHomeLanding(session, menu, request));
				json.put("tokenID", Token.CsrfID(session));
				json.put("tokenKey", Token.CsrfToken(session));
			} 
			
			json.put("action", Constants.ActionTypes.PAGE_REDIRECT);
			json.put("path", nextFlow);
			JSONObject jsObj = new JSONObject(json);

			jsonString = jsObj.toString();
					
					
			// Get next steps of Login process
			
			Log.debug("{WSTermsAccept} JSon Output: " + jsonString);
			
			response.setCharacterEncoding("utf-8");
	        response.setContentType("application/json; charset=utf-8");
			response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
			response.setDateHeader("Expires", 0);
			response.addHeader("Cache-Control", "post-check=0, pre-check=0");
			response.setHeader("Pragma", "no-cache");
			out = response.getWriter();
			out.print(jsonString);
			out.flush();
			out.close();
		} catch(Exception e) {
			Log.error(e);
		} finally {
			menu = null;
			loginMgr = null;
			terms = null;
		}
	}

}

package com.eab.webservice.login;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.eab.common.Language;
import com.eab.common.Log;
import com.eab.common.Token;
import com.eab.dao.system.Name;

/**
 * Servlet implementation class WSTerms
 */
@WebServlet("/Login/Terms")
public class WSTerms extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public WSTerms() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = null;
		HttpSession session = request.getSession();
		String jsonString = "{}";
		List<String> labelList = Arrays.asList("TERMS.TITLE", "TERMS.CONTENTS", "BUTTON.ACCEPT", "BUTTON.DECLINE");
		HashMap<String, String> nameMap = new HashMap<String, String>();
		
		try {
			String tokenID = Token.CsrfID(session);
			String tokenKey = Token.CsrfToken(session);
			
			Language langObj = new Language();
			ArrayList<Name> nameList = new ArrayList<Name>();
			for(String label: labelList) {
				Name nameObj = new Name();
				nameMap.put(label, "");
				nameObj.setCode(label);
				nameList.add(nameObj);
			}
			if ( langObj.getNameList(request, nameList) ) {
				for(Name dummy: nameList) {
					nameMap.replace(dummy.getCode(), dummy.getDesc());
				}
			}
			
			jsonString = "{";
			jsonString += "\"TokenID\": \"" + tokenID + "\"";
			jsonString += ", \"TokenKey\": \"" + tokenKey +"\"";
			jsonString += ", \"Title\": \"" + nameMap.get("TERMS.TITLE") +"\"";
			jsonString += ", \"Contents\": \"" + nameMap.get("TERMS.CONTENTS") +"\"";
			jsonString += ", \"Accept\": \"" + nameMap.get("BUTTON.ACCEPT") +"\"";
			jsonString += ", \"Decline\": \"" + nameMap.get("BUTTON.DECLINE") +"\"";
			jsonString += "}";
			
			Log.debug("{WSTerms} JSON=" + jsonString);

			response.setCharacterEncoding("utf-8");
			response.setContentType("application/json; charset=utf-8");
			response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
			response.setDateHeader("Expires", 0);
			response.addHeader("Cache-Control", "post-check=0, pre-check=0");
			response.setHeader("Pragma", "no-cache");
			out = response.getWriter();
			out.print(jsonString);		//TODO: Support multi-language
			out.flush();
			out.close();
		} catch(Exception e) {
			Log.error(e);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}

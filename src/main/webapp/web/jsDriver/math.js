(function webpackUniversalModuleDefinition(root) {
	return function webpackUniversalModuleDefinitionWrapBootstrap(fn) {
		return function webpackUniversalModuleDefinitionBootstrap(modules) {
			if(typeof exports === 'object' && typeof module === 'object')
				module.exports = fn(modules);
			else if(typeof define === 'function' && define.amd)
				define(function() { return fn(modules); });
			else if(typeof exports === 'object')
				exports["mathjs"] = fn(modules);
			else
				root["mathjs"] = fn(modules);
		}
	}
})(this)
  (function(modules) {  
  	 
  	var exports = "exports";
  	
  	 
  	var installedModules = {};
  	
  	 
  	function require(moduleId) {
  		 
  		if(installedModules[moduleId])
  			return installedModules[moduleId][exports];
  		
  		 
  		var module = installedModules[moduleId] = {
  			exports: {},
  			id: moduleId,
  			loaded: false
  		};
  		
  		 
  		modules[moduleId].call(module[exports], module, module[exports], require);
  		
  		 
  		module.loaded = true;
  		
  		 
  		return module[exports];
  	}
  	
  	
  	 
  	require.modules = modules;
  	
  	 
  	require.cache = installedModules;
  	
  	 
  	require.p = "";
  	
  	
  	 
  	return require(0);
  })
 
  ([
 
  function(module, exports, require) {

	module.exports = require(1);


  },
 
  function(module, exports, require) {

	var object = require(2);

	 
	function mathjs (settings) {
	   
	  if (typeof Object.create !== 'function') {
	    throw new Error('ES5 not supported by this JavaScript engine. ' +
	        'Please load the es5-shim and es5-sham library for compatibility.');
	  }

	   
	  var math = {};

	   
	  var _settings = {
	     
	    matrix: 'matrix',

	     
	    number: 'number'
	  };

	   
	  math.config = function config (settings) {
	    var BigNumber = require(220);

	    if (settings) {
	       
	      object.deepExtend(_settings, settings);

	      if (settings.decimals) {
	        BigNumber.config({
	          DECIMAL_PLACES: settings.decimals
	        });
	      }

	       
	      if (settings.number && settings.number.defaultType) {
	        throw new Error('setting `number.defaultType` is deprecated. ' +
	            'Use `number` instead.')
	      }

	       
	      if (settings.number && settings.number.precision) {
	        throw new Error('setting `number.precision` is deprecated. ' +
	            'Use `decimals` instead.')
	      }

	       
	      if (settings.matrix && settings.matrix.defaultType) {
	        throw new Error('setting `matrix.defaultType` is deprecated. ' +
	            'Use `matrix` instead.')
	      }

	       
	      if (settings.matrix && settings.matrix['default']) {
	        throw new Error('setting `matrix.default` is deprecated. ' +
	            'Use `matrix` instead.')
	      }
	    }

	     
	    var current = object.clone(_settings);
	    current.decimals = BigNumber.config().DECIMAL_PLACES;
	    return current;
	  };

	   
	  math.config(settings);

	   
	  math.expression = {};
	  math.expression.node = require(3);
	  math.expression.parse = require(4);
	  math.expression.Scope = function () {
	    throw new Error('Scope is deprecated. Use a regular Object instead');
	  };
	  math.expression.Parser = require(5);
	  math.expression.docs = require(6);

	   
	  math.type = {};
	  math.type.BigNumber = require(220);
	  math.type.Complex = require(7);
	  math.type.Range = require(8);
	  math.type.Index = require(9);
	  math.type.Matrix = require(10);
	  math.type.Unit = require(11);
	  math.type.Help = require(12);

	  math.collection = require(13);

	   
	  require(14)(math);

	   
	  require(15)(math, _settings);
	  require(16)(math, _settings);
	  require(17)(math, _settings);
	  require(18)(math, _settings);

	   
	  require(19)(math, _settings);
	  require(20)(math, _settings);
	  require(21)(math, _settings);
	  require(22)(math, _settings);
	  require(23)(math, _settings);
	  require(24)(math, _settings);
	  require(25)(math, _settings);
	  require(26)(math, _settings);
	  require(27)(math, _settings);
	  require(28)(math, _settings);
	  require(29)(math, _settings);
	  require(30)(math, _settings);
	  require(31)(math, _settings);
	  require(32)(math, _settings);
	  require(33)(math, _settings);
	  require(34)(math, _settings);
	  require(35)(math, _settings);
	  require(36)(math, _settings);
	  require(37)(math, _settings);
	  require(38)(math, _settings);
	  require(39)(math, _settings);
	  require(40)(math, _settings);
	  require(41)(math, _settings);
	  require(42)(math, _settings);
	  require(43)(math, _settings);
	  require(44)(math, _settings);
	  require(45)(math, _settings);
	  require(46)(math, _settings);
	  require(47)(math, _settings);
	  require(48)(math, _settings);
	  require(49)(math, _settings);

	   
	  require(50)(math, _settings);
	  require(51)(math, _settings);
	  require(52)(math, _settings);
	  require(53)(math, _settings);

	   
	  require(54)(math, _settings);
	  require(55)(math, _settings);
	  require(56)(math, _settings);
	  require(57)(math, _settings);
	  require(58)(math, _settings);
	  require(59)(math, _settings);
	  require(60)(math, _settings);
	  require(61)(math, _settings);
	  require(62)(math, _settings);
	  require(63)(math, _settings);

	   
	  require(64)(math, _settings);
	  require(65)(math, _settings);
	  require(66)(math, _settings);
	  require(67)(math, _settings);
	  require(68)(math, _settings);
	  require(69)(math, _settings);
	  require(70)(math, _settings);
	  require(71)(math, _settings);
	  require(72)(math, _settings);
	  require(73)(math, _settings);
	  require(74)(math, _settings);
	  require(75)(math, _settings);
	  require(76)(math, _settings);

	   
	  require(77)(math, _settings);
	  require(78)(math, _settings);
	  require(79)(math, _settings);
	  require(80)(math, _settings);

	   
	  require(81)(math, _settings);
	  require(82)(math, _settings);
	  require(83)(math, _settings);

	   
	  require(84)(math, _settings);
	  require(85)(math, _settings);
	  require(86)(math, _settings);
	  require(87)(math, _settings);
	  require(88)(math, _settings);
	  require(89)(math, _settings);
	  require(90)(math, _settings);
	  require(91)(math, _settings);
	  require(92)(math, _settings);
	  require(93)(math, _settings);

	   
	  require(94)(math, _settings);

	   
	  require(95)(math, _settings);
	  require(96)(math, _settings);
	  require(97)(math, _settings);
	  require(98)(math, _settings);
	  require(99)(math, _settings);
	  require(100)(math, _settings);
	  require(101)(math, _settings);

	   
	  require(102)(math, _settings);

	   
	  math.chaining = {};
	  math.chaining.Selector = require(103)(math, _settings);

	   
	  return math;
	}


	 
	module.exports = mathjs;


  },
 
  function(module, exports, require) {

	 
	exports.clone = function clone(x) {
	  var type = typeof x;

	   
	  if (type === 'number' || type === 'string' || type === 'boolean' ||
	      x === null || x === undefined) {
	    return x;
	  }

	   
	  if (typeof x.clone === 'function') {
	    return x.clone();
	  }

	   
	  if (Array.isArray(x)) {
	    return x.map(function (value) {
	      return clone(value);
	    });
	  }

	   
	  if (x instanceof Object) {
	    var m = {};
	    for (var key in x) {
	      if (x.hasOwnProperty(key)) {
	        m[key] = clone(x[key]);
	      }
	    }
	    return x;
	  }

	   
	  throw new TypeError('Cannot clone ' + x);
	};

	 
	exports.extend = function extend (a, b) {
	  for (var prop in b) {
	    if (b.hasOwnProperty(prop)) {
	      a[prop] = b[prop];
	    }
	  }
	  return a;
	};

	 
	exports.deepExtend = function deepExtend (a, b) {
	  for (var prop in b) {
	    if (b.hasOwnProperty(prop)) {
	      if (b[prop] && b[prop].constructor === Object) {
	        if (a[prop] === undefined) {
	          a[prop] = {};
	        }
	        if (a[prop].constructor === Object) {
	          deepExtend(a[prop], b[prop]);
	        }
	        else {
	          a[prop] = b[prop];
	        }
	      } else {
	        a[prop] = b[prop];
	      }
	    }
	  }
	  return a;
	};

	 
	exports.deepEqual = function deepEqual (a, b) {
	  var prop, i, len;
	  if (Array.isArray(a)) {
	    if (!Array.isArray(b)) {
	      return false;
	    }

	    if (a.length != b.length) {
	      return false;
	    }

	    for (i = 0, len = a.length; i < len; i++) {
	      if (!exports.deepEqual(a[i], b[i])) {
	        return false;
	      }
	    }
	    return true;
	  }
	  else if (a instanceof Object) {
	    if (Array.isArray(b) || !(b instanceof Object)) {
	      return false;
	    }

	    for (prop in a) {
	      if (a.hasOwnProperty(prop)) {
	        if (!exports.deepEqual(a[prop], b[prop])) {
	          return false;
	        }
	      }
	    }
	    for (prop in b) {
	      if (b.hasOwnProperty(prop)) {
	        if (!exports.deepEqual(a[prop], b[prop])) {
	          return false;
	        }
	      }
	    }
	    return true;
	  }
	  else {
	    return (a == b);
	  }
	};


  },
 
  function(module, exports, require) {

	exports.ArrayNode = require(104);
	exports.AssignmentNode = require(105);
	exports.BlockNode = require(106);
	exports.ConstantNode = require(107);
	exports.IndexNode = require(108);
	exports.FunctionNode = require(109);
	exports.Node = require(110);
	exports.OperatorNode = require(111);
	exports.ParamsNode = require(112);
	exports.RangeNode = require(113);
	exports.SymbolNode = require(114);
	exports.UnitNode = require(115);
	exports.UpdateNode = require(116);


  },
 
  function(module, exports, require) {

	var util = require(117),

	    toNumber = util.number.toNumber,
	    isString = util.string.isString,
	    isArray = Array.isArray,
	    type = util.types.type,

	     
	    Complex = require(7),
	    Matrix = require(10),
	    Unit = require(11),
	    collection = require(13),

	     
	    ArrayNode = require(104),
	    AssignmentNode = require(105),
	    BlockNode = require(106),
	    ConstantNode = require(107),
	    FunctionNode = require(109),
	    IndexNode = require(108),
	    OperatorNode = require(111),
	    ParamsNode = require(112),
	    RangeNode = require(113),
	    SymbolNode = require(114),
	    UnitNode = require(115),
	    UpdateNode = require(116);

	 
	function parse (expr, nodes) {
	  if (arguments.length != 1 && arguments.length != 2) {
	    throw new SyntaxError('Wrong number of arguments: 1 or 2 expected');
	  }

	   
	  extra_nodes = (type(nodes) === 'object') ? nodes : {};

	  if (isString(expr)) {
	     
	    expression = expr || '';
	    return parseStart();
	  }
	  else if (isArray(expr) || expr instanceof Matrix) {
	     
	    return collection.deepMap(expr, function (elem) {
	      expression = elem || '';
	      return parseStart();
	    });
	  }
	  else {
	     
	    throw new TypeError('String or matrix expected');
	  }
	}

	 
	var TOKENTYPE = {
	  NULL : 0,
	  DELIMITER : 1,
	  NUMBER : 2,
	  SYMBOL : 3,
	  UNKNOWN : 4
	};

	 
	var DELIMITERS = {
	  ',': true,
	  '(': true,
	  ')': true,
	  '[': true,
	  ']': true,
	  '\"': true,
	  '\n': true,
	  ';': true,

	  '+': true,
	  '-': true,
	  '*': true,
	  '.*': true,
	  '/': true,
	  './': true,
	  '%': true,
	  '^': true,
	  '.^': true,
	  '!': true,
	  '\'': true,
	  '=': true,
	  ':': true,

	  '==': true,
	  '!=': true,
	  '<': true,
	  '>': true,
	  '<=': true,
	  '>=': true
	};

	 
	var NAMED_DELIMITERS = {
	  'mod': true,
	  'to': true,
	  'in': true
	};

	var extra_nodes = {};              
	var expression = '';               
	var index = 0;                     
	var c = '';                        
	var token = '';                    
	var token_type = TOKENTYPE.NULL;   

	 
	function first() {
	  index = 0;
	  c = expression.charAt(0);
	}

	 
	function next() {
	  index++;
	  c = expression.charAt(index);
	}

	 
	function nextPreview() {
	  return expression.charAt(index + 1);
	}

	 
	function getToken() {
	  token_type = TOKENTYPE.NULL;
	  token = '';

	   
	  while (c == ' ' || c == '\t') {   
	     
	    next();
	  }

	   
	  if (c == '#') {
	    while (c != '\n' && c != '') {
	      next();
	    }
	  }

	   
	  if (c == '') {
	     
	    token_type = TOKENTYPE.DELIMITER;
	    return;
	  }

	   
	  var c2 = c + nextPreview();
	  if (DELIMITERS[c2]) {
	    token_type = TOKENTYPE.DELIMITER;
	    token = c2;
	    next();
	    next();
	    return;
	  }

	   
	  if (DELIMITERS[c]) {
	    token_type = TOKENTYPE.DELIMITER;
	    token = c;
	    next();
	    return;
	  }

	   
	  if (isDigitDot(c)) {
	    token_type = TOKENTYPE.NUMBER;

	     
	    if (c == '.') {
	      token += c;
	      next();

	      if (!isDigit(c)) {
	         
	        token_type = TOKENTYPE.UNKNOWN;
	      }
	    }
	    else {
	      while (isDigit(c)) {
	        token += c;
	        next();
	      }
	      if (c == '.') {
	        token += c;
	        next();
	      }
	    }
	    while (isDigit(c)) {
	      token += c;
	      next();
	    }

	     
	    if (c == 'E' || c == 'e') {
	      token += c;
	      next();

	      if (c == '+' || c == '-') {
	        token += c;
	        next();
	      }

	       
	      if (!isDigit(c)) {
	         
	        token_type = TOKENTYPE.UNKNOWN;
	      }

	      while (isDigit(c)) {
	        token += c;
	        next();
	      }
	    }

	    return;
	  }

	   
	  if (isAlpha(c)) {
	    while (isAlpha(c) || isDigit(c)) {
	      token += c;
	      next();
	    }

	    if (NAMED_DELIMITERS[token]) {
	      token_type = TOKENTYPE.DELIMITER;
	    }
	    else {
	      token_type = TOKENTYPE.SYMBOL;
	    }

	    return;
	  }

	   
	  token_type = TOKENTYPE.UNKNOWN;
	  while (c != '') {
	    token += c;
	    next();
	  }
	  throw createSyntaxError('Syntax error in part "' + token + '"');
	}

	 
	function skipNewlines () {
	  while (token == '\n') {
	    getToken();
	  }
	}

	 
	   
	function isValidSymbolName (name) {
	  for (var i = 0, iMax = name.length; i < iMax; i++) {
	    var c = name.charAt(i);
	     
	    var valid = (isAlpha(c));
	    if (!valid) {
	      return false;
	    }
	  }

	  return true;
	}

	 
	function isAlpha (c) {
	  return ((c >= 'a' && c <= 'z') ||
	      (c >= 'A' && c <= 'Z') ||
	      c == '_');
	}

	 
	function isDigitDot (c) {
	  return ((c >= '0' && c <= '9') ||
	      c == '.');
	}

	 
	function isDigit (c) {
	  return ((c >= '0' && c <= '9'));
	}

	 
	function parseStart () {
	   
	  first();

	  getToken();

	  var node;
	  if (token == '') {
	     
	    node = new ConstantNode('undefined', 'undefined');
	  }
	  else {
	    node = parseBlock();
	  }

	   
	   
	  if (token != '') {
	    if (token_type == TOKENTYPE.DELIMITER) {
	       

	       
	      throw createError('Unknown operator ' + token);
	    }
	    else {
	      throw createSyntaxError('Unexpected part "' + token + '"');
	    }
	  }

	  return node;
	}

	 
	function parseBlock () {
	  var node, block, visible;

	  if (token != '\n' && token != ';' && token != '') {
	    node = parseAns();
	  }

	  while (token == '\n' || token == ';') {
	    if (!block) {
	       
	      block = new BlockNode();
	      if (node) {
	        visible = (token != ';');
	        block.add(node, visible);
	      }
	    }

	    getToken();
	    if (token != '\n' && token != ';' && token != '') {
	      node = parseAns();

	      visible = (token != ';');
	      block.add(node, visible);
	    }
	  }

	  if (block) {
	    return block;
	  }

	  if (!node) {
	    node = parseAns();
	  }

	  return node;
	}

	 
	function parseAns () {
	  var expression = parseFunctionAssignment();

	   
	  var name = 'ans';
	  return new AssignmentNode(name, expression);
	}

	 
	function parseFunctionAssignment () {
	   
	  if (token_type == TOKENTYPE.SYMBOL && token == 'function') {
	    throw new Error('Deprecated keyword "function". ' +
	        'Functions can now be assigned without it, like "f(x) = x^2".');
	  }

	  return parseAssignment();
	}

	 
	function parseAssignment () {
	  var name, args, expr;

	  var node = parseRange();

	  if (token == '=') {
	    if (node instanceof SymbolNode) {
	       
	      name = node.name;
	      getToken();
	      expr = parseAssignment();
	      return new AssignmentNode(name, expr);
	    }
	    else if (node instanceof IndexNode) {
	       
	      getToken();
	      expr = parseAssignment();
	      return new UpdateNode(node, expr);
	    }
	    else if (node instanceof ParamsNode) {
	       
	      var valid = true;
	      args = [];
	      if (node.object instanceof SymbolNode) {
	        name = node.object.name;
	        node.params.forEach(function (param, index) {
	          if (param instanceof SymbolNode) {
	            args[index] = param.name;
	          }
	          else {
	            valid = false;
	          }
	        });
	      }
	      else {
	        valid = false;
	      }

	      if (valid) {
	        getToken();
	        expr = parseAssignment();
	        return new FunctionNode(name, args, expr);
	      }
	    }

	    throw createSyntaxError('Invalid left hand side of assignment operator =');
	  }

	  return node;
	}

	 
	function parseRange () {
	  var node, params = [];

	  if (token == ':') {
	     
	    node = new ConstantNode('number', '1');
	  }
	  else {
	     
	    node = parseBitwiseConditions();
	  }

	  if (token == ':') {
	    params.push(node);

	     
	    while (token == ':') {
	      getToken();
	      if (token == ')' || token == ']' || token == ',' || token == '') {
	         
	        params.push(new SymbolNode('end'));
	      }
	      else {
	         
	        params.push(parseBitwiseConditions());
	      }
	    }

	    if (params.length) {
	       
	      if (params.length == 3) {
	        var step = params[2];
	        params[2] = params[1];
	        params[1] = step;
	      }
	      node = new RangeNode(params);
	    }
	  }

	  return node;
	}

	 
	function parseBitwiseConditions () {
	  var node = parseComparison();

	   

	  return node;
	}

	 
	function parseComparison () {
	  var node, operators, name, fn, params;

	  node = parseConditions();

	  operators = {
	    '==': 'equal',
	    '!=': 'unequal',
	    '<': 'smaller',
	    '>': 'larger',
	    '<=': 'smallereq',
	    '>=': 'largereq'
	  };
	  while (token in operators) {
	    name = token;
	    fn = operators[name];

	    getToken();
	    params = [node, parseConditions()];
	    node = new OperatorNode(name, fn, params);
	  }

	  return node;
	}

	 
	function parseConditions () {
	  var node, operators, name, fn, params;

	  node = parseAddSubtract();

	   
	   
	  operators = {
	    'to' : 'to',
	    'in' : 'to'    
	     
	  };

	  while (token in operators) {
	    name = token;
	    fn = operators[name];

	    getToken();
	    params = [node, parseAddSubtract()];
	    node = new OperatorNode(name, fn, params);
	  }

	  return node;
	}

	 
	function parseAddSubtract ()  {
	  var node, operators, name, fn, params;

	  node = parseMultiplyDivide();

	  operators = {
	    '+': 'add',
	    '-': 'subtract'
	  };
	  while (token in operators) {
	    name = token;
	    fn = operators[name];

	    getToken();
	    params = [node, parseMultiplyDivide()];
	    node = new OperatorNode(name, fn, params);
	  }

	  return node;
	}

	 
	function parseMultiplyDivide () {
	  var node, operators, name, fn, params;

	  node = parseUnit();

	  operators = {
	    '*': 'multiply',
	    '.*': 'emultiply',
	    '/': 'divide',
	    './': 'edivide',
	    '%': 'mod',
	    'mod': 'mod'
	  };

	  while (token in operators) {
	    name = token;
	    fn = operators[name];

	    getToken();
	    params = [node, parseUnit()];
	    node = new OperatorNode(name, fn, params);
	  }

	  return node;
	}

	 
	function parseUnit() {
	  var node, symbol;

	  node = parseUnary();

	  if (token_type == TOKENTYPE.SYMBOL || token == 'in') {
	     
	    symbol = token;

	    getToken();

	    node = new UnitNode(node, symbol);
	  }

	  return node;
	}

	 
	function parseUnary () {
	  var name, fn, params;

	  if (token == '-') {
	    name = token;
	    fn = 'unary';
	    getToken();
	    params = [parseUnary()];

	    return new OperatorNode(name, fn, params);
	  }

	  return parsePow();
	}

	 
	function parsePow () {
	  var node, leftNode, nodes, ops, name, fn, params;

	  nodes = [
	    parseLeftHandOperators()
	  ];
	  ops = [];

	   
	  while (token == '^' || token == '.^') {
	    ops.push(token);
	    getToken();
	    nodes.push(parseLeftHandOperators());
	  }

	   
	  node = nodes.pop();
	  while (nodes.length) {
	    leftNode = nodes.pop();
	    name = ops.pop();
	    fn = (name == '^') ? 'pow' : 'epow';
	    params = [leftNode, node];
	    node = new OperatorNode(name, fn, params);
	  }

	  return node;
	}

	 
	function parseLeftHandOperators ()  {
	  var node, operators, name, fn, params;

	  node = parseCustomNodes();

	  operators = {
	    '!': 'factorial',
	    '\'': 'transpose'
	  };

	  while (token in operators) {
	    name = token;
	    fn = operators[name];

	    getToken();
	    params = [node];

	    node = new OperatorNode(name, fn, params);
	  }

	  return node;
	}

	 
	function parseCustomNodes () {
	  var params, handler;

	  if (token_type == TOKENTYPE.SYMBOL && extra_nodes[token]) {
	    handler = extra_nodes[token];

	    getToken();

	     
	    if (token == '(') {
	      params = [];

	      getToken();

	      if (token != ')') {
	        params.push(parseRange());

	         
	        while (token == ',') {
	          getToken();

	          params.push(parseRange());
	        }
	      }

	      if (token != ')') {
	        throw createSyntaxError('Parenthesis ) expected');
	      }
	      getToken();
	    }

	     
	     
	    return new handler(params);
	  }

	  return parseSymbol();
	}

	 
	function parseSymbol () {
	  var node, name;

	  if (token_type == TOKENTYPE.SYMBOL ||
	      (token_type == TOKENTYPE.DELIMITER && token in NAMED_DELIMITERS)) {
	    name = token;

	    getToken();

	     
	    node = new SymbolNode(name);

	     
	    return parseParams(node);
	  }

	  return parseString();
	}

	 
	function parseParams (node) {
	  var bracket, params;

	  while (token == '(' || token == '[') {
	    bracket = token;
	    params = [];

	    getToken();

	    if (token != ')' && token != ']') {
	      params.push(parseRange());

	       
	      while (token == ',') {
	        getToken();
	        params.push(parseRange());
	      }
	    }

	    if ((bracket == '(' && token != ')')) {
	      throw createSyntaxError('Parenthesis ) expected');
	    }
	    if ((bracket == '[' && token != ']')) {
	      throw createSyntaxError('Parenthesis ] expected');
	    }
	    getToken();

	    if (bracket == '(') {
	      node = new ParamsNode(node, params);
	    }
	    else {
	      node = new IndexNode(node, params);
	    }
	  }

	  return node;
	}

	 
	function parseString () {
	  var node, str, tPrev;

	  if (token == '"') {
	     
	    str = '';
	    tPrev = '';
	    while (c != '' && (c != '\"' || tPrev == '\\')) {  
	      str += c;
	      tPrev = c;
	      next();
	    }

	    getToken();
	    if (token != '"') {
	      throw createSyntaxError('End of string " expected');
	    }
	    getToken();

	     
	    node = new ConstantNode('string', str);

	     
	    node = parseParams(node);

	    return node;
	  }

	  return parseMatrix();
	}

	 
	function parseMatrix () {
	  var array, params, rows, cols;

	  if (token == '[') {
	     
	    getToken();
	    skipNewlines();

	    if (token != ']') {
	       
	      var row = parseRow();

	      if (token == ';') {
	         
	        rows = 1;
	        params = [row];

	         
	        while (token == ';') {
	          getToken();
	          skipNewlines();

	          params[rows] = parseRow();
	          rows++;

	          skipNewlines();
	        }

	        if (token != ']') {
	          throw createSyntaxError('End of matrix ] expected');
	        }
	        getToken();

	         
	        cols = (params.length > 0) ? params[0].length : 0;
	        for (var r = 1; r < rows; r++) {
	          if (params[r].length != cols) {
	            throw createError('Number of columns must match ' +
	                '(' + params[r].length + ' != ' + cols + ')');
	          }
	        }

	        array = new ArrayNode(params);
	      }
	      else {
	         
	        if (token != ']') {
	          throw createSyntaxError('End of matrix ] expected');
	        }
	        getToken();

	        array = row;
	      }
	    }
	    else {
	       
	      getToken();
	      array = new ArrayNode([]);
	    }

	     
	    array = parseParams(array);

	    return array;
	  }

	  return parseNumber();
	}

	 
	function parseRow () {
	  var params = [parseAssignment()];
	  var len = 1;

	  while (token == ',') {
	    getToken();
	    skipNewlines();

	     
	    params[len] = parseAssignment();
	    len++;

	    skipNewlines();
	  }

	  return new ArrayNode(params);
	}

	 
	function parseNumber () {
	  var node, complex, number;

	  if (token_type == TOKENTYPE.NUMBER) {
	     
	    number = token == '.' ? '0': token;
	    getToken();

	    if (token == 'i' || token == 'I') {
	       
	      getToken();
	      node = new ConstantNode('complex', number);
	    }
	    else {
	       
	      node = new ConstantNode('number', number);
	    }

	     
	    node = parseParams(node);

	    return node;
	  }

	  return parseParentheses();
	}

	 
	function parseParentheses () {
	  var node;

	   
	  if (token == '(') {
	     
	    getToken();
	    node = parseAssignment();  

	    if (token != ')') {
	      throw createSyntaxError('Parenthesis ) expected');
	    }
	    getToken();

	     

	     
	    node = parseParams(node);

	    return node;
	  }

	  return parseEnd();
	}

	 
	function parseEnd () {
	  if (token == '') {
	     
	    throw createSyntaxError('Unexpected end of expression');
	  } else {
	    throw createSyntaxError('Value expected');
	  }
	}

	 
	function row () {
	   
	  return undefined;
	}

	 
	function col () {
	  return index - token.length + 1;
	}

	 
	function createErrorMessage (message) {
	  var r = row();
	  var c = col();
	  if (r === undefined) {
	    if (c === undefined) {
	      return message;
	    } else {
	      return message + ' (char ' + c + ')';
	    }
	  } else {
	    return message + ' (line ' + r + ', char ' + c + ')';
	  }
	}

	 
	function createSyntaxError (message) {
	  return new SyntaxError(createErrorMessage(message));
	}

	 
	function createError (message) {
	  return new Error(createErrorMessage(message));
	}

	module.exports = parse;


  },
 
  function(module, exports, require) {

	var _parse = require(4);

	 
	function Parser(math) {
	  if (!(this instanceof Parser)) {
	    throw new SyntaxError(
	        'Parser constructor must be called with the new operator');
	  }

	  if (typeof math !== 'object') {
	    throw new TypeError('Object expected as parameter math');
	  }

	  this.math = math;
	  this.scope = {};
	}

	 
	Parser.prototype.parse = function (expr) {
	   
	  return _parse(expr);
	};

	 
	Parser.prototype.compile = function (expr) {
	   
	  return _parse(expr).compile(this.math);
	};

	 
	Parser.prototype.eval = function (expr) {
	   
	  return _parse(expr)
	      .compile(this.math)
	      .eval(this.scope);
	};

	 
	Parser.prototype.get = function (name) {
	   
	  return this.scope[name];
	};

	 
	Parser.prototype.set = function (name, value) {
	   
	  return this.scope[name] = value;
	};

	 
	Parser.prototype.remove = function (name) {
	   
	  delete this.scope[name];
	};

	 
	Parser.prototype.clear = function () {
	  for (var name in this.scope) {
	    if (this.scope.hasOwnProperty(name)) {
	      delete this.scope[name];
	    }
	  }
	};

	module.exports = Parser;


  },
 
  function(module, exports, require) {

	 
	exports.e = require(118);
	exports.E = require(118);
	exports['false'] = require(119);
	exports.i = require(120);
	exports['Infinity'] = require(121);
	exports.LN2 = require(122);
	exports.LN10 = require(123);
	exports.LOG2E = require(124);
	exports.LOG10E = require(125);
	exports.NaN = require(126);
	exports.pi = require(127);
	exports.PI = require(127);
	exports.SQRT1_2 = require(128);
	exports.SQRT2 = require(129);
	exports.tau = require(130);
	exports['true'] = require(131);

	 
	exports.abs = require(132);
	exports.add = require(133);
	exports.ceil = require(134);
	exports.cube = require(135);
	exports.divide = require(136);
	exports.edivide = require(137);
	exports.emultiply = require(138);
	exports.epow = require(139);
	exports.equal = require(140);
	exports.exp = require(141);
	exports.fix = require(142);
	exports.floor = require(143);
	exports.gcd = require(144);
	exports.larger = require(145);
	exports.largereq = require(146);
	exports.lcm = require(147);
	exports.log = require(148);
	exports.log10 = require(149);
	exports.mod = require(150);
	exports.multiply = require(151);
	exports.pow = require(152);
	exports.round = require(153);
	exports.sign = require(154);
	exports.smaller = require(155);
	exports.smallereq = require(156);
	exports.sqrt = require(157);
	exports.square = require(158);
	exports.subtract = require(159);
	exports.unary = require(160);
	exports.unequal = require(161);
	exports.xgcd = require(162);

	 
	exports.arg = require(163);
	exports.conj = require(164);
	exports.re = require(165);
	exports.im = require(166);

	 
	exports.bignumber = require(167);
	exports['boolean'] = require(168);
	exports.complex = require(169);
	exports.index = require(170);
	exports.matrix = require(171);
	exports.number = require(172);
	exports.string = require(173);
	exports.unit = require(174);

	 
	exports['eval'] =  require(175);
	exports.help =  require(176);

	 
	exports.concat = require(177);
	exports.det = require(178);
	exports.diag = require(179);
	exports.eye = require(180);
	exports.inv = require(181);
	exports.ones = require(182);
	exports.range = require(183);
	exports.resize = require(184);
	exports.size = require(185);
	exports.squeeze = require(186);
	exports.subset = require(187);
	exports.transpose = require(188);
	exports.zeros = require(189);

	 
	exports.combinations = require(190);
	exports.distribution = require(191);
	exports.factorial = require(192);
	exports.permutations = require(193);
	exports.pickRandom = require(194);
	exports.random = require(195);
	exports.randomInt = require(196);

	 
	exports.min = require(197);
	exports.mean = require(198);
	exports.max = require(199);

	 
	exports.acos = require(200);
	exports.asin = require(201);
	exports.atan = require(202);
	exports.atan2 = require(203);
	exports.cos = require(204);
	exports.cot = require(205);
	exports.csc = require(206);
	exports.sec = require(207);
	exports.sin = require(208);
	exports.tan = require(209);

	 
	exports.to = require(210);

	 
	exports.clone =  require(211);
	exports.map =  require(212);
	exports.forEach =  require(213);
	exports.format =  require(214);
	 
	exports['import'] =  require(215);
	exports['typeof'] =  require(216);


  },
 
  function(module, exports, require) {

	var util = require(117),
	    number = util.number,

	    isNumber = util.number.isNumber,
	    isString = util.string.isString;

	 
	function Complex(re, im) {
	  if (!(this instanceof Complex)) {
	    throw new SyntaxError(
	        'Complex constructor must be called with the new operator');
	  }

	  switch (arguments.length) {
	    case 0:
	      this.re = 0;
	      this.im = 0;
	      break;

	    case 2:
	      if (!isNumber(re) || !isNumber(im)) {
	        throw new TypeError('Two numbers expected in Complex constructor');
	      }
	      this.re = re;
	      this.im = im;
	      break;

	    default:
	      if (arguments.length != 0 && arguments.length != 2) {
	        throw new SyntaxError(
	            'Two or zero arguments expected in Complex constructor');
	      }
	      break;
	  }
	}

	 
	Complex.isComplex = function isComplex(value) {
	  return (value instanceof Complex);
	};

	 
	var text, index, c;

	function skipWhitespace() {
	  while (c == ' ' || c == '\t') {
	    next();
	  }
	}

	function isDigitDot (c) {
	  return ((c >= '0' && c <= '9') || c == '.');
	}

	function isDigit (c) {
	  return ((c >= '0' && c <= '9'));
	}

	function next() {
	  index++;
	  c = text.charAt(index);
	}

	function revert(oldIndex) {
	  index = oldIndex;
	  c = text.charAt(index);
	}

	function parseNumber () {
	  var number = '';
	  var oldIndex;
	  oldIndex = index;

	  if (c == '+') {
	    next();
	  }
	  else if (c == '-') {
	    number += c;
	    next();
	  }

	  if (!isDigitDot(c)) {
	     
	    revert(oldIndex);
	    return null;
	  }

	   
	  if (c == '.') {
	    number += c;
	    next();
	    if (!isDigit(c)) {
	       
	      revert(oldIndex);
	      return null;
	    }
	  }
	  else {
	    while (isDigit(c)) {
	      number += c;
	      next();
	    }
	    if (c == '.') {
	      number += c;
	      next();
	    }
	  }
	  while (isDigit(c)) {
	    number += c;
	    next();
	  }

	   
	  if (c == 'E' || c == 'e') {
	    number += c;
	    next();

	    if (c == '+' || c == '-') {
	      number += c;
	      next();
	    }

	     
	    if (!isDigit(c)) {
	       
	      revert(oldIndex);
	      return null;
	    }

	    while (isDigit(c)) {
	      number += c;
	      next();
	    }
	  }

	  return number;
	}

	function parseComplex () {
	   
	  var cnext = text.charAt(index + 1);
	  if (c == 'I' || c == 'i') {
	    next();
	    return '1';
	  }
	  else if ((c == '+' || c == '-') && (cnext == 'I' || cnext == 'i')) {
	    var number = (c == '+') ? '1' : '-1';
	    next();
	    next();
	    return number;
	  }

	  return null;
	}

	 
	Complex.parse = function parse (str) {
	  text = str;
	  index = -1;
	  c = '';

	  if (!isString(text)) {
	    return null;
	  }

	  next();
	  skipWhitespace();
	  var first = parseNumber();
	  if (first) {
	    if (c == 'I' || c == 'i') {
	       
	      next();
	      skipWhitespace();
	      if (c) {
	         
	        return null;
	      }

	      return new Complex(0, Number(first));
	    }
	    else {
	       
	      skipWhitespace();
	      var separator = c;
	      if (separator != '+' && separator != '-') {
	         
	        skipWhitespace();
	        if (c) {
	           
	          return null;
	        }

	        return new Complex(Number(first), 0);
	      }
	      else {
	         
	        next();
	        skipWhitespace();
	        var second = parseNumber();
	        if (second) {
	          if (c != 'I' && c != 'i') {
	             
	            return null;
	          }
	          next();
	        }
	        else {
	          second = parseComplex();
	          if (!second) {
	             
	            return null;
	          }
	        }

	        if (separator == '-') {
	          if (second[0] == '-') {
	            second =  '+' + second.substring(1);
	          }
	          else {
	            second = '-' + second;
	          }
	        }

	        next();
	        skipWhitespace();
	        if (c) {
	           
	          return null;
	        }

	        return new Complex(Number(first), Number(second));
	      }
	    }
	  }
	  else {
	     
	    first = parseComplex();
	    if (first) {
	      skipWhitespace();
	      if (c) {
	         
	        return null;
	      }

	      return new Complex(0, Number(first));
	    }
	  }

	  return null;
	};

	 
	Complex.prototype.clone = function clone () {
	  return new Complex(this.re, this.im);
	};

	 
	Complex.prototype.equals = function equals (other) {
	  return (this.re === other.re) && (this.im === other.im);
	};

	 
	Complex.prototype.format = function format (options) {
	  var str = '',
	      strRe = number.format(this.re, options),
	      strIm = number.format(this.im, options);

	  if (this.im == 0) {
	     
	    str = strRe;
	  }
	  else if (this.re == 0) {
	     
	    if (this.im == 1) {
	      str = 'i';
	    }
	    else if (this.im == -1) {
	      str = '-i';
	    }
	    else {
	      str = strIm + 'i';
	    }
	  }
	  else {
	     
	    if (this.im > 0) {
	      if (this.im == 1) {
	        str = strRe + ' + i';
	      }
	      else {
	        str = strRe + ' + ' + strIm + 'i';
	      }
	    }
	    else {
	      if (this.im == -1) {
	        str = strRe + ' - i';
	      }
	      else {
	        str = strRe + ' - ' + strIm.substring(1) + 'i';
	      }
	    }
	  }

	  return str;
	};

	 
	Complex.prototype.toString = function toString () {
	  return this.format();
	};

	 
	module.exports = Complex;

	 
	exports.isComplex = Complex.isComplex;
	exports.parse = Complex.parse;


  },
 
  function(module, exports, require) {

	var util = require(117),

	    number = util.number,
	    string = util.string,
	    array = util.array;

	 
	function Range(start, end, step) {
	  if (!(this instanceof Range)) {
	    throw new SyntaxError(
	        'Range constructor must be called with the new operator');
	  }

	  if (start != null && !number.isNumber(start)) {
	    throw new TypeError('Parameter start must be a number');
	  }
	  if (end != null && !number.isNumber(end)) {
	    throw new TypeError('Parameter end must be a number');
	  }
	  if (step != null && !number.isNumber(step)) {
	    throw new TypeError('Parameter step must be a number');
	  }

	  this.start = (start != null) ? parseFloat(start) : 0;
	  this.end   = (end != null) ? parseFloat(end) : 0;
	  this.step  = (step != null) ? parseFloat(step) : 1;
	}

	 
	Range.parse = function parse (str) {
	  if (!string.isString(str)) {
	    return null;
	  }

	  var args = str.split(':');
	  var nums = args.map(function (arg) {
	    return parseFloat(arg);
	  });

	  var invalid = nums.some(function (num) {
	    return isNaN(num);
	  });
	  if(invalid) {
	    return null;
	  }

	  switch (nums.length) {
	    case 2: return new Range(nums[0], nums[1]);
	    case 3: return new Range(nums[0], nums[2], nums[1]);
	    default: return null;
	  }
	};

	 
	Range.prototype.clone = function clone() {
	  return new Range(this.start, this.end, this.step);
	};

	 
	Range.isRange = function isRange(object) {
	  return (object instanceof Range);
	};

	 
	Range.prototype.size = function size() {
	  var len = 0,
	      start = this.start,
	      step = this.step,
	      end = this.end,
	      diff = end - start;

	  if (number.sign(step) == number.sign(diff)) {
	    len = Math.ceil((diff) / step);
	  }
	  else if (diff == 0) {
	    len = 0;
	  }

	  if (isNaN(len)) {
	    len = 0;
	  }
	  return [len];
	};

	 
	Range.prototype.min = function min () {
	  var size = this.size()[0];

	  if (size > 0) {
	    if (this.step > 0) {
	       
	      return this.start;
	    }
	    else {
	       
	      return this.start + (size - 1) * this.step;
	    }
	  }
	  else {
	    return undefined;
	  }
	};

	 
	Range.prototype.max = function max () {
	  var size = this.size()[0];

	  if (size > 0) {
	    if (this.step > 0) {
	       
	      return this.start + (size - 1) * this.step;
	    }
	    else {
	       
	      return this.start;
	    }
	  }
	  else {
	    return undefined;
	  }
	};


	 
	Range.prototype.forEach = function forEach(callback) {
	  var x = this.start;
	  var step = this.step;
	  var end = this.end;
	  var i = 0;

	  if (step > 0) {
	    while (x < end) {
	      callback(x, i, this);
	      x += step;
	      i++;
	    }
	  }
	  else if (step < 0) {
	    while (x > end) {
	      callback(x, i, this);
	      x += step;
	      i++;
	    }
	  }
	};

	 
	Range.prototype.map = function map(callback) {
	  var array = [];
	  this.forEach(function (value, index, obj) {
	    array[index] = callback(value, index, obj);
	  });
	  return array;
	};

	 
	Range.prototype.toArray = function toArray() {
	  var array = [];
	  this.forEach(function (value, index) {
	    array[index] = value;
	  });
	  return array;
	};

	 
	Range.prototype.valueOf = function valueOf() {
	   
	  return this.toArray();
	};

	 
	Range.prototype.format = function format(options) {
	  var str = number.format(this.start, options);

	  if (this.step != 1) {
	    str += ':' + number.format(this.step, options);
	  }
	  str += ':' + number.format(this.end, options);
	  return str;
	};

	 
	Range.prototype.toString = function toString() {
	  return this.format();
	};

	 
	module.exports = Range;

	 
	exports.isRange = Range.isRange;
	exports.parse = Range.parse;


  },
 
  function(module, exports, require) {

	var util = require(117),

	    Range = require(8),

	    number = util.number,

	    isNumber = number.isNumber,
	    isInteger = number.isInteger,
	    isArray = Array.isArray,
	    validateIndex = util.array.validateIndex;

	 
	function Index(ranges) {
	  if (!(this instanceof Index)) {
	    throw new SyntaxError(
	        'Index constructor must be called with the new operator');
	  }

	  this._ranges = [];

	  for (var i = 0, ii = arguments.length; i < ii; i++) {
	    var arg = arguments[i];

	    if (arg instanceof Range) {
	      this._ranges.push(arg);
	    }
	    else {
	      if (arg) {
	        arg = arg.valueOf();
	      }

	      if (isArray(arg)) {
	        this._ranges.push(_createRange(arg));
	      }
	      else if (isNumber(arg)) {
	        this._ranges.push(_createRange([arg, arg + 1]));
	      }
	       
	      else {
	        throw new TypeError('Range expected as Array, Number, or String');
	      }
	    }
	  }
	}

	 
	function _createRange(arg) {
	   

	   
	  var num = arg.length;
	  for (var i = 0; i < num; i++) {
	    if (!isNumber(arg[i]) || !isInteger(arg[i])) {
	      throw new TypeError('Index parameters must be integer numbers');
	    }
	  }

	  switch (arg.length) {
	    case 2:
	      return new Range(arg[0], arg[1]);  
	    case 3:
	      return new Range(arg[0], arg[1], arg[2]);  
	    default:
	       
	      throw new SyntaxError('Wrong number of arguments in Index (2 or 3 expected)');
	  }
	}

	 
	Index.prototype.clone = function clone () {
	  var index = new Index();
	  index._ranges = util.object.clone(this._ranges);
	  return index;
	};

	 
	Index.isIndex = function isIndex(object) {
	  return (object instanceof Index);
	};

	 
	Index.create = function create(ranges) {
	  var index = new Index();
	  Index.apply(index, ranges);
	  return index;
	};

	 
	Index.prototype.size = function size () {
	  var size = [];

	  for (var i = 0, ii = this._ranges.length; i < ii; i++) {
	    var range = this._ranges[i];

	    size[i] = range.size()[0];
	  }

	  return size;
	};

	 
	Index.prototype.max = function max () {
	  var values = [];

	  for (var i = 0, ii = this._ranges.length; i < ii; i++) {
	    var range = this._ranges[i];
	    values[i] = range.max();
	  }

	  return values;
	};

	 
	Index.prototype.min = function min () {
	  var values = [];

	  for (var i = 0, ii = this._ranges.length; i < ii; i++) {
	    var range = this._ranges[i];

	    values[i] = range.min();
	  }

	  return values;
	};

	 
	Index.prototype.forEach = function forEach(callback) {
	  for (var i = 0, ii = this._ranges.length; i < ii; i++) {
	    callback(this._ranges[i], i, this);
	  }
	};

	 
	Index.prototype.range = function range (dim) {
	  return this._ranges[dim];
	};

	 
	Index.prototype.isScalar = function isScalar () {
	  var size = this.size();

	  for (var i = 0, ii = size.length; i < ii; i++) {
	    if (size[i] !== 1) {
	      return false;
	    }
	  }

	  return true;
	};

	 
	Index.prototype.toArray = function toArray() {
	  var array = [];
	  for (var i = 0, ii = this._ranges.length; i < ii; i++) {
	    var range = this._ranges[i],
	        row = [],
	        x = range.start,
	        end = range.end,
	        step = range.step;

	    if (step > 0) {
	      while (x < end) {
	        row.push(x);
	        x += step;
	      }
	    }
	    else if (step < 0) {
	      while (x > end) {
	        row.push(x);
	        x += step;
	      }
	    }

	    array.push(row);
	  }

	  return array;
	};

	 
	Index.prototype.valueOf = Index.prototype.toArray;

	 
	Index.prototype.toString = function () {
	  var strings = [];

	  for (var i = 0, ii = this._ranges.length; i < ii; i++) {
	    var range = this._ranges[i];
	    var str = number.format(range.start);
	    if (range.step != 1) {
	      str += ':' + number.format(range.step);
	    }
	    str += ':' + number.format(range.end);
	    strings.push(str);
	  }

	  return '[' + strings.join(',') + ']';
	};

	 
	module.exports = Index;

	 
	exports.isIndex = Index.isIndex;
	exports.create = Index.create;


  },
 
  function(module, exports, require) {

	var util = require(117),
	    Index = require(9),

	    number = util.number,
	    string = util.string,
	    array = util.array,
	    object = util.object,

	    isArray = Array.isArray,
	    validateIndex = array.validateIndex;

	 
	function Matrix(data) {
	  if (!(this instanceof Matrix)) {
	    throw new SyntaxError(
	        'Matrix constructor must be called with the new operator');
	  }

	  if (data instanceof Matrix) {
	     
	    this._data = data.clone()._data;
	  }
	  else if (isArray(data)) {
	     
	     
	    this._data = preprocess(data);
	  }
	  else if (data != null) {
	     
	    throw new TypeError('Unsupported type of data (' + util.types.type(data) + ')');
	  }
	  else {
	     
	    this._data = [];
	  }

	   
	  this._size = array.size(this._data);
	}

	 
	Matrix.isMatrix = function isMatrix(object) {
	  return (object instanceof Matrix);
	};

	 
	Matrix.prototype.subset = function subset(index, replacement, defaultValue) {
	  switch (arguments.length) {
	    case 1:
	      return _get(this, index);

	     
	    case 2:
	    case 3:
	      return _set(this, index, replacement, defaultValue);

	    default:
	      throw new SyntaxError('Wrong number of arguments');
	  }
	};

	 
	Matrix.prototype.get = function get(index) {
	  if (!isArray(index)) {
	    throw new Error('Array expected');
	  }
	  if (index.length != this._size.length) {
	    throw new RangeError('Dimension mismatch ' +
	        '(' + index.length + ' != ' + this._size.length + ')');
	  }

	  var data = this._data;
	  for (var i = 0, ii = index.length; i < ii; i++) {
	    var index_i = index[i];
	    validateIndex(index_i, data.length);
	    data = data[index_i];
	  }

	  return object.clone(data);
	};

	 
	Matrix.prototype.set = function set (index, value, defaultValue) {
	  var i, ii;

	   
	  if (!isArray(index)) {
	    throw new Error('Array expected');
	  }
	  if (index.length < this._size.length) {
	    throw new RangeError('Dimension mismatch ' +
	        '(' + index.length + ' < ' + this._size.length + ')');
	  }

	   
	  var size = index.map(function (i) {
	    return i + 1;
	  });
	  _fit(this, size, defaultValue);

	   
	  var data = this._data;
	  for (i = 0, ii = index.length - 1; i < ii; i++) {
	    var index_i = index[i];
	    validateIndex(index_i, data.length);
	    data = data[index_i];
	  }

	   
	  index_i = index[index.length - 1];
	  validateIndex(index_i, data.length);
	  data[index_i] = value;

	  return this;
	};

	 
	function _get (matrix, index) {
	  if (!(index instanceof Index)) {
	    throw new TypeError('Invalid index');
	  }

	  var isScalar = index.isScalar();
	  if (isScalar) {
	     
	    return matrix.get(index.min());
	  }
	  else {
	     
	    var size = index.size();
	    if (size.length != matrix._size.length) {
	      throw new RangeError('Dimension mismatch ' +
	          '(' + size.length + ' != ' + matrix._size.length + ')');
	    }

	     
	    var submatrix = new Matrix(_getSubmatrix(matrix._data, index, size.length, 0));
	     

	     
	    while (isArray(submatrix._data) && submatrix._data.length == 1) {
	      submatrix._data = submatrix._data[0];
	      submatrix._size.shift();
	    }

	    return submatrix;
	  }
	}

	 
	function _getSubmatrix (data, index, dims, dim) {
	  var last = (dim == dims - 1);
	  var range = index.range(dim);

	  if (last) {
	    return range.map(function (i) {
	      validateIndex(i, data.length);
	      return data[i];
	    });
	  }
	  else {
	    return range.map(function (i) {
	      validateIndex(i, data.length);
	      var child = data[i];
	      return _getSubmatrix(child, index, dims, dim + 1);
	    });
	  }
	}

	 
	function _set (matrix, index, submatrix, defaultValue) {
	  if (!(index instanceof Index)) {
	    throw new TypeError('Invalid index');
	  }

	   
	  var iSize = index.size(),
	      isScalar = index.isScalar();

	   
	  var sSize;
	  if (submatrix instanceof Matrix) {
	    sSize = submatrix.size();
	    submatrix = submatrix.valueOf();
	  }
	  else {
	    sSize = array.size(submatrix);
	  }

	  if (isScalar) {
	     

	     
	    if (sSize.length != 0) {
	      throw new TypeError('Scalar value expected');
	    }

	    matrix.set(index.min(), submatrix, defaultValue);
	  }
	  else {
	     

	     
	    if (iSize.length < matrix._size.length) {
	      throw new RangeError('Dimension mismatch ' +
	          '(' + iSize.length + ' < ' + matrix._size.length + ')');
	    }

	     
	    for (var i = 0, ii = iSize.length - sSize.length; i < ii; i++) {
	      submatrix = [submatrix];
	      sSize.unshift(1);
	    }

	     
	    if (!object.deepEqual(iSize, sSize)) {
	      throw new RangeError('Dimensions mismatch ' +
	          '(' + string.format(iSize) + ' != '+ string.format(sSize) + ')');
	    }

	     
	    var size = index.max().map(function (i) {
	      return i + 1;
	    });
	    _fit(matrix, size, defaultValue);

	     
	    var dims = iSize.length,
	        dim = 0;
	    _setSubmatrix (matrix._data, index, submatrix, dims, dim);
	  }

	  return matrix;
	}

	 
	function _setSubmatrix (data, index, submatrix, dims, dim) {
	  var last = (dim == dims - 1),
	      range = index.range(dim);

	  if (last) {
	    range.forEach(function (dataIndex, subIndex) {
	      validateIndex(dataIndex);
	      data[dataIndex] = submatrix[subIndex];
	    });
	  }
	  else {
	    range.forEach(function (dataIndex, subIndex) {
	      validateIndex(dataIndex);
	      _setSubmatrix(data[dataIndex], index, submatrix[subIndex], dims, dim + 1);
	    });
	  }
	}

	 
	Matrix.prototype.resize = function resize(size, defaultValue) {
	  this._size = object.clone(size);
	  this._data = array.resize(this._data, this._size, defaultValue);

	   
	  return this;
	};

	 
	function _fit(matrix, size, defaultValue) {
	  if (!isArray(size)) {
	    throw new Error('Array expected');
	  }

	  var newSize = object.clone(matrix._size),
	      changed = false;

	   
	  while (newSize.length < size.length) {
	    newSize.unshift(0);
	    changed = true;
	  }

	   
	  for (var i = 0, ii = size.length; i < ii; i++) {
	    if (size[i] > newSize[i]) {
	      newSize[i] = size[i];
	      changed = true;
	    }
	  }

	  if (changed) {
	     
	    matrix.resize(newSize, defaultValue);
	  }
	}

	 
	Matrix.prototype.clone = function clone() {
	  var matrix = new Matrix();
	  matrix._data = object.clone(this._data);
	  matrix._size = object.clone(this._size);
	  return matrix;
	};

	 
	Matrix.prototype.size = function size() {
	  return this._size;
	};

	 
	Matrix.prototype.map = function map(callback) {
	  var me = this;
	  var matrix = new Matrix();
	  var index = [];
	  var recurse = function (value, dim) {
	    if (isArray(value)) {
	      return value.map(function (child, i) {
	        index[dim] = i;
	        return recurse(child, dim + 1);
	      });
	    }
	    else {
	      return callback(value, index, me);
	    }
	  };
	  matrix._data = recurse(this._data, 0);
	  matrix._size = object.clone(this._size);

	  return matrix;
	};

	 
	Matrix.prototype.forEach = function forEach(callback) {
	  var me = this;
	  var index = [];
	  var recurse = function (value, dim) {
	    if (isArray(value)) {
	      value.forEach(function (child, i) {
	        index[dim] = i;
	        recurse(child, dim + 1);
	      });
	    }
	    else {
	      callback(value, index, me);
	    }
	  };
	  recurse(this._data, 0);
	};

	 
	Matrix.prototype.toArray = function toArray() {
	  return object.clone(this._data);
	};

	 
	Matrix.prototype.valueOf = function valueOf() {
	  return this._data;
	};

	 
	Matrix.prototype.format = function format(options) {
	  return string.format(this._data, options);
	};

	 
	Matrix.prototype.toString = function toString() {
	  return string.format(this._data);
	};

	 
	function preprocess(data) {
	  for (var i = 0, ii = data.length; i < ii; i++) {
	    var elem = data[i];
	    if (isArray(elem)) {
	      data[i] = preprocess(elem);
	    }
	    else if (elem instanceof Matrix) {
	      data[i] = preprocess(elem._data);
	    }
	  }

	  return data;
	}

	 
	module.exports = Matrix;

	 
	exports.isMatrix = Matrix.isMatrix;


  },
 
  function(module, exports, require) {

	var util = require(117),

	    number = util.number,
	    string = util.string,
	    isNumber = util.number.isNumber,
	    isString = util.string.isString;

	 
	function Unit(value, unit) {
	  if (!(this instanceof Unit)) {
	    throw new Error('Unit constructor must be called with the new operator');
	  }

	  if (value != null && !isNumber(value)) {
	    throw new TypeError('First parameter in Unit constructor must be a number');
	  }
	  if (unit != null && !isString(unit)) {
	    throw new TypeError('Second parameter in Unit constructor must be a string');
	  }

	  if (unit != null) {
	     
	    var res = _findUnit(unit);
	    if (!res) {
	      throw new SyntaxError('Unknown unit "' + unit + '"');
	    }
	    this.unit = res.unit;
	    this.prefix = res.prefix;
	  }
	  else {
	    this.unit = UNIT_NONE;
	    this.prefix = PREFIX_NONE;   
	  }

	  if (value != null) {
	    this.value = this._normalize(value);
	    this.fixPrefix = false;   
	  }
	  else {
	    this.value = null;
	    this.fixPrefix = true;
	  }
	}

	 
	var text, index, c;

	function skipWhitespace() {
	  while (c == ' ' || c == '\t') {
	    next();
	  }
	}

	function isDigitDot (c) {
	  return ((c >= '0' && c <= '9') || c == '.');
	}

	function isDigit (c) {
	  return ((c >= '0' && c <= '9'));
	}

	function next() {
	  index++;
	  c = text.charAt(index);
	}

	function revert(oldIndex) {
	  index = oldIndex;
	  c = text.charAt(index);
	}

	function parseNumber () {
	  var number = '';
	  var oldIndex;
	  oldIndex = index;

	  if (c == '+') {
	    next();
	  }
	  else if (c == '-') {
	    number += c;
	    next();
	  }

	  if (!isDigitDot(c)) {
	     
	    revert(oldIndex);
	    return null;
	  }

	   
	  if (c == '.') {
	    number += c;
	    next();
	    if (!isDigit(c)) {
	       
	      revert(oldIndex);
	      return null;
	    }
	  }
	  else {
	    while (isDigit(c)) {
	      number += c;
	      next();
	    }
	    if (c == '.') {
	      number += c;
	      next();
	    }
	  }
	  while (isDigit(c)) {
	    number += c;
	    next();
	  }

	   
	  if (c == 'E' || c == 'e') {
	    number += c;
	    next();

	    if (c == '+' || c == '-') {
	      number += c;
	      next();
	    }

	     
	    if (!isDigit(c)) {
	       
	      revert(oldIndex);
	      return null;
	    }

	    while (isDigit(c)) {
	      number += c;
	      next();
	    }
	  }

	  return number;
	}

	function parseUnit() {
	  var unit = '';

	  skipWhitespace();
	  while (c && c != ' ' && c != '\t') {
	    unit += c;
	    next();
	  }

	  return unit || null;
	}

	 
	Unit.parse = function parse(str) {
	  text = str;
	  index = -1;
	  c = '';

	  if (!isString(text)) {
	    return null;
	  }

	  next();
	  skipWhitespace();
	  var value = parseNumber();
	  var unit;
	  if (value) {
	    unit = parseUnit();

	    next();
	    skipWhitespace();
	    if (c) {
	       
	      return null;
	    }

	    if (value && unit) {
	      return new Unit(Number(value), unit);
	    }
	  }
	  else {
	    unit = parseUnit();

	    next();
	    skipWhitespace();
	    if (c) {
	       
	      return null;
	    }

	    return new Unit(null, unit)
	  }

	  return null;
	};

	 
	Unit.isUnit = function isUnit(value) {
	  return (value instanceof Unit);
	};

	 
	Unit.prototype.clone = function () {
	  var clone = new Unit();

	  for (var p in this) {
	    if (this.hasOwnProperty(p)) {
	      clone[p] = this[p];
	    }
	  }

	  return clone;
	};

	 
	Unit.prototype._normalize = function(value) {
	  return (value + this.unit.offset) *
	      this.unit.value * this.prefix.value;
	};

	 
	Unit.prototype._unnormalize = function (value, prefixValue) {
	  if (prefixValue == undefined) {
	    return value / this.unit.value / this.prefix.value -
	        this.unit.offset;
	  }
	  else {
	    return value / this.unit.value / prefixValue -
	        this.unit.offset;
	  }
	};

	 
	function _findUnit(str) {
	  for (var name in UNITS) {
	    if (UNITS.hasOwnProperty(name)) {
	      if (string.endsWith(str, name) ) {
	        var unit = UNITS[name];
	        var prefixLen = (str.length - name.length);
	        var prefixName = str.substring(0, prefixLen);
	        var prefix = unit.prefixes[prefixName];
	        if (prefix !== undefined) {
	           
	          return {
	            unit: unit,
	            prefix: prefix
	          };
	        }
	      }
	    }
	  }

	  return null;
	}

	 
	Unit.isPlainUnit = function (unit) {
	  return (_findUnit(unit) != null);
	};

	 
	Unit.prototype.hasBase = function(base) {
	  if (this.unit.base === undefined) {
	    return (base === undefined);
	  }
	  return (this.unit.base === base);
	};

	 
	Unit.prototype.equalBase = function(other) {
	  return (this.unit.base === other.unit.base);
	};

	 
	Unit.prototype.equals = function(other) {
	  return (this.equalBase(other) && this.value == other.value);
	};

	 
	Unit.prototype.to = function (plainUnit) {
	  var other;
	  if (isString(plainUnit)) {
	    other = new Unit(null, plainUnit);

	    if (!this.equalBase(other)) {
	      throw new Error('Units do not match');
	    }

	    other.value = this.value;
	    return other;
	  }
	  else if (plainUnit instanceof Unit) {
	    if (!this.equalBase(plainUnit)) {
	      throw new Error('Units do not match');
	    }
	    if (plainUnit.value != null) {
	      throw new Error('Cannot convert to a unit with a value');
	    }
	    if (plainUnit.unit == null) {
	      throw new Error('Unit expected on the right hand side of function in');
	    }

	    other = plainUnit.clone();
	    other.value = this.value;
	    other.fixPrefix = true;
	    return other;
	  }
	  else {
	    throw new Error('String or Unit expected as parameter');
	  }
	};

	 
	Unit.prototype.toNumber = function (plainUnit) {
	  var other = this.to(plainUnit);
	  var prefix = this.fixPrefix ? other._bestPrefix() : other.prefix;
	  return other._unnormalize(other.value, prefix.value);
	};


	 
	Unit.prototype.toString = function toString() {
	  return this.format();
	};

	 
	Unit.prototype.format = function format(options) {
	  var value,
	      str;

	  if (!this.fixPrefix) {
	    var bestPrefix = this._bestPrefix();
	    value = this._unnormalize(this.value, bestPrefix.value);
	    str = (this.value != null) ? number.format(value, options) + ' ' : '';
	    str += bestPrefix.name + this.unit.name;
	  }
	  else {
	    value = this._unnormalize(this.value);
	    str = (this.value != null) ? number.format(value, options) + ' ' : '';
	    str += this.prefix.name + this.unit.name;
	  }
	  return str;
	};

	 
	Unit.prototype._bestPrefix = function () {
	   
	   
	   
	   
	  var absValue = Math.abs(this.value / this.unit.value);
	  var bestPrefix = PREFIX_NONE;
	  var bestDiff = Math.abs(
	      Math.log(absValue / bestPrefix.value) / Math.LN10 - 1.2);

	  var prefixes = this.unit.prefixes;
	  for (var p in prefixes) {
	    if (prefixes.hasOwnProperty(p)) {
	      var prefix = prefixes[p];
	      if (prefix.scientific) {
	        var diff = Math.abs(
	            Math.log(absValue / prefix.value) / Math.LN10 - 1.2);

	        if (diff < bestDiff) {
	          bestPrefix = prefix;
	          bestDiff = diff;
	        }
	      }
	    }
	  }

	  return bestPrefix;
	};

	var PREFIXES = {
	  'NONE': {
	    '': {'name': '', 'value': 1, 'scientific': true}
	  },
	  'SHORT': {
	    '': {'name': '', 'value': 1, 'scientific': true},

	    'da': {'name': 'da', 'value': 1e1, 'scientific': false},
	    'h': {'name': 'h', 'value': 1e2, 'scientific': false},
	    'k': {'name': 'k', 'value': 1e3, 'scientific': true},
	    'M': {'name': 'M', 'value': 1e6, 'scientific': true},
	    'G': {'name': 'G', 'value': 1e9, 'scientific': true},
	    'T': {'name': 'T', 'value': 1e12, 'scientific': true},
	    'P': {'name': 'P', 'value': 1e15, 'scientific': true},
	    'E': {'name': 'E', 'value': 1e18, 'scientific': true},
	    'Z': {'name': 'Z', 'value': 1e21, 'scientific': true},
	    'Y': {'name': 'Y', 'value': 1e24, 'scientific': true},

	    'd': {'name': 'd', 'value': 1e-1, 'scientific': false},
	    'c': {'name': 'c', 'value': 1e-2, 'scientific': false},
	    'm': {'name': 'm', 'value': 1e-3, 'scientific': true},
	    'u': {'name': 'u', 'value': 1e-6, 'scientific': true},
	    'n': {'name': 'n', 'value': 1e-9, 'scientific': true},
	    'p': {'name': 'p', 'value': 1e-12, 'scientific': true},
	    'f': {'name': 'f', 'value': 1e-15, 'scientific': true},
	    'a': {'name': 'a', 'value': 1e-18, 'scientific': true},
	    'z': {'name': 'z', 'value': 1e-21, 'scientific': true},
	    'y': {'name': 'y', 'value': 1e-24, 'scientific': true}
	  },
	  'LONG': {
	    '': {'name': '', 'value': 1, 'scientific': true},

	    'deca': {'name': 'deca', 'value': 1e1, 'scientific': false},
	    'hecto': {'name': 'hecto', 'value': 1e2, 'scientific': false},
	    'kilo': {'name': 'kilo', 'value': 1e3, 'scientific': true},
	    'mega': {'name': 'mega', 'value': 1e6, 'scientific': true},
	    'giga': {'name': 'giga', 'value': 1e9, 'scientific': true},
	    'tera': {'name': 'tera', 'value': 1e12, 'scientific': true},
	    'peta': {'name': 'peta', 'value': 1e15, 'scientific': true},
	    'exa': {'name': 'exa', 'value': 1e18, 'scientific': true},
	    'zetta': {'name': 'zetta', 'value': 1e21, 'scientific': true},
	    'yotta': {'name': 'yotta', 'value': 1e24, 'scientific': true},

	    'deci': {'name': 'deci', 'value': 1e-1, 'scientific': false},
	    'centi': {'name': 'centi', 'value': 1e-2, 'scientific': false},
	    'milli': {'name': 'milli', 'value': 1e-3, 'scientific': true},
	    'micro': {'name': 'micro', 'value': 1e-6, 'scientific': true},
	    'nano': {'name': 'nano', 'value': 1e-9, 'scientific': true},
	    'pico': {'name': 'pico', 'value': 1e-12, 'scientific': true},
	    'femto': {'name': 'femto', 'value': 1e-15, 'scientific': true},
	    'atto': {'name': 'atto', 'value': 1e-18, 'scientific': true},
	    'zepto': {'name': 'zepto', 'value': 1e-21, 'scientific': true},
	    'yocto': {'name': 'yocto', 'value': 1e-24, 'scientific': true}
	  },
	  'SQUARED': {
	    '': {'name': '', 'value': 1, 'scientific': true},

	    'da': {'name': 'da', 'value': 1e2, 'scientific': false},
	    'h': {'name': 'h', 'value': 1e4, 'scientific': false},
	    'k': {'name': 'k', 'value': 1e6, 'scientific': true},
	    'M': {'name': 'M', 'value': 1e12, 'scientific': true},
	    'G': {'name': 'G', 'value': 1e18, 'scientific': true},
	    'T': {'name': 'T', 'value': 1e24, 'scientific': true},
	    'P': {'name': 'P', 'value': 1e30, 'scientific': true},
	    'E': {'name': 'E', 'value': 1e36, 'scientific': true},
	    'Z': {'name': 'Z', 'value': 1e42, 'scientific': true},
	    'Y': {'name': 'Y', 'value': 1e48, 'scientific': true},

	    'd': {'name': 'd', 'value': 1e-2, 'scientific': false},
	    'c': {'name': 'c', 'value': 1e-4, 'scientific': false},
	    'm': {'name': 'm', 'value': 1e-6, 'scientific': true},
	    'u': {'name': 'u', 'value': 1e-12, 'scientific': true},
	    'n': {'name': 'n', 'value': 1e-18, 'scientific': true},
	    'p': {'name': 'p', 'value': 1e-24, 'scientific': true},
	    'f': {'name': 'f', 'value': 1e-30, 'scientific': true},
	    'a': {'name': 'a', 'value': 1e-36, 'scientific': true},
	    'z': {'name': 'z', 'value': 1e-42, 'scientific': true},
	    'y': {'name': 'y', 'value': 1e-42, 'scientific': true}
	  },
	  'CUBIC': {
	    '': {'name': '', 'value': 1, 'scientific': true},

	    'da': {'name': 'da', 'value': 1e3, 'scientific': false},
	    'h': {'name': 'h', 'value': 1e6, 'scientific': false},
	    'k': {'name': 'k', 'value': 1e9, 'scientific': true},
	    'M': {'name': 'M', 'value': 1e18, 'scientific': true},
	    'G': {'name': 'G', 'value': 1e27, 'scientific': true},
	    'T': {'name': 'T', 'value': 1e36, 'scientific': true},
	    'P': {'name': 'P', 'value': 1e45, 'scientific': true},
	    'E': {'name': 'E', 'value': 1e54, 'scientific': true},
	    'Z': {'name': 'Z', 'value': 1e63, 'scientific': true},
	    'Y': {'name': 'Y', 'value': 1e72, 'scientific': true},

	    'd': {'name': 'd', 'value': 1e-3, 'scientific': false},
	    'c': {'name': 'c', 'value': 1e-6, 'scientific': false},
	    'm': {'name': 'm', 'value': 1e-9, 'scientific': true},
	    'u': {'name': 'u', 'value': 1e-18, 'scientific': true},
	    'n': {'name': 'n', 'value': 1e-27, 'scientific': true},
	    'p': {'name': 'p', 'value': 1e-36, 'scientific': true},
	    'f': {'name': 'f', 'value': 1e-45, 'scientific': true},
	    'a': {'name': 'a', 'value': 1e-54, 'scientific': true},
	    'z': {'name': 'z', 'value': 1e-63, 'scientific': true},
	    'y': {'name': 'y', 'value': 1e-72, 'scientific': true}
	  },
	  'BINARY_SHORT': {
	    '': {'name': '', 'value': 1, 'scientific': true},
	    'k': {'name': 'k', 'value': 1024, 'scientific': true},
	    'M': {'name': 'M', 'value': Math.pow(1024, 2), 'scientific': true},
	    'G': {'name': 'G', 'value': Math.pow(1024, 3), 'scientific': true},
	    'T': {'name': 'T', 'value': Math.pow(1024, 4), 'scientific': true},
	    'P': {'name': 'P', 'value': Math.pow(1024, 5), 'scientific': true},
	    'E': {'name': 'E', 'value': Math.pow(1024, 6), 'scientific': true},
	    'Z': {'name': 'Z', 'value': Math.pow(1024, 7), 'scientific': true},
	    'Y': {'name': 'Y', 'value': Math.pow(1024, 8), 'scientific': true},

	    'Ki': {'name': 'Ki', 'value': 1024, 'scientific': true},
	    'Mi': {'name': 'Mi', 'value': Math.pow(1024, 2), 'scientific': true},
	    'Gi': {'name': 'Gi', 'value': Math.pow(1024, 3), 'scientific': true},
	    'Ti': {'name': 'Ti', 'value': Math.pow(1024, 4), 'scientific': true},
	    'Pi': {'name': 'Pi', 'value': Math.pow(1024, 5), 'scientific': true},
	    'Ei': {'name': 'Ei', 'value': Math.pow(1024, 6), 'scientific': true},
	    'Zi': {'name': 'Zi', 'value': Math.pow(1024, 7), 'scientific': true},
	    'Yi': {'name': 'Yi', 'value': Math.pow(1024, 8), 'scientific': true}
	  },
	  'BINARY_LONG': {
	    '': {'name': '', 'value': 1, 'scientific': true},
	    'kilo': {'name': 'kilo', 'value': 1024, 'scientific': true},
	    'mega': {'name': 'mega', 'value': Math.pow(1024, 2), 'scientific': true},
	    'giga': {'name': 'giga', 'value': Math.pow(1024, 3), 'scientific': true},
	    'tera': {'name': 'tera', 'value': Math.pow(1024, 4), 'scientific': true},
	    'peta': {'name': 'peta', 'value': Math.pow(1024, 5), 'scientific': true},
	    'exa': {'name': 'exa', 'value': Math.pow(1024, 6), 'scientific': true},
	    'zetta': {'name': 'zetta', 'value': Math.pow(1024, 7), 'scientific': true},
	    'yotta': {'name': 'yotta', 'value': Math.pow(1024, 8), 'scientific': true},

	    'kibi': {'name': 'kibi', 'value': 1024, 'scientific': true},
	    'mebi': {'name': 'mebi', 'value': Math.pow(1024, 2), 'scientific': true},
	    'gibi': {'name': 'gibi', 'value': Math.pow(1024, 3), 'scientific': true},
	    'tebi': {'name': 'tebi', 'value': Math.pow(1024, 4), 'scientific': true},
	    'pebi': {'name': 'pebi', 'value': Math.pow(1024, 5), 'scientific': true},
	    'exi': {'name': 'exi', 'value': Math.pow(1024, 6), 'scientific': true},
	    'zebi': {'name': 'zebi', 'value': Math.pow(1024, 7), 'scientific': true},
	    'yobi': {'name': 'yobi', 'value': Math.pow(1024, 8), 'scientific': true}
	  }
	};

	var PREFIX_NONE = {'name': '', 'value': 1, 'scientific': true};

	var BASE_UNITS = {
	  'NONE': {},

	  'LENGTH': {},                
	  'MASS': {},                  
	  'TIME': {},                  
	  'CURRENT': {},               
	  'TEMPERATURE': {},           
	  'LUMINOUS_INTENSITY': {},    
	  'AMOUNT_OF_SUBSTANCE': {},   

	  'FORCE': {},         
	  'SURFACE': {},       
	  'VOLUME': {},        
	  'ANGLE': {},         
	  'BIT': {}            
	};

	BASE_UNIT_NONE = {};

	UNIT_NONE = {'name': '', 'base': BASE_UNIT_NONE, 'value': 1, 'offset': 0};

	var UNITS = {
	   
	  meter: {'name': 'meter', 'base': BASE_UNITS.LENGTH, 'prefixes': PREFIXES.LONG, 'value': 1, 'offset': 0},
	  inch: {'name': 'inch', 'base': BASE_UNITS.LENGTH, 'prefixes': PREFIXES.NONE, 'value': 0.0254, 'offset': 0},
	  foot: {'name': 'foot', 'base': BASE_UNITS.LENGTH, 'prefixes': PREFIXES.NONE, 'value': 0.3048, 'offset': 0},
	  yard: {'name': 'yard', 'base': BASE_UNITS.LENGTH, 'prefixes': PREFIXES.NONE, 'value': 0.9144, 'offset': 0},
	  mile: {'name': 'mile', 'base': BASE_UNITS.LENGTH, 'prefixes': PREFIXES.NONE, 'value': 1609.344, 'offset': 0},
	  link: {'name': 'link', 'base': BASE_UNITS.LENGTH, 'prefixes': PREFIXES.NONE, 'value': 0.201168, 'offset': 0},
	  rod: {'name': 'rod', 'base': BASE_UNITS.LENGTH, 'prefixes': PREFIXES.NONE, 'value': 5.029210, 'offset': 0},
	  chain: {'name': 'chain', 'base': BASE_UNITS.LENGTH, 'prefixes': PREFIXES.NONE, 'value': 20.1168, 'offset': 0},
	  angstrom: {'name': 'angstrom', 'base': BASE_UNITS.LENGTH, 'prefixes': PREFIXES.NONE, 'value': 1e-10, 'offset': 0},

	  m: {'name': 'm', 'base': BASE_UNITS.LENGTH, 'prefixes': PREFIXES.SHORT, 'value': 1, 'offset': 0},
	  'in': {'name': 'in', 'base': BASE_UNITS.LENGTH, 'prefixes': PREFIXES.NONE, 'value': 0.0254, 'offset': 0},
	  ft: {'name': 'ft', 'base': BASE_UNITS.LENGTH, 'prefixes': PREFIXES.NONE, 'value': 0.3048, 'offset': 0},
	  yd: {'name': 'yd', 'base': BASE_UNITS.LENGTH, 'prefixes': PREFIXES.NONE, 'value': 0.9144, 'offset': 0},
	  mi: {'name': 'mi', 'base': BASE_UNITS.LENGTH, 'prefixes': PREFIXES.NONE, 'value': 1609.344, 'offset': 0},
	  li: {'name': 'li', 'base': BASE_UNITS.LENGTH, 'prefixes': PREFIXES.NONE, 'value': 0.201168, 'offset': 0},
	  rd: {'name': 'rd', 'base': BASE_UNITS.LENGTH, 'prefixes': PREFIXES.NONE, 'value': 5.029210, 'offset': 0},
	  ch: {'name': 'ch', 'base': BASE_UNITS.LENGTH, 'prefixes': PREFIXES.NONE, 'value': 20.1168, 'offset': 0},
	  mil: {'name': 'mil', 'base': BASE_UNITS.LENGTH, 'prefixes': PREFIXES.NONE, 'value': 0.0000254, 'offset': 0},  

	   
	  m2: {'name': 'm2', 'base': BASE_UNITS.SURFACE, 'prefixes': PREFIXES.SQUARED, 'value': 1, 'offset': 0},
	  sqin: {'name': 'sqin', 'base': BASE_UNITS.SURFACE, 'prefixes': PREFIXES.NONE, 'value': 0.00064516, 'offset': 0},  
	  sqft: {'name': 'sqft', 'base': BASE_UNITS.SURFACE, 'prefixes': PREFIXES.NONE, 'value': 0.09290304, 'offset': 0},  
	  sqyd: {'name': 'sqyd', 'base': BASE_UNITS.SURFACE, 'prefixes': PREFIXES.NONE, 'value': 0.83612736, 'offset': 0},  
	  sqmi: {'name': 'sqmi', 'base': BASE_UNITS.SURFACE, 'prefixes': PREFIXES.NONE, 'value': 2589988.110336, 'offset': 0},  
	  sqrd: {'name': 'sqrd', 'base': BASE_UNITS.SURFACE, 'prefixes': PREFIXES.NONE, 'value': 25.29295, 'offset': 0},  
	  sqch: {'name': 'sqch', 'base': BASE_UNITS.SURFACE, 'prefixes': PREFIXES.NONE, 'value': 404.6873, 'offset': 0},  
	  sqmil: {'name': 'sqmil', 'base': BASE_UNITS.SURFACE, 'prefixes': PREFIXES.NONE, 'value': 6.4516e-10, 'offset': 0},  

	   
	  m3: {'name': 'm3', 'base': BASE_UNITS.VOLUME, 'prefixes': PREFIXES.CUBIC, 'value': 1, 'offset': 0},
	  L: {'name': 'L', 'base': BASE_UNITS.VOLUME, 'prefixes': PREFIXES.SHORT, 'value': 0.001, 'offset': 0},  
	  l: {'name': 'l', 'base': BASE_UNITS.VOLUME, 'prefixes': PREFIXES.SHORT, 'value': 0.001, 'offset': 0},  
	  litre: {'name': 'litre', 'base': BASE_UNITS.VOLUME, 'prefixes': PREFIXES.LONG, 'value': 0.001, 'offset': 0},
	  cuin: {'name': 'cuin', 'base': BASE_UNITS.VOLUME, 'prefixes': PREFIXES.NONE, 'value': 1.6387064e-5, 'offset': 0},  
	  cuft: {'name': 'cuft', 'base': BASE_UNITS.VOLUME, 'prefixes': PREFIXES.NONE, 'value': 0.028316846592, 'offset': 0},  
	  cuyd: {'name': 'cuyd', 'base': BASE_UNITS.VOLUME, 'prefixes': PREFIXES.NONE, 'value': 0.764554857984, 'offset': 0},  
	  teaspoon: {'name': 'teaspoon', 'base': BASE_UNITS.VOLUME, 'prefixes': PREFIXES.NONE, 'value': 0.000005, 'offset': 0},  
	  tablespoon: {'name': 'tablespoon', 'base': BASE_UNITS.VOLUME, 'prefixes': PREFIXES.NONE, 'value': 0.000015, 'offset': 0},  
	   

	   
	  minim: {'name': 'minim', 'base': BASE_UNITS.VOLUME, 'prefixes': PREFIXES.NONE, 'value': 0.00000006161152, 'offset': 0},  
	  fluiddram: {'name': 'fluiddram', 'base': BASE_UNITS.VOLUME, 'prefixes': PREFIXES.NONE, 'value': 0.0000036966911, 'offset': 0},   
	  fluidounce: {'name': 'fluidounce', 'base': BASE_UNITS.VOLUME, 'prefixes': PREFIXES.NONE, 'value': 0.00002957353, 'offset': 0},  
	  gill: {'name': 'gill', 'base': BASE_UNITS.VOLUME, 'prefixes': PREFIXES.NONE, 'value': 0.0001182941, 'offset': 0},  
	  cc: {'name': 'cc', 'base': BASE_UNITS.VOLUME, 'prefixes': PREFIXES.NONE, 'value': 1e-6, 'offset': 0},  
	  cup: {'name': 'cup', 'base': BASE_UNITS.VOLUME, 'prefixes': PREFIXES.NONE, 'value': 0.0002365882, 'offset': 0},  
	  pint: {'name': 'pint', 'base': BASE_UNITS.VOLUME, 'prefixes': PREFIXES.NONE, 'value': 0.0004731765, 'offset': 0},  
	  quart: {'name': 'quart', 'base': BASE_UNITS.VOLUME, 'prefixes': PREFIXES.NONE, 'value': 0.0009463529, 'offset': 0},  
	  gallon: {'name': 'gallon', 'base': BASE_UNITS.VOLUME, 'prefixes': PREFIXES.NONE, 'value': 0.003785412, 'offset': 0},  
	  beerbarrel: {'name': 'beerbarrel', 'base': BASE_UNITS.VOLUME, 'prefixes': PREFIXES.NONE, 'value': 0.1173478, 'offset': 0},  
	  oilbarrel: {'name': 'oilbarrel', 'base': BASE_UNITS.VOLUME, 'prefixes': PREFIXES.NONE, 'value': 0.1589873, 'offset': 0},  
	  hogshead: {'name': 'hogshead', 'base': BASE_UNITS.VOLUME, 'prefixes': PREFIXES.NONE, 'value': 0.2384810, 'offset': 0},  

	   
	  fldr: {'name': 'fldr', 'base': BASE_UNITS.VOLUME, 'prefixes': PREFIXES.NONE, 'value': 0.0000036966911, 'offset': 0},   
	  floz: {'name': 'floz', 'base': BASE_UNITS.VOLUME, 'prefixes': PREFIXES.NONE, 'value': 0.00002957353, 'offset': 0},  
	  gi: {'name': 'gi', 'base': BASE_UNITS.VOLUME, 'prefixes': PREFIXES.NONE, 'value': 0.0001182941, 'offset': 0},  
	  cp: {'name': 'cp', 'base': BASE_UNITS.VOLUME, 'prefixes': PREFIXES.NONE, 'value': 0.0002365882, 'offset': 0},  
	  pt: {'name': 'pt', 'base': BASE_UNITS.VOLUME, 'prefixes': PREFIXES.NONE, 'value': 0.0004731765, 'offset': 0},  
	  qt: {'name': 'qt', 'base': BASE_UNITS.VOLUME, 'prefixes': PREFIXES.NONE, 'value': 0.0009463529, 'offset': 0},  
	  gal: {'name': 'gal', 'base': BASE_UNITS.VOLUME, 'prefixes': PREFIXES.NONE, 'value': 0.003785412, 'offset': 0},  
	  bbl: {'name': 'bbl', 'base': BASE_UNITS.VOLUME, 'prefixes': PREFIXES.NONE, 'value': 0.1173478, 'offset': 0},  
	  obl: {'name': 'obl', 'base': BASE_UNITS.VOLUME, 'prefixes': PREFIXES.NONE, 'value': 0.1589873, 'offset': 0},  
	   

	   
	  g: {'name': 'g', 'base': BASE_UNITS.MASS, 'prefixes': PREFIXES.SHORT, 'value': 0.001, 'offset': 0},
	  gram: {'name': 'gram', 'base': BASE_UNITS.MASS, 'prefixes': PREFIXES.LONG, 'value': 0.001, 'offset': 0},

	  ton: {'name': 'ton', 'base': BASE_UNITS.MASS, 'prefixes': PREFIXES.SHORT, 'value': 907.18474, 'offset': 0},
	  tonne: {'name': 'tonne', 'base': BASE_UNITS.MASS, 'prefixes': PREFIXES.SHORT, 'value': 1000, 'offset': 0},

	  grain: {'name': 'grain', 'base': BASE_UNITS.MASS, 'prefixes': PREFIXES.NONE, 'value': 64.79891e-6, 'offset': 0},
	  dram: {'name': 'dram', 'base': BASE_UNITS.MASS, 'prefixes': PREFIXES.NONE, 'value': 1.7718451953125e-3, 'offset': 0},
	  ounce: {'name': 'ounce', 'base': BASE_UNITS.MASS, 'prefixes': PREFIXES.NONE, 'value': 28.349523125e-3, 'offset': 0},
	  poundmass: {'name': 'poundmass', 'base': BASE_UNITS.MASS, 'prefixes': PREFIXES.NONE, 'value': 453.59237e-3, 'offset': 0},
	  hundredweight: {'name': 'hundredweight', 'base': BASE_UNITS.MASS, 'prefixes': PREFIXES.NONE, 'value': 45.359237, 'offset': 0},
	  stick: {'name': 'stick', 'base': BASE_UNITS.MASS, 'prefixes': PREFIXES.NONE, 'value': 115e-3, 'offset': 0},

	  gr: {'name': 'gr', 'base': BASE_UNITS.MASS, 'prefixes': PREFIXES.NONE, 'value': 64.79891e-6, 'offset': 0},
	  dr: {'name': 'dr', 'base': BASE_UNITS.MASS, 'prefixes': PREFIXES.NONE, 'value': 1.7718451953125e-3, 'offset': 0},
	  oz: {'name': 'oz', 'base': BASE_UNITS.MASS, 'prefixes': PREFIXES.NONE, 'value': 28.349523125e-3, 'offset': 0},
	  lbm: {'name': 'lbm', 'base': BASE_UNITS.MASS, 'prefixes': PREFIXES.NONE, 'value': 453.59237e-3, 'offset': 0},
	  cwt: {'name': 'cwt', 'base': BASE_UNITS.MASS, 'prefixes': PREFIXES.NONE, 'value': 45.359237, 'offset': 0},

	   
	  s: {'name': 's', 'base': BASE_UNITS.TIME, 'prefixes': PREFIXES.SHORT, 'value': 1, 'offset': 0},
	  min: {'name': 'min', 'base': BASE_UNITS.TIME, 'prefixes': PREFIXES.NONE, 'value': 60, 'offset': 0},
	  h: {'name': 'h', 'base': BASE_UNITS.TIME, 'prefixes': PREFIXES.NONE, 'value': 3600, 'offset': 0},
	  second: {'name': 'second', 'base': BASE_UNITS.TIME, 'prefixes': PREFIXES.LONG, 'value': 1, 'offset': 0},
	  sec: {'name': 'sec', 'base': BASE_UNITS.TIME, 'prefixes': PREFIXES.LONG, 'value': 1, 'offset': 0},
	  minute: {'name': 'minute', 'base': BASE_UNITS.TIME, 'prefixes': PREFIXES.NONE, 'value': 60, 'offset': 0},
	  hour: {'name': 'hour', 'base': BASE_UNITS.TIME, 'prefixes': PREFIXES.NONE, 'value': 3600, 'offset': 0},
	  day: {'name': 'day', 'base': BASE_UNITS.TIME, 'prefixes': PREFIXES.NONE, 'value': 86400, 'offset': 0},

	   
	  rad: {'name': 'rad', 'base': BASE_UNITS.ANGLE, 'prefixes': PREFIXES.NONE, 'value': 1, 'offset': 0},
	   
	  deg: {'name': 'deg', 'base': BASE_UNITS.ANGLE, 'prefixes': PREFIXES.NONE, 'value': 0.017453292519943295769236907684888, 'offset': 0},
	   
	  grad: {'name': 'grad', 'base': BASE_UNITS.ANGLE, 'prefixes': PREFIXES.NONE, 'value': 0.015707963267948966192313216916399, 'offset': 0},
	   
	  cycle: {'name': 'cycle', 'base': BASE_UNITS.ANGLE, 'prefixes': PREFIXES.NONE, 'value': 6.2831853071795864769252867665793, 'offset': 0},

	   
	  A: {'name': 'A', 'base': BASE_UNITS.CURRENT, 'prefixes': PREFIXES.SHORT, 'value': 1, 'offset': 0},
	  ampere: {'name': 'ampere', 'base': BASE_UNITS.CURRENT, 'prefixes': PREFIXES.LONG, 'value': 1, 'offset': 0},

	   
	   
	   
	   
	  K: {'name': 'K', 'base': BASE_UNITS.TEMPERATURE, 'prefixes': PREFIXES.NONE, 'value': 1, 'offset': 0},
	  degC: {'name': 'degC', 'base': BASE_UNITS.TEMPERATURE, 'prefixes': PREFIXES.NONE, 'value': 1, 'offset': 273.15},
	  degF: {'name': 'degF', 'base': BASE_UNITS.TEMPERATURE, 'prefixes': PREFIXES.NONE, 'value': 1/1.8, 'offset': 459.67},
	  degR: {'name': 'degR', 'base': BASE_UNITS.TEMPERATURE, 'prefixes': PREFIXES.NONE, 'value': 1/1.8, 'offset': 0},
	  kelvin: {'name': 'kelvin', 'base': BASE_UNITS.TEMPERATURE, 'prefixes': PREFIXES.NONE, 'value': 1, 'offset': 0},
	  celsius: {'name': 'celsius', 'base': BASE_UNITS.TEMPERATURE, 'prefixes': PREFIXES.NONE, 'value': 1, 'offset': 273.15},
	  fahrenheit: {'name': 'fahrenheit', 'base': BASE_UNITS.TEMPERATURE, 'prefixes': PREFIXES.NONE, 'value': 1/1.8, 'offset': 459.67},
	  rankine: {'name': 'rankine', 'base': BASE_UNITS.TEMPERATURE, 'prefixes': PREFIXES.NONE, 'value': 1/1.8, 'offset': 0},

	   
	  mol: {'name': 'mol', 'base': BASE_UNITS.AMOUNT_OF_SUBSTANCE, 'prefixes': PREFIXES.NONE, 'value': 1, 'offset': 0},
	  mole: {'name': 'mole', 'base': BASE_UNITS.AMOUNT_OF_SUBSTANCE, 'prefixes': PREFIXES.NONE, 'value': 1, 'offset': 0},

	   
	  cd: {'name': 'cd', 'base': BASE_UNITS.LUMINOUS_INTENSITY, 'prefixes': PREFIXES.NONE, 'value': 1, 'offset': 0},
	  candela: {'name': 'candela', 'base': BASE_UNITS.LUMINOUS_INTENSITY, 'prefixes': PREFIXES.NONE, 'value': 1, 'offset': 0},
	   
	   
	   

	   
	  N: {'name': 'N', 'base': BASE_UNITS.FORCE, 'prefixes': PREFIXES.SHORT, 'value': 1, 'offset': 0},
	  newton: {'name': 'newton', 'base': BASE_UNITS.FORCE, 'prefixes': PREFIXES.LONG, 'value': 1, 'offset': 0},
	  lbf: {'name': 'lbf', 'base': BASE_UNITS.FORCE, 'prefixes': PREFIXES.NONE, 'value': 4.4482216152605, 'offset': 0},
	  poundforce: {'name': 'poundforce', 'base': BASE_UNITS.FORCE, 'prefixes': PREFIXES.NONE, 'value': 4.4482216152605, 'offset': 0},

	   
	  b: {'name': 'b', 'base': BASE_UNITS.BIT, 'prefixes': PREFIXES.BINARY_SHORT, 'value': 1, 'offset': 0},
	  bits: {'name': 'bits', 'base': BASE_UNITS.BIT, 'prefixes': PREFIXES.BINARY_LONG, 'value': 1, 'offset': 0},
	  B: {'name': 'B', 'base': BASE_UNITS.BIT, 'prefixes': PREFIXES.BINARY_SHORT, 'value': 8, 'offset': 0},
	  bytes: {'name': 'bytes', 'base': BASE_UNITS.BIT, 'prefixes': PREFIXES.BINARY_LONG, 'value': 8, 'offset': 0}
	};

	 
	var PLURALS = {
	  meters: 'meter',
	  inches: 'inch',
	  feet: 'foot',
	  yards: 'yard',
	  miles: 'mile',
	  links: 'link',
	  rods: 'rod',
	  chains: 'chain',
	  angstroms: 'angstrom',

	  litres: 'litre',
	  teaspoons: 'teaspoon',
	  tablespoons: 'tablespoon',
	  minims: 'minim',
	  fluiddrams: 'fluiddram',
	  fluidounces: 'fluidounce',
	  gills: 'gill',
	  cups: 'cup',
	  pints: 'pint',
	  quarts: 'quart',
	  gallons: 'gallon',
	  beerbarrels: 'beerbarrel',
	  oilbarrels: 'oilbarrel',
	  hogsheads: 'hogshead',

	  grams: 'gram',
	  tons: 'ton',
	  tonnes: 'tonne',
	  grains: 'grain',
	  drams: 'dram',
	  ounces: 'ounce',
	  poundmasses: 'poundmass',
	  hundredweights: 'hundredweight',
	  sticks: 'stick',

	  seconds: 'second',
	  minutes: 'minute',
	  hours: 'hour',
	  days: 'day',

	  radians: 'rad',
	  degrees: 'deg',
	  gradients: 'grad',
	  cycles: 'cycle',

	  amperes: 'ampere',
	  moles: 'mole'
	};

	for (var name in PLURALS) {
	  if (PLURALS.hasOwnProperty(name)) {
	    var unit = UNITS[PLURALS[name]];
	    var plural = Object.create(unit);
	    plural.name = name;
	    UNITS[name] = plural;
	  }
	}

	 
	UNITS.lt = UNITS.l;
	UNITS.liter = UNITS.litre;
	UNITS.liters = UNITS.litres;
	UNITS.lb = UNITS.lbm;


	Unit.PREFIXES = PREFIXES;
	Unit.BASE_UNITS = BASE_UNITS;
	Unit.UNITS = UNITS;

	 


	 
	module.exports = Unit;

	 
	exports.isUnit = Unit.isUnit;
	exports.isPlainUnit = Unit.isPlainUnit;
	exports.parse = Unit.parse;


  },
 
  function(module, exports, require) {

	var util = require(117),
	    object = util.object,
	    string = util.string;

	 
	function Help (math, doc) {
	  this.math = math;
	  this.doc = doc;
	}

	 
	Help.isHelp = function isHelp (value) {
	  return (value instanceof Help);
	};

	 
	Help.prototype.toString = function () {
	  var doc = this.doc || {};
	  var desc = '\n';

	  if (doc.name) {
	    desc += 'Name: ' + doc.name + '\n\n';
	  }
	  if (doc.category) {
	    desc += 'Category: ' + doc.category + '\n\n';
	  }
	  if (doc.description) {
	    desc += 'Description:\n    ' + doc.description + '\n\n';
	  }
	  if (doc.syntax) {
	    desc += 'Syntax:\n    ' + doc.syntax.join('\n    ') + '\n\n';
	  }
	  if (doc.examples) {
	    var parser = this.math.parser();
	    desc += 'Examples:\n';
	    for (var i = 0; i < doc.examples.length; i++) {
	      var expr = doc.examples[i];
	      var res;
	      try {
	        res = parser.eval(expr);
	      }
	      catch (e) {
	        res = e;
	      }
	      desc += '    ' + expr + '\n';
	      if (res && !(res instanceof Help)) {
	        desc += '        ' + string.format(res) + '\n';
	      }
	    }
	    desc += '\n';
	  }
	  if (doc.seealso) {
	    desc += 'See also: ' + doc.seealso.join(', ') + '\n';
	  }

	  return desc;
	};

	 

	 
	Help.prototype.toJSON = function () {
	  return object.extend({}, this.doc);
	};

	 
	module.exports = Help;

	 
	exports.isHelp = Help.isHelp;


  },
 
  function(module, exports, require) {

	 

	var util = require(117),

	    Matrix = require(10),

	    isArray = util.array.isArray,
	    isString = util.string.isString;

	 
	exports.argsToArray = function argsToArray(args) {
	  var array;
	  if (args.length == 0) {
	     
	    array = [];
	  }
	  else if (args.length == 1) {
	     
	     
	    array = args[0];
	    if (array instanceof Matrix) {
	      array = array.valueOf();
	    }
	    if (!isArray(array)) {
	      array = [array];
	    }
	  }
	  else {
	     
	    array = Array.prototype.slice.apply(args);
	  }
	  return array;
	};


	 
	exports.isCollection = function isCollection (x) {
	  return (isArray(x) || (x instanceof Matrix));
	};

	 
	exports.deepMap = function deepMap(array, callback) {
	  if (array && (typeof array.map === 'function')) {
	    return array.map(function (x) {
	      return deepMap(x, callback);
	    });
	  }
	  else {
	    return callback(array);
	  }
	};

	 
	exports.deepMap2 = function deepMap2(array1, array2, callback) {
	  var res, len, i;

	  if (isArray(array1)) {
	    if (isArray(array2)) {
	       
	      if (array1.length != array2.length) {
	        throw new RangeError('Dimension mismatch ' +
	            '(' +  array1.length + ' != ' + array2.length + ')');
	      }

	      res = [];
	      len = array1.length;
	      for (i = 0; i < len; i++) {
	        res[i] = deepMap2(array1[i], array2[i], callback);
	      }
	    }
	    else if (array2 instanceof Matrix) {
	       
	      res = deepMap2(array1, array2.valueOf(), callback);
	      return new Matrix(res);
	    }
	    else {
	       
	      res = [];
	      len = array1.length;
	      for (i = 0; i < len; i++) {
	        res[i] = deepMap2(array1[i], array2, callback);
	      }
	    }
	  }
	  else if (array1 instanceof Matrix) {
	    if (array2 instanceof Matrix) {
	       
	      res = deepMap2(array1.valueOf(), array2.valueOf(), callback);
	      return new Matrix(res);
	    }
	    else {
	       
	       
	      res = deepMap2(array1.valueOf(), array2, callback);
	      return new Matrix(res);
	    }
	  }
	  else {
	    if (isArray(array2)) {
	       
	      res = [];
	      len = array2.length;
	      for (i = 0; i < len; i++) {
	        res[i] = deepMap2(array1, array2[i], callback);
	      }
	    }
	    else if (array2 instanceof Matrix) {
	       
	      res = deepMap2(array1, array2.valueOf(), callback);
	      return new Matrix(res);
	    }
	    else {
	       
	      res = callback(array1, array2);
	    }
	  }

	  return res;
	};

	 
	exports.reduce = function reduce (mat, dim, callback) {
		if (mat instanceof Matrix) {
			return new Matrix(_reduce(mat.valueOf(), dim, callback));
		}else {
			return _reduce(mat, dim, callback);
		}
	};

	 
	function _reduce(mat, dim, callback){
	  var i, ret, val, tran;

		if(dim<=0){
			if( !isArray(mat[0]) ){
				val = mat[0];
				for(i=1; i<mat.length; i++){
					val = callback(val, mat[i]);
				}
				return val;
			}else{
				tran = _switch(mat);
				ret = [];
				for(i=0; i<tran.length; i++){
					ret[i] = _reduce(tran[i], dim-1, callback);
				}
				return ret
			}
		}else{
			ret = [];
			for(i=0; i<mat.length; i++){
				ret[i] = _reduce(mat[i], dim-1, callback);
			}
			return ret;
		}
	}

	 
	function _switch(mat){
	  var I = mat.length;
	  var J = mat[0].length;
	  var i, j;
	  var ret = [];
	  for( j=0; j<J; j++) {
	    var tmp = [];
	    for( i=0; i<I; i++) {
	      tmp.push(mat[i][j]);
	    }
	    ret.push(tmp);
	  }
	  return ret;
	}

	 
	exports.deepForEach = function deepForEach (array, callback) {
	  if (array instanceof Matrix) {
	    array = array.valueOf();
	  }

	  for (var i = 0, ii = array.length; i < ii; i++) {
	    var value = array[i];

	    if (isArray(value)) {
	      deepForEach(value, callback);
	    }
	    else {
	      callback(value);
	    }
	  }
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var types = require(217);

	   
	  var error = {};
	  math.error = error;

	   
	   
	  error.UnsupportedTypeError = function UnsupportedTypeError(name, value1, value_n) {
	    if (arguments.length == 2) {
	      var type1 = math['typeof'](value1);
	      this.message = 'Function ' + name + '(' + type1 + ') not supported';
	    }
	    else if (arguments.length > 2) {
	      var values = Array.prototype.splice.call(arguments, 1);
	      var types = values.map(function (value) {
	        return math['typeof'](value);
	      });
	      this.message = 'Function ' + name + '(' + types.join(', ') + ') not supported';
	    }
	    else {
	      this.message = 'Unsupported type of argument in function ' + name;
	    }

	    this.stack = (new Error()).stack;
	  };

	  error.UnsupportedTypeError.prototype = new TypeError();
	  error.UnsupportedTypeError.prototype.constructor = TypeError;
	  error.UnsupportedTypeError.prototype.name = 'UnsupportedTypeError';

	   
	  error.ArgumentsError = function ArgumentsError(name, count, min, max) {
	    this.message = 'Wrong number of arguments in function ' + name +
	        ' (' + count + ' provided, ' +
	        min + ((max != undefined) ? ('-' + max) : '') + ' expected)';

	    this.stack = (new Error()).stack;
	  };

	  error.ArgumentsError.prototype = new Error();
	  error.ArgumentsError.prototype.constructor = Error;
	  error.ArgumentsError.prototype.name = 'ArgumentError';

	   

	};

  },
 
  function(module, exports, require) {

	module.exports = function (math, settings) {
	  var util = require(117),
	      _parse = require(4),

	      collection = require(13),

	      isString = util.string.isString,
	      isCollection = collection.isCollection;

	   
	  math.compile = function compile (expr) {
	    if (arguments.length != 1) {
	      throw new math.error.ArgumentsError('compile', arguments.length, 1);
	    }

	    if (isString(expr)) {
	       
	      return _parse(expr).compile(math);
	    }
	    else if (isCollection(expr)) {
	       
	      return collection.deepMap(expr, function (elem) {
	        return _parse(elem).compile(math);
	      });
	    }
	    else {
	       
	      throw new TypeError('String, array, or matrix expected');
	    }
	  }
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var util = require(117),
	      _parse = require(4),

	      collection = require(13),

	      isString = util.string.isString,
	      isCollection = collection.isCollection;

	   
	  math.eval = function _eval (expr, scope) {
	    if (arguments.length != 1 && arguments.length != 2) {
	      throw new math.error.ArgumentsError('eval', arguments.length, 1, 2);
	    }

	     
	    scope = scope || {};

	    if (isString(expr)) {
	       
	      return _parse(expr)
	          .compile(math)
	          .eval(scope);
	    }
	    else if (isCollection(expr)) {
	       
	      return collection.deepMap(expr, function (elem) {
	        return _parse(elem)
	            .compile(math).eval(scope);
	      });
	    }
	    else {
	       
	      throw new TypeError('String, array, or matrix expected');
	    }
	  };
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var Help = require(12);

	   
	  math.help = function help(search) {
	    if (arguments.length != 1) {
	      throw new SyntaxError('Wrong number of arguments in function help ' +
	          '(' + arguments.length + ' provided, 1 expected)');
	    }

	    var text = null;
	    if ((search instanceof String) || (typeof(search) === 'string')) {
	      text = search;
	    }
	    else {
	      var prop;
	      for (prop in math) {
	         
	        if (math.hasOwnProperty(prop)) {
	          if (search === math[prop]) {
	            text = prop;
	            break;
	          }
	        }
	      }

	      if (!text) {
	         
	        for (prop in math.type) {
	          if (math.type.hasOwnProperty(prop)) {
	            if (search === math.type[prop]) {
	              text = prop;
	              break;
	            }
	          }
	        }
	      }
	    }

	    if (!text) {
	      throw new Error('Could not find search term "' + search + '"');
	    }
	    else {
	      var doc = math.expression.docs[text];
	      if (!doc) {
	        throw new Error('No documentation found on "' + text + '"');
	      }
	      return new Help(math, doc);
	    }
	  };
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math, settings) {
	  var _parse = require(4);

	   
	  math.parse = function parse (expr, nodes) {
	    return _parse.apply(_parse, arguments);
	  }

	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var util = require(117),

	      BigNumber = require(220),
	      Complex = require(7),
	      Matrix = require(10),
	      collection = require(13),

	      isNumber = util.number.isNumber,
	      isBoolean = util['boolean'].isBoolean,
	      isComplex = Complex.isComplex,
	      isCollection = collection.isCollection;

	   
	  math.abs = function abs(x) {
	    if (arguments.length != 1) {
	      throw new math.error.ArgumentsError('abs', arguments.length, 1);
	    }

	    if (isNumber(x)) {
	      return Math.abs(x);
	    }

	    if (isComplex(x)) {
	      return Math.sqrt(x.re * x.re + x.im * x.im);
	    }

	    if (x instanceof BigNumber) {
	      return x.abs();
	    }

	    if (isCollection(x)) {
	      return collection.deepMap(x, abs);
	    }

	    if (isBoolean(x)) {
	      return Math.abs(x);
	    }

	    throw new math.error.UnsupportedTypeError('abs', x);
	  };
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var util = require(117),

	      BigNumber = require(220),
	      Complex = require(7),
	      Matrix = require(10),
	      Unit = require(11),
	      collection = require(13),

	      isBoolean = util['boolean'].isBoolean,
	      isNumber = util.number.isNumber,
	      toNumber = util.number.toNumber,
	      toBigNumber = util.number.toBigNumber,
	      isString = util.string.isString,
	      isComplex = Complex.isComplex,
	      isUnit = Unit.isUnit,
	      isCollection = collection.isCollection;

	   
	  math.add = function add(x, y) {
	    if (arguments.length != 2) {
	      throw new math.error.ArgumentsError('add', arguments.length, 2);
	    }

	    if (isNumber(x)) {
	      if (isNumber(y)) {
	         
	        return x + y;
	      }
	      else if (isComplex(y)) {
	         
	        return new Complex(
	            x + y.re,
	            y.im
	        )
	      }
	    }

	    if (isComplex(x)) {
	      if (isComplex(y)) {
	         
	        return new Complex(
	            x.re + y.re,
	            x.im + y.im
	        );
	      }
	      else if (isNumber(y)) {
	         
	        return new Complex(
	            x.re + y,
	            x.im
	        )
	      }
	    }

	    if (isUnit(x)) {
	      if (isUnit(y)) {
	        if (!x.equalBase(y)) {
	          throw new Error('Units do not match');
	        }

	        if (x.value == null) {
	          throw new Error('Unit on left hand side of operator + has an undefined value');
	        }

	        if (y.value == null) {
	          throw new Error('Unit on right hand side of operator + has an undefined value');
	        }

	        var res = x.clone();
	        res.value += y.value;
	        res.fixPrefix = false;
	        return res;
	      }
	    }

	    if (x instanceof BigNumber) {
	       
	      if (isNumber(y)) {
	        y = toBigNumber(y);
	      }
	      else if (isBoolean(y)) {
	        y = new BigNumber(y ? 1 : 0);
	      }

	      if (y instanceof BigNumber) {
	        return x.plus(y);
	      }

	       
	      return add(toNumber(x), y);
	    }
	    if (y instanceof BigNumber) {
	       
	      if (isNumber(x)) {
	        x = toBigNumber(x);
	      }
	      else if (isBoolean(x)) {
	        x = new BigNumber(x ? 1 : 0);
	      }

	      if (x instanceof BigNumber) {
	        return x.plus(y)
	      }

	       
	      return add(x, toNumber(y));
	    }

	    if (isString(x) || isString(y)) {
	      return x + y;
	    }

	    if (isCollection(x) || isCollection(y)) {
	      return collection.deepMap2(x, y, add);
	    }

	    if (isBoolean(x)) {
	      return add(+x, y);
	    }
	    if (isBoolean(y)) {
	      return add(x, +y);
	    }

	    throw new math.error.UnsupportedTypeError('add', x, y);
	  };
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var util = require(117),

	      BigNumber = require(220),
	      Complex = require(7),
	      collection = require(13),

	      isNumber = util.number.isNumber,
	      isBoolean = util['boolean'].isBoolean,
	      isCollection =collection.isCollection,
	      isComplex = Complex.isComplex;

	   
	  math.ceil = function ceil(x) {
	    if (arguments.length != 1) {
	      throw new math.error.ArgumentsError('ceil', arguments.length, 1);
	    }

	    if (isNumber(x)) {
	      return Math.ceil(x);
	    }

	    if (isComplex(x)) {
	      return new Complex (
	          Math.ceil(x.re),
	          Math.ceil(x.im)
	      );
	    }

	    if (x instanceof BigNumber) {
	      return x.ceil();
	    }

	    if (isCollection(x)) {
	      return collection.deepMap(x, ceil);
	    }

	    if (isBoolean(x)) {
	      return Math.ceil(x);
	    }

	    throw new math.error.UnsupportedTypeError('ceil', x);
	  };
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var util = require(117),

	      BigNumber = require(220),
	      Complex = require(7),
	      collection = require(13),

	      isNumber = util.number.isNumber,
	      isBoolean = util['boolean'].isBoolean,
	      isComplex = Complex.isComplex,
	      isCollection = collection.isCollection;

	   
	  math.cube = function cube(x) {
	    if (arguments.length != 1) {
	      throw new math.error.ArgumentsError('cube', arguments.length, 1);
	    }

	    if (isNumber(x)) {
	      return x * x * x;
	    }

	    if (isComplex(x)) {
	      return math.multiply(math.multiply(x, x), x);
	    }

	    if (x instanceof BigNumber) {
	      return x.times(x).times(x);
	    }

	    if (isCollection(x)) {
	      return collection.deepMap(x, cube);
	    }

	    if (isBoolean(x)) {
	      return cube(+x);
	    }

	    throw new math.error.UnsupportedTypeError('cube', x);
	  };
	};


  },
 
  function(module, exports, require) {

	module.exports = function(math) {
	  var util = require(117),

	      BigNumber = require(220),
	      Complex = require(7),
	      Matrix = require(10),
	      Unit = require(11),
	      collection = require(13),

	      isNumber = util.number.isNumber,
	      toNumber = util.number.toNumber,
	      toBigNumber = util.number.toBigNumber,
	      isBoolean = util['boolean'].isBoolean,
	      isComplex = Complex.isComplex,
	      isUnit = Unit.isUnit,
	      isCollection = collection.isCollection;

	   
	  math.divide = function divide(x, y) {
	    if (arguments.length != 2) {
	      throw new math.error.ArgumentsError('divide', arguments.length, 2);
	    }

	    if (isNumber(x)) {
	      if (isNumber(y)) {
	         
	        return x / y;
	      }
	      else if (isComplex(y)) {
	         
	        return _divideComplex(new Complex(x, 0), y);
	      }
	    }

	    if (isComplex(x)) {
	      if (isComplex(y)) {
	         
	        return _divideComplex(x, y);
	      }
	      else if (isNumber(y)) {
	         
	        return _divideComplex(x, new Complex(y, 0));
	      }
	    }

	    if (x instanceof BigNumber) {
	       
	      if (isNumber(y)) {
	        y = toBigNumber(y);
	      }
	      else if (isBoolean(y)) {
	        y = new BigNumber(y ? 1 : 0);
	      }

	      if (y instanceof BigNumber) {
	        return x.div(y);
	      }

	       
	      return divide(toNumber(x), y);
	    }
	    if (y instanceof BigNumber) {
	       
	      if (isNumber(x)) {
	        x = toBigNumber(x);
	      }
	      else if (isBoolean(x)) {
	        x = new BigNumber(x ? 1 : 0);
	      }

	      if (x instanceof BigNumber) {
	        return x.div(y)
	      }

	       
	      return divide(x, toNumber(y));
	    }

	    if (isUnit(x)) {
	      if (isNumber(y)) {
	        var res = x.clone();
	        res.value /= y;
	        return res;
	      }
	    }

	    if (isCollection(x)) {
	      if (isCollection(y)) {
	         
	         
	         
	         
	        return math.multiply(x, math.inv(y));
	      }
	      else {
	         
	        return collection.deepMap2(x, y, divide);
	      }
	    }

	    if (isCollection(y)) {
	       
	      return math.multiply(x, math.inv(y));
	    }

	    if (isBoolean(x)) {
	      return divide(+x, y);
	    }
	    if (isBoolean(y)) {
	      return divide(x, +y);
	    }

	    throw new math.error.UnsupportedTypeError('divide', x, y);
	  };

	   
	  function _divideComplex (x, y) {
	    var den = y.re * y.re + y.im * y.im;
	    if (den != 0) {
	      return new Complex(
	          (x.re * y.re + x.im * y.im) / den,
	          (x.im * y.re - x.re * y.im) / den
	      );
	    }
	    else {
	       
	      return new Complex(
	          (x.re != 0) ? (x.re / 0) : 0,
	          (x.im != 0) ? (x.im / 0) : 0
	      );
	    }
	  }
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var collection = require(13);

	   
	  math.edivide = function edivide(x, y) {
	    if (arguments.length != 2) {
	      throw new math.error.ArgumentsError('edivide', arguments.length, 2);
	    }

	    return collection.deepMap2(x, y, math.divide);
	  };
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var collection = require(13);

	   
	  math.emultiply = function emultiply(x, y) {
	    if (arguments.length != 2) {
	      throw new math.error.ArgumentsError('emultiply', arguments.length, 2);
	    }

	    return collection.deepMap2(x, y, math.multiply);
	  };
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var collection = require(13);

	   
	  math.epow = function epow(x, y) {
	    if (arguments.length != 2) {
	      throw new math.error.ArgumentsError('epow', arguments.length, 2);
	    }

	    return collection.deepMap2(x, y, math.pow);
	  };
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var util = require(117),

	      BigNumber = require(220),
	      Complex = require(7),
	      Unit = require(11),
	      collection = require(13),

	      isNumber = util.number.isNumber,
	      toNumber = util.number.toNumber,
	      toBigNumber = util.number.toBigNumber,
	      isBoolean = util['boolean'].isBoolean,
	      isString = util.string.isString,
	      isComplex = Complex.isComplex,
	      isUnit = Unit.isUnit,
	      isCollection = collection.isCollection;

	   
	  math.equal = function equal(x, y) {
	    if (arguments.length != 2) {
	      throw new math.error.ArgumentsError('equal', arguments.length, 2);
	    }

	    if (isNumber(x)) {
	      if (isNumber(y)) {
	        return x == y;
	      }
	      else if (isComplex(y)) {
	        return (x == y.re) && (y.im == 0);
	      }
	    }

	    if (isComplex(x)) {
	      if (isNumber(y)) {
	        return (x.re == y) && (x.im == 0);
	      }
	      else if (isComplex(y)) {
	        return (x.re == y.re) && (x.im == y.im);
	      }
	    }

	    if (x instanceof BigNumber) {
	       
	      if (isNumber(y)) {
	        y = toBigNumber(y);
	      }
	      else if (isBoolean(y)) {
	        y = new BigNumber(y ? 1 : 0);
	      }

	      if (y instanceof BigNumber) {
	        return x.eq(y);
	      }

	       
	      return equal(toNumber(x), y);
	    }
	    if (y instanceof BigNumber) {
	       
	      if (isNumber(x)) {
	        x = toBigNumber(x);
	      }
	      else if (isBoolean(x)) {
	        x = new BigNumber(x ? 1 : 0);
	      }

	      if (x instanceof BigNumber) {
	        return x.eq(y)
	      }

	       
	      return equal(x, toNumber(y));
	    }

	    if ((isUnit(x)) && (isUnit(y))) {
	      if (!x.equalBase(y)) {
	        throw new Error('Cannot compare units with different base');
	      }
	      return x.value == y.value;
	    }

	    if (isString(x) || isString(y)) {
	      return x == y;
	    }

	    if (isCollection(x) || isCollection(y)) {
	      return collection.deepMap2(x, y, equal);
	    }

	    if (isBoolean(x)) {
	      return equal(+x, y);
	    }
	    if (isBoolean(y)) {
	      return equal(x, +y);
	    }

	    throw new math.error.UnsupportedTypeError('equal', x, y);
	  };
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var util = require(117),

	      BigNumber = require(220),
	      Complex = require(7),
	      Matrix = require(10),
	      collection = require(13),

	      isNumber = util.number.isNumber,
	      isBoolean = util['boolean'].isBoolean,
	      isComplex = Complex.isComplex,
	      isCollection = collection.isCollection;

	   
	  math.exp = function exp (x) {
	    if (arguments.length != 1) {
	      throw new math.error.ArgumentsError('exp', arguments.length, 1);
	    }

	    if (isNumber(x)) {
	      return Math.exp(x);
	    }

	    if (isComplex(x)) {
	      var r = Math.exp(x.re);
	      return new Complex(
	          r * Math.cos(x.im),
	          r * Math.sin(x.im)
	      );
	    }

	    if (x instanceof BigNumber) {
	       
	       
	      return exp(util.number.toNumber(x));
	    }

	    if (isCollection(x)) {
	      return collection.deepMap(x, exp);
	    }

	    if (isBoolean(x)) {
	      return Math.exp(x);
	    }

	    throw new math.error.UnsupportedTypeError('exp', x);
	  };
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var util = require(117),

	      BigNumber = require(220),
	      Complex = require(7),
	      collection = require(13),

	      isNumber = util.number.isNumber,
	      isBoolean = util['boolean'].isBoolean,
	      isComplex = Complex.isComplex,
	      isCollection = collection.isCollection;

	   
	  math.fix = function fix(x) {
	    if (arguments.length != 1) {
	      throw new math.error.ArgumentsError('fix', arguments.length, 1);
	    }

	    if (isNumber(x)) {
	      return (x > 0) ? Math.floor(x) : Math.ceil(x);
	    }

	    if (isComplex(x)) {
	      return new Complex(
	          (x.re > 0) ? Math.floor(x.re) : Math.ceil(x.re),
	          (x.im > 0) ? Math.floor(x.im) : Math.ceil(x.im)
	      );
	    }

	    if (x instanceof BigNumber) {
	      return x.isNegative() ? x.ceil() : x.floor();
	    }

	    if (isCollection(x)) {
	      return collection.deepMap(x, fix);
	    }

	    if (isBoolean(x)) {
	      return fix(+x);
	    }

	    throw new math.error.UnsupportedTypeError('fix', x);
	  };
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var util = require(117),

	      BigNumber = require(220),
	      Complex = require(7),
	      collection = require(13),

	      isNumber = util.number.isNumber,
	      isBoolean = util['boolean'].isBoolean,
	      isComplex = Complex.isComplex,
	      isCollection = collection.isCollection;

	   
	  math.floor = function floor(x) {
	    if (arguments.length != 1) {
	      throw new math.error.ArgumentsError('floor', arguments.length, 1);
	    }

	    if (isNumber(x)) {
	      return Math.floor(x);
	    }

	    if (isComplex(x)) {
	      return new Complex (
	          Math.floor(x.re),
	          Math.floor(x.im)
	      );
	    }

	    if (x instanceof BigNumber) {
	      return x.floor();
	    }

	    if (isCollection(x)) {
	      return collection.deepMap(x, floor);
	    }

	    if (isBoolean(x)) {
	      return floor(+x);
	    }

	    throw new math.error.UnsupportedTypeError('floor', x);
	  };
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var util = require(117),

	      BigNumber = require(220),
	      collection = require(13),

	      isNumber = util.number.isNumber,
	      toNumber = util.number.toNumber,
	      isBoolean = util['boolean'].isBoolean,
	      isInteger = util.number.isInteger,
	      isCollection = collection.isCollection;

	   
	  math.gcd = function gcd(args) {
	    var a = arguments[0],
	        b = arguments[1],
	        r;  

	    if (arguments.length == 2) {
	       
	      if (isNumber(a) && isNumber(b)) {
	        if (!isInteger(a) || !isInteger(b)) {
	          throw new Error('Parameters in function gcd must be integer numbers');
	        }

	         
	        while (b != 0) {
	          r = a % b;
	          a = b;
	          b = r;
	        }
	        return (a < 0) ? -a : a;
	      }

	       
	      if (isCollection(a) || isCollection(b)) {
	        return collection.deepMap2(a, b, gcd);
	      }

	       

	       
	      if (a instanceof BigNumber) {
	        return gcd(toNumber(a), b);
	      }
	      if (b instanceof BigNumber) {
	        return gcd(a, toNumber(b));
	      }

	      if (isBoolean(a)) {
	        return gcd(+a, b);
	      }
	      if (isBoolean(b)) {
	        return gcd(a, +b);
	      }

	      throw new math.error.UnsupportedTypeError('gcd', a, b);
	    }

	    if (arguments.length > 2) {
	       
	      for (var i = 1; i < arguments.length; i++) {
	        a = gcd(a, arguments[i]);
	      }
	      return a;
	    }

	     
	    throw new SyntaxError('Function gcd expects two or more arguments');
	  };
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var util = require(117),

	      BigNumber = require(220),
	      Complex = require(7),
	      Unit = require(11),
	      collection = require(13),

	      isNumber = util.number.isNumber,
	      toNumber = util.number.toNumber,
	      toBigNumber = util.number.toBigNumber,
	      isBoolean = util['boolean'].isBoolean,
	      isString = util.string.isString,
	      isComplex = Complex.isComplex,
	      isUnit = Unit.isUnit,
	      isCollection = collection.isCollection;

	   
	  math.larger = function larger(x, y) {
	    if (arguments.length != 2) {
	      throw new math.error.ArgumentsError('larger', arguments.length, 2);
	    }

	    if (isNumber(x) && isNumber(y)) {
	      return x > y;
	    }

	    if (x instanceof BigNumber) {
	       
	      if (isNumber(y)) {
	        y = toBigNumber(y);
	      }
	      else if (isBoolean(y)) {
	        y = new BigNumber(y ? 1 : 0);
	      }

	      if (y instanceof BigNumber) {
	        return x.gt(y);
	      }

	       
	      return larger(toNumber(x), y);
	    }
	    if (y instanceof BigNumber) {
	       
	      if (isNumber(x)) {
	        x = toBigNumber(x);
	      }
	      else if (isBoolean(x)) {
	        x = new BigNumber(x ? 1 : 0);
	      }

	      if (x instanceof BigNumber) {
	        return x.gt(y)
	      }

	       
	      return larger(x, toNumber(y));
	    }

	    if ((isUnit(x)) && (isUnit(y))) {
	      if (!x.equalBase(y)) {
	        throw new Error('Cannot compare units with different base');
	      }
	      return x.value > y.value;
	    }

	    if (isString(x) || isString(y)) {
	      return x > y;
	    }

	    if (isCollection(x) || isCollection(y)) {
	      return collection.deepMap2(x, y, larger);
	    }

	    if (isBoolean(x)) {
	      return larger(+x, y);
	    }
	    if (isBoolean(y)) {
	      return larger(x, +y);
	    }

	    if (isComplex(x) || isComplex(y)) {
	      throw new TypeError('No ordering relation is defined for complex numbers');
	    }

	    throw new math.error.UnsupportedTypeError('larger', x, y);
	  };
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var util = require(117),

	      BigNumber = require(220),
	      Complex = require(7),
	      Unit = require(11),
	      collection = require(13),

	      isNumber = util.number.isNumber,
	      toNumber = util.number.toNumber,
	      toBigNumber = util.number.toBigNumber,
	      isBoolean = util['boolean'].isBoolean,
	      isString = util.string.isString,
	      isComplex = Complex.isComplex,
	      isUnit = Unit.isUnit,
	      isCollection = collection.isCollection;

	   
	  math.largereq = function largereq(x, y) {
	    if (arguments.length != 2) {
	      throw new math.error.ArgumentsError('largereq', arguments.length, 2);
	    }

	    if (isNumber(x) && isNumber(y)) {
	      return x >= y;
	    }

	    if (x instanceof BigNumber) {
	       
	      if (isNumber(y)) {
	        y = toBigNumber(y);
	      }
	      else if (isBoolean(y)) {
	        y = new BigNumber(y ? 1 : 0);
	      }

	      if (y instanceof BigNumber) {
	        return x.gte(y);
	      }

	       
	      return largereq(toNumber(x), y);
	    }
	    if (y instanceof BigNumber) {
	       
	      if (isNumber(x)) {
	        x = toBigNumber(x);
	      }
	      else if (isBoolean(x)) {
	        x = new BigNumber(x ? 1 : 0);
	      }

	      if (x instanceof BigNumber) {
	        return x.gte(y)
	      }

	       
	      return largereq(x, toNumber(y));
	    }

	    if ((isUnit(x)) && (isUnit(y))) {
	      if (!x.equalBase(y)) {
	        throw new Error('Cannot compare units with different base');
	      }
	      return x.value >= y.value;
	    }

	    if (isString(x) || isString(y)) {
	      return x >= y;
	    }

	    if (isCollection(x) || isCollection(y)) {
	      return collection.deepMap2(x, y, largereq);
	    }

	    if (isBoolean(x)) {
	      return largereq(+x, y);
	    }
	    if (isBoolean(y)) {
	      return largereq(x, +y);
	    }

	    if (isComplex(x) || isComplex(y)) {
	      throw new TypeError('No ordering relation is defined for complex numbers');
	    }

	    throw new math.error.UnsupportedTypeError('largereq', x, y);
	  };
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var util = require(117),

	      BigNumber = require(220),
	      collection = require(13),

	      isNumber = util.number.isNumber,
	      toNumber = util.number.toNumber,
	      isBoolean = util['boolean'].isBoolean,
	      isInteger = util.number.isInteger,
	      isCollection = collection.isCollection;

	   
	  math.lcm = function lcm(args) {
	    var a = arguments[0],
	        b = arguments[1],
	        t;

	    if (arguments.length == 2) {
	       
	      if (isNumber(a) && isNumber(b)) {
	        if (!isInteger(a) || !isInteger(b)) {
	          throw new Error('Parameters in function lcm must be integer numbers');
	        }

	        if (a == 0 || b == 0) {
	          return 0;
	        }

	         
	         
	        var prod = a * b;
	        while (b != 0) {
	          t = b;
	          b = a % t;
	          a = t;
	        }
	        return Math.abs(prod / a);
	      }

	       
	      if (isCollection(a) || isCollection(b)) {
	        return collection.deepMap2(a, b, lcm);
	      }

	      if (isBoolean(a)) {
	        return lcm(+a, b);
	      }
	      if (isBoolean(b)) {
	        return lcm(a, +b);
	      }

	       

	       
	      if (a instanceof BigNumber) {
	        return lcm(toNumber(a), b);
	      }
	      if (b instanceof BigNumber) {
	        return lcm(a, toNumber(b));
	      }

	      throw new math.error.UnsupportedTypeError('lcm', a, b);
	    }

	    if (arguments.length > 2) {
	       
	      for (var i = 1; i < arguments.length; i++) {
	        a = lcm(a, arguments[i]);
	      }
	      return a;
	    }

	     
	    throw new SyntaxError('Function lcm expects two or more arguments');
	  };
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var util = require(117),

	      BigNumber = require(220),
	      Complex = require(7),
	      collection = require(13),

	      isNumber = util.number.isNumber,
	      isBoolean = util['boolean'].isBoolean,
	      isComplex = Complex.isComplex,
	      isCollection = collection.isCollection;

	   
	  math.log = function log(x, base) {
	    if (arguments.length == 1) {
	       
	      if (isNumber(x)) {
	        if (x >= 0) {
	          return Math.log(x);
	        }
	        else {
	           
	          return log(new Complex(x, 0));
	        }
	      }

	      if (isComplex(x)) {
	        return new Complex (
	            Math.log(Math.sqrt(x.re * x.re + x.im * x.im)),
	            Math.atan2(x.im, x.re)
	        );
	      }

	      if (x instanceof BigNumber) {
	         
	         
	        return log(util.number.toNumber(x));
	      }

	      if (isCollection(x)) {
	        return collection.deepMap(x, log);
	      }

	      if (isBoolean(x)) {
	        return log(+x);
	      }

	      throw new math.error.UnsupportedTypeError('log', x);
	    }
	    else if (arguments.length == 2) {
	       
	      return math.divide(log(x), log(base));
	    }
	    else {
	      throw new math.error.ArgumentsError('log', arguments.length, 1, 2);
	    }
	  };
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var util = require(117),

	      BigNumber = require(220),
	      Complex = require(7),
	      collection = require(13),

	      isNumber = util.number.isNumber,
	      isBoolean = util['boolean'].isBoolean,
	      isComplex = Complex.isComplex,
	      isCollection = collection.isCollection;

	   
	  math.log10 = function log10(x) {
	    if (arguments.length != 1) {
	      throw new math.error.ArgumentsError('log10', arguments.length, 1);
	    }

	    if (isNumber(x)) {
	      if (x >= 0) {
	        return Math.log(x) / Math.LN10;
	      }
	      else {
	         
	        return log10(new Complex(x, 0));
	      }
	    }

	    if (x instanceof BigNumber) {
	       
	       
	      return log10(util.number.toNumber(x));
	    }

	    if (isComplex(x)) {
	      return new Complex (
	          Math.log(Math.sqrt(x.re * x.re + x.im * x.im)) / Math.LN10,
	          Math.atan2(x.im, x.re) / Math.LN10
	      );
	    }

	    if (isCollection(x)) {
	      return collection.deepMap(x, log10);
	    }

	    if (isBoolean(x)) {
	      return log10(+x);
	    }

	    throw new math.error.UnsupportedTypeError('log10', x);
	  };
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var util = require(117),

	      BigNumber = require(220),
	      collection = require(13),

	      isNumber = util.number.isNumber,
	      toNumber = util.number.toNumber,
	      toBigNumber = util.number.toBigNumber,
	      isBoolean = util['boolean'].isBoolean,
	      isCollection = collection.isCollection;

	   
	  math.mod = function mod(x, y) {
	    if (arguments.length != 2) {
	      throw new math.error.ArgumentsError('mod', arguments.length, 2);
	    }

	     

	    if (isNumber(x)) {
	      if (isNumber(y)) {
	         
	        return _mod(x, y);
	      }
	    }

	    if (x instanceof BigNumber) {
	       
	      if (isNumber(y)) {
	        y = toBigNumber(y);
	      }
	      else if (isBoolean(y)) {
	        y = new BigNumber(y ? 1 : 0);
	      }

	      if (y instanceof BigNumber) {
	        return x.mod(y);
	      }

	       
	      return mod(toNumber(x), y);
	    }
	    if (y instanceof BigNumber) {
	       
	      if (isNumber(x)) {
	        x = toBigNumber(x);
	      }
	      else if (isBoolean(x)) {
	        x = new BigNumber(x ? 1 : 0);
	      }

	      if (x instanceof BigNumber) {
	        return x.mod(y)
	      }

	       
	      return mod(x, toNumber(y));
	    }

	     

	    if (isCollection(x) || isCollection(y)) {
	      return collection.deepMap2(x, y, mod);
	    }

	    if (isBoolean(x)) {
	      return mod(+x, y);
	    }
	    if (isBoolean(y)) {
	      return mod(x, +y);
	    }

	    throw new math.error.UnsupportedTypeError('mod', x, y);
	  };

	   
	  function _mod(x, y) {
	    if (y > 0) {
	      if (x > 0) {
	        return x % y;
	      }
	      else if (x == 0) {
	        return 0;
	      }
	      else {  
	        return x - y * Math.floor(x / y);
	      }
	    }
	    else if (y == 0) {
	      return x;
	    }
	    else {  
	       
	      throw new Error('Cannot calculate mod for a negative divisor');
	    }
	  }
	};


  },
 
  function(module, exports, require) {

	module.exports = function(math) {
	  var util = require(117),

	      BigNumber = require(220),
	      Complex = require(7),
	      Matrix = require(10),
	      Unit = require(11),
	      collection = require(13),

	      array = util.array,
	      isNumber = util.number.isNumber,
	      toNumber = util.number.toNumber,
	      toBigNumber = util.number.toBigNumber,
	      isBoolean = util['boolean'].isBoolean,
	      isComplex = Complex.isComplex,
	      isArray = Array.isArray,
	      isUnit = Unit.isUnit;

	   
	  math.multiply = function multiply(x, y) {
	    if (arguments.length != 2) {
	      throw new math.error.ArgumentsError('multiply', arguments.length, 2);
	    }

	    if (isNumber(x)) {
	      if (isNumber(y)) {
	         
	        return x * y;
	      }
	      else if (isComplex(y)) {
	         
	        return _multiplyComplex (new Complex(x, 0), y);
	      }
	      else if (isUnit(y)) {
	        res = y.clone();
	        res.value *= x;
	        return res;
	      }
	    }

	    if (isComplex(x)) {
	      if (isNumber(y)) {
	         
	        return _multiplyComplex (x, new Complex(y, 0));
	      }
	      else if (isComplex(y)) {
	         
	        return _multiplyComplex (x, y);
	      }
	    }

	    if (x instanceof BigNumber) {
	       
	      if (isNumber(y)) {
	        y = toBigNumber(y);
	      }
	      else if (isBoolean(y)) {
	        y = new BigNumber(y ? 1 : 0);
	      }

	      if (y instanceof BigNumber) {
	        return x.times(y);
	      }

	       
	      return multiply(toNumber(x), y);
	    }
	    if (y instanceof BigNumber) {
	       
	      if (isNumber(x)) {
	        x = toBigNumber(x);
	      }
	      else if (isBoolean(x)) {
	        x = new BigNumber(x ? 1 : 0);
	      }

	      if (x instanceof BigNumber) {
	        return x.times(y)
	      }

	       
	      return multiply(x, toNumber(y));
	    }

	    if (isUnit(x)) {
	      if (isNumber(y)) {
	        res = x.clone();
	        res.value *= y;
	        return res;
	      }
	    }

	    if (isArray(x)) {
	      if (isArray(y)) {
	         
	        var sizeX = array.size(x);
	        var sizeY = array.size(y);

	        if (sizeX.length == 1) {
	          if (sizeY.length == 1) {
	             
	            if (sizeX[0] != sizeY[0]) {
	              throw new RangeError('Dimensions mismatch in multiplication. ' +
	                  'Length of A must match length of B ' +
	                  '(A is ' + sizeX[0] +
	                  ', B is ' + sizeY[0] +
	                  sizeX[0] + ' != ' + sizeY[0] + ')');
	            }

	            return _multiplyVectorVector(x, y);
	          }
	          else if (sizeY.length == 2) {
	             
	            if (sizeX[0] != sizeY[0]) {
	              throw new RangeError('Dimensions mismatch in multiplication. ' +
	                  'Length of A must match rows of B ' +
	                  '(A is ' + sizeX[0] +
	                  ', B is ' + sizeY[0] + 'x' + sizeY[1] + ', ' +
	                  sizeX[0] + ' != ' + sizeY[0] + ')');
	            }

	            return _multiplyVectorMatrix(x, y);
	          }
	          else {
	            throw new Error('Can only multiply a 1 or 2 dimensional matrix ' +
	                '(B has ' + sizeY.length + ' dimensions)');
	          }
	        }
	        else if (sizeX.length == 2) {
	          if (sizeY.length == 1) {
	             
	            if (sizeX[1] != sizeY[0]) {
	              throw new RangeError('Dimensions mismatch in multiplication. ' +
	                  'Columns of A must match length of B ' +
	                  '(A is ' + sizeX[0] + 'x' + sizeX[0] +
	                  ', B is ' + sizeY[0] + ', ' +
	                  sizeX[1] + ' != ' + sizeY[0] + ')');
	            }

	            return _multiplyMatrixVector(x, y);
	          }
	          else if (sizeY.length == 2) {
	             
	            if (sizeX[1] != sizeY[0]) {
	              throw new RangeError('Dimensions mismatch in multiplication. ' +
	                  'Columns of A must match rows of B ' +
	                  '(A is ' + sizeX[0] + 'x' + sizeX[1] +
	                  ', B is ' + sizeY[0] + 'x' + sizeY[1] + ', ' +
	                  sizeX[1] + ' != ' + sizeY[0] + ')');
	            }

	            return _multiplyMatrixMatrix(x, y);
	          }
	          else {
	            throw new Error('Can only multiply a 1 or 2 dimensional matrix ' +
	                '(B has ' + sizeY.length + ' dimensions)');
	          }
	        }
	        else {
	          throw new Error('Can only multiply a 1 or 2 dimensional matrix ' +
	              '(A has ' + sizeX.length + ' dimensions)');
	        }
	      }
	      else if (y instanceof Matrix) {
	         
	        return new Matrix(multiply(x, y.valueOf()));
	      }
	      else {
	         
	        return collection.deepMap2(x, y, multiply);
	      }
	    }

	    if (x instanceof Matrix) {
	      if (y instanceof Matrix) {
	         
	        return new Matrix(multiply(x.valueOf(), y.valueOf()));
	      }
	      else {
	         
	         
	        return new Matrix(multiply(x.valueOf(), y));
	      }
	    }

	    if (isArray(y)) {
	       
	      return collection.deepMap2(x, y, multiply);
	    }
	    else if (y instanceof Matrix) {
	       
	      return new Matrix(collection.deepMap2(x, y.valueOf(), multiply));
	    }

	    if (isBoolean(x)) {
	      return multiply(+x, y);
	    }
	    if (isBoolean(y)) {
	      return multiply(x, +y);
	    }

	    throw new math.error.UnsupportedTypeError('multiply', x, y);
	  };

	   
	  function _multiplyMatrixMatrix(x, y) {
	     
	    var res = [],
	        rows = x.length,
	        cols = y[0].length,
	        num = x[0].length;

	    for (var r = 0; r < rows; r++) {
	      res[r] = [];
	      for (var c = 0; c < cols; c++) {
	        var result = null;
	        for (var n = 0; n < num; n++) {
	          var p = math.multiply(x[r][n], y[n][c]);
	          result = (result === null) ? p : math.add(result, p);
	        }
	        res[r][c] = result;
	      }
	    }

	    return res;
	  }

	   
	  function _multiplyVectorMatrix(x, y) {
	     
	    var res = [],
	        rows = y.length,
	        cols = y[0].length;

	    for (var c = 0; c < cols; c++) {
	      var result = null;
	      for (var r = 0; r < rows; r++) {
	        var p = math.multiply(x[r], y[r][c]);
	        result = (r === 0) ? p : math.add(result, p);
	      }
	      res[c] = result;
	    }

	    return res;
	  }

	   
	  function _multiplyMatrixVector(x, y) {
	     
	    var res = [],
	        rows = x.length,
	        cols = x[0].length;

	    for (var r = 0; r < rows; r++) {
	      var result = null;
	      for (var c = 0; c < cols; c++) {
	        var p = math.multiply(x[r][c], y[c]);
	        result = (c === 0) ? p : math.add(result, p);
	      }
	      res[r] = result;
	    }

	    return res;
	  }

	   
	  function _multiplyVectorVector(x, y) {
	     
	    var len = x.length,
	        dot = null;

	    if (len) {
	      dot = 0;

	      for (var i = 0, ii = x.length; i < ii; i++) {
	        dot = math.add(dot, math.multiply(x[i], y[i]));
	      }
	    }

	    return dot;
	  }

	   
	  function _multiplyComplex (x, y) {
	     
	     
	     

	    if (x.im == 0) {
	       
	      if (y.im == 0) {
	         
	        return new Complex(x.re * y.re, 0);
	      }
	      else if (y.re == 0) {
	         
	        return new Complex(
	            0,
	            x.re * y.im
	        );
	      }
	      else {
	         
	        return new Complex(
	            x.re * y.re,
	            x.re * y.im
	        );
	      }
	    }
	    else if (x.re == 0) {
	       
	      if (y.im == 0) {
	         
	        return new Complex(
	            0,
	            x.im * y.re
	        );
	      }
	      else if (y.re == 0) {
	         
	        return new Complex(-x.im * y.im, 0);
	      }
	      else {
	         
	        return new Complex(
	            -x.im * y.im,
	            x.im * y.re
	        );
	      }
	    }
	    else {
	       
	      if (y.im == 0) {
	         
	        return new Complex(
	            x.re * y.re,
	            x.im * y.re
	        );
	      }
	      else if (y.re == 0) {
	         
	        return new Complex(
	            -x.im * y.im,
	            x.re * y.im
	        );
	      }
	      else {
	         
	        return new Complex(
	            x.re * y.re - x.im * y.im,
	            x.re * y.im + x.im * y.re
	        );
	      }
	    }
	  }
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var util = require(117),

	      BigNumber = require(220),
	      Complex = require(7),
	      Matrix = require(10),
	      collection = require(13),

	      array = util.array,
	      isNumber = util.number.isNumber,
	      toNumber = util.number.toNumber,
	      toBigNumber = util.number.toBigNumber,
	      isBoolean = util['boolean'].isBoolean,
	      isArray = Array.isArray,
	      isInteger = util.number.isInteger,
	      isComplex = Complex.isComplex;

	   
	  math.pow = function pow(x, y) {
	    if (arguments.length != 2) {
	      throw new math.error.ArgumentsError('pow', arguments.length, 2);
	    }

	    if (isNumber(x)) {
	      if (isNumber(y)) {
	        if (isInteger(y) || x >= 0) {
	           
	          return Math.pow(x, y);
	        }
	        else {
	          return powComplex(new Complex(x, 0), new Complex(y, 0));
	        }
	      }
	      else if (isComplex(y)) {
	        return powComplex(new Complex(x, 0), y);
	      }
	    }

	    if (isComplex(x)) {
	      if (isNumber(y)) {
	        return powComplex(x, new Complex(y, 0));
	      }
	      else if (isComplex(y)) {
	        return powComplex(x, y);
	      }
	    }

	     

	    if (x instanceof BigNumber) {
	       
	      if (isNumber(y)) {
	        y = toBigNumber(y);
	      }
	      else if (isBoolean(y)) {
	        y = new BigNumber(y ? 1 : 0);
	      }

	      if (y instanceof BigNumber) {
	        return x.pow(y);
	      }

	       
	      return pow(toNumber(x), y);
	    }
	    if (y instanceof BigNumber) {
	       
	      if (isNumber(x)) {
	        x = toBigNumber(x);
	      }
	      else if (isBoolean(x)) {
	        x = new BigNumber(x ? 1 : 0);
	      }

	      if (x instanceof BigNumber) {
	        return x.pow(y)
	      }

	       
	      return pow(x, toNumber(y));
	    }


	    if (isArray(x)) {
	      if (!isNumber(y) || !isInteger(y) || y < 0) {
	        throw new TypeError('For A^b, b must be a positive integer ' +
	            '(value is ' + y + ')');
	      }
	       
	      var s = array.size(x);
	      if (s.length != 2) {
	        throw new Error('For A^b, A must be 2 dimensional ' +
	            '(A has ' + s.length + ' dimensions)');
	      }
	      if (s[0] != s[1]) {
	        throw new Error('For A^b, A must be square ' +
	            '(size is ' + s[0] + 'x' + s[1] + ')');
	      }

	       
	      var res = math.eye(s[0]).valueOf();
	      var px = x;
	      while (y >= 1) {
	        if ((y & 1) == 1) {
	          res = math.multiply(px, res);
	        }
	        y >>= 1;
	        px = math.multiply(px, px);
	      }
	      return res;
	    }
	    else if (x instanceof Matrix) {
	      return new Matrix(pow(x.valueOf(), y));
	    }

	    if (isBoolean(x)) {
	      return pow(+x, y);
	    }
	    if (isBoolean(y)) {
	      return pow(x, +y);
	    }

	    throw new math.error.UnsupportedTypeError('pow', x, y);
	  };

	   
	  function powComplex (x, y) {
	     
	     
	    var temp1 = math.log(x);
	    var temp2 = math.multiply(temp1, y);
	    return math.exp(temp2);
	  }
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var util = require(117),

	      BigNumber = require(220),
	      Complex = require(7),
	      collection = require(13),

	      isNumber = util.number.isNumber,
	      isInteger = util.number.isInteger,
	      isBoolean = util['boolean'].isBoolean,
	      isComplex = Complex.isComplex,
	      isCollection = collection.isCollection;

	   
	  math.round = function round(x, n) {
	    if (arguments.length != 1 && arguments.length != 2) {
	      throw new math.error.ArgumentsError('round', arguments.length, 1, 2);
	    }

	    if (n == undefined) {
	       
	      if (isNumber(x)) {
	        return Math.round(x);
	      }

	      if (isComplex(x)) {
	        return new Complex (
	            Math.round(x.re),
	            Math.round(x.im)
	        );
	      }

	      if (x instanceof BigNumber) {
	        return x.round();
	      }

	      if (isCollection(x)) {
	        return collection.deepMap(x, round);
	      }

	      if (isBoolean(x)) {
	        return Math.round(x);
	      }

	      throw new math.error.UnsupportedTypeError('round', x);
	    }
	    else {
	       
	      if (n instanceof BigNumber) {
	        n = parseFloat(n.valueOf());
	      }

	      if (!isNumber(n) || !isInteger(n)) {
	        throw new TypeError('Number of decimals in function round must be an integer');
	      }
	      if (n < 0 || n > 9) {
	        throw new Error ('Number of decimals in function round must be in te range of 0-9');
	      }

	      if (isNumber(x)) {
	        return roundNumber(x, n);
	      }

	      if (isComplex(x)) {
	        return new Complex (
	            roundNumber(x.re, n),
	            roundNumber(x.im, n)
	        );
	      }

	      if (x instanceof BigNumber) {
	        if (isNumber(n)) {
	          return x.round(n);
	        }
	      }

	      if (isCollection(x) || isCollection(n)) {
	        return collection.deepMap2(x, n, round);
	      }

	      if (isBoolean(x)) {
	        return round(+x, n);
	      }
	      if (isBoolean(n)) {
	        return round(x, +n);
	      }

	      throw new math.error.UnsupportedTypeError('round', x, n);
	    }
	  };

	   
	  function roundNumber (value, decimals) {
	    if (decimals) {
	      var p = Math.pow(10, decimals);
	      return Math.round(value * p) / p;
	    }
	    else {
	      return Math.round(value);
	    }
	  }
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var util = require(117),

	      BigNumber = require(220),
	      Complex = require(7),
	      collection = require(13),

	      number = util.number,
	      isNumber = util.number.isNumber,
	      isBoolean = util['boolean'].isBoolean,
	      isComplex = Complex.isComplex,
	      isCollection = collection.isCollection;

	   
	  math.sign = function sign(x) {
	    if (arguments.length != 1) {
	      throw new math.error.ArgumentsError('sign', arguments.length, 1);
	    }

	    if (isNumber(x)) {
	      return number.sign(x);
	    }

	    if (isComplex(x)) {
	      var abs = Math.sqrt(x.re * x.re + x.im * x.im);
	      return new Complex(x.re / abs, x.im / abs);
	    }

	    if (x instanceof BigNumber) {
	      return new BigNumber(x.cmp(0));
	    }

	    if (isCollection(x)) {
	      return collection.deepMap(x, sign);
	    }

	    if (isBoolean(x)) {
	      return number.sign(x);
	    }

	    throw new math.error.UnsupportedTypeError('sign', x);
	  };
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var util = require(117),

	      BigNumber = require(220),
	      Complex = require(7),
	      Unit = require(11),
	      collection = require(13),

	      isNumber = util.number.isNumber,
	      toNumber = util.number.toNumber,
	      toBigNumber = util.number.toBigNumber,
	      isBoolean = util['boolean'].isBoolean,
	      isString = util.string.isString,
	      isComplex = Complex.isComplex,
	      isUnit = Unit.isUnit,
	      isCollection = collection.isCollection;

	   
	  math.smaller = function smaller(x, y) {
	    if (arguments.length != 2) {
	      throw new math.error.ArgumentsError('smaller', arguments.length, 2);
	    }

	    if (isNumber(x) && isNumber(y)) {
	      return x < y;
	    }

	    if (x instanceof BigNumber) {
	       
	      if (isNumber(y)) {
	        y = toBigNumber(y);
	      }
	      else if (isBoolean(y)) {
	        y = new BigNumber(y ? 1 : 0);
	      }

	      if (y instanceof BigNumber) {
	        return x.lt(y);
	      }

	       
	      return smaller(toNumber(x), y);
	    }
	    if (y instanceof BigNumber) {
	       
	      if (isNumber(x)) {
	        x = toBigNumber(x);
	      }
	      else if (isBoolean(x)) {
	        x = new BigNumber(x ? 1 : 0);
	      }

	      if (x instanceof BigNumber) {
	        return x.lt(y)
	      }

	       
	      return smaller(x, toNumber(y));
	    }

	    if ((isUnit(x)) && (isUnit(y))) {
	      if (!x.equalBase(y)) {
	        throw new Error('Cannot compare units with different base');
	      }
	      return x.value < y.value;
	    }

	    if (isString(x) || isString(y)) {
	      return x < y;
	    }

	    if (isCollection(x) || isCollection(y)) {
	      return collection.deepMap2(x, y, smaller);
	    }

	    if (isBoolean(x)) {
	      return smaller(+x, y);
	    }
	    if (isBoolean(y)) {
	      return smaller(x, +y);
	    }

	    if (isComplex(x) || isComplex(y)) {
	      throw new TypeError('No ordering relation is defined for complex numbers');
	    }

	    throw new math.error.UnsupportedTypeError('smaller', x, y);
	  };
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var util = require(117),

	      BigNumber = require(220),
	      Complex = require(7),
	      Unit = require(11),
	      collection = require(13),

	      isNumber = util.number.isNumber,
	      toNumber = util.number.toNumber,
	      toBigNumber = util.number.toBigNumber,
	      isBoolean = util['boolean'].isBoolean,
	      isString = util.string.isString,
	      isComplex = Complex.isComplex,
	      isUnit = Unit.isUnit,
	      isCollection = collection.isCollection;

	   
	  math.smallereq = function smallereq(x, y) {
	    if (arguments.length != 2) {
	      throw new math.error.ArgumentsError('smallereq', arguments.length, 2);
	    }

	    if (isNumber(x) && isNumber(y)) {
	      return x <= y;
	    }

	    if (x instanceof BigNumber) {
	       
	      if (isNumber(y)) {
	        y = toBigNumber(y);
	      }
	      else if (isBoolean(y)) {
	        y = new BigNumber(y ? 1 : 0);
	      }

	      if (y instanceof BigNumber) {
	        return x.lte(y);
	      }

	       
	      return smallereq(toNumber(x), y);
	    }
	    if (y instanceof BigNumber) {
	       
	      if (isNumber(x)) {
	        x = toBigNumber(x);
	      }
	      else if (isBoolean(x)) {
	        x = new BigNumber(x ? 1 : 0);
	      }

	      if (x instanceof BigNumber) {
	        return x.lte(y)
	      }

	       
	      return smallereq(x, toNumber(y));
	    }

	    if ((isUnit(x)) && (isUnit(y))) {
	      if (!x.equalBase(y)) {
	        throw new Error('Cannot compare units with different base');
	      }
	      return x.value <= y.value;
	    }

	    if (isString(x) || isString(y)) {
	      return x <= y;
	    }

	    if (isCollection(x) || isCollection(y)) {
	      return collection.deepMap2(x, y, smallereq);
	    }

	    if (isBoolean(x)) {
	      return smallereq(+x, y);
	    }
	    if (isBoolean(y)) {
	      return smallereq(x, +y);
	    }

	    if (isComplex(x) || isComplex(y)) {
	      throw new TypeError('No ordering relation is defined for complex numbers');
	    }

	    throw new math.error.UnsupportedTypeError('smallereq', x, y);
	  };
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var util = require(117),

	      BigNumber = require(220),
	      Complex = require(7),
	      collection = require(13),

	      isNumber = util.number.isNumber,
	      isBoolean = util['boolean'].isBoolean,
	      isComplex = Complex.isComplex,
	      isCollection = collection.isCollection;

	   
	  math.sqrt = function sqrt (x) {
	    if (arguments.length != 1) {
	      throw new math.error.ArgumentsError('sqrt', arguments.length, 1);
	    }

	    if (isNumber(x)) {
	      if (x >= 0) {
	        return Math.sqrt(x);
	      }
	      else {
	        return sqrt(new Complex(x, 0));
	      }
	    }

	    if (isComplex(x)) {
	      var r = Math.sqrt(x.re * x.re + x.im * x.im);
	      if (x.im >= 0) {
	        return new Complex(
	            0.5 * Math.sqrt(2.0 * (r + x.re)),
	            0.5 * Math.sqrt(2.0 * (r - x.re))
	        );
	      }
	      else {
	        return new Complex(
	            0.5 * Math.sqrt(2.0 * (r + x.re)),
	            -0.5 * Math.sqrt(2.0 * (r - x.re))
	        );
	      }
	    }

	    if (x instanceof BigNumber) {
	      return x.sqrt();
	    }

	    if (isCollection(x)) {
	      return collection.deepMap(x, sqrt);
	    }

	    if (isBoolean(x)) {
	      return sqrt(+x);
	    }

	    throw new math.error.UnsupportedTypeError('sqrt', x);
	  };
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var util = require(117),

	      BigNumber = require(220),
	      Complex = require(7),
	      collection = require(13),

	      isNumber = util.number.isNumber,
	      isBoolean = util['boolean'].isBoolean,
	      isComplex = Complex.isComplex,
	      isCollection = collection.isCollection;

	   
	  math.square = function square(x) {
	    if (arguments.length != 1) {
	      throw new math.error.ArgumentsError('square', arguments.length, 1);
	    }

	    if (isNumber(x)) {
	      return x * x;
	    }

	    if (isComplex(x)) {
	      return math.multiply(x, x);
	    }

	    if (x instanceof BigNumber) {
	      return x.times(x);
	    }

	    if (isCollection(x)) {
	      return collection.deepMap(x, square);
	    }

	    if (isBoolean(x)) {
	      return x * x;
	    }

	    throw new math.error.UnsupportedTypeError('square', x);
	  };
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var util = require(117),

	      BigNumber = require(220),
	      Complex = require(7),
	      Matrix = require(10),
	      Unit = require(11),
	      collection = require(13),

	      toNumber = util.number.toNumber,
	      toBigNumber = util.number.toBigNumber,
	      isBoolean = util['boolean'].isBoolean,
	      isNumber = util.number.isNumber,
	      isComplex = Complex.isComplex,
	      isUnit = Unit.isUnit,
	      isCollection = collection.isCollection;

	   
	  math.subtract = function subtract(x, y) {
	    if (arguments.length != 2) {
	      throw new math.error.ArgumentsError('subtract', arguments.length, 2);
	    }

	    if (isNumber(x)) {
	      if (isNumber(y)) {
	         
	        return x - y;
	      }
	      else if (isComplex(y)) {
	         
	        return new Complex (
	            x - y.re,
	            - y.im
	        );
	      }
	    }
	    else if (isComplex(x)) {
	      if (isNumber(y)) {
	         
	        return new Complex (
	            x.re - y,
	            x.im
	        )
	      }
	      else if (isComplex(y)) {
	         
	        return new Complex (
	            x.re - y.re,
	            x.im - y.im
	        )
	      }
	    }

	    if (x instanceof BigNumber) {
	       
	      if (isNumber(y)) {
	        y = toBigNumber(y);
	      }
	      else if (isBoolean(y)) {
	        y = new BigNumber(y ? 1 : 0);
	      }

	      if (y instanceof BigNumber) {
	        return x.minus(y);
	      }

	       
	      return subtract(toNumber(x), y);
	    }
	    if (y instanceof BigNumber) {
	       
	      if (isNumber(x)) {
	        x = toBigNumber(x);
	      }
	      else if (isBoolean(x)) {
	        x = new BigNumber(x ? 1 : 0);
	      }

	      if (x instanceof BigNumber) {
	        return x.minus(y)
	      }

	       
	      return subtract(x, toNumber(y));
	    }

	    if (isUnit(x)) {
	      if (isUnit(y)) {
	        if (!x.equalBase(y)) {
	          throw new Error('Units do not match');
	        }

	        if (x.value == null) {
	          throw new Error('Unit on left hand side of operator - has an undefined value');
	        }

	        if (y.value == null) {
	          throw new Error('Unit on right hand side of operator - has an undefined value');
	        }

	        var res = x.clone();
	        res.value -= y.value;
	        res.fixPrefix = false;

	        return res;
	      }
	    }

	    if (isCollection(x) || isCollection(y)) {
	      return collection.deepMap2(x, y, subtract);
	    }

	    if (isBoolean(x)) {
	      return subtract(+x, y);
	    }
	    if (isBoolean(y)) {
	      return subtract(x, +y);
	    }

	    throw new math.error.UnsupportedTypeError('subtract', x, y);
	  };
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var util = require(117),

	      BigNumber = require(220),
	      Complex = require(7),
	      Unit = require(11),
	      collection = require(13),

	      isNumber = util.number.isNumber,
	      isBoolean = util['boolean'].isBoolean,
	      isComplex = Complex.isComplex,
	      isUnit = Unit.isUnit,
	      isCollection = collection.isCollection;

	   
	  math.unary = function unary(x) {
	    if (arguments.length != 1) {
	      throw new math.error.ArgumentsError('unary', arguments.length, 1);
	    }

	    if (isNumber(x)) {
	      return -x;
	    }

	    if (isComplex(x)) {
	      return new Complex(
	          -x.re,
	          -x.im
	      );
	    }

	    if (x instanceof BigNumber) {
	      return x.neg();
	    }

	    if (isUnit(x)) {
	      var res = x.clone();
	      res.value = -x.value;
	      return res;
	    }

	    if (isCollection(x)) {
	      return collection.deepMap(x, unary);
	    }

	    if (isBoolean(x)) {
	      return -x;
	    }

	    throw new math.error.UnsupportedTypeError('unary', x);
	  };
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var util = require(117),

	      BigNumber = require(220),
	      Complex = require(7),
	      Unit = require(11),
	      collection = require(13),

	      isNumber = util.number.isNumber,
	      toNumber = util.number.toNumber,
	      toBigNumber = util.number.toBigNumber,
	      isBoolean = util['boolean'].isBoolean,
	      isString = util.string.isString,
	      isComplex = Complex.isComplex,
	      isUnit = Unit.isUnit,
	      isCollection = collection.isCollection;

	   
	  math.unequal = function unequal(x, y) {
	    if (arguments.length != 2) {
	      throw new math.error.ArgumentsError('unequal', arguments.length, 2);
	    }

	    if (isNumber(x)) {
	      if (isNumber(y)) {
	        return x != y;
	      }
	      else if (isComplex(y)) {
	        return (x != y.re) || (y.im != 0);
	      }
	    }

	    if (isComplex(x)) {
	      if (isNumber(y)) {
	        return (x.re != y) || (x.im != 0);
	      }
	      else if (isComplex(y)) {
	        return (x.re != y.re) || (x.im != y.im);
	      }
	    }

	    if (x instanceof BigNumber) {
	       
	      if (isNumber(y)) {
	        y = toBigNumber(y);
	      }
	      else if (isBoolean(y)) {
	        y = new BigNumber(y ? 1 : 0);
	      }

	      if (y instanceof BigNumber) {
	        return !x.eq(y);
	      }

	       
	      return unequal(toNumber(x), y);
	    }
	    if (y instanceof BigNumber) {
	       
	      if (isNumber(x)) {
	        x = toBigNumber(x);
	      }
	      else if (isBoolean(x)) {
	        x = new BigNumber(x ? 1 : 0);
	      }

	      if (x instanceof BigNumber) {
	        return !x.eq(y)
	      }

	       
	      return unequal(x, toNumber(y));
	    }

	    if ((isUnit(x)) && (isUnit(y))) {
	      if (!x.equalBase(y)) {
	        throw new Error('Cannot compare units with different base');
	      }
	      return x.value != y.value;
	    }

	    if (isString(x) || isString(y)) {
	      return x != y;
	    }

	    if (isCollection(x) || isCollection(y)) {
	      return collection.deepMap2(x, y, unequal);
	    }

	    if (isBoolean(x)) {
	      return unequal(+x, y);
	    }
	    if (isBoolean(y)) {
	      return unequal(x, +y);
	    }

	    throw new math.error.UnsupportedTypeError('unequal', x, y);
	  };
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var util = require(117),

	      BigNumber = require(220),

	      toNumber = util.number.toNumber,
	      isNumber = util.number.isNumber,
	      isBoolean = util['boolean'].isBoolean,
	      isInteger = util.number.isInteger;

	   
	  math.xgcd = function xgcd(a, b) {
	    if (arguments.length == 2) {
	       
	      if (isNumber(a) && isNumber(b)) {
	        if (!isInteger(a) || !isInteger(b)) {
	          throw new Error('Parameters in function xgcd must be integer numbers');
	        }

	        return _xgcd(a, b);
	      }

	       

	       
	      if (a instanceof BigNumber) {
	        return xgcd(toNumber(a), b);
	      }
	      if (b instanceof BigNumber) {
	        return xgcd(a, toNumber(b));
	      }

	      if (isBoolean(a)) {
	        return xgcd(+a, b);
	      }
	      if (isBoolean(b)) {
	        return xgcd(a, +b);
	      }

	      throw new math.error.UnsupportedTypeError('xgcd', a, b);
	    }

	     
	    throw new SyntaxError('Function xgcd expects two arguments');
	  };

	   
	  function _xgcd(a, b) {
	     
	     
	    var t,  
	        q,  
	        r,  
	        x = 0, lastx = 1,
	        y = 1, lasty = 0;

	    while (b) {
	      q = Math.floor(a / b);
	      r = a % b;

	      t = x;
	      x = lastx - q * x;
	      lastx = t;

	      t = y;
	      y = lasty - q * y;
	      lasty = t;

	      a = b;
	      b = r;
	    }

	    if (a < 0) {
	      return [-a, a ? -lastx : 0, -lasty];
	    }
	    else {
	      return [a, a ? lastx : 0, lasty];
	    }
	  }
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var util = require(117),

	      BigNumber = require(220),
	      Complex = require(7),
	      collection = require(13),

	      isNumber = util.number.isNumber,
	      isBoolean = util['boolean'].isBoolean,
	      isCollection = collection.isCollection,
	      isComplex = Complex.isComplex;

	   
	  math.arg = function arg(x) {
	    if (arguments.length != 1) {
	      throw new math.error.ArgumentsError('arg', arguments.length, 1);
	    }

	    if (isNumber(x)) {
	      return Math.atan2(0, x);
	    }

	    if (isComplex(x)) {
	      return Math.atan2(x.im, x.re);
	    }

	    if (isCollection(x)) {
	      return collection.deepMap(x, arg);
	    }

	    if (isBoolean(x)) {
	      return arg(+x);
	    }

	    if (x instanceof BigNumber) {
	       
	       
	      return arg(util.number.toNumber(x));
	    }

	    throw new math.error.UnsupportedTypeError('arg', x);
	  };
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var util = require(117),

	      BigNumber = require(220),
	      Complex = require(7),
	      collection = require(13),

	      object = util.object,
	      isNumber = util.number.isNumber,
	      isBoolean = util['boolean'].isBoolean,
	      isCollection =collection.isCollection,
	      isComplex = Complex.isComplex;

	   
	  math.conj = function conj(x) {
	    if (arguments.length != 1) {
	      throw new math.error.ArgumentsError('conj', arguments.length, 1);
	    }

	    if (isNumber(x)) {
	      return x;
	    }

	    if (x instanceof BigNumber) {
	      return new BigNumber(x);
	    }

	    if (isComplex(x)) {
	      return new Complex(x.re, -x.im);
	    }

	    if (isCollection(x)) {
	      return collection.deepMap(x, conj);
	    }

	    if (isBoolean(x)) {
	      return +x;
	    }

	     
	    return object.clone(x);
	  };
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var util = require(117),

	      BigNumber = require(220),
	      Complex = require(7),
	      collection = require(13),

	      object = util.object,
	      isNumber = util.number.isNumber,
	      isBoolean = util['boolean'].isBoolean,
	      isCollection = collection.isCollection,
	      isComplex = Complex.isComplex;

	   
	  math.re = function re(x) {
	    if (arguments.length != 1) {
	      throw new math.error.ArgumentsError('re', arguments.length, 1);
	    }

	    if (isNumber(x)) {
	      return x;
	    }

	    if (x instanceof BigNumber) {
	      return new BigNumber(x);
	    }

	    if (isComplex(x)) {
	      return x.re;
	    }

	    if (isCollection(x)) {
	      return collection.deepMap(x, re);
	    }

	    if (isBoolean(x)) {
	      return +x;
	    }

	     
	    return object.clone(x);
	  };
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var util = require(117),

	      BigNumber = require(220),
	      Complex = require(7),
	      collection = require(13),

	      isNumber = util.number.isNumber,
	      isBoolean = util['boolean'].isBoolean,
	      isCollection =collection.isCollection,
	      isComplex = Complex.isComplex;

	   
	  math.im = function im(x) {
	    if (arguments.length != 1) {
	      throw new math.error.ArgumentsError('im', arguments.length, 1);
	    }

	    if (isNumber(x)) {
	      return 0;
	    }

	    if (x instanceof BigNumber) {
	      return new BigNumber(0);
	    }

	    if (isComplex(x)) {
	      return x.im;
	    }

	    if (isCollection(x)) {
	      return collection.deepMap(x, im);
	    }

	    if (isBoolean(x)) {
	      return 0;
	    }

	     
	    return 0;
	  };
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var util = require(117),

	      BigNumber = require(220),
	      collection = require(13),

	      isCollection = collection.isCollection,
	      isNumber = util.number.isNumber,
	      isString = util.string.isString,
	      isBoolean = util['boolean'].isBoolean;

	   
	  if (typeof BigNumber.prototype.clone !== 'function') {
	     
	    BigNumber.prototype.clone = function clone () {
	      return new BigNumber(this);
	    };
	  }

	   
	  math.bignumber = function bignumber(value) {
	    if (arguments.length > 1) {
	      throw new math.error.ArgumentsError('bignumber', arguments.length, 0, 1);
	    }

	    if ((value instanceof BigNumber) || isNumber(value) || isString(value)) {
	      return new BigNumber(value);
	    }

	    if (isBoolean(value)) {
	      return new BigNumber(+value);
	    }

	    if (isCollection(value)) {
	      return collection.deepMap(value, bignumber);
	    }

	    if (arguments.length == 0) {
	      return new BigNumber(0);
	    }

	    throw new math.error.UnsupportedTypeError('bignumber', value);
	  };
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var util = require(117),

	      BigNumber = require(220),
	      collection = require(13),

	      isCollection = collection.isCollection,
	      isNumber = util.number.isNumber,
	      isString = util.string.isString;

	   
	  math['boolean'] = function bool (value) {
	    if (arguments.length != 1) {
	      throw new math.error.ArgumentsError('boolean', arguments.length, 0, 1);
	    }

	    if (value === 'true' || value === true) {
	      return true;
	    }

	    if (value === 'false' || value === false) {
	      return false;
	    }

	    if (value instanceof Boolean) {
	      return value ? true : false;
	    }

	    if (isNumber(value)) {
	      return (value !== 0);
	    }

	    if (value instanceof BigNumber) {
	      return !value.isZero();
	    }

	    if (isString(value)) {
	       
	      var lcase = value.toLowerCase();
	      if (lcase === 'true') {
	        return true;
	      }
	      else if (lcase === 'false') {
	        return false;
	      }

	       
	      var num = Number(value);
	      if (value != '' && !isNaN(num)) {
	        return (num !== 0);
	      }
	    }

	    if (isCollection(value)) {
	      return collection.deepMap(value, bool);
	    }

	    throw new SyntaxError(value.toString() + ' is no valid boolean');
	  };
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var util = require(117),

	      BigNumber = require(220),
	      Complex = require(7),
	      collection = require(13),

	      isCollection = collection.isCollection,
	      isNumber = util.number.isNumber,
	      toNumber = util.number.toNumber,
	      isString = util.string.isString,
	      isComplex = Complex.isComplex;

	   
	  math.complex = function complex(args) {
	    switch (arguments.length) {
	      case 0:
	         
	        return new Complex(0, 0);
	        break;

	      case 1:
	         
	        var arg = arguments[0];

	        if (isNumber(arg)) {
	          return new Complex(arg, 0);
	        }

	        if (arg instanceof BigNumber) {
	           
	          return new Complex(toNumber(arg), 0);
	        }

	        if (isComplex(arg)) {
	           
	          return arg.clone();
	        }

	        if (isString(arg)) {
	          var c = Complex.parse(arg);
	          if (c) {
	            return c;
	          }
	          else {
	            throw new SyntaxError('String "' + arg + '" is no valid complex number');
	          }
	        }

	        if (isCollection(arg)) {
	          return collection.deepMap(arg, complex);
	        }

	        throw new TypeError(
	            'Two numbers or a single string expected in function complex');
	        break;

	      case 2:
	         
	        var re = arguments[0],
	            im = arguments[1];

	         
	        if (re instanceof BigNumber) {
	          re = toNumber(re);
	        }

	         
	        if (im instanceof BigNumber) {
	          im = toNumber(im);
	        }

	        if (isNumber(re) && isNumber(im)) {
	          return new Complex(re, im);
	        }
	        else {
	          throw new TypeError(
	              'Two numbers or a single string expected in function complex');
	        }

	        break;

	      default:
	        throw new math.error.ArgumentsError('complex', arguments.length, 0, 2);
	    }
	  };
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var util = require(117),

	      BigNumber = require(220),
	      Index = require(9),

	      toNumber = util.number.toNumber;

	   
	  math.index = function matrix(ranges) {
	    var i = new Index();

	     
	    var args = Array.prototype.slice.apply(arguments).map(function (arg) {
	      if (arg instanceof BigNumber) {
	        return toNumber(arg);
	      }
	      else if (Array.isArray(arg)) {
	        return arg.map(function (elem) {
	          return (elem instanceof BigNumber) ? toNumber (elem) : elem;
	        });
	      }
	      else {
	        return arg;
	      }
	    });

	    Index.apply(i, args);
	    return i;
	  };
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var Matrix = require(10);

	   
	  math.matrix = function matrix(data) {
	    if (arguments.length > 1) {
	      throw new math.error.ArgumentsError('matrix', arguments.length, 0, 1);
	    }

	    return new Matrix(data);
	  };
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var util = require(117),

	      BigNumber = require(220),
	      collection = require(13),

	      isCollection = collection.isCollection,
	      toNumber = util.number.toNumber,
	      isNumber = util.number.isNumber,
	      isBoolean = util['boolean'].isBoolean,
	      isString = util.string.isString;

	   
	  math.number = function number (value) {
	    switch (arguments.length) {
	      case 0:
	        return 0;

	      case 1:
	        if (isCollection(value)) {
	          return collection.deepMap(value, number);
	        }

	        if (value instanceof BigNumber) {
	          return toNumber(value);
	        }

	        if (isString(value)) {
	          var num = Number(value);
	          if (isNaN(num)) {
	            num = Number(value.valueOf());
	          }
	          if (isNaN(num)) {
	            throw new SyntaxError(value.toString() + ' is no valid number');
	          }
	          return num;
	        }

	        if (isBoolean(value)) {
	          return value + 0;
	        }

	        if (isNumber(value)) {
	          return value;
	        }

	        throw new math.error.UnsupportedTypeError('number', value);

	      default:
	        throw new math.error.ArgumentsError('number', arguments.length, 0, 1);
	    }
	  };
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var Parser = require(5);

	   
	  math.parser = function parser() {
	    return new Parser(math);
	  };
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	   
	  math.select = function select(value) {
	     
	    return new math.chaining.Selector(value);
	  };
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var util = require(117),

	      collection = require(13),

	      number = util.number,
	      isNumber = util.number.isNumber,
	      isCollection = collection.isCollection;

	   
	  math.string = function string (value) {
	    switch (arguments.length) {
	      case 0:
	        return '';

	      case 1:
	        if (isNumber(value)) {
	          return number.format(value);
	        }

	        if (isCollection(value)) {
	          return collection.deepMap(value, string);
	        }

	        if (value === null) {
	          return 'null';
	        }

	        return value.toString();

	      default:
	        throw new math.error.ArgumentsError('string', arguments.length, 0, 1);
	    }
	  };
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var util = require(117),

	      BigNumber = require(220),
	      Unit = require(11),
	      collection = require(13),

	      isCollection = collection.isCollection,
	      toNumber = util.number.toNumber,
	      isString = util.string.isString;

	   
	  math.unit = function unit(args) {
	    switch(arguments.length) {
	      case 1:
	         
	        var arg = arguments[0];

	        if (arg instanceof Unit) {
	           
	          return arg.clone();
	        }

	        if (isString(arg)) {
	          if (Unit.isPlainUnit(arg)) {
	            return new Unit(null, arg);  
	          }

	          var u = Unit.parse(arg);         
	          if (u) {
	            return u;
	          }

	          throw new SyntaxError('String "' + arg + '" is no valid unit');
	        }

	        if (isCollection(args)) {
	          return collection.deepMap(args, unit);
	        }

	        throw new TypeError('A string or a number and string expected in function unit');
	        break;

	      case 2:
	         

	        if (arguments[0] instanceof BigNumber) {
	           
	          return new Unit(toNumber(arguments[0]), arguments[1]);
	        }
	        else {
	          return new Unit(arguments[0], arguments[1]);
	        }
	        break;

	      default:
	        throw new math.error.ArgumentsError('unit', arguments.length, 1, 2);
	    }
	  };
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var util = require(117),

	      Matrix = require(10),
	      collection = require(13),

	      object = util.object,
	      array = util.array,
	      isNumber = util.number.isNumber,
	      isInteger = util.number.isInteger,
	      isCollection = collection.isCollection;

	   
	  math.concat = function concat (args) {
	    var i,
	        len = arguments.length,
	        dim = -1,   
	        prevDim,
	        asMatrix = false,
	        matrices = [];   

	    for (i = 0; i < len; i++) {
	      var arg = arguments[i];

	       
	      if (arg instanceof Matrix) {
	        asMatrix = true;
	      }

	      if ((i == len - 1) && isNumber(arg)) {
	         
	        prevDim = dim;
	        dim = arg;

	        if (!isInteger(dim) || dim < 0) {
	          throw new TypeError('Dimension number must be a positive integer ' +
	              '(dim = ' + dim + ')');
	        }

	        if (i > 0 && dim > prevDim) {
	          throw new RangeError('Dimension out of range ' +
	              '(' + dim + ' > ' + prevDim + ')');
	        }
	      }
	      else if (isCollection(arg)) {
	         
	        var matrix = object.clone(arg).valueOf();
	        var size = array.size(arg.valueOf());
	        matrices[i] = matrix;
	        prevDim = dim;
	        dim = size.length - 1;

	         
	        if (i > 0 && dim != prevDim) {
	          throw new RangeError('Dimension mismatch ' +
	              '(' + prevDim + ' != ' + dim + ')');
	        }
	      }
	      else {
	        throw new math.error.UnsupportedTypeError('concat', arg);
	      }
	    }

	    if (matrices.length == 0) {
	      throw new SyntaxError('At least one matrix expected');
	    }

	    var res = matrices.shift();
	    while (matrices.length) {
	      res = _concat(res, matrices.shift(), dim, 0);
	    }

	    return asMatrix ? new Matrix(res) : res;
	  };

	   
	  function _concat(a, b, concatDim, dim) {
	    if (dim < concatDim) {
	       
	      if (a.length != b.length) {
	        throw new Error('Dimensions mismatch (' + a.length + ' != ' + b.length + ')');
	      }

	      var c = [];
	      for (var i = 0; i < a.length; i++) {
	        c[i] = _concat(a[i], b[i], concatDim, dim + 1);
	      }
	      return c;
	    }
	    else {
	       
	      return a.concat(b);
	    }
	  }
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var util = require(117),

	      Matrix = require(10),

	      object = util.object,
	      array = util.array,
	      string = util.string;

	   
	  math.det = function det (x) {
	    if (arguments.length != 1) {
	      throw new math.error.ArgumentsError('det', arguments.length, 1);
	    }

	    if (!(x instanceof Matrix)) {
	      if (x instanceof Array) {
	        x = new Matrix(x);
	      } else {
	        throw new TypeError('Determinant is only defined for Matrix or Array.');
	      }
	    } 

	    var size = x.size();

	    switch (size.length) {
	      case 0:
	         
	        return object.clone(x);
	        break;

	      case 1:
	         
	        if (size[0] == 1) {
	          return object.clone(x.valueOf()[0]);
	        }
	        else {
	          throw new RangeError('Matrix must be square ' +
	              '(size: ' + string.format(size) + ')');
	        }
	        break;

	      case 2:
	         
	        var rows = size[0];
	        var cols = size[1];
	        if (rows == cols) {
	          return _det(x.clone().valueOf(), rows, cols);
	        }
	        else {
	          throw new RangeError('Matrix must be square ' +
	              '(size: ' + string.format(size) + ')');
	        }
	        break;

	      default:
	         
	        throw new RangeError('Matrix must be two dimensional ' +
	            '(size: ' + string.format(size) + ')');
	    }
	  };

	   
	  function _det (matrix, rows, cols) {
	    if (rows == 1) {
	       
	      return matrix[0][0];
	    }
	    else if (rows == 2) {
	       
	       
	      return math.subtract(
	          math.multiply(matrix[0][0], matrix[1][1]),
	          math.multiply(matrix[1][0], matrix[0][1])
	      );
	    }
	    else {
	       
	      var d = 1;
	      var lead = 0;
	      for (var r = 0; r < rows; r++) {
	        if (lead >= cols) {
	          break;
	        }
	        var i = r;
	         
	        while (matrix[i][lead] == 0) {
	          i++;
	          if (i == rows) {
	            i = r;
	            lead++;
	            if (lead == cols) {
	               
	              if (object.deepEqual(matrix, math.eye(rows).valueOf())) {
	                return math.round(d, 6);
	              } else {
	                return 0;
	              }
	            }
	          }
	        }
	        if (i != r) {
	           
	          for (var a = 0; a < cols; a++) {
	            var temp = matrix[i][a];
	            matrix[i][a] = matrix[r][a];
	            matrix[r][a] = temp;
	          }
	          d *= -1;
	        }
	         
	        var div = matrix[r][lead];
	        for (var a = 0; a < cols; a++) {
	          matrix[r][a] = matrix[r][a] / div;
	        }
	        d *= div;
	         
	        for (var j = 0; j < rows; j++) {
	          if (j != r) {
	             
	            var c = matrix[j][lead];
	            for (var a = 0; a < cols; a++) {
	              matrix[j][a] = matrix[j][a] - matrix[r][a] * c;
	            }
	          }
	        }
	        lead++;  
	      }
	       
	      if (object.deepEqual(matrix, math.eye(rows).valueOf())) {
	        return math.round(d, 6);
	      } else {
	        return 0;
	      }
	    }
	  }
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math, settings) {
	  var util = require(117),

	      Matrix = require(10),
	      collection = require(13),

	      object = util.object,
	      isArray = util.array.isArray,
	      isNumber = util.number.isNumber,
	      isInteger = util.number.isInteger;

	   
	  math.diag = function diag (x, k) {
	    var data, vector, i, iMax;

	    if (arguments.length != 1 && arguments.length != 2) {
	      throw new math.error.ArgumentsError('diag', arguments.length, 1, 2);
	    }

	    if (k) {
	      if (!isNumber(k) || !isInteger(k)) {
	        throw new TypeError ('Second parameter in function diag must be an integer');
	      }
	    }
	    else {
	      k = 0;
	    }
	    var kSuper = k > 0 ? k : 0;
	    var kSub = k < 0 ? -k : 0;

	     
	    if (x instanceof Matrix) {
	       
	    }
	    else if (isArray(x)) {
	       
	      x = new Matrix(x);
	    }
	    else {
	      throw new TypeError ('First parameter in function diag must be a Matrix or Array');
	    }

	    var s = x.size();
	    switch (s.length) {
	      case 1:
	         
	        vector = x.valueOf();
	        var matrix = new Matrix();
	        var defaultValue = 0;
	        matrix.resize([vector.length + kSub, vector.length + kSuper], defaultValue);
	        data = matrix.valueOf();
	        iMax = vector.length;
	        for (i = 0; i < iMax; i++) {
	          data[i + kSub][i + kSuper] = object.clone(vector[i]);
	        }
	        return (settings.matrix === 'array') ? matrix.valueOf() : matrix;
	        break;

	      case 2:
	         
	        vector = [];
	        data = x.valueOf();
	        iMax = Math.min(s[0] - kSub, s[1] - kSuper);
	        for (i = 0; i < iMax; i++) {
	          vector[i] = object.clone(data[i + kSub][i + kSuper]);
	        }
	        return (settings.matrix === 'array') ? vector : new Matrix(vector);
	        break;

	      default:
	        throw new RangeError('Matrix for function diag must be 2 dimensional');
	    }
	  };
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math, settings) {
	  var util = require(117),

	      BigNumber = require(220),
	      Matrix = require(10),
	      collection = require(13),

	      toNumber = util.number.toNumber,
	      isNumber = util.number.isNumber,
	      isInteger = util.number.isInteger,
	      isArray = Array.isArray;

	   
	  math.eye = function eye (size) {
	    var args = collection.argsToArray(arguments),
	        asMatrix = (size instanceof Matrix) ? true :
	        (isArray(size) ? false : (settings.matrix === 'matrix'));


	    if (args.length == 0) {
	       
	      return asMatrix ? new Matrix() : [];
	    }
	    else if (args.length == 1) {
	       
	      args[1] = args[0];
	    }
	    else if (args.length > 2) {
	       
	      throw new math.error.ArgumentsError('eye', args.length, 0, 2);
	    }

	    var asBigNumber = args[0] instanceof BigNumber,
	        rows = args[0],
	        cols = args[1];

	    if (rows instanceof BigNumber) {
	      rows = toNumber(rows);
	    }
	    if (cols instanceof BigNumber) {
	      cols = toNumber(cols);
	    }

	    if (!isNumber(rows) || !isInteger(rows) || rows < 1) {
	      throw new Error('Parameters in function eye must be positive integers');
	    }
	    if (cols) {
	      if (!isNumber(cols) || !isInteger(cols) || cols < 1) {
	        throw new Error('Parameters in function eye must be positive integers');
	      }
	    }

	     
	    var matrix = new Matrix();
	    var one = asBigNumber ? new BigNumber(1) : 1;
	    var defaultValue = asBigNumber ? new BigNumber(0) : 0;
	    matrix.resize(args.map(toNumber), defaultValue);

	     
	    var minimum = math.min(args);
	    var data = matrix.valueOf();
	    for (var d = 0; d < minimum; d++) {
	      data[d][d] = one;
	    }

	    return asMatrix ? matrix : matrix.valueOf();
	  };
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var string = require(218),

	      Matrix = require(10),
	      collection = require(13);

	   
	  math.inv = function inv (x) {
	    if (arguments.length != 1) {
	      throw new math.error.ArgumentsError('inv', arguments.length, 1);
	    }
	    var size = math.size(x).valueOf();
	    switch (size.length) {
	      case 0:
	         
	        return math.divide(1, x);
	        break;

	      case 1:
	         
	        if (size[0] == 1) {
	          if (x instanceof Matrix) {
	            return new Matrix([
	              math.divide(1, x.valueOf()[0])
	            ]);
	          }
	          else {
	            return [
	              math.divide(1, x[0])
	            ];
	          }
	        }
	        else {
	          throw new RangeError('Matrix must be square ' +
	              '(size: ' + string.format(size) + ')');
	        }
	        break;

	      case 2:
	         
	        var rows = size[0];
	        var cols = size[1];
	        if (rows == cols) {
	          if (x instanceof Matrix) {
	            return new Matrix(
	                _inv(x.valueOf(), rows, cols)
	            );
	          }
	          else {
	             
	            return _inv(x, rows, cols);
	          }
	        }
	        else {
	          throw new RangeError('Matrix must be square ' +
	              '(size: ' + string.format(size) + ')');
	        }
	        break;

	      default:
	         
	        throw new RangeError('Matrix must be two dimensional ' +
	            '(size: ' + string.format(size) + ')');
	    }
	  };

	   
	  function _inv (matrix, rows, cols){
	    var r, s, f, value, temp;

	    if (rows == 1) {
	       
	      value = matrix[0][0];
	      if (value == 0) {
	        throw Error('Cannot calculate inverse, determinant is zero');
	      }
	      return [[
	        math.divide(1, value)
	      ]];
	    }
	    else if (rows == 2) {
	       
	      var d = math.det(matrix);
	      if (d == 0) {
	        throw Error('Cannot calculate inverse, determinant is zero');
	      }
	      return [
	        [
	          math.divide(matrix[1][1], d),
	          math.divide(math.unary(matrix[0][1]), d)
	        ],
	        [
	          math.divide(math.unary(matrix[1][0]), d),
	          math.divide(matrix[0][0], d)
	        ]
	      ];
	    }
	    else {
	       
	       
	       
	       
	       

	       
	      var A = matrix.concat();
	      for (r = 0; r < rows; r++) {
	        A[r] = A[r].concat();
	      }

	       
	       
	      var B = math.eye(rows).valueOf();

	       
	      for (var c = 0; c < cols; c++) {
	         
	         
	        r = c;
	        while (r < rows && A[r][c] == 0) {
	          r++;
	        }
	        if (r == rows || A[r][c] == 0) {
	          throw Error('Cannot calculate inverse, determinant is zero');
	        }
	        if (r != c) {
	          temp = A[c]; A[c] = A[r]; A[r] = temp;
	          temp = B[c]; B[c] = B[r]; B[r] = temp;
	        }

	         
	        var Ac = A[c],
	            Bc = B[c];
	        for (r = 0; r < rows; r++) {
	          var Ar = A[r],
	              Br = B[r];
	          if(r != c) {
	             
	            if (Ar[c] != 0) {
	              f = math.divide(math.unary(Ar[c]), Ac[c]);

	               
	               
	              for (s = c; s < cols; s++) {
	                Ar[s] = math.add(Ar[s], math.multiply(f, Ac[s]));
	              }
	              for (s = 0; s < cols; s++) {
	                Br[s] = math.add(Br[s],  math.multiply(f, Bc[s]));
	              }
	            }
	          }
	          else {
	             
	             
	            f = Ac[c];
	            for (s = c; s < cols; s++) {
	              Ar[s] = math.divide(Ar[s], f);
	            }
	            for (s = 0; s < cols; s++) {
	              Br[s] = math.divide(Br[s], f);
	            }
	          }
	        }
	      }
	      return B;
	    }
	  }
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math, settings) {
	  var util = require(117),

	      BigNumber = require(220),
	      Matrix = require(10),
	      collection = require(13),

	      array = util.array,

	      toNumber = util.number.toNumber,
	      isArray = Array.isArray;

	   
	  math.ones = function ones (size) {
	    var args = collection.argsToArray(arguments);
	    var asMatrix = (size instanceof Matrix) ? true :
	        (isArray(size) ? false : (settings.matrix === 'matrix'));

	    if (args.length == 0) {
	       
	      return asMatrix ? new Matrix() : [];
	    }
	    else {
	       
	      var res = [];
	      var defaultValue = (args[0] instanceof BigNumber) ? new BigNumber(1) : 1;
	      res = array.resize(res, args.map(toNumber), defaultValue);

	      return asMatrix ? new Matrix(res) : res;
	    }
	  };
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math, settings) {
	  var util = require(117),

	      BigNumber = require(220),
	      Matrix = require(10),
	      collection = require(13),

	      isBoolean = util['boolean'].isBoolean,
	      isString = util.string.isString,
	      isNumber = util.number.isNumber,
	      toNumber = util.number.toNumber,
	      toBigNumber = util.number.toBigNumber;

	   
	  math.range = function range(args) {
	    var params = Array.prototype.slice.call(arguments),
	        start,
	        end,
	        step,
	        includeEnd = false;

	     
	    if (isBoolean(params[params.length - 1])) {
	      includeEnd = params.pop() ? true : false;
	    }

	    switch (params.length) {
	      case 1:
	         
	         
	        if (isString(params[0])) {
	          var r = _parse(params[0]);
	          if (!r){
	            throw new SyntaxError('String "' + r + '" is no valid range');
	          }

	          start = r.start;
	          end = r.end;
	          step = r.step;
	        }
	        else {
	          throw new TypeError(
	              'Two or three numbers or a single string expected in function range');
	        }
	        break;

	      case 2:
	         
	         
	        start = params[0];
	        end = params[1];
	        step = 1;
	        break;

	      case 3:
	         
	        start = params[0];
	        end = params[1];
	        step = params[2];
	        break;

	      default:
	        throw new math.error.ArgumentsError('range', arguments.length, 2, 4);
	    }

	     
	    if (!isNumber(start) && !(start instanceof BigNumber)) {
	      throw new TypeError('Parameter start must be a number');
	    }
	    if (!isNumber(end) && !(end instanceof BigNumber)) {
	      throw new TypeError('Parameter end must be a number');
	    }
	    if (!isNumber(step) && !(step instanceof BigNumber)) {
	      throw new TypeError('Parameter step must be a number');
	    }
	    if (!isBoolean(includeEnd)) {
	      throw new TypeError('Parameter includeEnd must be a boolean');
	    }

	     
	    if (start instanceof BigNumber || end instanceof BigNumber || step instanceof BigNumber) {
	       
	      var asBigNumber = true;

	       
	      if (!(start instanceof BigNumber)) {
	        start = toBigNumber(start);
	      }
	      if (!(end instanceof BigNumber)) {
	        end = toBigNumber(end);
	      }
	      if (!(step instanceof BigNumber)) {
	        step = toBigNumber(step);
	      }

	      if (!(start instanceof BigNumber) || !(end instanceof BigNumber) || !(step instanceof BigNumber)) {
	         
	         
	        asBigNumber = false;
	        start = toNumber(start);
	        end   = toNumber(end);
	        step  = toNumber(step);
	      }
	    }

	     
	    var fn = asBigNumber ?
	        (includeEnd ? _bigRangeInc : _bigRange) :
	        (includeEnd ? _rangeInc    : _range);
	    var array = fn(start, end, step);

	     
	    return (settings.matrix === 'array') ? array : new Matrix(array);
	  };

	   
	  function _range (start, end, step) {
	    var array = [],
	        x = start;
	    if (step > 0) {
	      while (x < end) {
	        array.push(x);
	        x += step;
	      }
	    }
	    else if (step < 0) {
	      while (x > end) {
	        array.push(x);
	        x += step;
	      }
	    }

	    return array;
	  }

	   
	  function _rangeInc (start, end, step) {
	    var array = [],
	        x = start;
	    if (step > 0) {
	      while (x <= end) {
	        array.push(x);
	        x += step;
	      }
	    }
	    else if (step < 0) {
	      while (x >= end) {
	        array.push(x);
	        x += step;
	      }
	    }

	    return array;
	  }

	   
	  function _bigRange (start, end, step) {
	    var array = [],
	        x = start.clone(),
	        zero = new BigNumber(0);
	    if (step.gt(zero)) {
	      while (x.lt(end)) {
	        array.push(x);
	        x = x.plus(step);
	      }
	    }
	    else if (step.lt(zero)) {
	      while (x.gt(end)) {
	        array.push(x);
	        x = x.plus(step);
	      }
	    }

	    return array;
	  }

	   
	  function _bigRangeInc (start, end, step) {
	    var array = [],
	        x = start.clone(),
	        zero = new BigNumber(0);
	    if (step.gt(zero)) {
	      while (x.lte(end)) {
	        array.push(x);
	        x = x.plus(step);
	      }
	    }
	    else if (step.lt(zero)) {
	      while (x.gte(end)) {
	        array.push(x);
	        x = x.plus(step);
	      }
	    }

	    return array;
	  }

	   
	  function _parse (str) {
	    var args = str.split(':'),
	        nums = null;

	    if (settings.number === 'bignumber') {
	       
	      try {
	        nums = args.map(function (arg) {
	          return new BigNumber(arg);
	        });
	      }
	      catch (err) {
	        return null;
	      }
	    }
	    else {
	       
	      nums = args.map(function (arg) {
	        return parseFloat(arg);
	      });

	      var invalid = nums.some(function (num) {
	        return isNaN(num);
	      });
	      if(invalid) {
	        return null;
	      }
	    }

	    switch (nums.length) {
	      case 2:
	        return {
	          start: nums[0],
	          end: nums[1],
	          step: 1
	        };

	      case 3:
	        return {
	          start: nums[0],
	          end: nums[2],
	          step: nums[1]
	        };

	      default:
	        return null;
	    }
	  }

	};


  },
 
  function(module, exports, require) {

	module.exports = function (math, settings) {
	  var util = require(117),

	      BigNumber = require(220),
	      Matrix = require(10),

	      array = util.array,
	      clone = util.object.clone,
	      isString = util.string.isString,
	      toNumber = util.number.toNumber,
	      isNumber = util.number.isNumber,
	      isInteger = util.number.isInteger,
	      isArray = array.isArray;

	   
	  math.resize = function resize (x, size, defaultValue) {
	    if (arguments.length != 2 && arguments.length != 3) {
	      throw new math.error.ArgumentsError('resize', arguments.length, 2, 3);
	    }

	    var asMatrix = (x instanceof Matrix) ? true : isArray(x) ? false : (settings.matrix !== 'array');

	    if (x instanceof Matrix) {
	      x = x.valueOf();  
	    }
	    if (size instanceof Matrix) {
	      size = size.valueOf();  
	    }

	    if (size.length && size[0] instanceof BigNumber) {
	       
	      size = size.map(toNumber);
	    }

	    if (isString(x)) {
	      return _resizeString(x, size, defaultValue);
	    }
	    else {
	      if (size.length == 0) {
	         
	        while (isArray(x)) {
	          x = x[0];
	        }

	        return clone(x);
	      }
	      else {
	         
	        if (!isArray(x)) {
	          x = [x];
	        }
	        x = clone(x);

	        var res = array.resize(x, size, defaultValue);
	        return asMatrix ? new Matrix(res) : res;
	      }
	    }
	  };

	   
	  function _resizeString(str, size, defaultChar) {
	    if (defaultChar !== undefined) {
	      if (!isString(defaultChar) || defaultChar.length !== 1) {
	        throw new TypeError('Single character expected as defaultValue');
	      }
	    }
	    else {
	      defaultChar = ' ';
	    }

	    if (size.length !== 1) {
	      throw new Error('Dimension mismatch: (' + size.length + ' != 1)');
	    }
	    var len = size[0];
	    if (!isNumber(len) || !isInteger(len)) {
	      throw new TypeError('Size must contain numbers');
	    }

	    if (str.length > len) {
	      return str.substring(0, len);
	    }
	    else if (str.length < len) {
	      var res = str;
	      for (var i = 0, ii = len - str.length; i < ii; i++) {
	        res += defaultChar;
	      }
	      return res;
	    }
	    else {
	      return str;
	    }
	  }
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math, settings) {
	  var util = require(117),

	      BigNumber = require(220),
	      Complex = require(7),
	      Unit = require(11),
	      Matrix = require(10),

	      array = util.array,
	      isNumber = util.number.isNumber,
	      isBoolean = util['boolean'].isBoolean,
	      isString = util.string.isString,
	      isComplex = Complex.isComplex,
	      isUnit = Unit.isUnit;

	   
	  math.size = function size (x) {
	    if (arguments.length != 1) {
	      throw new math.error.ArgumentsError('size', arguments.length, 1);
	    }

	    var asArray = (settings.matrix === 'array');

	    if (isNumber(x) || isComplex(x) || isUnit(x) || isBoolean(x) ||
	        x == null || x instanceof BigNumber) {
	      return asArray ? [] : new Matrix([]);
	    }

	    if (isString(x)) {
	      return asArray ? [x.length] : new Matrix([x.length]);
	    }

	    if (Array.isArray(x)) {
	      return array.size(x);
	    }

	    if (x instanceof Matrix) {
	      return new Matrix(x.size());
	    }

	    throw new math.error.UnsupportedTypeError('size', x);
	  };
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var util = require(117),

	      Matrix = require(10),

	      object = util.object,
	      array = util.array,
	      isArray = Array.isArray;

	   
	  math.squeeze = function squeeze (x) {
	    if (arguments.length != 1) {
	      throw new math.error.ArgumentsError('squeeze', arguments.length, 1);
	    }

	    if (isArray(x)) {
	      return array.squeeze(object.clone(x));
	    }
	    else if (x instanceof Matrix) {
	      var res = array.squeeze(x.toArray());
	      return isArray(res) ? new Matrix(res) : res;
	    }
	    else {
	       
	      return object.clone(x);
	    }
	  };
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var util = require(117),

	      Matrix = require(10),
	      Index = require(9),

	      array = util.array,
	      isString = util.string.isString,
	      isArray = Array.isArray;

	   
	  math.subset = function subset (args) {
	    switch (arguments.length) {
	      case 2:  
	        return _getSubset(arguments[0], arguments[1]);

	       
	      case 3:  
	      case 4:  
	        return _setSubset(arguments[0], arguments[1], arguments[2], arguments[3]);

	      default:  
	        throw new math.error.ArgumentsError('subset', arguments.length, 2, 4);
	    }
	  };

	   
	  function _getSubset(value, index) {
	    var m, subset;

	    if (isArray(value)) {
	      m = new Matrix(value);
	      subset = m.subset(index);
	      return subset.valueOf();
	    }
	    else if (value instanceof Matrix) {
	      return value.subset(index);
	    }
	    else if (isString(value)) {
	      return _getSubstring(value, index);
	    }
	    else {
	      throw new math.error.UnsupportedTypeError('subset', value);
	    }
	  }

	   
	  function _getSubstring(str, index) {
	    if (!(index instanceof Index)) {
	       
	      throw new TypeError('Index expected');
	    }
	    if (index.size().length != 1) {
	      throw new RangeError('Dimension mismatch (' + index.size().length + ' != 1)');
	    }

	    var range = index.range(0);

	    var substr = '';
	    var strLen = str.length;
	    range.forEach(function (v) {
	      array.validateIndex(v, strLen);
	      substr += str.charAt(v);
	    });

	    return substr;
	  }

	   
	  function _setSubset(value, index, replacement, defaultValue) {
	    var m;

	    if (isArray(value)) {
	      m = new Matrix(math.clone(value));
	      m.subset(index, replacement, defaultValue);
	      return m.valueOf();
	    }
	    else if (value instanceof Matrix) {
	      return value.clone().subset(index, replacement, defaultValue);
	    }
	    else if (isString(value)) {
	      return _setSubstring(value, index, replacement, defaultValue);
	    }
	    else {
	      throw new math.error.UnsupportedTypeError('subset', value);
	    }
	  }

	   
	  function _setSubstring(str, index, replacement, defaultValue) {
	    if (!(index instanceof Index)) {
	       
	      throw new TypeError('Index expected');
	    }
	    if (index.size().length != 1) {
	      throw new RangeError('Dimension mismatch (' + index.size().length + ' != 1)');
	    }
	    if (defaultValue !== undefined) {
	      if (!isString(defaultValue) || defaultValue.length !== 1) {
	        throw new TypeError('Single character expected as defaultValue');
	      }
	    }
	    else {
	      defaultValue = ' ';
	    }

	    var range = index.range(0);
	    var len = range.size()[0];

	    if (len != replacement.length) {
	      throw new RangeError('Dimension mismatch ' +
	          '(' + range.size()[0] + ' != ' + replacement.length + ')');
	    }

	     
	    var strLen = str.length;
	    var chars = [];
	    for (var i = 0; i < strLen; i++) {
	      chars[i] = str.charAt(i);
	    }

	    range.forEach(function (v, i) {
	      array.validateIndex(v);
	      chars[v] = replacement.charAt(i);
	    });

	     
	    if (chars.length > strLen) {
	      for (i = strLen - 1, len = chars.length; i < len; i++) {
	        if (!chars[i]) {
	          chars[i] = defaultValue;
	        }
	      }
	    }

	    return chars.join('');
	  }
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var util = require(117),

	      Matrix = require(10),
	      collection = require(13),

	      object = util.object,
	      string = util.string;

	   
	  math.transpose = function transpose (x) {
	    if (arguments.length != 1) {
	      throw new math.error.ArgumentsError('transpose', arguments.length, 1);
	    }

	    var size = math.size(x).valueOf();
	    switch (size.length) {
	      case 0:
	         
	        return object.clone(x);
	        break;

	      case 1:
	         
	        return object.clone(x);
	        break;

	      case 2:
	         
	        var rows = size[1],
	            cols = size[0],
	            asMatrix = (x instanceof Matrix),
	            data = x.valueOf(),
	            transposed = [],
	            transposedRow,
	            clone = object.clone;

	        if (rows === 0) {
	           
	          throw new RangeError('Cannot transpose a 2D matrix with no rows' +
	              '(size: ' + string.format(size) + ')');
	        }

	        for (var r = 0; r < rows; r++) {
	          transposedRow = transposed[r] = [];
	          for (var c = 0; c < cols; c++) {
	            transposedRow[c] = clone(data[c][r]);
	          }
	        }
	        if (cols == 0) {
	          transposed[0] = [];
	        }

	        return asMatrix ? new Matrix(transposed) : transposed;
	        break;

	      default:
	         
	        throw new RangeError('Matrix must be two dimensional ' +
	            '(size: ' + string.format(size) + ')');
	    }
	  };
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math, settings) {
	  var util = require(117),

	      BigNumber = require(220),
	      Matrix = require(10),
	      collection = require(13),

	      array = util.array,
	      toNumber = util.number.toNumber,
	      isArray = Array.isArray;

	   
	  math.zeros = function zeros (size) {
	    var args = collection.argsToArray(arguments);
	    var asMatrix = (size instanceof Matrix) ? true :
	        (isArray(size) ? false : (settings.matrix === 'matrix'));

	    if (args.length == 0) {
	       
	      return asMatrix ? new Matrix() : [];
	    }
	    else {
	       
	      var res = [];
	      var defaultValue = (args[0] instanceof BigNumber) ? new BigNumber(0) : 0;
	      res = array.resize(res, args.map(toNumber), defaultValue);

	      return asMatrix ? new Matrix(res) : res;
	    }
	  };
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var util = require(117),

	      BigNumber = require(220),
	      collection = require(13),

	      isNumber = util.number.isNumber,
	      isBoolean = util['boolean'].isBoolean,
	      isInteger = util.number.isInteger,
	      isCollection = collection.isCollection;

	   
	  math.factorial = function factorial (n) {
	    var value, res;

	    if (arguments.length != 1) {
	      throw new math.error.ArgumentsError('factorial', arguments.length, 1);
	    }

	    if (isNumber(n)) {
	      if (!isInteger(n) || n < 0) {
	        throw new TypeError('Positive integer value expected in function factorial');
	      }

	      value = n - 1;
	      res = n;
	      while (value > 1) {
	        res *= value;
	        value--;
	      }

	      if (res == 0) {
	        res = 1;         
	      }

	      return res;
	    }

	    if (n instanceof BigNumber) {
	      if (!(isPositiveInteger(n))) {
	        throw new TypeError('Positive integer value expected in function factorial');
	      }

	      var one = new BigNumber(1);

	      value = n.minus(one);
	      res = n;
	      while (value.gt(one)) {
	        res = res.times(value);
	        value = value.minus(one);
	      }

	      if (res.equals(0)) {
	        res = one;         
	      }

	      return res;
	    }

	    if (isBoolean(n)) {
	      return 1;  
	    }

	    if (isCollection(n)) {
	      return collection.deepMap(n, factorial);
	    }

	    throw new math.error.UnsupportedTypeError('factorial', n);
	  };

	   
	  var isPositiveInteger = function(n) {
	    return n.round().equals(n) && n.gte(0);
	  };
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math, settings) {
	  var Matrix = require(10),
	      collection = require(13);

	   

	   

	   
	   
	  var distributions = {

	    uniform: function() {
	      return Math.random;
	    },

	     
	     
	     
	     
	    normal: function() {
	      return function() {
	        var u1, u2,
	            picked = -1;
	         
	         
	        while (picked < 0 || picked > 1) {
	          u1 = Math.random();
	          u2 = Math.random();
	          picked = 1/6 * Math.pow(-2 * Math.log(u1), 0.5) * Math.cos(2 * Math.PI * u2) + 0.5;
	        }
	        return picked;
	      }
	    }
	  };

	   
	  math.distribution = function(name) {
	    if (!distributions.hasOwnProperty(name))
	      throw new Error('unknown distribution ' + name);

	    var args = Array.prototype.slice.call(arguments, 1),
	        distribution = distributions[name].apply(this, args);

	    return (function(distribution) {

	       
	      var randFunctions = {

	        random: function(arg1, arg2, arg3) {
	          var size, min, max;
	          if (arguments.length > 3) {
	            throw new math.error.ArgumentsError('random', arguments.length, 0, 3);

	           
	          } else if (arguments.length === 1) {
	            if (Array.isArray(arg1))
	              size = arg1;
	            else
	              max = arg1;
	           
	          } else if (arguments.length === 2) {
	            if (Array.isArray(arg1))
	              size = arg1;
	            else {
	              min = arg1;
	              max = arg2;
	            }
	           
	          } else {
	            size = arg1;
	            min = arg2;
	            max = arg3;
	          }

	          if (max === undefined) max = 1;
	          if (min === undefined) min = 0;
	          if (size !== undefined) {
	            var res = _randomDataForMatrix(size, min, max, _random);
	            return (settings.matrix === 'array') ? res : new Matrix(res);
	          }
	          else return _random(min, max);
	        },

	        randomInt: function(arg1, arg2, arg3) {
	          var size, min, max;
	          if (arguments.length > 3 || arguments.length < 1)
	            throw new math.error.ArgumentsError('randomInt', arguments.length, 1, 3);

	           
	          else if (arguments.length === 1) max = arg1;
	           
	          else if (arguments.length === 2) {
	            if (Object.prototype.toString.call(arg1) === '[object Array]')
	              size = arg1;
	            else {
	              min = arg1;
	              max = arg2;
	            }
	           
	          } else {
	            size = arg1;
	            min = arg2;
	            max = arg3;
	          }

	          if (min === undefined) min = 0;
	          if (size !== undefined) {
	            var res = _randomDataForMatrix(size, min, max, _randomInt);
	            return (settings.matrix === 'array') ? res : new Matrix(res);
	          }
	          else return _randomInt(min, max);
	        },

	        pickRandom: function(possibles) {
	          if (arguments.length !== 1) {
	            throw new math.error.ArgumentsError('pickRandom', arguments.length, 1);
	          }
	          if (!Array.isArray(possibles)) {
	            throw new math.error.UnsupportedTypeError('pickRandom', possibles);
	          }

	           
	          return possibles[Math.floor(Math.random() * possibles.length)];
	        }

	      };

	      var _random = function(min, max) {
	        return min + distribution() * (max - min);
	      };

	      var _randomInt = function(min, max) {
	        return Math.floor(min + distribution() * (max - min));
	      };

	       
	      var _randomDataForMatrix = function(size, min, max, randFunc) {
	        var data = [], length, i;
	        size = size.slice(0);

	        if (size.length > 1) {
	          for (i = 0, length = size.shift(); i < length; i++)
	            data.push(_randomDataForMatrix(size, min, max, randFunc));
	        } else {
	          for (i = 0, length = size.shift(); i < length; i++)
	            data.push(randFunc(min, max));
	        }

	        return data;
	      };

	      return randFunctions;

	    })(distribution);

	  };

	   
	   
	  var uniformRandFunctions = math.distribution('uniform');
	  math.random = uniformRandFunctions.random;
	  math.randomInt = uniformRandFunctions.randomInt;
	  math.pickRandom = uniformRandFunctions.pickRandom;
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var util = require(117),

	      BigNumber = require(220),

	      isNumber = util.number.isNumber,
	      isInteger = util.number.isInteger,
	      toBigNumber = util.number.toBigNumber;

	   
	  math.permutations = function permutations (n, k) {
	    var result, i;

	    var arity = arguments.length;
	    if (arity > 2) {
	      throw new math.error.ArgumentsError('permutations', arguments.length, 2);
	    }

	    if (isNumber(n)) {
	      if (!isInteger(n) || n < 0) {
	        throw new TypeError('Positive integer value expected in function permutations');
	      }
	      
	       
	      if (arity == 1) {
	        return math.factorial(n);
	      }
	      
	       
	      if (arity == 2) {
	        if (isNumber(k)) {
	          if (!isInteger(k) || k < 0) {
	            throw new TypeError('Positive integer value expected in function permutations');
	          }
	          if (k > n) {
	            throw new TypeError('second argument k must be less than or equal to first argument n');
	          }

	          result = 1;
	          for (i = n - k + 1; i <= n; i++) {
	            result = result * i;
	          }
	          return result;
	        }
	      }
	    }

	    if (n instanceof BigNumber) {
	      if (k === undefined && isPositiveInteger(n)) {
	        return math.factorial(n);
	      }

	       
	       
	      k = toBigNumber(k);

	      if (!(k instanceof BigNumber) || !isPositiveInteger(n) || !isPositiveInteger(k)) {
	        throw new TypeError('Positive integer value expected in function permutations');
	      }
	      if (k.gt(n)) {
	        throw new TypeError('second argument k must be less than or equal to first argument n');
	      }

	      result = new BigNumber(1);
	      for (i = n.minus(k).plus(1); i.lte(n); i = i.plus(1)) {
	        result = result.times(i);
	      }
	      return result;
	    }

	    throw new math.error.UnsupportedTypeError('permutations', n);
	  };

	   
	  var isPositiveInteger = function(n) {
	    return n.round().equals(n) && n.gte(0);
	  };
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var util = require(117),

	      BigNumber = require(220),
	      collection = require(13),

	      isNumber = util.number.isNumber,
	      isInteger = util.number.isInteger,
	      toBigNumber = util.number.toBigNumber;

	   
	  math.combinations = function combinations (n, k) {
	    var max, result, i,ii;

	    var arity = arguments.length;
	    if (arity != 2) {
	      throw new math.error.ArgumentsError('combinations', arguments.length, 2);
	    }

	    if (isNumber(n)) {
	      if (!isInteger(n) || n < 0) {
	        throw new TypeError('Positive integer value enpected in function combinations');
	      }
	      if (k > n) {
	        throw new TypeError('k must be less than or equal to n');
	      }

	      max = Math.max(k, n - k);
	      result = 1;
	      for (i = 1; i <= n - max; i++) {
	        result = result * (max + i) / i;
	      }
	      return result;
	    }

	    if (n instanceof BigNumber) {
	       
	       
	      k = toBigNumber(k);

	      if (!(k instanceof BigNumber) || !isPositiveInteger(n) || !isPositiveInteger(k)) {
	        throw new TypeError('Positive integer value expected in function combinations');
	      }
	      if (k.gt(n)) {
	        throw new TypeError('k must be less than n in function combinations');
	      }

	      max = n.minus(k);
	      if (k.lt(max)) max = k;
	      result = new BigNumber(1);
	      for (i = new BigNumber(1), ii = n.minus(max); i.lte(ii); i = i.plus(1)) {
	        result = result.times(max.plus(i)).dividedBy(i);
	      }
	      return result;
	    }

	    throw new math.error.UnsupportedTypeError('combinations', n);
	  };

	   
	  var isPositiveInteger = function(n) {
	    return n.round().equals(n) && n.gte(0);
	  };
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var Matrix = require(10),
	      collection = require(13),

	      isCollection = collection.isCollection;

	   
	  math.min = function min(args) {
	    if (arguments.length == 0) {
	      throw new SyntaxError('Function min requires one or more parameters (0 provided)');
	    }

	    if (isCollection(args)) {
	      if (arguments.length == 1) {
	         
	        return _min(args);
	      }
	      else if (arguments.length == 2) {
	         
	        return collection.reduce(arguments[0], arguments[1], _getsmaller);
	      }
	      else {
	        throw new SyntaxError('Wrong number of parameters');
	      }
	    }
	    else {
	       
	      return _min(arguments);
	    }
	  };

	  function _getsmaller(x, y){
		  if( math.smaller(x,y) )
			  return x;
		  else
			  return y;
	  }

	   
	  function _min(array) {
	    var min = null;

	    collection.deepForEach(array, function (value) {
	      if (min === null || math.smaller(value, min)) {
	        min = value;
	      }
	    });

	    if (min === null) {
	      throw new Error('Cannot calculate min of an empty array');
	    }

	    return min;
	  }
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var Matrix = require(10),
	      collection = require(13),

	      isCollection = collection.isCollection;

	   
	  math.max = function max(args) {
	    if (arguments.length == 0) {
	      throw new SyntaxError('Function max requires one or more parameters (0 provided)');
	    }

	    if (isCollection(args)) {
	      if (arguments.length == 1) {
	         
	        return _max(args);
	      }
	      else if (arguments.length == 2) {
	         
	        return collection.reduce(arguments[0], arguments[1], _getlarger);
	      }
	      else {
	        throw new SyntaxError('Wrong number of parameters');
	      }
	    }
	    else {
	       
	      return _max(arguments);
	    }
	  };

	  function _getlarger(x, y){
		  if( math.larger(x,y) )
			  return x;
		  else
			  return y;
	  }

	   
	  function _max(array) {
	    var max = null;

	    collection.deepForEach(array, function (value) {
	      if (max === null || math.larger(value, max)) {
	        max = value;
	      }
	    });

	    if (max === null) {
	      throw new Error('Cannot calculate max of an empty array');
	    }

	    return max;
	  }
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var Matrix = require(10),
	      collection = require(13),

	      isCollection = collection.isCollection;

	   
	  math.mean = function mean(args) {
	    if (arguments.length == 0) {
	      throw new SyntaxError('Function mean requires one or more parameters (0 provided)');
	    }

	    if (isCollection(args)) {
	      if (arguments.length == 1) {
	         
	        return _mean(args);
	      }
	      else if (arguments.length == 2) {
	         
	        return _nmean(arguments[0], arguments[1]);
	      }
	      else {
	        throw new SyntaxError('Wrong number of parameters');
	      }
	    }
	    else {
	       
	      return _mean(arguments);
	    }
	  };

	   
	  function _nmean(array, dim){
		  var sum;
		  sum = collection.reduce(array, dim, math.add);
		  return math.divide(sum, size(array)[dim]);
	  };

	   
	  function _mean(array) {
	    var sum = 0;
	    var num = 0;

	    collection.deepForEach(array, function (value) {
	      sum = math.add(sum, value);
	      num++;
	    });

	    if (num === 0) {
	      throw new Error('Cannot calculate mean of an empty array');
	    }

	    return math.divide(sum, num);
	  }
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var util = require(117),

	      BigNumber = require(220),
	      Complex = require(7),
	      collection = require(13),

	      isNumber = util.number.isNumber,
	      isBoolean = util['boolean'].isBoolean,
	      isComplex = Complex.isComplex,
	      isCollection = collection.isCollection;

	   
	  math.acos = function acos(x) {
	    if (arguments.length != 1) {
	      throw new math.error.ArgumentsError('acos', arguments.length, 1);
	    }

	    if (isNumber(x)) {
	      if (x >= -1 && x <= 1) {
	        return Math.acos(x);
	      }
	      else {
	        return acos(new Complex(x, 0));
	      }
	    }

	    if (isComplex(x)) {
	       
	      var temp1 = new Complex(
	          x.im * x.im - x.re * x.re + 1.0,
	          -2.0 * x.re * x.im
	      );
	      var temp2 = math.sqrt(temp1);
	      var temp3;
	      if (temp2 instanceof Complex) {
	        temp3 = new Complex(
	            temp2.re - x.im,
	            temp2.im + x.re
	        )
	      }
	      else {
	        temp3 = new Complex(
	            temp2 - x.im,
	            x.re
	        )
	      }
	      var temp4 = math.log(temp3);

	       
	      if (temp4 instanceof Complex) {
	        return new Complex(
	            1.57079632679489661923 - temp4.im,
	            temp4.re
	        );
	      }
	      else {
	        return new Complex(
	            1.57079632679489661923,
	            temp4
	        );
	      }
	    }

	    if (isCollection(x)) {
	      return collection.deepMap(x, acos);
	    }

	    if (isBoolean(x)) {
	      return Math.acos(x);
	    }

	    if (x instanceof BigNumber) {
	       
	       
	      return acos(util.number.toNumber(x));
	    }

	    throw new math.error.UnsupportedTypeError('acos', x);
	  };
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var util = require(117),

	      BigNumber = require(220),
	      Complex = require(7),
	      collection = require(13),

	      isNumber = util.number.isNumber,
	      isBoolean = util['boolean'].isBoolean,
	      isComplex = Complex.isComplex,
	      isCollection = collection.isCollection;

	   
	  math.asin = function asin(x) {
	    if (arguments.length != 1) {
	      throw new math.error.ArgumentsError('asin', arguments.length, 1);
	    }

	    if (isNumber(x)) {
	      if (x >= -1 && x <= 1) {
	        return Math.asin(x);
	      }
	      else {
	        return asin(new Complex(x, 0));
	      }
	    }

	    if (isComplex(x)) {
	       
	      var re = x.re;
	      var im = x.im;
	      var temp1 = new Complex(
	          im * im - re * re + 1.0,
	          -2.0 * re * im
	      );

	      var temp2 = math.sqrt(temp1);
	      var temp3;
	      if (temp2 instanceof Complex) {
	        temp3 = new Complex(
	            temp2.re - im,
	            temp2.im + re
	        );
	      }
	      else {
	        temp3 = new Complex(
	            temp2 - im,
	            re
	        );
	      }

	      var temp4 = math.log(temp3);

	      if (temp4 instanceof Complex) {
	        return new Complex(temp4.im, -temp4.re);
	      }
	      else {
	        return new Complex(0, -temp4);
	      }
	    }

	    if (isCollection(x)) {
	      return collection.deepMap(x, asin);
	    }

	    if (isBoolean(x)) {
	      return Math.asin(x);
	    }

	    if (x instanceof BigNumber) {
	       
	       
	      return asin(util.number.toNumber(x));
	    }

	    throw new math.error.UnsupportedTypeError('asin', x);
	  };
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var util = require(117),

	      BigNumber = require(220),
	      Complex = require(7),
	      collection = require(13),

	      isNumber = util.number.isNumber,
	      isBoolean = util['boolean'].isBoolean,
	      isComplex = Complex.isComplex,
	      isCollection = collection.isCollection;

	   
	  math.atan = function atan(x) {
	    if (arguments.length != 1) {
	      throw new math.error.ArgumentsError('atan', arguments.length, 1);
	    }

	    if (isNumber(x)) {
	      return Math.atan(x);
	    }

	    if (isComplex(x)) {
	       
	      var re = x.re;
	      var im = x.im;
	      var den = re * re + (1.0 - im) * (1.0 - im);

	      var temp1 = new Complex(
	          (1.0 - im * im - re * re) / den,
	          (-2.0 * re) / den
	      );
	      var temp2 = math.log(temp1);

	      if (temp2 instanceof Complex) {
	        return new Complex(
	            -0.5 * temp2.im,
	            0.5 * temp2.re
	        );
	      }
	      else {
	        return new Complex(
	            0,
	            0.5 * temp2
	        );
	      }
	    }

	    if (isCollection(x)) {
	      return collection.deepMap(x, atan);
	    }

	    if (isBoolean(x)) {
	      return Math.atan(x);
	    }

	    if (x instanceof BigNumber) {
	       
	       
	      return atan(util.number.toNumber(x));
	    }

	    throw new math.error.UnsupportedTypeError('atan', x);
	  };
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var util = require(117),

	      BigNumber = require(220),
	      Complex = require(7),
	      collection = require(13),

	      toNumber = util.number.toNumber,
	      isNumber = util.number.isNumber,
	      isBoolean = util['boolean'].isBoolean,
	      isComplex = Complex.isComplex,
	      isCollection = collection.isCollection;

	   
	  math.atan2 = function atan2(y, x) {
	    if (arguments.length != 2) {
	      throw new math.error.ArgumentsError('atan2', arguments.length, 2);
	    }

	    if (isNumber(y)) {
	      if (isNumber(x)) {
	        return Math.atan2(y, x);
	      }
	       
	    }
	    else if (isComplex(y)) {
	      if (isNumber(x)) {
	        return Math.atan2(y.re, x);
	      }
	       
	    }

	    if (isCollection(y) || isCollection(x)) {
	      return collection.deepMap2(y, x, atan2);
	    }

	    if (isBoolean(y)) {
	      return atan2(+y, x);
	    }
	    if (isBoolean(x)) {
	      return atan2(y, +x);
	    }

	     
	    if (y instanceof BigNumber) {
	      return atan2(toNumber(y), x);
	    }
	    if (x instanceof BigNumber) {
	      return atan2(y, toNumber(x));
	    }

	    throw new math.error.UnsupportedTypeError('atan2', y, x);
	  };
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var util = require(117),

	      BigNumber = require(220),
	      Complex = require(7),
	      Unit = require(11),
	      collection = require(13),

	      isNumber = util.number.isNumber,
	      isBoolean = util['boolean'].isBoolean,
	      isComplex = Complex.isComplex,
	      isUnit = Unit.isUnit,
	      isCollection = collection.isCollection;

	   
	  math.cos = function cos(x) {
	    if (arguments.length != 1) {
	      throw new math.error.ArgumentsError('cos', arguments.length, 1);
	    }

	    if (isNumber(x)) {
	      return Math.cos(x);
	    }

	    if (isComplex(x)) {
	       
	      return new Complex(
	          0.5 * Math.cos(x.re) * (Math.exp(-x.im) + Math.exp(x.im)),
	          0.5 * Math.sin(x.re) * (Math.exp(-x.im) - Math.exp(x.im))
	      );
	    }

	    if (isUnit(x)) {
	      if (!x.hasBase(Unit.BASE_UNITS.ANGLE)) {
	        throw new TypeError ('Unit in function cos is no angle');
	      }
	      return Math.cos(x.value);
	    }

	    if (isCollection(x)) {
	      return collection.deepMap(x, cos);
	    }

	    if (isBoolean(x)) {
	      return Math.cos(x);
	    }

	    if (x instanceof BigNumber) {
	       
	       
	      return cos(util.number.toNumber(x));
	    }

	    throw new math.error.UnsupportedTypeError('cos', x);
	  };
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var util = require(117),

	      BigNumber = require(220),
	      Complex = require(7),
	      Unit = require(11),
	      collection = require(13),

	      isNumber = util.number.isNumber,
	      isBoolean = util['boolean'].isBoolean,
	      isComplex = Complex.isComplex,
	      isUnit = Unit.isUnit,
	      isCollection = collection.isCollection;

	   
	  math.cot = function cot(x) {
	    if (arguments.length != 1) {
	      throw new math.error.ArgumentsError('cot', arguments.length, 1);
	    }

	    if (isNumber(x)) {
	      return 1 / Math.tan(x);
	    }

	    if (isComplex(x)) {
	      var den = Math.exp(-4.0 * x.im) -
	          2.0 * Math.exp(-2.0 * x.im) * Math.cos(2.0 * x.re) + 1.0;

	      return new Complex(
	          2.0 * Math.exp(-2.0 * x.im) * Math.sin(2.0 * x.re) / den,
	          (Math.exp(-4.0 * x.im) - 1.0) / den
	      );
	    }

	    if (isUnit(x)) {
	      if (!x.hasBase(Unit.BASE_UNITS.ANGLE)) {
	        throw new TypeError ('Unit in function cot is no angle');
	      }
	      return 1 / Math.tan(x.value);
	    }

	    if (isCollection(x)) {
	      return collection.deepMap(x, cot);
	    }

	    if (isBoolean(x)) {
	      return cot(+x);
	    }

	    if (x instanceof BigNumber) {
	       
	       
	      return cot(util.number.toNumber(x));
	    }

	    throw new math.error.UnsupportedTypeError('cot', x);
	  };
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var util = require(117),

	      BigNumber = require(220),
	      Complex = require(7),
	      Unit = require(11),
	      collection = require(13),

	      isNumber = util.number.isNumber,
	      isBoolean = util['boolean'].isBoolean,
	      isComplex = Complex.isComplex,
	      isUnit = Unit.isUnit,
	      isCollection = collection.isCollection;

	   
	  math.csc = function csc(x) {
	    if (arguments.length != 1) {
	      throw new math.error.ArgumentsError('csc', arguments.length, 1);
	    }

	    if (isNumber(x)) {
	      return 1 / Math.sin(x);
	    }

	    if (isComplex(x)) {
	       
	      var den = 0.25 * (Math.exp(-2.0 * x.im) + Math.exp(2.0 * x.im)) -
	          0.5 * Math.cos(2.0 * x.re);

	      return new Complex (
	          0.5 * Math.sin(x.re) * (Math.exp(-x.im) + Math.exp(x.im)) / den,
	          0.5 * Math.cos(x.re) * (Math.exp(-x.im) - Math.exp(x.im)) / den
	      );
	    }

	    if (isUnit(x)) {
	      if (!x.hasBase(Unit.BASE_UNITS.ANGLE)) {
	        throw new TypeError ('Unit in function csc is no angle');
	      }
	      return 1 / Math.sin(x.value);
	    }

	    if (isCollection(x)) {
	      return collection.deepMap(x, csc);
	    }

	    if (isBoolean(x)) {
	      return csc(+x);
	    }

	    if (x instanceof BigNumber) {
	       
	       
	      return csc(util.number.toNumber(x));
	    }

	    throw new math.error.UnsupportedTypeError('csc', x);
	  };
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var util = require(117),

	      BigNumber = require(220),
	      Complex = require(7),
	      Unit = require(11),
	      collection = require(13),

	      isNumber = util.number.isNumber,
	      isBoolean = util['boolean'].isBoolean,
	      isComplex = Complex.isComplex,
	      isUnit = Unit.isUnit,
	      isCollection = collection.isCollection;

	   
	  math.sec = function sec(x) {
	    if (arguments.length != 1) {
	      throw new math.error.ArgumentsError('sec', arguments.length, 1);
	    }

	    if (isNumber(x)) {
	      return 1 / Math.cos(x);
	    }

	    if (isComplex(x)) {
	       
	      var den = 0.25 * (Math.exp(-2.0 * x.im) + Math.exp(2.0 * x.im)) +
	          0.5 * Math.cos(2.0 * x.re);
	      return new Complex(
	          0.5 * Math.cos(x.re) * (Math.exp(-x.im) + Math.exp( x.im)) / den,
	          0.5 * Math.sin(x.re) * (Math.exp( x.im) - Math.exp(-x.im)) / den
	      );
	    }

	    if (isUnit(x)) {
	      if (!x.hasBase(Unit.BASE_UNITS.ANGLE)) {
	        throw new TypeError ('Unit in function sec is no angle');
	      }
	      return 1 / Math.cos(x.value);
	    }

	    if (isCollection(x)) {
	      return collection.deepMap(x, sec);
	    }

	    if (isBoolean(x)) {
	      return sec(+x);
	    }

	    if (x instanceof BigNumber) {
	       
	       
	      return sec(util.number.toNumber(x));
	    }

	    throw new math.error.UnsupportedTypeError('sec', x);
	  };
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var util = require(117),

	      BigNumber = require(220),
	      Complex = require(7),
	      Unit = require(11),
	      collection = require(13),

	      isNumber = util.number.isNumber,
	      isBoolean = util['boolean'].isBoolean,
	      isComplex = Complex.isComplex,
	      isUnit = Unit.isUnit,
	      isCollection = collection.isCollection;

	   
	  math.sin = function sin(x) {
	    if (arguments.length != 1) {
	      throw new math.error.ArgumentsError('sin', arguments.length, 1);
	    }

	    if (isNumber(x)) {
	      return Math.sin(x);
	    }

	    if (isComplex(x)) {
	      return new Complex(
	          0.5 * Math.sin(x.re) * (Math.exp(-x.im) + Math.exp( x.im)),
	          0.5 * Math.cos(x.re) * (Math.exp( x.im) - Math.exp(-x.im))
	      );
	    }

	    if (isUnit(x)) {
	      if (!x.hasBase(Unit.BASE_UNITS.ANGLE)) {
	        throw new TypeError ('Unit in function sin is no angle');
	      }
	      return Math.sin(x.value);
	    }

	    if (isCollection(x)) {
	      return collection.deepMap(x, sin);
	    }

	    if (isBoolean(x)) {
	      return Math.sin(x);
	    }

	    if (x instanceof BigNumber) {
	       
	       
	      return sin(util.number.toNumber(x));
	    }

	    throw new math.error.UnsupportedTypeError('sin', x);
	  };
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var util = require(117),

	      BigNumber = require(220),
	      Complex = require(7),
	      Unit = require(11),
	      collection = require(13),

	      isNumber = util.number.isNumber,
	      isBoolean = util['boolean'].isBoolean,
	      isComplex = Complex.isComplex,
	      isUnit = Unit.isUnit,
	      isCollection = collection.isCollection;

	   
	  math.tan = function tan(x) {
	    if (arguments.length != 1) {
	      throw new math.error.ArgumentsError('tan', arguments.length, 1);
	    }

	    if (isNumber(x)) {
	      return Math.tan(x);
	    }

	    if (isComplex(x)) {
	      var den = Math.exp(-4.0 * x.im) +
	          2.0 * Math.exp(-2.0 * x.im) * Math.cos(2.0 * x.re) +
	          1.0;

	      return new Complex(
	          2.0 * Math.exp(-2.0 * x.im) * Math.sin(2.0 * x.re) / den,
	          (1.0 - Math.exp(-4.0 * x.im)) / den
	      );
	    }

	    if (isUnit(x)) {
	      if (!x.hasBase(Unit.BASE_UNITS.ANGLE)) {
	        throw new TypeError ('Unit in function tan is no angle');
	      }
	      return Math.tan(x.value);
	    }

	    if (isCollection(x)) {
	      return collection.deepMap(x, tan);
	    }

	    if (isBoolean(x)) {
	      return Math.tan(x);
	    }

	    if (x instanceof BigNumber) {
	       
	       
	      return tan(util.number.toNumber(x));
	    }

	    throw new math.error.UnsupportedTypeError('tan', x);
	  };
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var util = require(117),

	      Unit = require(11),
	      collection = require(13),

	      isString = util.string.isString,
	      isUnit = Unit.isUnit,
	      isCollection = collection.isCollection;

	   
	  math.to = function to(x, unit) {
	    if (arguments.length != 2) {
	      throw new math.error.ArgumentsError('to', arguments.length, 2);
	    }

	    if (isUnit(x)) {
	      if (isUnit(unit) || isString(unit)) {
	        return x.to(unit);
	      }
	    }

	     

	    if (isCollection(x) || isCollection(unit)) {
	      return collection.deepMap2(x, unit, to);
	    }

	    throw new math.error.UnsupportedTypeError('to', x, unit);
	  };
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var object = require(2);

	   
	  math.clone = function clone (x) {
	    if (arguments.length != 1) {
	      throw new math.error.ArgumentsError('clone', arguments.length, 1);
	    }

	    return object.clone(x);
	  };
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var string = require(218);

	   
	  math.format = function format (value, options) {
	    var num = arguments.length;
	    if (num !== 1 && num !== 2) {
	      throw new math.error.ArgumentsError('format', num, 1, 2);
	    }

	    return string.format(value, options);
	  };
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var util = require(117),

	      Complex = require(7),
	      Unit = require(11),

	      isNumber = util.number.isNumber,
	      isString = util.string.isString,
	      isComplex = Complex.isComplex,
	      isUnit = Unit.isUnit;

	   
	 
	  math['import'] = function math_import(object, options) {
	    var name;
	    var opts = {
	      override: false,
	      wrap: true
	    };
	    if (options && options instanceof Object) {
	      util.object.extend(opts, options);
	    }

	    if (isString(object)) {
	       
	      if (true) {
	         
	        var _module = require(219)(object);
	        math_import(_module);
	      }
	      else {
	        throw new Error('Cannot load file: require not available.');
	      }
	    }
	    else if (isSupportedType(object)) {
	       
	      name = object.name;
	      if (name) {
	        if (opts.override || math[name] === undefined) {
	          _import(name, object, opts);
	        }
	      }
	      else {
	        throw new Error('Cannot import an unnamed function or object');
	      }
	    }
	    else if (object instanceof Object) {
	       
	      for (name in object) {
	        if (object.hasOwnProperty(name)) {
	          var value = object[name];
	          if (isSupportedType(value)) {
	            _import(name, value, opts);
	          }
	          else {
	            math_import(value);
	          }
	        }
	      }
	    }
	  };

	   
	  function _import(name, value, options) {
	    if (options.override || math[name] === undefined) {
	       
	      if (options.wrap && typeof value === 'function') {
	         
	        math[name] = function () {
	          var args = [];
	          for (var i = 0, len = arguments.length; i < len; i++) {
	            args[i] = arguments[i].valueOf();
	          }
	          return value.apply(math, args);
	        };
	      }
	      else {
	         
	        math[name] = value;
	      }

	       
	      math.chaining.Selector.createProxy(name, value);
	    }
	  }

	   
	  function isSupportedType(object) {
	    return (typeof object == 'function') ||
	        isNumber(object) || isString(object) ||
	        isComplex(object) || isUnit(object);
	     
	  }
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var isMatrix = require(10).isMatrix;

	   
	  math.map = function (x, callback) {
	    if (arguments.length != 2) {
	      throw new math.error.ArgumentsError('map', arguments.length, 2);
	    }

	    if (Array.isArray(x)) {
	      return _mapArray(x, callback);
	    } else if (isMatrix(x)) {
	      return x.map(callback);
	    } else {
	      throw new math.error.UnsupportedTypeError('map', x);
	    }
	  };

	  function _mapArray (arrayIn, callback) {
	    var index = [];
	    var recurse = function (value, dim) {
	      if (Array.isArray(value)) {
	        return value.map(function (child, i) {
	          index[dim] = i;
	          return recurse(child, dim + 1);
	        });
	      }
	      else {
	        return callback(value, index, arrayIn);
	      }
	    };

	    return recurse(arrayIn, 0);
	  };
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var string = require(218),

	      isString = string.isString;

	   
	  math.print = function print (template, values, precision) {
	    var num = arguments.length;
	    if (num != 2 && num != 3) {
	      throw new math.error.ArgumentsError('print', num, 2, 3);
	    }

	    if (!isString(template)) {
	      throw new TypeError('String expected as first parameter in function format');
	    }
	    if (!(values instanceof Object)) {
	      throw new TypeError('Object expected as second parameter in function format');
	    }

	     
	    return template.replace(/\$([\w\.]+)/g, function (original, key) {
	          var keys = key.split('.');
	          var value = values[keys.shift()];
	          while (keys.length && value !== undefined) {
	            var k = keys.shift();
	            value = k ? value[k] : value + '.';
	          }

	          if (value !== undefined) {
	            if (!isString(value)) {
	              return math.format(value, precision);
	            }
	            else {
	              return value;
	            }
	          }

	          return original;
	        }
	    );
	  };
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var types = require(217),

	      BigNumber = require(220),
	      Complex = require(7),
	      Matrix = require(10),
	      Unit = require(11),
	      Index = require(9),
	      Range = require(8),
	      Help = require(12);

	   
	  math['typeof'] = function _typeof (x) {
	    if (arguments.length != 1) {
	      throw new math.error.ArgumentsError('typeof', arguments.length, 1);
	    }

	     
	    var type = types.type(x);

	     
	    if (type === 'object') {
	      if (x instanceof Complex) return 'complex';
	      if (x instanceof BigNumber) return 'bignumber';
	      if (x instanceof Matrix) return 'matrix';
	      if (x instanceof Unit) return 'unit';
	      if (x instanceof Index) return 'index';
	      if (x instanceof Range) return 'range';
	      if (x instanceof Help) return 'matrix';

	      if (x instanceof math.chaining.Selector) return 'selector';
	    }

	    return type;
	  };
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var isMatrix = require(10).isMatrix;

	   
	  math.forEach = function (x, callback) {
	    if (arguments.length != 2) {
	      throw new math.error.ArgumentsError('forEach', arguments.length, 2);
	    }

	    if (Array.isArray(x)) {
	      return _forEachArray(x, callback);
	    } else if (isMatrix(x)) {
	      return x.forEach(callback);
	    } else {
	      throw new math.error.UnsupportedTypeError('forEach', x);
	    }
	  };

	  function _forEachArray (array, callback) {
	    var index = [];
	    var recurse = function (value, dim) {
	      if (Array.isArray(value)) {
	        value.forEach(function (child, i) {
	          index[dim] = i;  
	          recurse(child, dim + 1);
	        });
	      }
	      else {
	        callback(value, index, array);
	      }
	    };
	    recurse(array, 0);
	  };

	};

  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var Complex = require(7);

	  math.pi          = Math.PI;
	  math.e           = Math.E;
	  math.tau         = Math.PI * 2;
	  math.i           = new Complex(0, 1);

	  math['Infinity'] = Infinity;
	  math['NaN']      = NaN;
	  math['true']     = true;
	  math['false']    = false;

	   
	  math.E           = Math.E;
	  math.LN2         = Math.LN2;
	  math.LN10        = Math.LN10;
	  math.LOG2E       = Math.LOG2E;
	  math.LOG10E      = Math.LOG10E;
	  math.PI          = Math.PI;
	  math.SQRT1_2     = Math.SQRT1_2;
	  math.SQRT2       = Math.SQRT2;
	};


  },
 
  function(module, exports, require) {

	module.exports = function (math) {
	  var string = require(218);

	   
	  function Selector (value) {
	    if (!(this instanceof Selector)) {
	      throw new SyntaxError(
	          'Selector constructor must be called with the new operator');
	    }

	    if (value instanceof Selector) {
	      this.value = value.value;
	    }
	    else {
	      this.value = value;
	    }
	  }

	   
	  Selector.prototype.done = function () {
	    return this.value;
	  };

	   
	  Selector.prototype.valueOf = function () {
	    return this.value;
	  };

	   
	  Selector.prototype.toString = function () {
	    return string.format(this.value);
	  };

	   
	  function createProxy(name, value) {
	    var slice = Array.prototype.slice;
	    if (typeof value === 'function') {
	       
	      Selector.prototype[name] = function () {
	        var args = [this.value].concat(slice.call(arguments, 0));
	        return new Selector(value.apply(this, args));
	      }
	    }
	    else {
	       
	      Selector.prototype[name] = new Selector(value);
	    }
	  }

	  Selector.createProxy = createProxy;

	   
	  for (var prop in math) {
	    if (math.hasOwnProperty(prop) && prop) {
	      createProxy(prop, math[prop]);
	    }
	  }

	  return Selector;
	};


  },
 
  function(module, exports, require) {

	var Node = require(110),
	    object = require(2),
	    string = require(218),
	    collection = require(13),
	    Matrix = require(10);

	 
	function ArrayNode(nodes) {
	  this.nodes = nodes || [];
	}

	ArrayNode.prototype = new Node();

	 
	ArrayNode.prototype._compile = function (defs) {
	  var asMatrix = (defs.math.config().matrix !== 'array');

	  var nodes = this.nodes.map(function (node) {
	    return node._compile(defs);
	  });

	  return (asMatrix ? 'math.matrix([' : '[') +
	      nodes.join(',') +
	      (asMatrix ? '])' : ']');
	};

	 
	ArrayNode.prototype.find = function (filter) {
	  var results = [];

	   
	  if (this.match(filter)) {
	    results.push(this);
	  }

	   
	  var nodes = this.nodes;
	  for (var r = 0, rows = nodes.length; r < rows; r++) {
	    var nodes_r = nodes[r];
	    for (var c = 0, cols = nodes_r.length; c < cols; c++) {
	      results = results.concat(nodes_r[c].find(filter));
	    }
	  }

	  return results;
	};

	 
	ArrayNode.prototype.toString = function() {
	  return string.format(this.nodes);
	};

	module.exports = ArrayNode;


  },
 
  function(module, exports, require) {

	var Node = require(110);

	 
	function AssignmentNode(name, expr) {
	  this.name = name;
	  this.expr = expr;
	}

	AssignmentNode.prototype = new Node();

	 
	AssignmentNode.prototype._compile = function (defs) {
	  return 'scope["' + this.name + '"] = ' + this.expr._compile(defs) + '';
	};

	 
	AssignmentNode.prototype.find = function (filter) {
	  var nodes = [];

	   
	  if (this.match(filter)) {
	    nodes.push(this);
	  }

	   
	  if (this.expr) {
	    nodes = nodes.concat(this.expr.find(filter));
	  }

	  return nodes;
	};

	 
	AssignmentNode.prototype.toString = function() {
	  return this.name + ' = ' + this.expr.toString();
	};

	module.exports = AssignmentNode;

  },
 
  function(module, exports, require) {

	var Node = require(110);

	 
	function BlockNode() {
	  this.params = [];
	}

	BlockNode.prototype = new Node();

	 
	BlockNode.prototype.add = function (node, visible) {
	  var index = this.params.length;
	  this.params[index] = {
	    node: node,
	    visible: (visible != undefined) ? visible : true
	  };
	};

	 
	BlockNode.prototype._compile = function (defs) {
	  var params = this.params.map(function (param) {
	    var js = param.node._compile(defs);
	    if (param.visible) {
	      return 'results.push(' + js + ');';
	    }
	    else {
	      return js + ';';
	    }
	  });

	  return '(function () {' +
	      'var results = [];' +
	      params.join('') +
	      'return results;' +
	      '})()';
	};

	 
	BlockNode.prototype.find = function (filter) {
	  var nodes = [];

	   
	  if (this.match(filter)) {
	    nodes.push(this);
	  }

	   
	  var params = this.params;
	  if (params) {
	    for (var i = 0, len = params.length; i < len; i++) {
	      nodes = nodes.concat(params[i].node.find(filter));
	    }
	  }

	  return nodes;
	};

	 
	BlockNode.prototype.toString = function() {
	  return this.params.map(function (param) {
	    return param.node.toString() + (param.visible ? '' : ';');
	  }).join('\n');
	};

	module.exports = BlockNode;


  },
 
  function(module, exports, require) {

	var Node = require(110),
	    Complex = require(7),
	    BigNumber = require(220),
	    string = require(218),
	    isString = string.isString;

	 
	function ConstantNode(type, value) {
	  if (!isString(type)) {
	    throw new TypeError('Constant type must be a string')
	  }

	  if (!isString(value)) {
	    throw new TypeError('Constant value must be a string')
	  }

	  this.type = type;
	  this.value = value;
	}

	ConstantNode.prototype = new Node();

	 
	ConstantNode.prototype._compile = function (defs) {
	  switch (this.type) {
	    case 'number':
	      if (defs.math.config().number === 'bignumber') {
	        return 'math.bignumber("' + this.value + '")';
	      }
	      else {
	         
	        return this.value.replace(/^(0*)[0-9]/, function (match, zeros) {
	          return match.substring(zeros.length);
	        });
	      }

	    case 'string':
	      return '"' + this.value + '"';

	    case 'complex':
	      return 'math.complex(0, ' + this.value + ')';

	    case 'boolean':
	      return this.value;

	    case 'undefined':
	      return this.value;

	    case 'null':
	      return this.value;

	    default:
	      throw new TypeError('Unsupported type of constant "' + this.type + '"');
	  }
	};

	 
	ConstantNode.prototype.toString = function() {
	  switch (this.type) {
	    case 'string':
	      return '"' + this.value + '"';

	    case 'complex':
	      return this.value + 'i';

	    default:
	      return this.value;
	  }
	};

	module.exports = ConstantNode;


  },
 
  function(module, exports, require) {

	var number= require(221),

	    Node = require(110),
	    RangeNode = require(113),
	    SymbolNode = require(114),

	    BigNumber = require(220),
	    Index = require(9),
	    Range = require(8),

	    isNumber = number.isNumber,
	    toNumber = number.toNumber;

	 
	function IndexNode (object, ranges) {
	  this.object = object;
	  this.ranges = ranges;
	}

	IndexNode.prototype = new Node();

	 
	IndexNode.prototype._compile = function (defs) {
	  return this.compileSubset(defs);
	};

	 
	IndexNode.prototype.compileSubset = function compileIndex (defs, replacement) {
	   
	  var filter = {
	    type: SymbolNode,
	    properties: {
	      name: 'end'
	    }
	  };
	  var rangesUseEnd = this.ranges.map(function (range) {
	    return range.find(filter).length > 0;
	  });

	   
	   

	   
	   

	  var ranges = this.ranges.map(function(range, i) {
	    var useEnd = rangesUseEnd[i];
	    if (range instanceof RangeNode) {
	      if (useEnd) {
	         
	        return '(function (scope) {' +
	            '  scope = Object.create(scope); ' +
	            '  scope["end"] = size[' + i + '];' +
	            '  var step = ' + (range.step ? range.step._compile(defs) : '1') + ';' +
	            '  return [' +
	            '    ' + range.start._compile(defs) + ' - 1, ' +
	            '    ' + range.end._compile(defs) + ' - (step > 0 ? 0 : 2), ' +
	            '    step' +
	            '  ];' +
	            '})(scope)';
	      }
	      else {
	         
	        return '(function () {' +
	            '  var step = ' + (range.step ? range.step._compile(defs) : '1') + ';' +
	            '  return [' +
	            '    ' + range.start._compile(defs) + ' - 1, ' +
	            '    ' + range.end._compile(defs) + ' - (step > 0 ? 0 : 2), ' +
	            '    step' +
	            '  ];' +
	            '})()';
	      }
	    }
	    else {
	      if (useEnd) {
	         
	        return '(function (scope) {' +
	            '  scope = Object.create(scope); ' +
	            '  scope["end"] = size[' + i + '];' +
	            '  return ' + range._compile(defs) + ' - 1;' +
	            '})(scope)'
	      }
	      else {
	         
	        return range._compile(defs) + ' - 1';
	      }
	    }
	  });

	   
	  var someUseEnd = ranges.some(function (useEnd) {
	    return useEnd;
	  });
	  if (someUseEnd) {
	    return '(function () {' +
	        '  var obj = ' + this.object._compile(defs) + ';' +
	        '  var size = math.size(obj).valueOf();' +
	        '  return math.subset(' +
	        '    obj, ' +
	        '    math.index(' + ranges.join(', ') + ')' +
	        '    ' + (replacement ? (', ' + replacement) : '') +
	        '  );' +
	        '})()';
	  }
	  else {
	    return 'math.subset(' +
	        this.object._compile(defs) + ',' +
	        'math.index(' + ranges.join(', ') +
	        (replacement ? (', ' + replacement) : '') +
	        ')';
	  }
	};

	 
	IndexNode.prototype.find = function (filter) {
	  var nodes = [];

	   
	  if (this.match(filter)) {
	    nodes.push(this);
	  }

	   
	  if (this.object) {
	    nodes = nodes.concat(this.object.find(filter));
	  }

	   
	  var ranges = this.ranges;
	  if (ranges) {
	    for (var i = 0, len = ranges.length; i < len; i++) {
	      nodes = nodes.concat(ranges[i].find(filter));
	    }
	  }

	  return nodes;
	};

	 
	IndexNode.prototype.objectName = function objectName () {
	  return this.object.name;
	};

	 
	IndexNode.prototype.toString = function() {
	   
	  var str = this.object ? this.object.toString() : '';
	  if (this.ranges) {
	    str += '[' + this.ranges.join(', ') + ']';
	  }
	  return str;
	};

	module.exports = IndexNode;

  },
 
  function(module, exports, require) {

	var Node = require(110);

	 
	function FunctionNode(name, args, expr) {
	  this.name = name;
	  this.args = args;
	  this.expr = expr;
	}

	FunctionNode.prototype = new Node();

	 
	 
	FunctionNode.prototype._eval = function() {
	   
	  this.scope.set(this.name, this.fn);

	  return this.fn;
	};

	 
	FunctionNode.prototype._compile = function (defs) {

	   

	  return 'scope["' + this.name + '"] = ' +
	      '  (function (scope) {' +
	      '    scope = Object.create(scope); ' +
	      '    var fn = function ' + this.name + '(' + this.args.join(',') + ') {' +
	      '      if (arguments.length != ' + this.args.length + ') {' +
	      '        throw new SyntaxError("Wrong number of arguments in function ' + this.name + ' (" + arguments.length + " provided, ' + this.args.length + ' expected)");' +
	      '      }' +
	      this.args.map(function (variable, index) {
	        return 'scope["' + variable + '"] = arguments[' + index + '];';
	      }).join('') +
	      '      return ' + this.expr._compile(defs) + '' +
	      '    };' +
	      '    fn.syntax = "' + this.name + '(' + this.args.join(', ') + ')";' +
	      '    return fn;' +
	      '  })(scope);';
	};

	 
	FunctionNode.prototype.find = function (filter) {
	  var nodes = [];

	   
	  if (this.match(filter)) {
	    nodes.push(this);
	  }

	   
	  if (this.expr) {
	    nodes = nodes.concat(this.expr.find(filter));
	  }

	  return nodes;
	};

	 
	FunctionNode.prototype.toString = function() {
	  return 'function ' + this.name +
	      '(' + this.args.join(', ') + ') = ' +
	      this.expr.toString();
	};

	module.exports = FunctionNode;


  },
 
  function(module, exports, require) {

	 
	function Node() {}

	 
	 
	Node.prototype.eval = function () {
	  throw new Error('Node.eval is deprecated. ' +
	      'Use Node.compile(math).eval([scope]) instead.');
	};

	 
	Node.prototype.compile = function (math) {
	  if (typeof math !== 'object') {
	    throw new TypeError('Object expected as parameter math');
	  }

	   
	  var defs = {
	    math: math
	  };

	  var code = this._compile(defs);

	  var defsCode = Object.keys(defs).map(function (name) {
	    return '    var ' + name + ' = defs["' + name + '"];';
	  });

	  var factoryCode =
	      defsCode.join(' ') +
	      'return {' +
	      '  "eval": function (scope) {' +
	      '    scope = scope || {};' +
	      '    return ' + code + ';' +
	      '  }' +
	      '};';

	  var factory = new Function ('defs', factoryCode);
	  return factory(defs);
	};

	 
	Node.prototype._compile = function (defs) {
	  throw new Error('Cannot compile a Node interface');
	};

	 
	Node.prototype.find = function (filter) {
	  return this.match(filter) ? [this] : [];
	};

	 
	Node.prototype.match = function (filter) {
	  var match = true;

	  if (filter) {
	    if (filter.type && !(this instanceof filter.type)) {
	      match = false;
	    }
	    if (match && filter.properties) {
	      for (var prop in filter.properties) {
	        if (filter.properties.hasOwnProperty(prop)) {
	          if (this[prop] != filter.properties[prop]) {
	            match = false;
	            break;
	          }
	        }
	      }
	    }
	  }

	  return match;
	};

	 
	Node.prototype.toString = function() {
	  return '';
	};

	module.exports = Node;


  },
 
  function(module, exports, require) {

	var Node = require(110);

	 
	function OperatorNode (op, fn, params) {
	  this.op = op;
	  this.fn = fn;
	  this.params = params;
	}

	OperatorNode.prototype = new Node();

	 
	OperatorNode.prototype._compile = function (defs) {
	  if (!(this.fn in defs.math)) {
	    throw new Error('Function ' + this.fn + ' missing in provided namespace "math"');
	  }

	  var params = this.params.map(function (param) {
	    return param._compile(defs);
	  });
	  return 'math.' + this.fn + '(' + params.join(', ') + ')';
	};

	 
	OperatorNode.prototype.find = function (filter) {
	  var nodes = [];

	   
	  if (this.match(filter)) {
	    nodes.push(this);
	  }

	   
	  var params = this.params;
	  if (params) {
	    for (var i = 0, len = params.length; i < len; i++) {
	      nodes = nodes.concat(params[i].find(filter));
	    }
	  }

	  return nodes;
	};

	 
	OperatorNode.prototype.toString = function() {
	  var params = this.params;

	  switch (params.length) {
	    case 1:
	      if (this.op == '-') {
	         
	        return '-' + params[0].toString();
	      }
	      else {
	         
	        return params[0].toString() + this.op;
	      }

	    case 2:  
	      var lhs = params[0].toString();
	      if (params[0] instanceof OperatorNode) {
	        lhs = '(' + lhs + ')';
	      }
	      var rhs = params[1].toString();
	      if (params[1] instanceof OperatorNode) {
	        rhs = '(' + rhs + ')';
	      }
	      return lhs + ' ' + this.op + ' ' + rhs;

	    default:  
	      return this.op + '(' + this.params.join(', ') + ')';
	  }
	};

	module.exports = OperatorNode;


  },
 
  function(module, exports, require) {

	var number= require(221),

	    Node = require(110),
	    RangeNode = require(113),
	    SymbolNode = require(114),

	    BigNumber = require(220),
	    Index = require(9),
	    Range = require(8),

	    isNumber = number.isNumber,
	    toNumber = number.toNumber;

	 
	function ParamsNode (object, params) {
	  this.object = object;
	  this.params = params;
	}

	ParamsNode.prototype = new Node();

	 
	ParamsNode.prototype._compile = function (defs) {
	   
	  var params = this.params.map(function (param) {
	    return param._compile(defs);
	  });

	  return this.object._compile(defs) + '(' + params.join(', ') + ')';
	};

	 
	ParamsNode.prototype.find = function (filter) {
	  var nodes = [];

	   
	  if (this.match(filter)) {
	    nodes.push(this);
	  }

	   
	  if (this.object) {
	    nodes = nodes.concat(this.object.find(filter));
	  }

	   
	  var params = this.params;
	  if (params) {
	    for (var i = 0, len = params.length; i < len; i++) {
	      nodes = nodes.concat(params[i].find(filter));
	    }
	  }

	  return nodes;
	};

	 
	ParamsNode.prototype.toString = function() {
	   
	  var str = this.object ? this.object.toString() : '';
	  if (this.params) {
	    str += '(' + this.params.join(', ') + ')';
	  }
	  return str;
	};

	module.exports = ParamsNode;


  },
 
  function(module, exports, require) {

	var number = require(221),
	    Node = require(110),

	    BigNumber = require(220),
	    Range = require(8),
	    Matrix = require(10),

	    toNumber = number.toNumber;

	 
	function RangeNode (params) {
	  if (params.length != 2 && params.length != 3) {
	    throw new SyntaxError('Wrong number of arguments. ' +
	        'Expected [start, end] or [start, end, step]');
	  }

	  this.start = params[0];   
	  this.end   = params[1];   
	  this.step  = params[2];   
	}

	RangeNode.prototype = new Node();

	 
	RangeNode.prototype._compile = function (defs) {
	  return 'math.range(' +
	      this.start._compile(defs) + ', ' +
	      this.end._compile(defs) + ', ' +
	      (this.step ? (this.step._compile(defs) + ', ') : '') +
	      'true)';  
	};

	 
	RangeNode.prototype.find = function (filter) {
	  var nodes = [];

	   
	  if (this.match(filter)) {
	    nodes.push(this);
	  }

	   
	  if (this.start) {
	    nodes = nodes.concat(this.start.find(filter));
	  }
	  if (this.step) {
	    nodes = nodes.concat(this.step.find(filter));
	  }
	  if (this.end) {
	    nodes = nodes.concat(this.end.find(filter));
	  }

	  return nodes;
	};

	 
	RangeNode.prototype.toString = function() {
	   
	  var str = this.start.toString();
	  if (this.step) {
	    str += ':' + this.step.toString();
	  }
	  str += ':' + this.end.toString();

	  return str;
	};

	module.exports = RangeNode;


  },
 
  function(module, exports, require) {

	var Node = require(110),
	    Unit = require(11);

	 
	function SymbolNode(name) {
	  this.name = name;
	}

	SymbolNode.prototype = new Node();

	 
	SymbolNode.prototype._compile = function (defs) {
	   
	  defs['undef'] = undef;
	  defs['Unit'] = Unit;

	  return '(' +
	      'scope["' + this.name + '"] !== undefined ? scope["' + this.name + '"] : ' +
	      'math["' + this.name + '"] !== undefined ? math["' + this.name + '"] : ' +
	      (Unit.isPlainUnit(this.name) ?
	        'new Unit(null, "' + this.name + '")' :
	        'undef("' + this.name + '")') +
	      ')';
	};

	 
	function undef (name) {
	  throw new Error('Undefined symbol ' + name);
	}

	 
	SymbolNode.prototype.toString = function() {
	  return this.name;
	};

	module.exports = SymbolNode;


  },
 
  function(module, exports, require) {

	var Node = require(110),

	    BigNumber = require(220),
	    Complex = require(7),
	    Unit = require(11),

	    number = require(221),
	    toNumber = number.toNumber;

	 
	function UnitNode (value, unit) {
	  this.value = value;
	  this.unit = unit;
	}

	UnitNode.prototype = new Node();

	 
	UnitNode.prototype._compile = function (defs) {
	  return 'math.unit(' + this.value._compile(defs) + ', "' + this.unit + '")';
	};

	 
	UnitNode.prototype.find = function (filter) {
	  var nodes = [];

	   
	  if (this.match(filter)) {
	    nodes.push(this);
	  }

	   
	  nodes = nodes.concat(this.value.find(filter));

	  return nodes;
	};

	 
	UnitNode.prototype.toString = function() {
	  return this.value + ' ' + this.unit;
	};

	module.exports = UnitNode;


  },
 
  function(module, exports, require) {

	var number= require(221),

	    Node = require(110),
	    RangeNode = require(113),
	    IndexNode = require(108),
	    SymbolNode = require(114),

	    BigNumber = require(220),
	    Index = require(9),
	    Range = require(8),

	    isNumber = number.isNumber,
	    toNumber = number.toNumber;

	 
	function UpdateNode(index, expr) {
	  if (!(index instanceof IndexNode)) {
	    throw new TypeError('index mus be an IndexNode');
	  }

	  this.index = index;
	  this.expr = expr;
	}

	UpdateNode.prototype = new Node();

	 
	UpdateNode.prototype._compile = function (defs) {
	  return 'scope["' + this.index.objectName() + '\"] = ' +
	      this.index.compileSubset(defs,  this.expr._compile(defs));
	};

	 
	UpdateNode.prototype.find = function (filter) {
	  var nodes = [];

	   
	  if (this.match(filter)) {
	    nodes.push(this);
	  }

	   
	  var ranges = this.ranges;
	  if (ranges) {
	    for (var i = 0, len = ranges.length; i < len; i++) {
	      nodes = nodes.concat(ranges[i].find(filter));
	    }
	  }

	   
	  if (this.expr) {
	    nodes = nodes.concat(this.expr.find(filter));
	  }

	  return nodes;
	};

	 
	UpdateNode.prototype.toString = function() {
	  return this.index.toString() + ' = ' + this.expr.toString();
	};

	module.exports = UpdateNode;


  },
 
  function(module, exports, require) {

	exports.array = require(222);
	exports['boolean'] = require(223);
	exports.number = require(221);
	exports.object = require(2);
	exports.string = require(218);
	exports.types = require(217);


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'e',
	  'category': 'Constants',
	  'syntax': [
	    'e'
	  ],
	  'description': 'Euler\'s number, the base of the natural logarithm. Approximately equal to 2.71828',
	  'examples': [
	    'e',
	    'e ^ 2',
	    'exp(2)',
	    'log(e)'
	  ],
	  'seealso': ['exp']
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'false',
	  'category': 'Constants',
	  'syntax': [
	    'false'
	  ],
	  'description': 'Boolean value false',
	  'examples': [
	    'false'
	  ],
	  'seealso': ['true']
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'i',
	  'category': 'Constants',
	  'syntax': [
	    'i'
	  ],
	  'description': 'Imaginary unit, defined as i*i=-1. A complex number is described as a + b*i, where a is the real part, and b is the imaginary part.',
	  'examples': [
	    'i',
	    'i * i',
	    'sqrt(-1)'
	  ],
	  'seealso': []
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'Infinity',
	  'category': 'Constants',
	  'syntax': [
	    'Infinity'
	  ],
	  'description': 'Infinity, a number which is larger than the maximum number that can be handled by a floating point number.',
	  'examples': [
	    'Infinity',
	    '1 / 0'
	  ],
	  'seealso': []
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'LN2',
	  'category': 'Constants',
	  'syntax': [
	    'LN2'
	  ],
	  'description': 'Returns the natural logarithm of 2, approximately equal to 0.693',
	  'examples': [
	    'LN2',
	    'log(2)'
	  ],
	  'seealso': []
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'LN10',
	  'category': 'Constants',
	  'syntax': [
	    'LN10'
	  ],
	  'description': 'Returns the natural logarithm of 10, approximately equal to 2.302',
	  'examples': [
	    'LN10',
	    'log(10)'
	  ],
	  'seealso': []
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'LOG2E',
	  'category': 'Constants',
	  'syntax': [
	    'LOG2E'
	  ],
	  'description': 'Returns the base-2 logarithm of E, approximately equal to 1.442',
	  'examples': [
	    'LOG2E',
	    'log(e, 2)'
	  ],
	  'seealso': []
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'LOG10E',
	  'category': 'Constants',
	  'syntax': [
	    'LOG10E'
	  ],
	  'description': 'Returns the base-10 logarithm of E, approximately equal to 0.434',
	  'examples': [
	    'LOG10E',
	    'log(e, 10)'
	  ],
	  'seealso': []
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'NaN',
	  'category': 'Constants',
	  'syntax': [
	    'NaN'
	  ],
	  'description': 'Not a number',
	  'examples': [
	    'NaN',
	    '0 / 0'
	  ],
	  'seealso': []
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'pi',
	  'category': 'Constants',
	  'syntax': [
	    'pi'
	  ],
	  'description': 'The number pi is a mathematical constant that is the ratio of a circle\'s circumference to its diameter, and is approximately equal to 3.14159',
	  'examples': [
	    'pi',
	    'sin(pi/2)'
	  ],
	  'seealso': ['tau']
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'SQRT1_2',
	  'category': 'Constants',
	  'syntax': [
	    'SQRT1_2'
	  ],
	  'description': 'Returns the square root of 1/2, approximately equal to 0.707',
	  'examples': [
	    'SQRT1_2',
	    'sqrt(1/2)'
	  ],
	  'seealso': []
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'SQRT2',
	  'category': 'Constants',
	  'syntax': [
	    'SQRT2'
	  ],
	  'description': 'Returns the square root of 2, approximately equal to 1.414',
	  'examples': [
	    'SQRT2',
	    'sqrt(2)'
	  ],
	  'seealso': []
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'tau',
	  'category': 'Constants',
	  'syntax': [
	    'pi'
	  ],
	  'description': 'Tau is the ratio constant of a circle\'s circumference to radius, equal to 2 * pi, approximately 6.2832.',
	  'examples': [
	    'tau',
	    '2 * pi'
	  ],
	  'seealso': ['pi']
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'true',
	  'category': 'Constants',
	  'syntax': [
	    'true'
	  ],
	  'description': 'Boolean value true',
	  'examples': [
	    'true'
	  ],
	  'seealso': ['false']
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'abs',
	  'category': 'Arithmetic',
	  'syntax': [
	    'abs(x)'
	  ],
	  'description': 'Compute the absolute value.',
	  'examples': [
	    'abs(3.5)',
	    'abs(-4.2)'
	  ],
	  'seealso': ['sign']
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'add',
	  'category': 'Operators',
	  'syntax': [
	    'x + y',
	    'add(x, y)'
	  ],
	  'description': 'Add two values.',
	  'examples': [
	    '2.1 + 3.6',
	    'ans - 3.6',
	    '3 + 2i',
	    '"hello" + " world"',
	    '3 cm + 2 inch'
	  ],
	  'seealso': [
	    'subtract'
	  ]
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'ceil',
	  'category': 'Arithmetic',
	  'syntax': [
	    'ceil(x)'
	  ],
	  'description':
	      'Round a value towards plus infinity.If x is complex, both real and imaginary part are rounded towards plus infinity.',
	  'examples': [
	    'ceil(3.2)',
	    'ceil(3.8)',
	    'ceil(-4.2)'
	  ],
	  'seealso': ['floor', 'fix', 'round']
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'cube',
	  'category': 'Arithmetic',
	  'syntax': [
	    'cube(x)'
	  ],
	  'description': 'Compute the cube of a value. The cube of x is x * x * x.',
	  'examples': [
	    'cube(2)',
	    '2^3',
	    '2 * 2 * 2'
	  ],
	  'seealso': [
	    'multiply',
	    'square',
	    'pow'
	  ]
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'divide',
	  'category': 'Operators',
	  'syntax': [
	    'x / y',
	    'divide(x, y)'
	  ],
	  'description': 'Divide two values.',
	  'examples': [
	    '2 / 3',
	    'ans * 3',
	    '4.5 / 2',
	    '3 + 4 / 2',
	    '(3 + 4) / 2',
	    '18 km / 4.5'
	  ],
	  'seealso': [
	    'multiply'
	  ]
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'edivide',
	  'category': 'Operators',
	  'syntax': [
	    'x ./ y',
	    'edivide(x, y)'
	  ],
	  'description': 'divide two values element wise.',
	  'examples': [
	    'a = [1, 2, 3; 4, 5, 6]',
	    'b = [2, 1, 1; 3, 2, 5]',
	    'a ./ b'
	  ],
	  'seealso': [
	    'multiply',
	    'emultiply',
	    'divide'
	  ]
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'emultiply',
	  'category': 'Operators',
	  'syntax': [
	    'x .* y',
	    'emultiply(x, y)'
	  ],
	  'description': 'multiply two values element wise.',
	  'examples': [
	    'a = [1, 2, 3; 4, 5, 6]',
	    'b = [2, 1, 1; 3, 2, 5]',
	    'a .* b'
	  ],
	  'seealso': [
	    'multiply',
	    'divide',
	    'edivide'
	  ]
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'epow',
	  'category': 'Operators',
	  'syntax': [
	    'x .^ y',
	    'epow(x, y)'
	  ],
	  'description':
	      'Calculates the power of x to y element wise.',
	  'examples': [
	    'a = [1, 2, 3; 4, 5, 6]',
	    'a .^ 2'
	  ],
	  'seealso': [
	    'pow'
	  ]
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'equal',
	  'category': 'Operators',
	  'syntax': [
	    'x == y',
	    'equal(x, y)'
	  ],
	  'description':
	      'Check equality of two values. Returns 1 if the values are equal, and 0 if not.',
	  'examples': [
	    '2+2 == 3',
	    '2+2 == 4',
	    'a = 3.2',
	    'b = 6-2.8',
	    'a == b',
	    '50cm == 0.5m'
	  ],
	  'seealso': [
	    'unequal', 'smaller', 'larger', 'smallereq', 'largereq'
	  ]
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'exp',
	  'category': 'Arithmetic',
	  'syntax': [
	    'exp(x)'
	  ],
	  'description': 'Calculate the exponent of a value.',
	  'examples': [
	    'exp(1.3)',
	    'e ^ 1.3',
	    'log(exp(1.3))',
	    'x = 2.4',
	    '(exp(i*x) == cos(x) + i*sin(x))   # Euler\'s formula'
	  ],
	  'seealso': [
	    'square',
	    'multiply',
	    'log'
	  ]
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'fix',
	  'category': 'Arithmetic',
	  'syntax': [
	    'fix(x)'
	  ],
	  'description':
	      'Round a value towards zero.If x is complex, both real and imaginary part are rounded towards zero.',
	  'examples': [
	    'fix(3.2)',
	    'fix(3.8)',
	    'fix(-4.2)',
	    'fix(-4.8)'
	  ],
	  'seealso': ['ceil', 'floor', 'round']
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'floor',
	  'category': 'Arithmetic',
	  'syntax': [
	    'floor(x)'
	  ],
	  'description':
	      'Round a value towards minus infinity.If x is complex, both real and imaginary part are rounded towards minus infinity.',
	  'examples': [
	    'floor(3.2)',
	    'floor(3.8)',
	    'floor(-4.2)'
	  ],
	  'seealso': ['ceil', 'fix', 'round']
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'gcd',
	  'category': 'Arithmetic',
	  'syntax': [
	    'gcd(a, b)',
	    'gcd(a, b, c, ...)'
	  ],
	  'description': 'Compute the greatest common divisor.',
	  'examples': [
	    'gcd(8, 12)',
	    'gcd(-4, 6)',
	    'gcd(25, 15, -10)'
	  ],
	  'seealso': [ 'lcm', 'xgcd' ]
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'larger',
	  'category': 'Operators',
	  'syntax': [
	    'x > y',
	    'larger(x, y)'
	  ],
	  'description':
	      'Check if value x is larger than y. Returns 1 if x is larger than y, and 0 if not.',
	  'examples': [
	    '2 > 3',
	    '5 > 2*2',
	    'a = 3.3',
	    'b = 6-2.8',
	    '(a > b)',
	    '(b < a)',
	    '5 cm > 2 inch'
	  ],
	  'seealso': [
	    'equal', 'unequal', 'smaller', 'smallereq', 'largereq'
	  ]
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'largereq',
	  'category': 'Operators',
	  'syntax': [
	    'x >= y',
	    'largereq(x, y)'
	  ],
	  'description':
	      'Check if value x is larger or equal to y. Returns 1 if x is larger or equal to y, and 0 if not.',
	  'examples': [
	    '2 > 1+1',
	    '2 >= 1+1',
	    'a = 3.2',
	    'b = 6-2.8',
	    '(a > b)'
	  ],
	  'seealso': [
	    'equal', 'unequal', 'smallereq', 'smaller', 'largereq'
	  ]
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'lcm',
	  'category': 'Arithmetic',
	  'syntax': [
	    'lcm(x, y)'
	  ],
	  'description': 'Compute the least common multiple.',
	  'examples': [
	    'lcm(4, 6)',
	    'lcm(6, 21)',
	    'lcm(6, 21, 5)'
	  ],
	  'seealso': [ 'gcd' ]
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'log',
	  'category': 'Arithmetic',
	  'syntax': [
	    'log(x)',
	    'log(x, base)'
	  ],
	  'description': 'Compute the logarithm of a value. If no base is provided, the natural logarithm of x is calculated. If base if provided, the logarithm is calculated for the specified base. log(x, base) is defined as log(x) / log(base).',
	  'examples': [
	    'log(3.5)',
	    'a = log(2.4)',
	    'exp(a)',
	    '10 ^ 3',
	    'log(1000, 10)',
	    'log(1000) / log(10)',
	    'b = logb(1024, 2)',
	    '2 ^ b'
	  ],
	  'seealso': [
	    'exp',
	    'log10'
	  ]
	};

  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'log10',
	  'category': 'Arithmetic',
	  'syntax': [
	    'log10(x)'
	  ],
	  'description': 'Compute the 10-base logarithm of a value.',
	  'examples': [
	    'log10(1000)',
	    '10 ^ 3',
	    'log10(0.01)',
	    'log(1000) / log(10)',
	    'log(1000, 10)'
	  ],
	  'seealso': [
	    'exp',
	    'log'
	  ]
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'mod',
	  'category': 'Operators',
	  'syntax': [
	    'x % y',
	    'x mod y',
	    'mod(x, y)'
	  ],
	  'description':
	      'Calculates the modulus, the remainder of an integer division.',
	  'examples': [
	    '7 % 3',
	    '11 % 2',
	    '10 mod 4',
	    'function isOdd(x) = x % 2',
	    'isOdd(2)',
	    'isOdd(3)'
	  ],
	  'seealso': []
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'multiply',
	  'category': 'Operators',
	  'syntax': [
	    'x * y',
	    'multiply(x, y)'
	  ],
	  'description': 'multiply two values.',
	  'examples': [
	    '2.1 * 3.6',
	    'ans / 3.6',
	    '2 * 3 + 4',
	    '2 * (3 + 4)',
	    '3 * 2.1 km'
	  ],
	  'seealso': [
	    'divide'
	  ]
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'pow',
	  'category': 'Operators',
	  'syntax': [
	    'x ^ y',
	    'pow(x, y)'
	  ],
	  'description':
	      'Calculates the power of x to y, x^y.',
	  'examples': [
	    '2^3 = 8',
	    '2*2*2',
	    '1 + e ^ (pi * i)'
	  ],
	  'seealso': [
	    'unequal', 'smaller', 'larger', 'smallereq', 'largereq'
	  ]
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'round',
	  'category': 'Arithmetic',
	  'syntax': [
	    'round(x)',
	    'round(x, n)'
	  ],
	  'description':
	      'round a value towards the nearest integer.If x is complex, both real and imaginary part are rounded towards the nearest integer. When n is specified, the value is rounded to n decimals.',
	  'examples': [
	    'round(3.2)',
	    'round(3.8)',
	    'round(-4.2)',
	    'round(-4.8)',
	    'round(pi, 3)',
	    'round(123.45678, 2)'
	  ],
	  'seealso': ['ceil', 'floor', 'fix']
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'sign',
	  'category': 'Arithmetic',
	  'syntax': [
	    'sign(x)'
	  ],
	  'description':
	      'Compute the sign of a value. The sign of a value x is 1 when x>1, -1 when x<0, and 0 when x=0.',
	  'examples': [
	    'sign(3.5)',
	    'sign(-4.2)',
	    'sign(0)'
	  ],
	  'seealso': [
	    'abs'
	  ]
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'smaller',
	  'category': 'Operators',
	  'syntax': [
	    'x < y',
	    'smaller(x, y)'
	  ],
	  'description':
	      'Check if value x is smaller than value y. Returns 1 if x is smaller than y, and 0 if not.',
	  'examples': [
	    '2 < 3',
	    '5 < 2*2',
	    'a = 3.3',
	    'b = 6-2.8',
	    '(a < b)',
	    '5 cm < 2 inch'
	  ],
	  'seealso': [
	    'equal', 'unequal', 'larger', 'smallereq', 'largereq'
	  ]
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'smallereq',
	  'category': 'Operators',
	  'syntax': [
	    'x <= y',
	    'smallereq(x, y)'
	  ],
	  'description':
	      'Check if value x is smaller or equal to value y. Returns 1 if x is smaller than y, and 0 if not.',
	  'examples': [
	    '2 < 1+1',
	    '2 <= 1+1',
	    'a = 3.2',
	    'b = 6-2.8',
	    '(a < b)'
	  ],
	  'seealso': [
	    'equal', 'unequal', 'larger', 'smaller', 'largereq'
	  ]
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'sqrt',
	  'category': 'Arithmetic',
	  'syntax': [
	    'sqrt(x)'
	  ],
	  'description':
	      'Compute the square root value. If x = y * y, then y is the square root of x.',
	  'examples': [
	    'sqrt(25)',
	    '5 * 5',
	    'sqrt(-1)'
	  ],
	  'seealso': [
	    'square',
	    'multiply'
	  ]
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'square',
	  'category': 'Arithmetic',
	  'syntax': [
	    'square(x)'
	  ],
	  'description':
	      'Compute the square of a value. The square of x is x * x.',
	  'examples': [
	    'square(3)',
	    'sqrt(9)',
	    '3^2',
	    '3 * 3'
	  ],
	  'seealso': [
	    'multiply',
	    'pow',
	    'sqrt',
	    'cube'
	  ]
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'subtract',
	  'category': 'Operators',
	  'syntax': [
	    'x - y',
	    'subtract(x, y)'
	  ],
	  'description': 'subtract two values.',
	  'examples': [
	    '5.3 - 2',
	    'ans + 2',
	    '2/3 - 1/6',
	    '2 * 3 - 3',
	    '2.1 km - 500m'
	  ],
	  'seealso': [
	    'add'
	  ]
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'unary',
	  'category': 'Operators',
	  'syntax': [
	    '-x',
	    'unary(x)'
	  ],
	  'description':
	      'Inverse the sign of a value.',
	  'examples': [
	    '-4.5',
	    '-(-5.6)'
	  ],
	  'seealso': [
	    'add', 'subtract'
	  ]
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'unequal',
	  'category': 'Operators',
	  'syntax': [
	    'x != y',
	    'unequal(x, y)'
	  ],
	  'description':
	      'Check unequality of two values. Returns 1 if the values are unequal, and 0 if they are equal.',
	  'examples': [
	    '2+2 != 3',
	    '2+2 != 4',
	    'a = 3.2',
	    'b = 6-2.8',
	    'a != b',
	    '50cm != 0.5m',
	    '5 cm != 2 inch'
	  ],
	  'seealso': [
	    'equal', 'smaller', 'larger', 'smallereq', 'largereq'
	  ]
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'xgcd',
	  'category': 'Arithmetic',
	  'syntax': [
	    'xgcd(a, b)'
	  ],
	  'description': 'Calculate the extended greatest common divisor for two values',
	  'examples': [
	    'xgcd(8, 12)',
	    'gcd(8, 12)',
	    'xgcd(36163, 21199)'
	  ],
	  'seealso': [ 'gcd', 'lcm' ]
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'arg',
	  'category': 'Complex',
	  'syntax': [
	    'arg(x)'
	  ],
	  'description':
	      'Compute the argument of a complex value. If x = a+bi, the argument is computed as atan2(b, a).',
	  'examples': [
	    'arg(2 + 2i)',
	    'atan2(3, 2)',
	    'arg(2 - 3i)'
	  ],
	  'seealso': [
	    're',
	    'im',
	    'conj',
	    'abs'
	  ]
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'conj',
	  'category': 'Complex',
	  'syntax': [
	    'conj(x)'
	  ],
	  'description':
	      'Compute the complex conjugate of a complex value. If x = a+bi, the complex conjugate is a-bi.',
	  'examples': [
	    'conj(2 + 3i)',
	    'conj(2 - 3i)',
	    'conj(-5.2i)'
	  ],
	  'seealso': [
	    're',
	    'im',
	    'abs',
	    'arg'
	  ]
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 're',
	  'category': 'Complex',
	  'syntax': [
	    're(x)'
	  ],
	  'description': 'Get the real part of a complex number.',
	  'examples': [
	    're(2 + 3i)',
	    'im(2 + 3i)',
	    're(-5.2i)',
	    're(2.4)'
	  ],
	  'seealso': [
	    'im',
	    'conj',
	    'abs',
	    'arg'
	  ]
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'im',
	  'category': 'Complex',
	  'syntax': [
	    'im(x)'
	  ],
	  'description': 'Get the imaginary part of a complex number.',
	  'examples': [
	    'im(2 + 3i)',
	    're(2 + 3i)',
	    'im(-5.2i)',
	    'im(2.4)'
	  ],
	  'seealso': [
	    're',
	    'conj',
	    'abs',
	    'arg'
	  ]
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'bignumber',
	  'category': 'Type',
	  'syntax': [
	    'bignumber(x)'
	  ],
	  'description':
	      'Create a big number from a number or string.',
	  'examples': [
	    '0.1 + 0.2',
	    'bignumber(0.1) + bignumber(0.2)',
	    'bignumber("7.2")',
	    'bignumber("7.2e500")',
	    'bignumber([0.1, 0.2, 0.3])'
	  ],
	  'seealso': [
	    'boolean', 'complex', 'index', 'matrix', 'string', 'unit'
	  ]
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'boolean',
	  'category': 'Type',
	  'syntax': [
	    'x',
	    'boolean(x)'
	  ],
	  'description':
	      'Convert a string or number into a boolean.',
	  'examples': [
	    'boolean(0)',
	    'boolean(1)',
	    'boolean(3)',
	    'boolean("true")',
	    'boolean("false")',
	    'boolean([1, 0, 1, 1])'
	  ],
	  'seealso': [
	    'bignumber', 'complex', 'index', 'matrix', 'number', 'string', 'unit'
	  ]
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'complex',
	  'category': 'Type',
	  'syntax': [
	    'complex()',
	    'complex(re, im)',
	    'complex(string)'
	  ],
	  'description':
	      'Create a complex number.',
	  'examples': [
	    'complex()',
	    'complex(2, 3)',
	    'complex("7 - 2i")'
	  ],
	  'seealso': [
	    'bignumber', 'boolean', 'index', 'matrix', 'number', 'string', 'unit'
	  ]
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'index',
	  'category': 'Type',
	  'syntax': [
	    '[start]',
	    '[start:end]',
	    '[start:step:end]',
	    '[start1, start 2, ...]',
	    '[start1:end1, start2:end2, ...]',
	    '[start1:step1:end1, start2:step2:end2, ...]'
	  ],
	  'description':
	      'Create an index to get or replace a subset of a matrix',
	  'examples': [
	    '[]',
	    '[1, 2, 3]',
	    'A = [1, 2, 3; 4, 5, 6]',
	    'A[1, :]',
	    'A[1, 2] = 50',
	    'A[0:2, 0:2] = ones(2, 2)'
	  ],
	  'seealso': [
	    'bignumber', 'boolean', 'complex', 'matrix,', 'number', 'range', 'string', 'unit'
	  ]
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'matrix',
	  'category': 'Type',
	  'syntax': [
	    '[]',
	    '[a1, b1, ...; a2, b2, ...]',
	    'matrix()',
	    'matrix([...])'
	  ],
	  'description':
	      'Create a matrix.',
	  'examples': [
	    '[]',
	    '[1, 2, 3]',
	    '[1, 2, 3; 4, 5, 6]',
	    'matrix()',
	    'matrix([3, 4])'
	  ],
	  'seealso': [
	    'bignumber', 'boolean', 'complex', 'index', 'number', 'string', 'unit'
	  ]
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'number',
	  'category': 'Type',
	  'syntax': [
	    'x',
	    'number(x)'
	  ],
	  'description':
	      'Create a number or convert a string or boolean into a number.',
	  'examples': [
	    '2',
	    '2e3',
	    '4.05',
	    'number(2)',
	    'number("7.2")',
	    'number(true)',
	    'number([true, false, true, true])'
	  ],
	  'seealso': [
	    'bignumber', 'boolean', 'complex', 'index', 'matrix', 'string', 'unit'
	  ]
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'string',
	  'category': 'Type',
	  'syntax': [
	    '"text"',
	    'string(x)'
	  ],
	  'description':
	      'Create a string or convert a value to a string',
	  'examples': [
	    '"Hello World!"',
	    'string(4.2)',
	    'string(3 + 2i)'
	  ],
	  'seealso': [
	    'bignumber', 'boolean', 'complex', 'index', 'matrix', 'number', 'unit'
	  ]
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'unit',
	  'category': 'Type',
	  'syntax': [
	    'value unit',
	    'unit(value, unit)',
	    'unit(string)'
	  ],
	  'description':
	      'Create a unit.',
	  'examples': [
	    '5.5 mm',
	    '3 inch',
	    'unit(7.1, "kilogram")',
	    'unit("23 deg")'
	  ],
	  'seealso': [
	    'bignumber', 'boolean', 'complex', 'index', 'matrix', 'number', 'string'
	  ]
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'eval',
	  'category': 'Expression',
	  'syntax': [
	    'eval(expression)',
	    'eval([expr1, expr2, expr3, ...])'
	  ],
	  'description': 'Evaluate an expression or an array with expressions.',
	  'examples': [
	    'eval("2 + 3")',
	    'eval("sqrt(" + 4 + ")")'
	  ],
	  'seealso': []
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'help',
	  'category': 'Expression',
	  'syntax': [
	    'help(object)',
	    'help(string)'
	  ],
	  'description': 'Display documentation on a function or data type.',
	  'examples': [
	    'help(sqrt)',
	    'help("complex")'
	  ],
	  'seealso': []
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'concat',
	  'category': 'Matrix',
	  'syntax': [
	    'concat(a, b, c, ...)',
	    'concat(a, b, c, ..., dim)'
	  ],
	  'description': 'Concatenate matrices. By default, the matrices are concatenated by the first dimension. The dimension on which to concatenate can be provided as last argument.',
	  'examples': [
	    'a = [1, 2; 5, 6]',
	    'b = [3, 4; 7, 8]',
	    'concat(a, b)',
	    '[a, b]',
	    'concat(a, b, 2)',
	    '[a; b]'
	  ],
	  'seealso': [
	    'det', 'diag', 'eye', 'inv', 'ones', 'range', 'size', 'squeeze', 'subset', 'transpose', 'zeros'
	  ]
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'det',
	  'category': 'Matrix',
	  'syntax': [
	    'det(x)'
	  ],
	  'description': 'Calculate the determinant of a matrix',
	  'examples': [
	    'det([1, 2; 3, 4])',
	    'det([-2, 2, 3; -1, 1, 3; 2, 0, -1])'
	  ],
	  'seealso': [
	    'concat', 'diag', 'eye', 'inv', 'ones', 'range', 'size', 'squeeze', 'subset', 'transpose', 'zeros'
	  ]
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'diag',
	  'category': 'Matrix',
	  'syntax': [
	    'diag(x)',
	    'diag(x, k)'
	  ],
	  'description': 'Create a diagonal matrix or retrieve the diagonal of a matrix. When x is a vector, a matrix with the vector values on the diagonal will be returned. When x is a matrix, a vector with the diagonal values of the matrix is returned.When k is provided, the k-th diagonal will be filled in or retrieved, if k is positive, the values are placed on the super diagonal. When k is negative, the values are placed on the sub diagonal.',
	  'examples': [
	    'diag(1:3)',
	    'diag(1:3, 1)',
	    'a = [1, 2, 3; 4, 5, 6; 7, 8, 9]',
	    'diag(a)'
	  ],
	  'seealso': [
	    'concat', 'det', 'eye', 'inv', 'ones', 'range', 'size', 'squeeze', 'subset', 'transpose', 'zeros'
	  ]
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'eye',
	  'category': 'Matrix',
	  'syntax': [
	    'eye(n)',
	    'eye(m, n)',
	    'eye([m, n])',
	    'eye'
	  ],
	  'description': 'Returns the identity matrix with size m-by-n. The matrix has ones on the diagonal and zeros elsewhere.',
	  'examples': [
	    'eye(3)',
	    'eye(3, 5)',
	    'a = [1, 2, 3; 4, 5, 6]',
	    'eye(size(a))'
	  ],
	  'seealso': [
	    'concat', 'det', 'diag', 'inv', 'ones', 'range', 'size', 'squeeze', 'subset', 'transpose', 'zeros'
	  ]
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'inv',
	  'category': 'Matrix',
	  'syntax': [
	    'inv(x)'
	  ],
	  'description': 'Calculate the inverse of a matrix',
	  'examples': [
	    'inv([1, 2; 3, 4])',
	    'inv(4)',
	    '1 / 4'
	  ],
	  'seealso': [
	    'concat', 'det', 'diag', 'eye', 'ones', 'range', 'size', 'squeeze', 'subset', 'transpose', 'zeros'
	  ]
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'ones',
	  'category': 'Matrix',
	  'syntax': [
	    'ones(m)',
	    'ones(m, n)',
	    'ones(m, n, p, ...)',
	    'ones([m])',
	    'ones([m, n])',
	    'ones([m, n, p, ...])',
	    'ones'
	  ],
	  'description': 'Create a matrix containing ones.',
	  'examples': [
	    'ones(3)',
	    'ones(3, 5)',
	    'ones([2,3]) * 4.5',
	    'a = [1, 2, 3; 4, 5, 6]',
	    'ones(size(a))'
	  ],
	  'seealso': [
	    'concat', 'det', 'diag', 'eye', 'inv', 'range', 'size', 'squeeze', 'subset', 'transpose', 'zeros'
	  ]
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'range',
	  'category': 'Type',
	  'syntax': [
	    'start:end',
	    'start:step:end',
	    'range(start, end)',
	    'range(start, end, step)',
	    'range(string)'
	  ],
	  'description':
	      'Create a range. Lower bound of the range is included, upper bound is excluded.',
	  'examples': [
	    '1:5',
	    '3:-1:-3',
	    'range(3, 7)',
	    'range(0, 12, 2)',
	    'range("4:10")',
	    'a = [1, 2, 3, 4; 5, 6, 7, 8]',
	    'a[1:2, 1:2]'
	  ],
	  'seealso': [
	    'concat', 'det', 'diag', 'eye', 'inv', 'ones', 'size', 'squeeze', 'subset', 'transpose', 'zeros'
	  ]
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'resize',
	  'category': 'Matrix',
	  'syntax': [
	    'resize(x, size)',
	    'resize(x, size, defaultValue)'
	  ],
	  'description': 'Resize a matrix.',
	  'examples': [
	    'resize([1,2,3,4,5], [3])',
	    'resize([1,2,3], [5], 0)',
	    'resize(2, [2, 3], 0)',
	    'resize("hello", [8], "!")'
	  ],
	  'seealso': [
	    'size', 'subset', 'squeeze'
	  ]
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'size',
	  'category': 'Matrix',
	  'syntax': [
	    'size(x)'
	  ],
	  'description': 'Calculate the size of a matrix.',
	  'examples': [
	    'size(2.3)',
	    'size("hello world")',
	    'a = [1, 2; 3, 4; 5, 6]',
	    'size(a)',
	    'size(1:6)'
	  ],
	  'seealso': [
	    'concat', 'det', 'diag', 'eye', 'inv', 'ones', 'range', 'squeeze', 'subset', 'transpose', 'zeros'
	  ]
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'squeeze',
	  'category': 'Matrix',
	  'syntax': [
	    'squeeze(x)'
	  ],
	  'description': 'Remove singleton dimensions from a matrix.',
	  'examples': [
	    'a = zeros(1,3,2)',
	    'size(squeeze(a))',
	    'b = zeros(3,1,1)',
	    'size(squeeze(b))'
	  ],
	  'seealso': [
	    'concat', 'det', 'diag', 'eye', 'inv', 'ones', 'range', 'size', 'subset', 'transpose', 'zeros'
	  ]
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'subset',
	  'category': 'Matrix',
	  'syntax': [
	    'value(index)',
	    'value(index) = replacement',
	    'subset(value, [index])',
	    'subset(value, [index], replacement)'
	  ],
	  'description': 'Get or set a subset of a matrix or string. ' +
	      'Indexes are one-based. ' +
	      'Both the ranges lower-bound and upper-bound are included.',
	  'examples': [
	    'd = [1, 2; 3, 4]',
	    'e = []',
	    'e[1, 1:2] = [5, 6]',
	    'e[2, :] = [7, 8]',
	    'f = d * e',
	    'f[2, 1]',
	    'f[:, 1]'
	  ],
	  'seealso': [
	    'concat', 'det', 'diag', 'eye', 'inv', 'ones', 'range', 'size', 'squeeze', 'transpose', 'zeros'
	  ]
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'transpose',
	  'category': 'Matrix',
	  'syntax': [
	    'x\'',
	    'transpose(x)'
	  ],
	  'description': 'Transpose a matrix',
	  'examples': [
	    'a = [1, 2, 3; 4, 5, 6]',
	    'a\'',
	    'transpose(a)'
	  ],
	  'seealso': [
	    'concat', 'det', 'diag', 'eye', 'inv', 'ones', 'range', 'size', 'squeeze', 'subset', 'zeros'
	  ]
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'zeros',
	  'category': 'Matrix',
	  'syntax': [
	    'zeros(m)',
	    'zeros(m, n)',
	    'zeros(m, n, p, ...)',
	    'zeros([m])',
	    'zeros([m, n])',
	    'zeros([m, n, p, ...])',
	    'zeros'
	  ],
	  'description': 'Create a matrix containing zeros.',
	  'examples': [
	    'zeros(3)',
	    'zeros(3, 5)',
	    'a = [1, 2, 3; 4, 5, 6]',
	    'zeros(size(a))'
	  ],
	  'seealso': [
	    'concat', 'det', 'diag', 'eye', 'inv', 'ones', 'range', 'size', 'squeeze', 'subset', 'transpose'
	  ]
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'combinations',
	  'category': 'Probability',
	  'syntax': [
	    'combinations(n, k)'
	  ],
	  'description': 'Compute the number of combinations of n items taken k at a time',
	  'examples': [
	    'combinations(7, 5)'
	  ],
	  'seealso': ['permutations', 'factorial']
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'distribution',
	  'category': 'Probability',
	  'syntax': [
	    'distribution(name)',
	    'distribution(name, arg1, arg2, ...)'
	  ],
	  'description':
	      'Create a distribution object of a specific type. ' +
	          'A distribution object contains functions `random([size,] [min,] [max])`, ' +
	          '`randomInt([size,] [min,] [max])`, and `pickRandom(array)`. ' +
	          'Available types of distributions: "uniform", "normal". ' +
	          'Note that the function distribution is currently not available via the expression parser.',
	  'examples': [
	  ],
	  'seealso': ['random', 'randomInt']
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'factorial',
	  'category': 'Probability',
	  'syntax': [
	    'n!',
	    'factorial(n)'
	  ],
	  'description': 'Compute the factorial of a value',
	  'examples': [
	    '5!',
	    '5*4*3*2*1',
	    '3!'
	  ],
	  'seealso': []
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'permutations',
	  'category': 'Probability',
	  'syntax': [
	    'permutations(n)',
	    'permutations(n, k)'
	  ],
	  'description': 'Compute the number of permutations of n items taken k at a time',
	  'examples': [
	    'permutations(5)',
	    'permutations(5, 4)'
	  ],
	  'seealso': ['combinations', 'factorial']
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'pickRandom',
	  'category': 'Probability',
	  'syntax': [
	    'pickRandom(array)'
	  ],
	  'description':
	      'Pick a random entry from a given array.',
	  'examples': [
	    'pickRandom(0:10)',
	    'pickRandom([1, 3, 1, 6])'
	  ],
	  'seealso': ['distribution', 'random', 'randomInt']
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'random',
	  'category': 'Probability',
	  'syntax': [
	    'random()',
	    'random(max)',
	    'random(min, max)',
	    'random(size)',
	    'random(size, max)',
	    'random(size, min, max)'
	  ],
	  'description':
	      'Return a random number.',
	  'examples': [
	    'random()',
	    'random(10, 20)',
	    'random([2, 3])'
	  ],
	  'seealso': ['distribution', 'pickRandom', 'randomInt']
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'randInt',
	  'category': 'Probability',
	  'syntax': [
	    'randInt()',
	    'randInt(max)',
	    'randInt(min, max)',
	    'randInt(size)',
	    'randInt(size, max)',
	    'randInt(size, min, max)'
	  ],
	  'description':
	      'Return a random integer number',
	  'examples': [
	    'randInt()',
	    'randInt(10, 20)',
	    'randInt([2, 3], 10)'
	  ],
	  'seealso': ['distribution', 'pickRandom', 'random']
	};

  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'min',
	  'category': 'Statistics',
	  'syntax': [
	    'min(a, b, c, ...)',
	    'min(A)',
	    'min(A, dim)'
	  ],
	  'description': 'Compute the minimum value of a list of values.',
	  'examples': [
	    'min(2, 3, 4, 1)',
	    'min([2, 3, 4, 1])',
	    'min([2, 5; 4, 3], 0)',
	    'min([2, 5; 4, 3], 1)',
	    'min(2.7, 7.1, -4.5, 2.0, 4.1)',
	    'max(2.7, 7.1, -4.5, 2.0, 4.1)'
	  ],
	  'seealso': [
	     
	     
	     
	     
	     
	    'max',
	    'mean',
	     
	    'min'
	  ]
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'mean',
	  'category': 'Statistics',
	  'syntax': [
	    'mean(a, b, c, ...)',
	    'mean(A)',
	    'mean(A, dim)'
	  ],
	  'description': 'Compute the arithmetic mean of a list of values.',
	  'examples': [
	    'mean(2, 3, 4, 1)',
	    'mean([2, 3, 4, 1])',
	    'mean([2, 5; 4, 3], 0)',
	    'mean([2, 5; 4, 3], 1)',
	    'mean([1.0, 2.7, 3.2, 4.0])'
	  ],
	  'seealso': [
	     
	     
	     
	     
	     
		  'max',
	    'min'
	     
	  ]
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'max',
	  'category': 'Statistics',
	  'syntax': [
	    'max(a, b, c, ...)',
	    'max(A)',
	    'max(A, dim)'
	  ],
	  'description': 'Compute the maximum value of a list of values.',
	  'examples': [
	    'max(2, 3, 4, 1)',
	    'max([2, 3, 4, 1])',
	    'max([2, 5; 4, 3], 0)',
	    'max([2, 5; 4, 3], 1)',
	    'max(2.7, 7.1, -4.5, 2.0, 4.1)',
	    'min(2.7, 7.1, -4.5, 2.0, 4.1)'
	  ],
	  'seealso': [
	     
	     
	     
	     
	     
	    'mean',
	     
	    'min'
	  ]
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'acos',
	  'category': 'Trigonometry',
	  'syntax': [
	    'acos(x)'
	  ],
	  'description': 'Compute the inverse cosine of a value in radians.',
	  'examples': [
	    'acos(0.5)',
	    'acos(cos(2.3))'
	  ],
	  'seealso': [
	    'cos',
	    'acos',
	    'asin'
	  ]
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'asin',
	  'category': 'Trigonometry',
	  'syntax': [
	    'asin(x)'
	  ],
	  'description': 'Compute the inverse sine of a value in radians.',
	  'examples': [
	    'asin(0.5)',
	    'asin(sin(2.3))'
	  ],
	  'seealso': [
	    'sin',
	    'acos',
	    'asin'
	  ]
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'atan',
	  'category': 'Trigonometry',
	  'syntax': [
	    'atan(x)'
	  ],
	  'description': 'Compute the inverse tangent of a value in radians.',
	  'examples': [
	    'atan(0.5)',
	    'atan(tan(2.3))'
	  ],
	  'seealso': [
	    'tan',
	    'acos',
	    'asin'
	  ]
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'atan2',
	  'category': 'Trigonometry',
	  'syntax': [
	    'atan2(y, x)'
	  ],
	  'description':
	      'Computes the principal value of the arc tangent of y/x in radians.',
	  'examples': [
	    'atan2(2, 2) / pi',
	    'angle = 60 deg in rad',
	    'x = cos(angle)',
	    'y = sin(angle)',
	    'atan2(y, x)'
	  ],
	  'seealso': [
	    'sin',
	    'cos',
	    'tan'
	  ]
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'cos',
	  'category': 'Trigonometry',
	  'syntax': [
	    'cos(x)'
	  ],
	  'description': 'Compute the cosine of x in radians.',
	  'examples': [
	    'cos(2)',
	    'cos(pi / 4) ^ 2',
	    'cos(180 deg)',
	    'cos(60 deg)',
	    'sin(0.2)^2 + cos(0.2)^2'
	  ],
	  'seealso': [
	    'acos',
	    'sin',
	    'tan'
	  ]
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'cot',
	  'category': 'Trigonometry',
	  'syntax': [
	    'cot(x)'
	  ],
	  'description': 'Compute the cotangent of x in radians. Defined as 1/tan(x)',
	  'examples': [
	    'cot(2)',
	    '1 / tan(2)'
	  ],
	  'seealso': [
	    'sec',
	    'csc',
	    'tan'
	  ]
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'csc',
	  'category': 'Trigonometry',
	  'syntax': [
	    'csc(x)'
	  ],
	  'description': 'Compute the cosecant of x in radians. Defined as 1/sin(x)',
	  'examples': [
	    'csc(2)',
	    '1 / sin(2)'
	  ],
	  'seealso': [
	    'sec',
	    'cot',
	    'sin'
	  ]
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'sec',
	  'category': 'Trigonometry',
	  'syntax': [
	    'sec(x)'
	  ],
	  'description': 'Compute the secant of x in radians. Defined as 1/cos(x)',
	  'examples': [
	    'sec(2)',
	    '1 / cos(2)'
	  ],
	  'seealso': [
	    'cot',
	    'csc',
	    'cos'
	  ]
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'sin',
	  'category': 'Trigonometry',
	  'syntax': [
	    'sin(x)'
	  ],
	  'description': 'Compute the sine of x in radians.',
	  'examples': [
	    'sin(2)',
	    'sin(pi / 4) ^ 2',
	    'sin(90 deg)',
	    'sin(30 deg)',
	    'sin(0.2)^2 + cos(0.2)^2'
	  ],
	  'seealso': [
	    'asin',
	    'cos',
	    'tan'
	  ]
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'tan',
	  'category': 'Trigonometry',
	  'syntax': [
	    'tan(x)'
	  ],
	  'description': 'Compute the tangent of x in radians.',
	  'examples': [
	    'tan(0.5)',
	    'sin(0.5) / cos(0.5)',
	    'tan(pi / 4)',
	    'tan(45 deg)'
	  ],
	  'seealso': [
	    'atan',
	    'sin',
	    'cos'
	  ]
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'to',
	  'category': 'Units',
	  'syntax': [
	    'x to unit',
	    'to(x, unit)'
	  ],
	  'description': 'Change the unit of a value.',
	  'examples': [
	    '5 inch in cm',
	    '3.2kg in g',
	    '16 bytes in bits'
	  ],
	  'seealso': []
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'clone',
	  'category': 'Utils',
	  'syntax': [
	    'clone(x)'
	  ],
	  'description': 'Clone a variable. Creates a copy of primitive variables,and a deep copy of matrices',
	  'examples': [
	    'clone(3.5)',
	    'clone(2 - 4i)',
	    'clone(45 deg)',
	    'clone([1, 2; 3, 4])',
	    'clone("hello world")'
	  ],
	  'seealso': []
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'map',
	  'category': 'Utils',
	  'syntax': [
	    'map(x, callback)'
	  ],
	  'description': 'Create a new matrix or array with the results of the callback function executed on each entry of the matrix/array.',
	  'examples': [
	    'map([1, 2, 3], function(val) { return math.max(val, 1.5) })'
	  ],
	  'seealso': []
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'forEach',
	  'category': 'Utils',
	  'syntax': [
	    'forEach(x, callback)'
	  ],
	  'description': 'Iterates over all elements of a matrix/array, and executes the given callback.',
	  'examples': [
	    'forEach([1, 2, 3], function(val) { console.log(val) })'
	  ],
	  'seealso': []
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'format',
	  'category': 'Utils',
	  'syntax': [
	    'format(value)',
	    'format(value, precision)'
	  ],
	  'description': 'Format a value of any type as string.',
	  'examples': [
	    'format(2.3)',
	    'format(3 - 4i)',
	    'format([])',
	    'format(pi, 3)'
	  ],
	  'seealso': ['print']
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'import',
	  'category': 'Utils',
	  'syntax': [
	    'import(string)'
	  ],
	  'description': 'Import functions from a file.',
	  'examples': [
	    'import("numbers")',
	    'import("./mylib.js")'
	  ],
	  'seealso': []
	};


  },
 
  function(module, exports, require) {

	module.exports = {
	  'name': 'typeof',
	  'category': 'Utils',
	  'syntax': [
	    'typeof(x)'
	  ],
	  'description': 'Get the type of a variable.',
	  'examples': [
	    'typeof(3.5)',
	    'typeof(2 - 4i)',
	    'typeof(45 deg)',
	    'typeof("hello world")'
	  ],
	  'seealso': []
	};


  },
 
  function(module, exports, require) {

	 
	exports.type = function type (x) {
	  var type = typeof x;

	  if (type === 'object') {
	    if (x === null) {
	      return 'null';
	    }
	    if (x instanceof Boolean) {
	      return 'boolean';
	    }
	    if (x instanceof Number) {
	      return 'number';
	    }
	    if (x instanceof String) {
	      return 'string';
	    }
	    if (Array.isArray(x)) {
	      return 'array';
	    }
	    if (x instanceof Date) {
	      return 'date';
	    }
	  }

	  return type;
	};


  },
 
  function(module, exports, require) {

	var number = require(221),
	    BigNumber = require(220);

	 
	exports.isString = function isString(value) {
	  return (value instanceof String) || (typeof value == 'string');
	};

	 
	exports.endsWith = function endsWith(text, search) {
	  var start = text.length - search.length;
	  var end = text.length;
	  return (text.substring(start, end) === search);
	};

	 
	exports.format = function format(value, options) {
	  if (number.isNumber(value) || value instanceof BigNumber) {
	    return number.format(value, options);
	  }

	  if (Array.isArray(value)) {
	    return formatArray(value, options);
	  }

	  if (exports.isString(value)) {
	    return '"' + value + '"';
	  }

	  if (typeof value === 'function') {
	    return value.syntax ? value.syntax + '' : 'function';
	  }

	  if (value instanceof Object) {
	    if (typeof value.format === 'function') {
	      return value.format(options);
	    }
	    else {
	      return value.toString();
	    }
	  }

	  return String(value);
	};

	 
	function formatArray (array, options) {
	  if (Array.isArray(array)) {
	    var str = '[';
	    var len = array.length;
	    for (var i = 0; i < len; i++) {
	      if (i != 0) {
	        str += ', ';
	      }
	      str += formatArray(array[i], options);
	    }
	    str += ']';
	    return str;
	  }
	  else {
	    return exports.format(array, options);
	  }
	}


  },
 
  function(module, exports, require) {

	var map = {
		"./clone": 95,
		"./clone.js": 95,
		"./forEach": 101,
		"./forEach.js": 101,
		"./format": 96,
		"./format.js": 96,
		"./import": 97,
		"./import.js": 97,
		"./map": 98,
		"./map.js": 98,
		"./print": 99,
		"./print.js": 99,
		"./typeof": 100,
		"./typeof.js": 100
	};
	function webpackContext(req) {
		return require(webpackContextResolve(req));
	};
	function webpackContextResolve(req) {
		return map[req] || (function() { throw new Error("Cannot find module '" + req + "'.") }());
	};
	webpackContext.keys = function webpackContextKeys() {
		return Object.keys(map);
	};
	webpackContext.resolve = webpackContextResolve;
	module.exports = webpackContext;


  },
 
  function(module, exports, require) {

	var __WEBPACK_AMD_DEFINE_RESULT__; 

	 
	 

	;(function ( global ) {
	    'use strict';

	     

	     

	     

	     
	    var MAX = 1E9,                                    

	         
	        MAX_POWER = 1E6,                              

	         
	        DECIMAL_PLACES = 20,                          

	         
	        ROUNDING_MODE = 4,                            

	         

	         
	         
	        TO_EXP_NEG = -7,                              

	         
	         
	        TO_EXP_POS = 21,                              

	         

	         
	         
	        MIN_EXP = -MAX,                               

	         
	         
	        MAX_EXP = MAX,                                

	         
	         
	        ERRORS = true,                                
	        parse = parseInt,                             

	     

	        P = BigNumber.prototype,
	        DIGITS = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ$_',
	        outOfRange,
	        id = 0,
	        isValid = /^-?(\d+(\.\d*)?|\.\d+)(e[+-]?\d+)?$/i,
	        trim = String.prototype.trim || function () {return this.replace(/^\s+|\s+$/g, '')},
	        ONE = BigNumber(1);


	     


	     
	    function BigNumber( n, b ) {
	        var e, i, isNum, digits, valid, orig,
	            x = this;

	         
	        if ( !(x instanceof BigNumber) ) {
	            return new BigNumber( n, b )
	        }

	         
	        if ( n instanceof BigNumber ) {
	            id = 0;

	             
	            if ( b !== e ) {
	                n += ''
	            } else {
	                x['s'] = n['s'];
	                x['e'] = n['e'];
	                x['c'] = ( n = n['c'] ) ? n.slice() : n;
	                return;
	            }
	        }

	         
	        if ( typeof n != 'string' ) {
	            n = ( isNum = typeof n == 'number' ||
	                Object.prototype.toString.call(n) == '[object Number]' ) &&
	                    n === 0 && 1 / n < 0 ? '-0' : n + '';
	        }

	        orig = n;

	        if ( b === e && isValid.test(n) ) {

	             
	            x['s'] = n.charAt(0) == '-' ? ( n = n.slice(1), -1 ) : 1;

	         
	        } else {

	             
	             
	            if ( b == 10 ) {

	                return setMode( n, DECIMAL_PLACES, ROUNDING_MODE );
	            }

	            n = trim.call(n).replace( /^\+(?!-)/, '' );

	            x['s'] = n.charAt(0) == '-' ? ( n = n.replace( /^-(?!-)/, '' ), -1 ) : 1;

	            if ( b != null ) {

	                if ( ( b == (b | 0) || !ERRORS ) &&
	                  !( outOfRange = !( b >= 2 && b < 65 ) ) ) {

	                    digits = '[' + DIGITS.slice( 0, b = b | 0 ) + ']+';

	                     
	                     
	                    n = n.replace( /\.$/, '' ).replace( /^\./, '0.' );

	                     
	                    if ( valid = new RegExp(
	                      '^' + digits + '(?:\\.' + digits + ')?$', b < 37 ? 'i' : '' ).test(n) ) {

	                        if ( isNum ) {

	                            if ( n.replace( /^0\.0*|\./, '' ).length > 15 ) {

	                                 
	                                ifExceptionsThrow( orig, 0 );
	                            }

	                             
	                            isNum = !isNum;
	                        }
	                        n = convert( n, 10, b, x['s'] );

	                    } else if ( n != 'Infinity' && n != 'NaN' ) {

	                         
	                        ifExceptionsThrow( orig, 1, b );
	                        n = 'NaN';
	                    }
	                } else {

	                     
	                     
	                    ifExceptionsThrow( b, 2 );

	                     
	                    valid = isValid.test(n);
	                }
	            } else {
	                valid = isValid.test(n);
	            }

	            if ( !valid ) {

	                 
	                x['c'] = x['e'] = null;

	                 
	                if ( n != 'Infinity' ) {

	                     
	                    if ( n != 'NaN' ) {

	                         
	                        ifExceptionsThrow( orig, 3 );
	                    }
	                    x['s'] = null;
	                }
	                id = 0;

	                return;
	            }
	        }

	         
	        if ( ( e = n.indexOf('.') ) > -1 ) {
	            n = n.replace( '.', '' );
	        }

	         
	        if ( ( i = n.search( /e/i ) ) > 0 ) {

	             
	            if ( e < 0 ) {
	                e = i;
	            }
	            e += +n.slice( i + 1 );
	            n = n.substring( 0, i );

	        } else if ( e < 0 ) {

	             
	            e = n.length;
	        }

	         
	        for ( i = 0; n.charAt(i) == '0'; i++ ) {
	        }

	        b = n.length;

	         
	        if ( isNum && b > 15 && n.slice(i).length > 15 ) {

	             
	            ifExceptionsThrow( orig, 0 );
	        }
	        id = 0;

	         
	        if ( ( e -= i + 1 ) > MAX_EXP ) {

	             
	            x['c'] = x['e'] = null;

	         
	        } else if ( i == b || e < MIN_EXP ) {

	             
	            x['c'] = [ x['e'] = 0 ];
	        } else {

	             
	            for ( ; n.charAt(--b) == '0'; ) {
	            }

	            x['e'] = e;
	            x['c'] = [];

	             
	            for ( e = 0; i <= b; x['c'][e++] = +n.charAt(i++) ) {
	            }
	        }
	    }


	     


	    BigNumber['ROUND_UP'] = 0;
	    BigNumber['ROUND_DOWN'] = 1;
	    BigNumber['ROUND_CEIL'] = 2;
	    BigNumber['ROUND_FLOOR'] = 3;
	    BigNumber['ROUND_HALF_UP'] = 4;
	    BigNumber['ROUND_HALF_DOWN'] = 5;
	    BigNumber['ROUND_HALF_EVEN'] = 6;
	    BigNumber['ROUND_HALF_CEIL'] = 7;
	    BigNumber['ROUND_HALF_FLOOR'] = 8;


	     
	    BigNumber['config'] = function () {
	        var v, p,
	            i = 0,
	            r = {},
	            a = arguments,
	            o = a[0],
	            c = 'config',
	            inRange = function ( n, lo, hi ) {
	              return !( ( outOfRange = n < lo || n > hi ) ||
	                parse(n) != n && n !== 0 );
	            },
	            has = o && typeof o == 'object'
	              ? function () {if ( o.hasOwnProperty(p) ) return ( v = o[p] ) != null}
	              : function () {if ( a.length > i ) return ( v = a[i++] ) != null};

	         
	        if ( has( p = 'DECIMAL_PLACES' ) ) {

	            if ( inRange( v, 0, MAX ) ) {
	                DECIMAL_PLACES = v | 0;
	            } else {

	                 
	                 
	                ifExceptionsThrow( v, p, c );
	            }
	        }
	        r[p] = DECIMAL_PLACES;

	         
	        if ( has( p = 'ROUNDING_MODE' ) ) {

	            if ( inRange( v, 0, 8 ) ) {
	                ROUNDING_MODE = v | 0;
	            } else {

	                 
	                 
	                ifExceptionsThrow( v, p, c );
	            }
	        }
	        r[p] = ROUNDING_MODE;

	         
	        if ( has( p = 'EXPONENTIAL_AT' ) ) {

	            if ( inRange( v, -MAX, MAX ) ) {
	                TO_EXP_NEG = -( TO_EXP_POS = ~~( v < 0 ? -v : +v ) );
	            } else if ( !outOfRange && v && inRange( v[0], -MAX, 0 ) &&
	              inRange( v[1], 0, MAX ) ) {
	                TO_EXP_NEG = ~~v[0];
	                TO_EXP_POS = ~~v[1];
	            } else {

	                 
	                 
	                ifExceptionsThrow( v, p, c, 1 );
	            }
	        }
	        r[p] = [ TO_EXP_NEG, TO_EXP_POS ];

	         
	        if ( has( p = 'RANGE' ) ) {

	            if ( inRange( v, -MAX, MAX ) && ~~v ) {
	                MIN_EXP = -( MAX_EXP = ~~( v < 0 ? -v : +v ) );
	            } else if ( !outOfRange && v && inRange( v[0], -MAX, -1 ) &&
	              inRange( v[1], 1, MAX ) ) {
	                MIN_EXP = ~~v[0], MAX_EXP = ~~v[1];
	            } else {

	                 
	                 
	                ifExceptionsThrow( v, p, c, 1, 1 );
	            }
	        }
	        r[p] = [ MIN_EXP, MAX_EXP ];

	         
	        if ( has( p = 'ERRORS' ) ) {

	            if ( v === !!v || v === 1 || v === 0 ) {
	                parse = ( outOfRange = id = 0, ERRORS = !!v )
	                  ? parseInt
	                  : parseFloat;
	            } else {

	                 
	                ifExceptionsThrow( v, p, c, 0, 0, 1 );
	            }
	        }
	        r[p] = ERRORS;

	        return r;
	    };


	     


	     
	    function ifExceptionsThrow( arg, i, j, isArray, isRange, isErrors) {

	        if ( ERRORS ) {
	            var error,
	                method = ['new BigNumber', 'cmp', 'div', 'eq', 'gt', 'gte', 'lt',
	                     'lte', 'minus', 'mod', 'plus', 'times', 'toFr'
	                    ][ id ? id < 0 ? -id : id : 1 / id < 0 ? 1 : 0 ] + '()',
	                message = outOfRange ? ' out of range' : ' not a' +
	                  ( isRange ? ' non-zero' : 'n' ) + ' integer';

	            message = ( [
	                method + ' number type has more than 15 significant digits',
	                method + ' not a base ' + j + ' number',
	                method + ' base' + message,
	                method + ' not a number' ][i] ||
	                  j + '() ' + i + ( isErrors
	                    ? ' not a boolean or binary digit'
	                    : message + ( isArray
	                      ? ' or not [' + ( outOfRange
	                        ? ' negative, positive'
	                        : ' integer, integer' ) + ' ]'
	                      : '' ) ) ) + ': ' + arg;

	            outOfRange = id = 0;
	            error = new Error(message);
	            error['name'] = 'BigNumber Error';

	            throw error;
	        }
	    }


	     
	    function convert( nStr, baseOut, baseIn, sign ) {
	        var e, dvs, dvd, nArr, fracArr, fracBN;

	         
	         
	         
	        function strToArr( str, bIn ) {
	            var j,
	                i = 0,
	                strL = str.length,
	                arrL,
	                arr = [0];

	            for ( bIn = bIn || baseIn; i < strL; i++ ) {

	                for ( arrL = arr.length, j = 0; j < arrL; arr[j] *= bIn, j++ ) {
	                }

	                for ( arr[0] += DIGITS.indexOf( str.charAt(i) ), j = 0;
	                      j < arr.length;
	                      j++ ) {

	                    if ( arr[j] > baseOut - 1 ) {

	                        if ( arr[j + 1] == null ) {
	                            arr[j + 1] = 0;
	                        }
	                        arr[j + 1] += arr[j] / baseOut ^ 0;
	                        arr[j] %= baseOut;
	                    }
	                }
	            }

	            return arr.reverse();
	        }

	         
	         
	        function arrToStr( arr ) {
	            var i = 0,
	                arrL = arr.length,
	                str = '';

	            for ( ; i < arrL; str += DIGITS.charAt( arr[i++] ) ) {
	            }

	            return str;
	        }

	        if ( baseIn < 37 ) {
	            nStr = nStr.toLowerCase();
	        }

	         
	        if ( ( e = nStr.indexOf( '.' ) ) > -1 ) {

	             
	            e = nStr.length - e - 1;

	             
	            dvs = strToArr( new BigNumber(baseIn)['pow'](e)['toF'](), 10 );

	            nArr = nStr.split('.');

	             
	            dvd = strToArr( nArr[1] );

	             
	            nArr = strToArr( nArr[0] );

	             
	            fracBN = divide( dvd, dvs, dvd.length - dvs.length, sign, baseOut,
	               
	              nArr[nArr.length - 1] & 1 );

	            fracArr = fracBN['c'];

	             
	            if ( e = fracBN['e'] ) {

	                 
	                for ( ; ++e; fracArr.unshift(0) ) {
	                }

	                 
	                nStr = arrToStr(nArr) + '.' + arrToStr(fracArr);

	             
	             
	            } else if ( fracArr[0] ) {

	                if ( nArr[ e = nArr.length - 1 ] < baseOut - 1 ) {
	                    ++nArr[e];
	                    nStr = arrToStr(nArr);
	                } else {
	                    nStr = new BigNumber( arrToStr(nArr),
	                      baseOut )['plus'](ONE)['toS'](baseOut);
	                }

	             
	            } else {
	                nStr = arrToStr(nArr);
	            }
	        } else {

	             
	            nStr = arrToStr( strToArr(nStr) );
	        }

	        return nStr;
	    }


	     
	    function divide( dvd, dvs, exp, s, base, isOdd ) {
	        var dvsL, dvsT, next, cmp, remI,
	            dvsZ = dvs.slice(),
	            dvdI = dvsL = dvs.length,
	            dvdL = dvd.length,
	            rem = dvd.slice( 0, dvsL ),
	            remL = rem.length,
	            quo = new BigNumber(ONE),
	            qc = quo['c'] = [],
	            qi = 0,
	            dig = DECIMAL_PLACES + ( quo['e'] = exp ) + 1;

	        quo['s'] = s;
	        s = dig < 0 ? 0 : dig;

	         
	        for ( ; remL++ < dvsL; rem.push(0) ) {
	        }

	         
	        dvsZ.unshift(0);

	        do {

	             
	            for ( next = 0; next < base; next++ ) {

	                 
	                if ( dvsL != ( remL = rem.length ) ) {
	                    cmp = dvsL > remL ? 1 : -1;
	                } else {
	                    for ( remI = -1, cmp = 0; ++remI < dvsL; ) {

	                        if ( dvs[remI] != rem[remI] ) {
	                            cmp = dvs[remI] > rem[remI] ? 1 : -1;
	                            break;
	                        }
	                    }
	                }

	                 
	                if ( cmp < 0 ) {

	                     
	                     
	                    for ( dvsT = remL == dvsL ? dvs : dvsZ; remL; ) {

	                        if ( rem[--remL] < dvsT[remL] ) {

	                            for ( remI = remL;
	                              remI && !rem[--remI];
	                                rem[remI] = base - 1 ) {
	                            }
	                            --rem[remI];
	                            rem[remL] += base;
	                        }
	                        rem[remL] -= dvsT[remL];
	                    }
	                    for ( ; !rem[0]; rem.shift() ) {
	                    }
	                } else {
	                    break;
	                }
	            }

	             
	            qc[qi++] = cmp ? next : ++next;

	             
	            rem[0] && cmp
	              ? ( rem[remL] = dvd[dvdI] || 0 )
	              : ( rem = [ dvd[dvdI] ] );

	        } while ( ( dvdI++ < dvdL || rem[0] != null ) && s-- );

	         
	        if ( !qc[0] && qi != 1 ) {

	             
	            --quo['e'];
	            qc.shift();
	        }

	         
	        if ( qi > dig ) {
	            rnd( quo, DECIMAL_PLACES, base, isOdd, rem[0] != null );
	        }

	         
	        if ( quo['e'] > MAX_EXP ) {

	             
	            quo['c'] = quo['e'] = null;

	         
	        } else if ( quo['e'] < MIN_EXP ) {

	             
	            quo['c'] = [quo['e'] = 0];
	        }

	        return quo;
	    }


	     
	    function format( n, d, exp ) {

	         
	        var i = d - (n = new BigNumber(n))['e'],
	            c = n['c'];

	         
	        if ( !c ) {
	            return n['toS']();
	        }

	         
	        if ( c.length > ++d ) {
	            rnd( n, i, 10 );
	        }

	         
	        i = c[0] == 0 ? i + 1 : exp ? d : n['e'] + i + 1;

	         
	        for ( ; c.length < i; c.push(0) ) {
	        }
	        i = n['e'];

	         
	        return exp == 1 || exp == 2 && ( --d < i || i <= TO_EXP_NEG )

	           
	          ? ( n['s'] < 0 && c[0] ? '-' : '' ) + ( c.length > 1
	            ? ( c.splice( 1, 0, '.' ), c.join('') )
	            : c[0] ) + ( i < 0 ? 'e' : 'e+' ) + i

	           
	          : n['toS']();
	    }


	     
	     
	    function rnd( x, dp, base, isOdd, r ) {
	        var xc = x['c'],
	            isNeg = x['s'] < 0,
	            half = base / 2,
	            i = x['e'] + dp + 1,

	             
	            next = xc[i],

	             
	            more = r || i < 0 || xc[i + 1] != null;

	        r = ROUNDING_MODE < 4
	          ? ( next != null || more ) &&
	            ( ROUNDING_MODE == 0 ||
	               ROUNDING_MODE == 2 && !isNeg ||
	                 ROUNDING_MODE == 3 && isNeg )
	          : next > half || next == half &&
	            ( ROUNDING_MODE == 4 || more ||

	               
	              ROUNDING_MODE == 6 && ( xc[i - 1] & 1 || !dp && isOdd ) ||
	                ROUNDING_MODE == 7 && !isNeg ||
	                  ROUNDING_MODE == 8 && isNeg );

	        if ( i < 1 || !xc[0] ) {
	            xc.length = 0;
	            xc.push(0);

	            if ( r ) {

	                 
	                xc[0] = 1;
	                x['e'] = -dp;
	            } else {

	                 
	                x['e'] = 0;
	            }

	            return x;
	        }

	         
	        xc.length = i--;

	         
	        if ( r ) {

	             
	            for ( --base; ++xc[i] > base; ) {
	                xc[i] = 0;

	                if ( !i-- ) {
	                    ++x['e'];
	                    xc.unshift(1);
	                }
	            }
	        }

	         
	        for ( i = xc.length; !xc[--i]; xc.pop() ) {
	        }

	        return x;
	    }


	     
	     
	    function setMode( x, dp, rm ) {
	        var r = ROUNDING_MODE;

	        ROUNDING_MODE = rm;
	        x = new BigNumber(x);
	        x['c'] && rnd( x, dp, 10 );
	        ROUNDING_MODE = r;

	        return x;
	    }


	     


	     
	    P['abs'] = P['absoluteValue'] = function () {
	        var x = new BigNumber(this);

	        if ( x['s'] < 0 ) {
	            x['s'] = 1;
	        }

	        return x;
	    };


	     
	    P['ceil'] = function () {
	        return setMode( this, 0, 2 );
	    };


	     
	    P['comparedTo'] = P['cmp'] = function ( y, b ) {
	        var a,
	            x = this,
	            xc = x['c'],
	            yc = ( id = -id, y = new BigNumber( y, b ) )['c'],
	            i = x['s'],
	            j = y['s'],
	            k = x['e'],
	            l = y['e'];

	         
	        if ( !i || !j ) {
	            return null;
	        }

	        a = xc && !xc[0], b = yc && !yc[0];

	         
	        if ( a || b ) {
	            return a ? b ? 0 : -j : i;
	        }

	         
	        if ( i != j ) {
	            return i;
	        }

	         
	        if ( a = i < 0, b = k == l, !xc || !yc ) {
	            return b ? 0 : !xc ^ a ? 1 : -1;
	        }

	         
	        if ( !b ) {
	            return k > l ^ a ? 1 : -1;
	        }

	         
	        for ( i = -1,
	              j = ( k = xc.length ) < ( l = yc.length ) ? k : l;
	              ++i < j; ) {

	            if ( xc[i] != yc[i] ) {
	                return xc[i] > yc[i] ^ a ? 1 : -1;
	            }
	        }
	         
	        return k == l ? 0 : k > l ^ a ? 1 : -1;
	    };


	     
	    P['dividedBy'] = P['div'] = function ( y, b ) {
	        var xc = this['c'],
	            xe = this['e'],
	            xs = this['s'],
	            yc = ( id = 2, y = new BigNumber( y, b ) )['c'],
	            ye = y['e'],
	            ys = y['s'],
	            s = xs == ys ? 1 : -1;

	         
	        return !xe && ( !xc || !xc[0] ) || !ye && ( !yc || !yc[0] )

	           
	          ? new BigNumber( !xs || !ys ||

	             
	            ( xc ? yc && xc[0] == yc[0] : !yc )

	               
	              ? NaN

	               
	              : xc && xc[0] == 0 || !yc

	                 
	                ? s * 0

	                 
	                : s / 0 )

	          : divide( xc, yc, xe - ye, s, 10 );
	    };


	     
	    P['equals'] = P['eq'] = function ( n, b ) {
	        id = 3;
	        return this['cmp']( n, b ) === 0;
	    };


	     
	    P['floor'] = function () {
	        return setMode( this, 0, 3 );
	    };


	     
	    P['greaterThan'] = P['gt'] = function ( n, b ) {
	        id = 4;
	        return this['cmp']( n, b ) > 0;
	    };


	     
	    P['greaterThanOrEqualTo'] = P['gte'] = function ( n, b ) {
	        id = 5;
	        return ( b = this['cmp']( n, b ) ) == 1 || b === 0;
	    };


	     
	    P['isFinite'] = P['isF'] = function () {
	        return !!this['c'];
	    };


	     
	    P['isNaN'] = function () {
	        return !this['s'];
	    };


	     
	    P['isNegative'] = P['isNeg'] = function () {
	        return this['s'] < 0;
	    };


	     
	    P['isZero'] = P['isZ'] = function () {
	        return !!this['c'] && this['c'][0] == 0;
	    };


	     
	    P['lessThan'] = P['lt'] = function ( n, b ) {
	        id = 6;
	        return this['cmp']( n, b ) < 0;
	    };


	     
	    P['lessThanOrEqualTo'] = P['lte'] = function ( n, b ) {
	        id = 7;
	        return ( b = this['cmp']( n, b ) ) == -1 || b === 0;
	    };


	     
	    P['minus'] = function ( y, b ) {
	        var d, i, j, xLTy,
	            x = this,
	            a = x['s'];

	        b = ( id = 8, y = new BigNumber( y, b ) )['s'];

	         
	        if ( !a || !b ) {
	            return new BigNumber(NaN);
	        }

	         
	        if ( a != b ) {
	            return y['s'] = -b, x['plus'](y);
	        }

	        var xc = x['c'],
	            xe = x['e'],
	            yc = y['c'],
	            ye = y['e'];

	        if ( !xe || !ye ) {

	             
	            if ( !xc || !yc ) {
	                return xc ? ( y['s'] = -b, y ) : new BigNumber( yc ? x : NaN );
	            }

	             
	            if ( !xc[0] || !yc[0] ) {

	                 
	                return yc[0]
	                  ? ( y['s'] = -b, y )

	                   
	                  : new BigNumber( xc[0]
	                    ? x

	                     
	                     
	                    : ROUNDING_MODE == 3 ? -0 : 0 );
	            }
	        }

	         
	         
	        if ( xc = xc.slice(), a = xe - ye ) {
	            d = ( xLTy = a < 0 ) ? ( a = -a, xc ) : ( ye = xe, yc );

	            for ( d.reverse(), b = a; b--; d.push(0) ) {
	            }
	            d.reverse();
	        } else {

	             
	            j = ( ( xLTy = xc.length < yc.length ) ? xc : yc ).length;

	            for ( a = b = 0; b < j; b++ ) {

	                if ( xc[b] != yc[b] ) {
	                    xLTy = xc[b] < yc[b];
	                    break;
	                }
	            }
	        }

	         
	        if ( xLTy ) {
	            d = xc, xc = yc, yc = d;
	            y['s'] = -y['s'];
	        }

	         
	        if ( ( b = -( ( j = xc.length ) - yc.length ) ) > 0 ) {

	            for ( ; b--; xc[j++] = 0 ) {
	            }
	        }

	         
	        for ( b = yc.length; b > a; ){

	            if ( xc[--b] < yc[b] ) {

	                for ( i = b; i && !xc[--i]; xc[i] = 9 ) {
	                }
	                --xc[i];
	                xc[b] += 10;
	            }
	            xc[b] -= yc[b];
	        }

	         
	        for ( ; xc[--j] == 0; xc.pop() ) {
	        }

	         
	        for ( ; xc[0] == 0; xc.shift(), --ye ) {
	        }

	         

	         
	        if ( ye < MIN_EXP || !xc[0] ) {

	             
	            if ( !xc[0] ) {
	                y['s'] = ROUNDING_MODE == 3 ? -1 : 1;
	            }

	             
	            xc = [ye = 0];
	        }

	        return y['c'] = xc, y['e'] = ye, y;
	    };


	     
	    P['modulo'] = P['mod'] = function ( y, b ) {
	        var x = this,
	            xc = x['c'],
	            yc = ( id = 9, y = new BigNumber( y, b ) )['c'],
	            i = x['s'],
	            j = y['s'];

	         
	        b = !i || !j || yc && !yc[0];

	        if ( b || xc && !xc[0] ) {
	            return new BigNumber( b ? NaN : x );
	        }

	        x['s'] = y['s'] = 1;
	        b = y['cmp'](x) == 1;
	        x['s'] = i, y['s'] = j;

	        return b
	          ? new BigNumber(x)
	          : ( i = DECIMAL_PLACES, j = ROUNDING_MODE,
	            DECIMAL_PLACES = 0, ROUNDING_MODE = 1,
	              x = x['div'](y),
	                DECIMAL_PLACES = i, ROUNDING_MODE = j,
	                  this['minus']( x['times'](y) ) );
	    };


	     
	    P['negated'] = P['neg'] = function () {
	        var x = new BigNumber(this);

	        return x['s'] = -x['s'] || null, x;
	    };


	     
	    P['plus'] = function ( y, b ) {
	        var d,
	            x = this,
	            a = x['s'];

	        b = ( id = 10, y = new BigNumber( y, b ) )['s'];

	         
	        if ( !a || !b ) {
	            return new BigNumber(NaN);
	        }

	         
	        if ( a != b ) {
	            return y['s'] = -b, x['minus'](y);
	        }

	        var xe = x['e'],
	            xc = x['c'],
	            ye = y['e'],
	            yc = y['c'];

	        if ( !xe || !ye ) {

	             
	            if ( !xc || !yc ) {

	                 
	                return new BigNumber( a / 0 );
	            }

	             
	            if ( !xc[0] || !yc[0] ) {

	                 
	                return yc[0]
	                  ? y

	                   
	                  : new BigNumber( xc[0]
	                    ? x

	                     
	                    : a * 0 );
	            }
	        }

	         
	         
	        if ( xc = xc.slice(), a = xe - ye ) {
	            d = a > 0 ? ( ye = xe, yc ) : ( a = -a, xc );

	            for ( d.reverse(); a--; d.push(0) ) {
	            }
	            d.reverse();
	        }

	         
	        if ( xc.length - yc.length < 0 ) {
	            d = yc, yc = xc, xc = d;
	        }

	         
	        for ( a = yc.length, b = 0; a;
	             b = ( xc[--a] = xc[a] + yc[a] + b ) / 10 ^ 0, xc[a] %= 10 ) {
	        }

	         

	        if ( b ) {
	            xc.unshift(b);

	             
	            if ( ++ye > MAX_EXP ) {

	                 
	                xc = ye = null;
	            }
	        }

	          
	        for ( a = xc.length; xc[--a] == 0; xc.pop() ) {
	        }

	        return y['c'] = xc, y['e'] = ye, y;
	    };


	     
	    P['toPower'] = P['pow'] = function ( e ) {

	         
	        var i = e * 0 == 0 ? e | 0 : e,
	            x = new BigNumber(this),
	            y = new BigNumber(ONE);

	         
	         
	        if ( ( ( ( outOfRange = e < -MAX_POWER || e > MAX_POWER ) &&
	          (i = e * 1 / 0) ) ||

	              
	             parse(e) != e && e !== 0 && !(i = NaN) ) &&

	               
	               
	              !ifExceptionsThrow( e, 'exponent', 'pow' ) ||

	                 
	                !i ) {

	             
	            return new BigNumber( Math.pow( x['toS'](), i ) );
	        }

	        for ( i = i < 0 ? -i : i; ; ) {

	            if ( i & 1 ) {
	                y = y['times'](x);
	            }
	            i >>= 1;

	            if ( !i ) {
	                break;
	            }
	            x = x['times'](x);
	        }

	        return e < 0 ? ONE['div'](y) : y;
	    };


	     
	    P['round'] = function ( dp, rm ) {

	        dp = dp == null || ( ( ( outOfRange = dp < 0 || dp > MAX ) ||
	          parse(dp) != dp ) &&

	             
	             
	            !ifExceptionsThrow( dp, 'decimal places', 'round' ) )
	              ? 0
	              : dp | 0;

	        rm = rm == null || ( ( ( outOfRange = rm < 0 || rm > 8 ) ||

	           
	          parse(rm) != rm && rm !== 0 ) &&

	             
	             
	            !ifExceptionsThrow( rm, 'mode', 'round' ) )
	              ? ROUNDING_MODE
	              : rm | 0;

	        return setMode( this, dp, rm );
	    };


	     
	    P['squareRoot'] = P['sqrt'] = function () {
	        var n, r, re, t,
	            x = this,
	            c = x['c'],
	            s = x['s'],
	            e = x['e'],
	            dp = DECIMAL_PLACES,
	            rm = ROUNDING_MODE,
	            half = new BigNumber('0.5');

	         
	        if ( s !== 1 || !c || !c[0] ) {

	            return new BigNumber( !s || s < 0 && ( !c || c[0] )
	              ? NaN
	              : c ? x : 1 / 0 );
	        }

	         
	        s = Math.sqrt( x['toS']() );
	        ROUNDING_MODE = 1;

	         
	        if ( s == 0 || s == 1 / 0 ) {
	            n = c.join('');

	            if ( !( n.length + e & 1 ) ) {
	                n += '0';
	            }
	            r = new BigNumber( Math.sqrt(n) + '' );

	             
	            if ( !r['c'] ) {
	                r['c'] = [1];
	            }
	            r['e'] = ( ( ( e + 1 ) / 2 ) | 0 ) - ( e < 0 || e & 1 );
	        } else {
	            r = new BigNumber( n = s.toString() );
	        }
	        re = r['e'];
	        s = re + ( DECIMAL_PLACES += 4 );

	        if ( s < 3 ) {
	            s = 0;
	        }
	        e = s;

	         
	        for ( ; ; ) {
	            t = r;
	            r = half['times']( t['plus']( x['div'](t) ) );

	            if ( t['c'].slice( 0, s ).join('') === r['c'].slice( 0, s ).join('') ) {
	                c = r['c'];

	                 
	                s = s - ( n && r['e'] < re );

	                 
	                if ( c[s] == 9 && c[s - 1] == 9 && c[s - 2] == 9 &&
	                        ( c[s - 3] == 9 || n && c[s - 3] == 4 ) ) {

	                     
	                    if ( n && c[s - 3] == 9 ) {
	                        t = r['round']( dp, 0 );

	                        if ( t['times'](t)['eq'](x) ) {
	                            ROUNDING_MODE = rm;
	                            DECIMAL_PLACES = dp;

	                            return t;
	                        }
	                    }
	                    DECIMAL_PLACES += 4;
	                    s += 4;
	                    n = '';
	                } else {

	                     
	                    if ( !c[e] && !c[e - 1] && !c[e - 2] &&
	                            ( !c[e - 3] || c[e - 3] == 5 ) ) {

	                         
	                        if ( c.length > e - 2 ) {
	                            c.length = e - 2;
	                        }

	                        if ( !r['times'](r)['eq'](x) ) {

	                            while ( c.length < e - 3 ) {
	                                c.push(0);
	                            }
	                            c[e - 3]++;
	                        }
	                    }
	                    ROUNDING_MODE = rm;
	                    rnd( r, DECIMAL_PLACES = dp, 10 );

	                    return r;
	                }
	            }
	        }
	    };


	     
	    P['times'] = function ( y, b ) {
	        var c,
	            x = this,
	            xc = x['c'],
	            yc = ( id = 11, y = new BigNumber( y, b ) )['c'],
	            i = x['e'],
	            j = y['e'],
	            a = x['s'];

	        y['s'] = a == ( b = y['s'] ) ? 1 : -1;

	         
	        if ( !i && ( !xc || !xc[0] ) || !j && ( !yc || !yc[0] ) ) {

	             
	            return new BigNumber( !a || !b ||

	               
	              xc && !xc[0] && !yc || yc && !yc[0] && !xc

	                 
	                ? NaN

	                 
	                : !xc || !yc

	                   
	                  ? y['s'] / 0

	                   
	                  : y['s'] * 0 );
	        }
	        y['e'] = i + j;

	        if ( ( a = xc.length ) < ( b = yc.length ) ) {
	            c = xc, xc = yc, yc = c, j = a, a = b, b = j;
	        }

	        for ( j = a + b, c = []; j--; c.push(0) ) {
	        }

	         
	        for ( i = b - 1; i > -1; i-- ) {

	            for ( b = 0, j = a + i;
	                  j > i;
	                  b = c[j] + yc[i] * xc[j - i - 1] + b,
	                  c[j--] = b % 10 | 0,
	                  b = b / 10 | 0 ) {
	            }

	            if ( b ) {
	                c[j] = ( c[j] + b ) % 10;
	            }
	        }

	        b && ++y['e'];

	         
	        !c[0] && c.shift();

	         
	        for ( j = c.length; !c[--j]; c.pop() ) {
	        }

	         

	         
	        y['c'] = y['e'] > MAX_EXP

	           
	          ? ( y['e'] = null )

	           
	          : y['e'] < MIN_EXP

	             
	            ? [ y['e'] = 0 ]

	             
	            : c;

	        return y;
	    };


	     
	    P['toExponential'] = P['toE'] = function ( dp ) {

	        return format( this,
	          ( dp == null || ( ( outOfRange = dp < 0 || dp > MAX ) ||

	             
	            parse(dp) != dp && dp !== 0 ) &&

	               
	               
	              !ifExceptionsThrow( dp, 'decimal places', 'toE' ) ) && this['c']
	                ? this['c'].length - 1
	                : dp | 0, 1 );
	    };


	     
	    P['toFixed'] = P['toF'] = function ( dp ) {
	        var n, str, d,
	            x = this;

	        if ( !( dp == null || ( ( outOfRange = dp < 0 || dp > MAX ) ||
	            parse(dp) != dp && dp !== 0 ) &&

	             
	             
	            !ifExceptionsThrow( dp, 'decimal places', 'toF' ) ) ) {
	              d = x['e'] + ( dp | 0 );
	        }

	        n = TO_EXP_NEG, dp = TO_EXP_POS;
	        TO_EXP_NEG = -( TO_EXP_POS = 1 / 0 );

	         
	        if ( d == str ) {
	            str = x['toS']();
	        } else {
	            str = format( x, d );

	             
	             
	            if ( x['s'] < 0 && x['c'] ) {

	                 
	                if ( !x['c'][0] ) {
	                    str = str.replace(/^-/, '');

	                 
	                } else if ( str.indexOf('-') < 0 ) {
	                    str = '-' + str;
	                }
	            }
	        }
	        TO_EXP_NEG = n, TO_EXP_POS = dp;

	        return str;
	    };


	     
	    P['toFraction'] = P['toFr'] = function ( maxD ) {
	        var q, frac, n0, d0, d2, n, e,
	            n1 = d0 = new BigNumber(ONE),
	            d1 = n0 = new BigNumber('0'),
	            x = this,
	            xc = x['c'],
	            exp = MAX_EXP,
	            dp = DECIMAL_PLACES,
	            rm = ROUNDING_MODE,
	            d = new BigNumber(ONE);

	         
	        if ( !xc ) {
	            return x['toS']();
	        }

	        e = d['e'] = xc.length - x['e'] - 1;

	         
	        if ( maxD == null ||

	              
	             ( !( id = 12, n = new BigNumber(maxD) )['s'] ||

	                
	               ( outOfRange = n['cmp'](n1) < 0 || !n['c'] ) ||

	                  
	                 ( ERRORS && n['e'] < n['c'].length - 1 ) ) &&

	                    
	                    
	                   !ifExceptionsThrow( maxD, 'max denominator', 'toFr' ) ||

	                      
	                     ( maxD = n )['cmp'](d) > 0 ) {

	             
	            maxD = e > 0 ? d : n1;
	        }

	        MAX_EXP = 1 / 0;
	        n = new BigNumber( xc.join('') );

	        for ( DECIMAL_PLACES = 0, ROUNDING_MODE = 1; ; )  {
	            q = n['div'](d);
	            d2 = d0['plus']( q['times'](d1) );

	            if ( d2['cmp'](maxD) == 1 ) {
	                break;
	            }

	            d0 = d1, d1 = d2;

	            n1 = n0['plus']( q['times']( d2 = n1 ) );
	            n0 = d2;

	            d = n['minus']( q['times']( d2 = d ) );
	            n = d2;
	        }

	        d2 = maxD['minus'](d0)['div'](d1);
	        n0 = n0['plus']( d2['times'](n1) );
	        d0 = d0['plus']( d2['times'](d1) );

	        n0['s'] = n1['s'] = x['s'];

	        DECIMAL_PLACES = e * 2;
	        ROUNDING_MODE = rm;

	         
	        frac = n1['div'](d1)['minus'](x)['abs']()['cmp'](
	          n0['div'](d0)['minus'](x)['abs']() ) < 1
	          ? [ n1['toS'](), d1['toS']() ]
	          : [ n0['toS'](), d0['toS']() ];

	        return MAX_EXP = exp, DECIMAL_PLACES = dp, frac;
	    };


	     
	    P['toPrecision'] = P['toP'] = function ( sd ) {

	         
	        return sd == null || ( ( ( outOfRange = sd < 1 || sd > MAX ) ||
	          parse(sd) != sd ) &&

	             
	             
	            !ifExceptionsThrow( sd, 'precision', 'toP' ) )
	              ? this['toS']()
	              : format( this, --sd | 0, 2 );
	    };


	     
	    P['toString'] = P['toS'] = function ( b ) {
	        var u, str, strL,
	            x = this,
	            xe = x['e'];

	         
	        if ( xe === null ) {
	            str = x['s'] ? 'Infinity' : 'NaN';

	         
	        } else if ( b === u && ( xe <= TO_EXP_NEG || xe >= TO_EXP_POS ) ) {
	            return format( x, x['c'].length - 1, 1 );
	        } else {
	            str = x['c'].join('');

	             
	            if ( xe < 0 ) {

	                 
	                for ( ; ++xe; str = '0' + str ) {
	                }
	                str = '0.' + str;

	             
	            } else if ( strL = str.length, xe > 0 ) {

	                if ( ++xe > strL ) {

	                     
	                    for ( xe -= strL; xe-- ; str += '0' ) {
	                    }
	                } else if ( xe < strL ) {
	                    str = str.slice( 0, xe ) + '.' + str.slice(xe);
	                }

	             
	            } else {
	                if ( u = str.charAt(0), strL > 1 ) {
	                    str = u + '.' + str.slice(1);

	                 
	                } else if ( u == '0' ) {
	                    return u;
	                }
	            }

	            if ( b != null ) {

	                if ( !( outOfRange = !( b >= 2 && b < 65 ) ) &&
	                  ( b == (b | 0) || !ERRORS ) ) {
	                    str = convert( str, b | 0, 10, x['s'] );

	                     
	                    if ( str == '0' ) {
	                        return str;
	                    }
	                } else {

	                     
	                     
	                    ifExceptionsThrow( b, 'base', 'toS' );
	                }
	            }

	        }

	        return x['s'] < 0 ? '-' + str : str;
	    };


	     
	    P['valueOf'] = function () {
	        return this['toS']();
	    };


	     
	     
	     
	     
	     
	     
	     
	     


	     


	     
	    if ( typeof module !== 'undefined' && module.exports ) {
	        module.exports = BigNumber;

	     
	    } else if ( true ) {
	        !(__WEBPACK_AMD_DEFINE_RESULT__ = (function () {
	            return BigNumber;
	        }.call(exports, require, exports, module)), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));

	     
	    } else {
	        global['BigNumber'] = BigNumber;
	    }

	})( this );


  },
 
  function(module, exports, require) {

	var BigNumber = require(220);

	 
	exports.isNumber = function isNumber(value) {
	  return (value instanceof Number) || (typeof value == 'number');
	};

	 
	exports.isInteger = function isInteger(value) {
	  return (value == Math.round(value));
	   
	};

	 
	exports.sign = function sign (x) {
	  if (x > 0) {
	    return 1;
	  }
	  else if (x < 0) {
	    return -1;
	  }
	  else {
	    return 0;
	  }
	};

	 
	exports.format = function format(value, options) {
	  if (typeof options === 'function') {
	     
	    return options(value);
	  }

	   
	  if (value === Infinity) {
	    return 'Infinity';
	  }
	  else if (value === -Infinity) {
	    return '-Infinity';
	  }
	  else if (isNaN(value)) {
	    return 'NaN';
	  }

	   
	  var notation = 'auto';
	  var precision = undefined;

	  if (options !== undefined) {
	     
	    if (options.notation) {
	      notation = options.notation;
	    }

	     
	    if (options) {
	      if (exports.isNumber(options)) {
	        precision = options;
	      }
	      else if (options.precision) {
	        precision = options.precision;
	      }
	    }
	  }

	   
	  switch (notation) {
	    case 'fixed':
	      return exports.toFixed(value, precision);

	     
	    case 'scientific':
	      throw new Error('Format notation "scientific" is deprecated. Use "exponential" instead.');

	    case 'exponential':
	      return exports.toExponential(value, precision);

	    case 'auto':
	       
	         
	      var lower = 1e-3;
	      var upper = 1e5;
	      if (options && options.exponential) {
	        if (options.exponential.lower !== undefined) {
	          lower = options.exponential.lower;
	        }
	        if (options.exponential.upper !== undefined) {
	          upper = options.exponential.upper;
	        }
	      }
	      else if (options && options.scientific) {
	         
	        throw new Error('options.scientific is deprecated, use options.exponential instead.');
	      }

	       
	      var isBigNumber = value instanceof BigNumber;
	      if (isBigNumber) {
	        var oldScientific = BigNumber.config().EXPONENTIAL_AT;
	        BigNumber.config({
	          EXPONENTIAL_AT: [
	            Math.round(Math.log(lower) / Math.LN10),
	            Math.round(Math.log(upper) / Math.LN10)
	          ]
	        });
	      }

	       
	      if (_isZero(value)) {
	        return '0';
	      }

	       
	      var str;
	      if (_isBetween(value, lower, upper)) {
	         
	        if (isBigNumber) {
	          str = new BigNumber(value.toPrecision(precision)).toString();
	        }
	        else {  
	           
	          var valueStr = precision ?
	              value.toPrecision(Math.min(precision, 21)) :
	              value.toPrecision();
	          str = parseFloat(valueStr) + '';
	        }
	      }
	      else {
	         
	        str = exports.toExponential(value, precision);
	      }

	       
	      if (isBigNumber) {
	        BigNumber.config({EXPONENTIAL_AT: oldScientific});
	      }

	       
	      return str.replace(/((\.\d*?)(0+))($|e)/, function () {
	        var digits = arguments[2];
	        var e = arguments[4];
	        return (digits !== '.') ? digits + e : e;
	      });

	    default:
	      throw new Error('Unknown notation "' + notation + '". ' +
	          'Choose "auto", "exponential", or "fixed".');
	  }
	};

	 
	function _isZero (value) {
	  return (value instanceof BigNumber) ? value.isZero() : (value === 0);
	}

	 
	function _isBetween(value, lower, upper) {
	  var abs;
	  if (value instanceof BigNumber) {
	    abs = value.abs();
	    return (abs.gte(lower) && abs.lt(upper));
	  }
	  else {
	    abs = Math.abs(value);
	    return (abs >= lower && abs < upper);
	  }
	}

	 
	exports.toExponential = function toExponential (value, precision) {
	  if (precision !== undefined) {
	    if (value instanceof BigNumber) {
	      return value.toExponential(precision - 1);
	    }
	    else {  
	      return value.toExponential(Math.min(precision - 1, 20));
	    }
	  }
	  else {
	    return value.toExponential();
	  }
	};

	 
	exports.toFixed = function toFixed (value, precision) {
	  if (value instanceof BigNumber) {
	    return value.toFixed(precision || 0);
	     
	     
	  }
	  else {  
	    return value.toFixed(Math.min(precision, 20));
	  }
	};

	 
	exports.digits = function digits (value) {
	  return value
	      .toExponential()
	      .replace(/e[\+\-0-9]*$/, '')   
	      .replace( /^0\.0*|\./, '')     
	      .length
	};

	 
	exports.toBigNumber = function toBigNumber (number) {
	  if (exports.digits(number) > 15) {
	    return number;
	  }
	  else {
	    return new BigNumber(number);
	  }
	};

	 
	exports.toNumber = function toNumber (bignumber) {
	  return parseFloat(bignumber.valueOf());
	};


  },
 
  function(module, exports, require) {

	var number = require(221),
	    string = require(218),
	    object = require(2),
	    types = require(217),
	    isArray = Array.isArray;

	 
	function _size(x) {
	  var size = [];

	  while (isArray(x)) {
	    size.push(x.length);
	    x = x[0];
	  }

	  return size;
	}

	 
	exports.size = function size (x) {
	   
	  var s = _size(x);

	   
	  exports.validate(x, s);
	   

	  return s;
	};

	 
	function _validate(array, size, dim) {
	  var i;
	  var len = array.length;

	  if (len != size[dim]) {
	    throw new RangeError('Dimension mismatch (' + len + ' != ' + size[dim] + ')');
	  }

	  if (dim < size.length - 1) {
	     
	    var dimNext = dim + 1;
	    for (i = 0; i < len; i++) {
	      var child = array[i];
	      if (!isArray(child)) {
	        throw new RangeError('Dimension mismatch ' +
	            '(' + (size.length - 1) + ' < ' + size.length + ')');
	      }
	      _validate(array[i], size, dimNext);
	    }
	  }
	  else {
	     
	    for (i = 0; i < len; i++) {
	      if (isArray(array[i])) {
	        throw new RangeError('Dimension mismatch ' +
	            '(' + (size.length + 1) + ' > ' + size.length + ')');
	      }
	    }
	  }
	}

	 
	exports.validate = function validate(array, size) {
	  var isScalar = (size.length == 0);
	  if (isScalar) {
	     
	    if (isArray(array)) {
	      throw new RangeError('Dimension mismatch (' + array.length + ' != 0)');
	    }
	  }
	  else {
	     
	    _validate(array, size, 0);
	  }
	};

	 
	exports.validateIndex = function validateIndex (index, length) {
	  if (!number.isNumber(index) || !number.isInteger(index)) {
	    throw new TypeError('Index must be an integer (value: ' + index + ')');
	  }
	  if (index < 0) {
	    throw new RangeError('Index out of range (' + index + ' < 0)');
	  }
	  if (length !== undefined && index >= length) {
	    throw new RangeError('Index out of range (' + index + ' > ' + (length - 1) +  ')');
	  }
	};

	 
	exports.resize = function resize(array, size, defaultValue) {
	   

	   
	  if (!isArray(array) || !isArray(size)) {
	    throw new TypeError('Array expected');
	  }
	  if (size.length === 0) {
	    throw new Error('Resizing to scalar is not supported');
	  }

	   
	  size.forEach(function (value) {
	    if (!number.isNumber(value) || !number.isInteger(value) || value < 0) {
	      throw new TypeError('Invalid size, must contain positive integers ' +
	          '(size: ' + string.format(size) + ')');
	    }
	  });

	   
	  var dims = 1;
	  var elem = array[0];
	  while (isArray(elem)) {
	    dims++;
	    elem = elem[0];
	  }

	   
	  while (dims < size.length) {  
	    array = [array];
	    dims++;
	  }
	  while (dims > size.length) {  
	    array = array[0];
	    dims--;
	  }

	   
	  _resize(array, size, 0, defaultValue);

	  return array;
	};

	 
	function _resize (array, size, dim, defaultValue) {
	  if (!isArray(array)) {
	    throw Error('Array expected');
	  }

	  var i, elem,
	      oldLen = array.length,
	      newLen = size[dim],
	      minLen = Math.min(oldLen, newLen);

	   
	  array.length = newLen;

	  if (dim < size.length - 1) {
	     
	    var dimNext = dim + 1;

	     
	    for (i = 0; i < minLen; i++) {
	       
	      elem = array[i];
	      _resize(elem, size, dimNext, defaultValue);
	    }

	     
	    for (i = minLen; i < newLen; i++) {
	       
	      elem = [];
	      array[i] = elem;

	       
	      _resize(elem, size, dimNext, defaultValue);
	    }
	  }
	  else {
	     
	    if(defaultValue !== undefined) {
	       
	      for (i = oldLen; i < newLen; i++) {
	        array[i] = object.clone(defaultValue);
	      }
	    }
	  }
	}

	 
	exports.squeeze = function squeeze(array) {
	  while(isArray(array) && array.length === 1) {
	    array = array[0];
	  }

	  return array;
	};

	 
	exports.unsqueeze = function unsqueeze(array, dims) {
	  var size = exports.size(array);

	  for (var i = 0, ii = (dims - size.length); i < ii; i++) {
	    array = [array];
	  }

	  return array;
	};

	 
	exports.isArray = isArray;

  },
 
  function(module, exports, require) {

	 
	exports.isBoolean = function isBoolean(value) {
	  return (value instanceof Boolean) || (typeof value == 'boolean');
	};


  }
  ])
<%@page import="com.eab.common.Log"%>
<%@page import="com.eab.common.Constants"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
	Log.info("{JSP} /views/error.jsp");
	String appkey = Constants.APP_STARTUP_KEY;
	String errorMessage = (String) request.getAttribute("errorMessage");
	
	if (errorMessage == null) {
		errorMessage = "";
	}
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Error</title>
<link href="<%=request.getContextPath()%>/web/js/mui/css/mui.min.css" rel="stylesheet" type="text/css" />
<link href="<%=request.getContextPath()%>/web/css/style.css" rel="stylesheet" type="text/css" />
<script src="<%=request.getContextPath()%>/web/js/mui/js/mui.min.js"></script>
</head>
<body>
	<header class="mui-appbar mui--z1">
		<div class="mui-container">
			<table width="100%">
				<tr class="mui--appbar-height">
					<td class="mui--text-title">121 Configurator</td>
					<td align="right">
						<ul class="mui-list--inline mui--text-body2">
						<li><a href="#">About</a></li>
						</ul>
					</td>
				</tr>
			</table>
		</div>
	</header>
	<div id="content-wrapper" class="mui--text-center">
		<div class="mui--appbar-height"></div>
		<div class="mui-container-fluid" style="width: 500px;">
			<div class="mui-row">
				<div class="mui-col-sm-10 mui-col-sm-offset-1">
					<div class="space-top"></div>
					<div class="mui-panel">
						<p>System Error.</p>
						<p><%=errorMessage %></p>
						<button class="mui-btn mui-btn--raised" onClick="document.location.href='<%=request.getContextPath()%>/Login';">Go to Login</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<footer>
		<div class="mui-container mui--text-center">
			Made with by <a href="http://www.eabsystems.com">EAB R&D Team</a> | &copy;2016 EAB Systems Hong Kong Limited
		</div>
	</footer>
</body>
</html>
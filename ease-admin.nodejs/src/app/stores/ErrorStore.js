let AppDispatcher = require('../dispatcher/AppDispatcher.js');
let EventEmitter = require('events').EventEmitter;
let assign = require('object-assign');
let ConfigConstants = require('../constants/ConfigConstants');

var ActionTypes = ConfigConstants.ActionTypes;

var _currentActions = 0;

var _title = {};
var _actions = [];
var _values = {};
var _open = false;
var _dialog = undefined;

var UPDATE_DIALOG = 'UPDATE_DIALOG';

function _setDialog(dialog) {
  _dialog = dialog;
}

var _updateValues = function(values) {
  _values = values;
};

var ErrorStore = assign({}, EventEmitter.prototype, {

  emitChange: function() {
    this.emit(UPDATE_DIALOG);
  },

  /**
   * @param {function} callback
   */
  addChangeListener: function(callback) {
    this.on(UPDATE_DIALOG, callback);
  },

  /**
   * @param {function} callback
   */
  removeChangeListener: function(callback) {
    this.removeListener(UPDATE_DIALOG, callback);
  },

  getOpen: function() {
    return _open;
  },

  setOpen: function(){
    _open = true;
  },

  setClose: function(){
    _open = false;
  },

  getDialog() {
    return _dialog;
  },
  getValues() {
    return _values;
  },
  setValues(values){
    _values = values;
  }
});

DialogStore.dispatchToken = AppDispatcher.register(function(action) {
  switch (action.actionType) {
    case ActionTypes.PAGE_REDIRECT:
    case ActionTypes.CHANGE_PAGE:
      _values = {};
      _currentActions = 0;
    case ActionTypes.CHANGE_CONTENT:
      //_open = false;
    case ActionTypes.CHANGE_SECTION:
      break;
    case ActionTypes.UPDATE_DIALOG:
      console.debug('[DialogStore] - [Dispatching] - [' + action.actionType + ']', action)
      DialogStore.emitChange();
      break;
    case ActionTypes.SHOW_DIALOG: // HIM
      console.debug('[DialogStore] - [Dispatching] - [' + action.actionType + ']', action);
      var dialog;
      if (action.data.dialog) {
        dialog = action.data.dialog;
      }
      if (action.dialog) {
        dialog = action.dialog;
      }

      _setDialog(dialog);
      DialogStore.emitChange();

      break;
    default:
      // no action
  }
});

module.exports = ErrorStore;

var AppDispatcher = require('../dispatcher/AppDispatcher.js');
var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');
var ConfigConstants = require('../constants/ConfigConstants');
// var HomeStore = require('../stores/HomeStore.js')
//var GET_RELEASE_LIST = 'TASK_RELEASE';

var ActionTypes = ConfigConstants.ActionTypes;
var CURRENT_PAGE_CHANGE = 'PasswordResult';
var _template = [];
var _result = [];
var _fitler = [];

function _getUassignedReleaseList(list, template) {
  _list = list;
  _template = template;
}

function _setResult(result){
  _result = result;
}
function _setTemplate(template){
  _template = template;
}

var PasswordStore = assign({}, EventEmitter.prototype, {
  emitChange: function() {
    this.emit(CURRENT_PAGE_CHANGE);
  },

  /**_selectPage
   * @param {function} callback
   */
  addChangeListener: function(callback) {
    // console.debug('home store add listener');
    this.on(CURRENT_PAGE_CHANGE, callback);
  },

  /**
   * @param {function} callback
   */
  removeChangeListener: function(callback) {
    this.removeListener(CURRENT_PAGE_CHANGE, callback);
  },

  getList() {
      return _list;
  },
  getResult() {
      return _result;
  },
  getTemplate() {
      return _template;
  },

  setFilter(insearch){
	   _fitler = {search : insearch};
   },

   getFilter(){
	    return _fitler;
   }
});

PasswordStore.dispatchToken = AppDispatcher.register(function(action) {
  // console.debug('PasswordStore dispatching', action);
  switch(action.actionType) {
		case ActionTypes.CHANGE_CONTENT:
      // AppDispatcher.waitFor([HomeStore.dispatchToken]);
      // console.debug('PasswordStore dispatching', action.page);
      if(action.page && action.page.id == "/Password/Reset" ){
        _setResult(action.data);
        _setTemplate(action.data.template);
        PasswordStore.emitChange();
      }
      if(action.page && action.page.id == "/Password/ForgotRequest" ){
        _setResult(action.data);
        _setTemplate(action.data.template);
        PasswordStore.emitChange();
      }
		break;
		default:
			// no action
	}
});

module.exports = PasswordStore;

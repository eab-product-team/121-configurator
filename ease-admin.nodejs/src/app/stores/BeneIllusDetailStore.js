var AppDispatcher = require('../dispatcher/AppDispatcher.js');
var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');
var ConfigConstants = require('../constants/ConfigConstants');


// var HomeStore = require('../stores/HomeStore.js')

var CURRENT_PAGE_CHANGE = 'BiChangePage';
var ActionTypes = ConfigConstants.ActionTypes;

var _currentPage = 'TaskInfo';
var _uiPage = '';
var _lock = '';
var _reflesh = false;

function _selectPage(currentPage) {
  _currentPage = currentPage.id;
	_uiPage = currentPage.uid;
	_reflesh = true;
}

function _getSelectPage(){
  return _currentPage;
}

function _getUiPage(){
	return _uiPage;
}


var BeneIllusDetailStore = assign({}, EventEmitter.prototype, {

  emitChange: function() {

    this.emit(CURRENT_PAGE_CHANGE);
  },

  /**_selectPage
   * @param {function} callback
   */
  addChangeListener: function(callback) {
    // console.debug('home store add listener');

    this.on(CURRENT_PAGE_CHANGE, callback);
  },

  /**
   * @param {function} callback
   */
  removeChangeListener: function(callback) {

    this.removeListener(CURRENT_PAGE_CHANGE, callback);
  },

  getCurrentPage(){
    return _getSelectPage();
  },

	getUiPage(){
		return _getUiPage();
	},

  isLock(){
    if(_lock=='Y'){
      return true;
    }else{
      return false;
    }
  },

  updateLock(lock){
    if(lock){
      _lock='Y';
    }else{
      _lock='N';
    }
  },

	isReflesh(){
		if(_reflesh){
			_reflesh = false;
			return true;
		}
		return false;
	}
});

 BeneIllusDetailStore.dispatchToken = AppDispatcher.register(function(action) {
   switch(action.actionType) {
     case ActionTypes.CHANGE_PAGE:
				 if(action.data && action.data.content && action.data.content.values){
					 // update the currenct page
					 if(action.data.content.values.selectPage){
						 _selectPage(action.data.content.values.selectPage);
					 }

					 //update user can access the record or not){
	        BeneIllusDetailStore.updateLock(action.data.content.values.readOnly);

				 }
         BeneIllusDetailStore.emitChange();
  			break;
 		case ActionTypes.RESET_VALUES:
       // AppDispatcher.waitFor([HomeStore.dispatchToken]);
       if(checkExist(action, 'data.content.values.selectPage')){
         _selectPage(action.data.content.values.selectPage);
         BeneIllusDetailStore.emitChange();
       }

 			break;
 		default:
 			// no action
 	}
});

module.exports = BeneIllusDetailStore;

var AppDispatcher = require('../dispatcher/AppDispatcher.js');
var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');
var ConfigConstants = require('../constants/ConfigConstants');
var ActionTypes = ConfigConstants.ActionTypes;

var CHANGE_EVENT = 'BCProcessingChange';


var _bcp_page = {
  index: true,
  detail: false,
  error: false
};

// only one true each time
var _updateBcpPage = function(page) {
  Object.keys(_bcp_page).map(function(key, index) {
    if(_bcp_page[key]) {
      _bcp_page[key] = !_bcp_page[key];
    }
  })
  _bcp_page[page] = true;
}

var BCProcessingStore = assign({}, EventEmitter.prototype, {

  emitChange: function() {
    this.emit(CHANGE_EVENT);
  },

  /**
   * @param {function} callback
   */
  addChangeListener: function(callback) {
    this.on(CHANGE_EVENT, callback);
  },

  /**
   * @param {function} callback
   */
  removeChangeListener: function(callback) {
    this.removeListener(CHANGE_EVENT, callback);
  },


  getBcpPage() {
    return _bcp_page;
  },


});

BCProcessingStore.dispatchToken = AppDispatcher.register(function(action) {

  switch (action.actionType) {
    case 'switchBcpPage': 
      _updateBcpPage(action.page);
      BCProcessingStore.emitChange();
      break;
    default:
      // no action
  }
});

module.exports = BCProcessingStore;

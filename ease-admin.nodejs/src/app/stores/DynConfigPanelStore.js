var AppDispatcher = require('../dispatcher/AppDispatcher.js');
var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');
var ConfigConstants = require('../constants/ConfigConstants');
var ActionTypes = ConfigConstants.ActionTypes;

var _show = false;
var _reflesh = false;
var UPDATE_PANEL = 'UPDATE_PANEL';
var _changedValues = {};
var _type = '';
var _actions = {};

function _open() {
  _show = true;
}

function _close() {
  _show = false;
}

var DynConfigPanelStore = assign({}, EventEmitter.prototype, {

  emitChange: function() {
    this.emit(UPDATE_PANEL);
  },

  /**
   * @param {function} callback
   */
  addChangeListener: function(callback) {
    this.on(UPDATE_PANEL, callback);
  },

  /**
   * @param {function} callback
   */
  removeChangeListener: function(callback) {
    this.removeListener(UPDATE_PANEL, callback);
  },

  getPanelState() {
    return _show;
  },

  getChangedValues(){
    return _changedValues;
  },

  getActions(){
    return _actions;
  },

  isReflesh(){
    if(_reflesh){
      _reflesh = false;
      return true;
    }else{
      return false;
    }
  },


  getType(){
    return _type;
  },

  updatePanel(show){

    if(show=='Y'){
      this.clearData();
      _open();
    }else{
      _close();
    }
  },

  clearData(){
    _changedValues = {};
    _actions = {};
    _type = '';
  },
});

DynConfigPanelStore.dispatchToken = AppDispatcher.register(function(action) {

  switch(action.actionType) {
    case ActionTypes.CHANGE_PAGE:
    case ActionTypes.NEEDS_CHANGE_PAGE:
      DynConfigPanelStore.updatePanel('N');
      DynConfigPanelStore.emitChange();
      break;
    case ActionTypes.PANEL_UPDATE:
      //control open status and initialize values
      if(action.show){
          DynConfigPanelStore.updatePanel(action.show);
      }
      if(action.changedValues){
        _changedValues = action.changedValues;
      }
      if(action.actions){
        _actions = action.actions;
      }
      if(action.type){
        _type = action.type;
      }
      _reflesh = true;
      DynConfigPanelStore.emitChange();  // called on Home
			break;
		default:
			// no action
	}
});

module.exports = DynConfigPanelStore;

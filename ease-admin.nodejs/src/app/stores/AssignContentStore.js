var AppDispatcher = require('../dispatcher/AppDispatcher.js');
var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');
var ConfigConstants = require('../constants/ConfigConstants');
var ProductStore = require('./ProductStore.js');
let HomeStore = require('../stores/HomeStore.js');

var CURRENT_PAGE_CONTENT = 'AssignContentStore';
var ActionTypes = ConfigConstants.ActionTypes;

var _currentActions = 0;
var _content = {};
var _values = {};
var _hint = '';
var _actions = [];
var _sortBy = '';
var _sortDir = 'A';
var _selectedRows = [];
var _currentPage = undefined;
var _selectedRowNumbers = 0;
var _pageSize = ConfigConstants.PageSize;
var _show = false;

function _selectPage(currentPage) {
  _currentPage = currentPage;
}

var _updateAppBarActions = function(actions) {
  _actions = actions;
};

var _updateAppBarHint = function(hint){
  _hint = hint;
}

var _updateSortInfo = function(sortInfo) {
  _sortBy = sortInfo.sortBy;
  _sortDir = sortInfo.sortDir;
}

var _updateSelectedRows = function(rows) {
  _selectedRows = rows;
}

var _updateValues = function(values) {
  _values = values;
}

var _updateCurrentActions = function(currentAction) {
  if(_actions){
    if (_actions.length > currentAction) {
      _currentActions = currentAction;
    } else {
      _currentActions = _actions.length - 1;
    }
  }
}

function setPageContent(content) {

  if (content && _content && content.values && content.values.isMore && _content.values && _content.values.list instanceof Array) {
      _content.values.list.push.apply(_content.values.list, content.values.list);
  } else {
    _content = content;
  }

}

 var _updateSelectedRowNumbers = function(rowNumbers) {
   _selectedRowNumbers = rowNumbers;
 }

var AssignContentStore = assign({}, EventEmitter.prototype, {
  emitChange: function() {
    this.emit(CURRENT_PAGE_CONTENT);
  },

  /**
   * @param {function} callback
   */
  addChangeListener: function(callback) {
    // console.debug('home store add listener');
    this.on(CURRENT_PAGE_CONTENT, callback);
  },

  /**
   * @param {function} callback
   */
  removeChangeListener: function(callback) {
    this.removeListener(CURRENT_PAGE_CONTENT, callback);
  },

  getPageContent() {
    return _content;
  },

  getActions: function() {
    if(_actions)
      return _actions[_currentActions];
    else
      return {};
  },

  getHint: function(){
    return _hint;
  },

  getValues() {
    return _values;
  },

  getSelectedRowNumbers() {
     return _selectedRowNumbers;
  },

  clearData(){
    _currentActions = 0;
    _content = {};
    _currentPage = null;
    _values = {};
    _actions = [];
    _sortBy = '';
    _hint = '';
    _sortDir = 'A';
    _selectedRows = [];
    _pageSize = ConfigConstants.PageSize;
  },

  getCurrentPage() {
    return _currentPage;
  },

  getSortInfo() {
    return {
      sortBy: _sortBy,
      sortDir: _sortDir
    }
  },

  getSelectedRows() {
    return _selectedRows;
  },

  getPageSize() {
    return _pageSize;
  },

  setPageSize(size) {
    _pageSize = size;
  },

  isShow(){
    return _show;
  }


});


AssignContentStore.dispatchToken = AppDispatcher.register(function(action) {
  let appBarVal = action.data && action.data.assignAppBar && action.data.assignAppBar.values? action.data.assignAppBar.values:"";
  let pageVal = action.data && action.data.page && action.data.page.value? action.data.page.value:"";
  let rowNumbers = pageVal.reloadStatus? appBarVal.recordStart: appBarVal.recordStart  + appBarVal.pageSize;
  switch (action.actionType) {
    case ActionTypes.AssignPage.ASSIGN_PAGE:
      _show = true;
      if (action.data) {
        var content =   action.data.assignContent;
        setPageContent(content);
        _selectPage(action.data.page);
        _updateCurrentActions(0);
      }
      if (action.data && action.data.assignAppBar){
        if(action.data.assignAppBar.actions){
          _updateAppBarActions(action.data.assignAppBar.actions);
        }
        if(action.data.assignAppBar.title && action.data.assignAppBar.title.value){
          _updateAppBarHint(action.data.assignAppBar.title.value);
        }
        if(action.data.assignAppBar.values){
          _updateSortInfo(action.data.assignAppBar.values);
        }

      }
      if(appBarVal)
         _updateSelectedRowNumbers(rowNumbers)

      AssignContentStore.emitChange();
      break;
    case ActionTypes.AssignPage.SELECTION_CHANGE:
      // console.debug('[MasterTableStore] - [Dispatching] - [' + action.actionType + ']', action);
      _updateSelectedRows(action.rows);
      if(appBarVal)
         _updateSelectedRowNumbers(rowNumbers)

      if(action.rows && action.rows.length && action.rows.length > 0){
        _updateCurrentActions(1);
      }else{
        _updateCurrentActions(0);
      }


      AssignContentStore.emitChange();
      break;
    case ActionTypes.AssignPage.UPDATE_VALUES:
      _updateValues(action.values);
      AssignContentStore.emitChange();
      break;
    case ActionTypes.AssignPage.SORT_TARGET_CHANGE:
      // console.debug('[MasterTableStore] - [Dispatching] - [' + action.actionType + ']', action);
      _updateSortInfo(action);
      AssignContentStore.emitChange();
      break;
    case ActionTypes.UPDATE_LOADING_STATE:
    case ActionTypes.SHOW_DIALOG:
    case ActionTypes.UPDATE_DIALOG:
      break;
    default:
      if (_show) {
        _show = false;
        AssignContentStore.clearData();
        AssignContentStore.emitChange();
      }
      // no action
  }
});

module.exports = AssignContentStore;

var AppDispatcher = require('../dispatcher/AppDispatcher.js');
var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');
var ConfigConstants = require('../constants/ConfigConstants');
var ActionTypes = ConfigConstants.ActionTypes;
var AppStore = require('../stores/AppStore.js');

var LOADING_STORE = 'LoadingStore';

var _state = false;

function _updateState(state) {
    _state = state;
}

var LoadingStore = assign({}, EventEmitter.prototype, {

  emitChange: function() {
    this.emit(LOADING_STORE);
  },

  /**
   * @param {function} callback
   */
  addChangeListener: function(callback) {
    this.on(LOADING_STORE, callback);
  },

  /**
   * @param {function} callback
   */
  removeChangeListener: function(callback) {
    this.removeListener(LOADING_STORE, callback);
  },

  getState() {
    return _state;
  }
});

LoadingStore.dispatchToken = AppDispatcher.register(function(action) {
  switch(action.actionType) {
    case ActionTypes.CHANGE_MENU_PAGE:
    case ActionTypes.UPDATE_LOADING_STATE:
      _updateState(action.loadingState);
      LoadingStore.emitChange();
      break;
    case ActionTypes.PAGE_REDIRECT:
    case ActionTypes.CHANGE_PAGE:
    case ActionTypes.CHANGE_CONTENT:
    case ActionTypes.UPDATE_FUNC_EDITOR_DATA:
    // case ActionTypes.CHANGE_VALUES:
    case ActionTypes.SHOW_DIALOG:
    case ActionTypes.AssignPage.ASSIGN_PAGE:
    case ActionTypes.RESET_VALUES:
    case ActionTypes.NEEDS_CHANGE_PAGE:
    case ActionTypes.PREVIEW_PDF:
      _updateState(false);
      LoadingStore.emitChange();
      break;
		default:
      if (action.loadingState != undefined) {
        _updateState(!!action.loadingState);
        LoadingStore.emitChange();
        break;
      }
			// no action
	}
});

module.exports = LoadingStore;

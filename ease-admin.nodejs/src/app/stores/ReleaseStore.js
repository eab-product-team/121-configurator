var AppDispatcher = require('../dispatcher/AppDispatcher.js');
var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');
var ConfigConstants = require('../constants/ConfigConstants');
var AppStore = require('../stores/AppStore.js')
var TaskStore = require('../stores/TaskStore.js')
var CURRENT_PAGE_CHANGE = 'TaskToReleaseAssign';
var ActionTypes = ConfigConstants.ActionTypes;

var _currentPage = '';

var _template = [];
var _list = [];
var _fitler = [];

function _getTaskList(list, template) {
  _list = list;
  _template = template;
}

function _selectPage(currentPage) {
  _currentPage = currentPage;
}

var ReleaseStore = assign({}, EventEmitter.prototype, {

  emitChange: function() {

    this.emit(CURRENT_PAGE_CHANGE);
  },

  /**_selectPage
   * @param {function} callback
   */
  addChangeListener: function(callback) {
    // console.debug('home store add listener');

    this.on(CURRENT_PAGE_CHANGE, callback);
  },

  /**
   * @param {function} callback
   */
  removeChangeListener: function(callback) {

    this.removeListener(CURRENT_PAGE_CHANGE, callback);
  },

  getList() {
      return _list;
  },

  getTemplate() {
      return _template;
  },

  getCurrentPage() {
    return _currentPage;
  },


setFilter(intype , instatus ,insearch){
	_fitler = { type : intype , status : instatus , search : insearch};
},
getFilter(){
	return _fitler;
}
  // var _getTaskList = function(list, template) {
  //   _list = list;
  //   _template = template;
  // };
});

ReleaseStore.dispatchToken = AppDispatcher.register(function(action) {
  if(!!action.actionType){
    switch(action.actionType) {
  		case ActionTypes.RELEASE_LIST:
        _getTaskList(action.data.list, action.data.template);
        TaskStore.emitChange();
  			break;
  		default:
  			// no action
  	}
  }
});

module.exports = ReleaseStore;

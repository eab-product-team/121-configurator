let AppDispatcher = require('../dispatcher/AppDispatcher.js');
let EventEmitter = require('events').EventEmitter;
let assign = require('object-assign');
let ConfigConstants = require('../constants/ConfigConstants');

var ActionTypes = ConfigConstants.ActionTypes;

var _currentActions = 0;

var _title = {};
var _actions = [];
var _open = false;
var _dialog = undefined;

var UPDATE_DIALOG = 'UPDATE_DIALOG';

function _setDialog(dialog) {
  _dialog = dialog;
}

var _updateValues = function(values) {
  _values = values;
};

var MsgDialogStore = assign({}, EventEmitter.prototype, {
  /**
   * @param {function} callback
   */
  addChangeListener: function(callback) {
    this.on(UPDATE_DIALOG, callback);
  },

  /**
   * @param {function} callback
   */
  removeChangeListener: function(callback) {
    this.removeListener(UPDATE_DIALOG, callback);
  },

  getOpen: function() {
    return _open;
  },

  setOpen: function(){
    _open = true;
    this.emit(UPDATE_DIALOG);
  },

  setClose: function(){
    _open = false;
    this.emit(UPDATE_DIALOG);
  },

  setDialog(dialog) {
    _open = !!dialog;
    _setDialog(dialog);
    this.emit(UPDATE_DIALOG);
  },

  getDialog() {
    return _dialog;
  }
});

MsgDialogStore.dispatchToken = AppDispatcher.register(function(action) {
  switch (action.actionType) {
    case ActionTypes.PAGE_REDIRECT:
    case ActionTypes.CHANGE_PAGE:
    case ActionTypes.CLOSE_MSG_DIALOG:
      if (MsgDialogStore.getOpen()) {
        MsgDialogStore.setClose();
      }
      break;
    case ActionTypes.SHOW_MSG_DIALOG:
      var dialog = undefined;
      if (action) {
        if (action.dialog) {
          dialog = action.dialog;
        }
        if (action.data) {
          if (action.data.dialog) {
            dialog = action.data.dialog;
          }
        }
      }

      if (dialog) {
        MsgDialogStore.setDialog(dialog);
      }
      break;
    default:
      // no action
  }
});

module.exports = MsgDialogStore;

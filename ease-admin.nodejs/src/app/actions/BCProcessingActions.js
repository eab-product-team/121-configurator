
let ConfigConstants = require('../constants/ConfigConstants.js');
var HomeStore = require('../stores/HomeStore');
var AppBarStore = require('../stores/AppBarStore');
var AppDispatcher = require('../dispatcher/AppDispatcher');
let ActionTypes = ConfigConstants.ActionTypes;
var BCProcessingActions = {	
	 
	getStatus: function(){
		var page = HomeStore.getCurrentPage();
		var para = {
			p1: page.module
		}
		callServer("/BCProcessing", para);
	},
	perform: function(action, callback) {
		var page = HomeStore.getCurrentPage();
		var changedValues =  ContentStore.getPageContent().changedValues;
		  
		if (action) {
			var para = {
				p0: convertObj2EncodedJsonStr(changedValues),
				p1: page.module
			}
			callServer(action, para, callback);
		}
	},
	resubmit: function(action, data, callback) {
		if(data.selectedType[0] == 'rls' || data.selectedType[0] == 'wfi') {
			let para = {
				p0: convertObj2EncodedJsonStr(Object.assign(data, {"resubmit" : true}))
			};
			callServer("/BCProcessing", para);
		} else if(data.selectedType[0] == 'pymt') {
			let parm = {
				"policyNumbers" : [data.policyNumber],
				"resubmit": true
			}
			let para = {
				p0: convertObj2EncodedJsonStr(Object.assign(data, parm))
			};
			callServer("/BCProcessing", para);
		}
		// console.log('this is bcp action.js', data);
	},

	getBCPDetail: function(type, policynumber) {
			var para = {
				p0: type.toUpperCase(),
				p1: policynumber
			}
			// console.log('callserver para', para);
			callServer("/BCProcessing/Detail", para);
		
	},
	getBCPDetailError: function(type, policynumber, taken_by_batch_no, created_by) {
		var para = {
			p0: type.toUpperCase(),
			p1: policynumber,
			p2: taken_by_batch_no,
			p3: created_by
		}
		callServer("/BCProcessing/Detail/Error", para);
	},
	changeErrorMsg: function(msg) {
		AppDispatcher.dispatch({
			actionType: ActionTypes.CHANGE_ERROR_MSG,
			values: msg
		})
	}

};

module.exports = BCProcessingActions;

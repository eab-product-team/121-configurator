var ConfigConstants = require('../constants/ConfigConstants');
var AppDispatcher = require('../dispatcher/AppDispatcher');
let DialogStore = require('../stores/DialogStore.js');
var ActionTypes = ConfigConstants.ActionTypes;

var DialogActions = {

  showDialog(dialog,beforePositive, afterPositive) { // HIM
    if (dialog) {
      dialog.beforePositive = beforePositive;
      dialog.afterPositive = afterPositive;
    }

    AppDispatcher.dispatch({
      actionType: ActionTypes.SHOW_DIALOG,
      dialog: dialog,
    })
  },

  showMessageDialog(dialog, beforePositive, afterPositive) { // HIM
    if (dialog) {
      dialog.beforePositive = beforePositive;
      dialog.afterPositive = afterPositive;
    }

    AppDispatcher.dispatch({
      actionType: ActionTypes.SHOW_MSG_DIALOG,
      dialog: dialog,
    })
  },

  updatevalues(values){
    AppDispatcher.dispatch({
      actionType: ActionTypes.UPDATE_DIALOG,
      values: values
    })
  },
  closeMsgDialog() {
    AppDispatcher.dispatch({
      actionType: ActionTypes.CLOSE_MSG_DIALOG
    })
  },
  closeDialog() {
    AppDispatcher.dispatch({
      actionType: ActionTypes.CLOSE_DIALOG,
    })
  },

  submitSelected(path, values, rows, content, page) {
    if (values) {
      values['rows'] = rows;
    }
    if (content.values.id != null) {
      values['keyList'] = content.values.id;
    }
    if (content.values.backPageKey != null) {
      values['backPageKey'] = content.values.backPageKey;
    }
    if (checkExist(content, 'values.list') && content.values.list[0] && !isEmpty(content.values.list[0].channel_code)) {
      values['channelCode'] = content.values.list[0].channel_code;
    }

    var data = {
      p0: convertObj2EncodedJsonStr(values),
      p1: page.module,
      p10: convertObj2EncodedJsonStr(page)
    }
    AppDispatcher.dispatch({
      actionType: ActionTypes.MasterTable.SELECTION_CHANGE,
      rows: [],
      actionIndex: 0 
    });

    callServer(path, data);

  },

  submitEditValues(path, values, content, page) {
    if (!values) {
      values = {};
    }
    values.values = content.changedValues || content.values;

    var data = {
      p0: convertObj2EncodedJsonStr(values),
      p1: page.module,
      p10: convertObj2EncodedJsonStr(page)
    }
    callServer(path, data);
  }
}

module.exports = DialogActions;

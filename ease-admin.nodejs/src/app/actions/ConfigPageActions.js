
import AppDispatcher from '../dispatcher/AppDispatcher';
import {ActionTypes} from '../constants/ConfigConstants';
import HomeStore from '../stores/HomeStore';
import ContentStore from '../stores/ContentStore';

module.exports = {

  getSummaries: function (module, type) {
    const paramStr = window.convertObj2ParaStr({ module, type });
    return window.callRestAPI(`/rest/configs?${paramStr}`, 'GET');
  },

  getSummary: function (id) {
    return window.callRestAPI(`/rest/configs/${id}`, 'GET');
  },

  createSummary: function (summary) {
    return window.callRestAPI('/rest/configs', 'POST', summary);
  },

  updateSummary: function (summary) {
    return window.callRestAPI(`/rest/configs/${summary.id}`, 'PUT', summary);
  },

  deleteSummaries: function (ids) {
    const paramStr = window.convertObj2ParaStr({ ids });
    return window.callRestAPI(`/rest/configs?${paramStr}`, 'DELETE');
  },

  createVersion: function (summaryId, description) {
    return window.callRestAPI(`/rest/configs/${summaryId}/versions`, 'POST', { description });
  },

  updateVersion: function (summaryId, version) {
    return window.callRestAPI(`/rest/configs/${summaryId}/versions/${version.id}`, 'PUT', version);
  },

  deleteVersions: function (summaryId, versionIds) {
    const paramStr = window.convertObj2ParaStr({ versionIds });
    return window.callRestAPI(`/rest/configs/${summaryId}/versions?${paramStr}`, 'DELETE');
  },

  cloneVersion: function (summaryId, versionId, description, targetSummaryId) {
    const paramStr = window.convertObj2ParaStr({
      description,
      targetId: targetSummaryId
    });
    return window.callRestAPI(`/rest/configs/${summaryId}/versions/${versionId}/clone?${paramStr}`, 'POST', null);
  },

  openConfig: function (configId) {
    AppDispatcher.dispatch({
      actionType: ActionTypes.CHANGE_PAGE,
      page: {
        id: 'configSummary',
        configId,
        value: {
          id: 'configSummary',
          selectPageId: null,
          version: 1
        }
      }
    });
  },

  initConfigPage: function (versionId) {
    window.callRestAPI(`/controller/configDetails/init/${versionId}`, 'GET').then(({ template, sections, version }) => {
      const pageId = 'ConfigDetail';
      const values = Object.assign({
        id: `config-${versionId}`,
        version: 1,
        selectPage: {
          id: null
        },
        noSave: true
      }, sections);
      AppDispatcher.dispatch({
        actionType: ActionTypes.CHANGE_PAGE,
        page: {
          id: pageId,
          module: version.summary.module,
          value: {
            id: pageId,
            selectPageId: null,
            version: 1
          },
          configVersion: version
        },
        data: {
          appBar: {
            title: {
              type: 'label',
              primary: `${version.summary.name} (v${version.versionNum})`
            },
            actions: [[{
              id: 'save',
              title: 'Save',
              type: 'customButton',
              onTouchTap: () => {
                this.saveConfigFromPage(versionId);
              }
            }]]
          },
          content: {
            template,
            changedValues: _.cloneDeep(values),
            values: values
          }
        }
      });
    });
  },

  saveConfigFromPage: function (versionId) {
    const page = HomeStore.getCurrentPage();
    const {changedValues: values, template} = ContentStore.getPageContent();
    const sections = {};
    const sectionKeys = this.getSectionKeys(template);
    _.each(values, (value, key) => {
      if (sectionKeys.indexOf(key) > -1) {
        sections[key] = value;
      }
    });
    return window.callRestAPI(`/controller/configDetails/${versionId}`, 'PUT', sections).then(result => {
      const content = ContentStore.getPageContent();
      content.valuesBackup = cloneObject(values);
      return result;
    });
  },

  getSectionKeys: function (template) {
    let keys = [];
    _.each(template.items, item => {
      if (item.type === 'sectionGroup' && item.items) {
        keys = keys.concat(this.getSectionKeys(item));
      } else if (item.type === 'section') {
        keys.push(item.id);
      }
    });
    return keys;
  }

};

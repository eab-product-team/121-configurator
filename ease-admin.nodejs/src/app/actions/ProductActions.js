var ConfigConstants = require('../constants/ConfigConstants');
var AppDispatcher = require('../dispatcher/AppDispatcher');
var ActionTypes = ConfigConstants.ActionTypes;
var AppStore = require('../stores/AppStore');
var HomeStore = require('../stores/HomeStore');
var ProductStore = require('../stores/ProductStore.js');

var ProductActions = {

  goProductDetail(product, backPage) {
    var data = {
      id: product.prodCode,
      version: product.version
    };
    callServer("/Products/Detail", {
      p0: convertObj2EncodedJsonStr(data),
    });
  },

  changePage(action){
  	AppDispatcher.dispatch(action)
  }
}

module.exports = ProductActions;

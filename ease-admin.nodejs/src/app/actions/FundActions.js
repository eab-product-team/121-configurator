import ConfigConstants from '../constants/ConfigConstants';
import AppDispatcher from '../dispatcher/AppDispatcher';
const ActionTypes = ConfigConstants.ActionTypes;

module.exports = {

  listFunds: function () {
    return window.callRestAPI('/rest/funds', 'GET');
  },

  createFund: function (value) {
    return window.callRestAPI('/rest/funds', 'POST', value);
  },

  getFund: function (id) {
    return window.callRestAPI('/rest/funds/' + id, 'GET');
  },

  updateFund: function (fund) {
    return window.callRestAPI('/rest/funds/' + fund.id, 'PUT', fund);
  },

  deleteFund: function (id) {
    return window.callRestAPI('/rest/funds/' + id, 'DELETE');
  },

  exportFund: function (ids) {
    const paramStr = window.convertObj2ParaStr({ ids });
    window.downloadFromServer(`/rest/funds/export?${paramStr}`, 'GET');
  },

  openFundList: function () {
    AppDispatcher.dispatch({
      actionType: ActionTypes.CHANGE_PAGE,
      page: {
        id: '/Funds',
        module: 'fund',
        value: {
          id: '/Funds',
          selectPageId: null,
          version: 1
        }
      }
    });
  },

  openFundDetail: function (fundId) {
    AppDispatcher.dispatch({
      actionType: ActionTypes.CHANGE_PAGE,
      page: {
        id: '/Funds/Detail',
        module: 'fund',
        fundId,
        value: {
          id: '/Funds/Detail',
          selectPageId: null,
          version: 1
        }
      }
    });
  }
}

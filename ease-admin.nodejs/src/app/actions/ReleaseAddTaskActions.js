var ConfigConstants = require('../constants/ConfigConstants');
var AppDispatcher = require('../dispatcher/AppDispatcher');
var ActionTypes = ConfigConstants.ActionTypes;

var ReleaseAddTaskActions = {
  addTask(rows, taskList) {

    var _rows = {
     rows: rows,
     taskList: taskList
    };

    AppDispatcher.dispatch({
      actionType: ActionTypes.ADD_TASK,
      action: '',
      rows: _rows
    })
  },
}

module.exports = ReleaseAddTaskActions;

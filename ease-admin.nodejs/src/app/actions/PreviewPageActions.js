let AppDispatcher = require('../dispatcher/AppDispatcher.js');
let ConfigConstants = require('../constants/ConfigConstants.js');
let ActionTypes = ConfigConstants.ActionTypes;

module.exports = {

  back(){
    AppDispatcher.dispatch({
      actionType: ActionTypes.PREVIEW_PDF
    })
  }

}

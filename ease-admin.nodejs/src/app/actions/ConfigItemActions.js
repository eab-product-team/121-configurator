
module.exports = {

  listConfigs: function (type, query) {
    const params = {
      configType: type,
      q: query
    };
    return window.callRestAPI('/rest/configItems?' + window.convertObj2ParaStr(params), 'GET');
  },

  createConfig: function (type, name, description, value) {
    return window.callRestAPI('/rest/configItems', 'POST', {
      configType: type,
      name,
      description,
      value: typeof value === 'string' ? value : JSON.stringify(value)
    });
  },

  getConfig: function (id) {
    return window.callRestAPI('/rest/configItems/' + id, 'GET');
  },

  updateConfig: function (config) {
    return window.callRestAPI('/rest/configItems/' + config.id, 'PUT', config);
  },

  deleteConfig: function (id) {
    return window.callRestAPI('/rest/configItems/' + id, 'DELETE');
  }
}

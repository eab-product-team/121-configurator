import React, {Component, PropTypes} from 'react';
import {Dialog, FlatButton, TextField} from 'material-ui';

export default class EditReleaseDialog extends Component {

  static propTypes = {
    onSave: PropTypes.func,
    onClose: PropTypes.func
  };

  constructor() {
    super();
    this.state = {
      name: null
    };
  }

  onRequestClose() {
    const {onClose} = this.props;
    onClose && onClose();
  }

  render() {
    const {onSave} = this.props;
    const {name} = this.state;
    const actions = [
      <FlatButton label="Cancel" onTouchTap={() => this.onRequestClose()} />,
      <FlatButton
        primary
        label="Save"
        disabled={!name}
        onTouchTap={() => onSave && onSave(name)}
      />
    ];
    return (
      <Dialog
        open={true}
        title="Save Release"
        modal={false}
        onRequestClose={() => this.onRequestClose()}
        actions={actions}
      >
        <TextField
          fullWidth
          floatingLabelText="Name"
          value={name}
          onChange={(event) => this.setState({ name: event.target.value })}
        />
      </Dialog>
    );
  }

}

import React, {Component, PropTypes} from 'react';
import {Table, TableHeader, TableHeaderColumn, TableBody, TableRow, TableRowColumn} from 'material-ui';

import ReadOnlyField from '../CustomView/ReadOnlyField.jsx';
import ConfirmDialog from '../CustomView/ConfirmDialog.jsx';
import MsgDialog from '../CustomView/MsgDialog.jsx';
import EditReleaseDialog from './EditReleaseDialog.jsx';
import AddItemDialog from './AddItemDialog.jsx';
import PublishDialog from './PublishDialog.jsx';

import ReleaseActions from '../../actions/ReleaseActions';
import AppBarActions from '../../actions/AppBarActions';
import ConfigPageActions from '../../actions/ConfigPageActions';
import ConfigConstants from '../../constants/ConfigConstants';

export default class ReleaseSummary extends Component {

  static propTypes = {
    id: PropTypes.number
  };

  constructor(props) {
    super(props);
    this.state = {
      release: {},
      selectedRows: [],
      confirmRemove: false,
      editing: false,
      adding: false,
      publishing: false,
      dialogMsg: null
    };
    this.fetch(this.props.id);
  }

  componentDidUpdate() {
    this.updateAppbar();
  }

  fetch(id) {
    ReleaseActions.getRelease(id).then(this.reload);
  }

  reload = (release) => {
    this.setState({
      release,
      selectedRows: [],
      confirmRemove: false,
      editing: false,
      adding: false,
      publishing: false,
      dialogMsg: null
    }, () => this.updateAppbar());
  }

  updateAppbar() {
    const {id} = this.props;
    const {release, selectedRows} = this.state;
    const actions = [];
    if (selectedRows.length) {
      actions.push({
        id: 'remove',
        title: 'Remove',
        type: 'customButton',
        onTouchTap: () => this.setState({ confirmRemove: true })
      });
    } else {
      actions.push({
        id: 'export',
        title: 'Export',
        type: 'customButton',
        onTouchTap: () => ReleaseActions.exportRelease(id)
      });
      actions.push({
        id: 'publish',
        title: 'Publish',
        type: 'customButton',
        onTouchTap: () => this.setState({ publishing: true })
      });
      actions.push({
        id: 'edit',
        title: 'Edit',
        type: 'customButton',
        onTouchTap: () => this.setState({ editing: true })
      });
      actions.push({
        id: 'add',
        title: 'Add Item',
        type: 'customButton',
        onTouchTap: () => this.setState({ adding: true })
      });
    }
    AppBarActions.updateAppBar(release.name, actions);
  }

  _getDetails() {
    const {id} = this.props;
    const {name, updateBy, updateDate, createBy, createDate} = this.state.release;
    return [
      { key: 'id', label: 'ID', value: id },
      { key: 'name', label: 'Name', value: name },
      { key: 'updateBy', label: 'Last Updated By', value: updateDate && (updateBy + ' / ' + new Date(updateDate).format(ConfigConstants.DateTimeFormat)) },
      { key: 'createBy', label: 'Created By', value: createDate && (createBy + ' / ' + new Date(createDate).format(ConfigConstants.DateTimeFormat)) }
    ];
  }

  selectVersion(versionId) {
    ConfigPageActions.initConfigPage(versionId);
  }

  addVersion(versionId) {
    const {id} = this.props;
    ReleaseActions.addVersion(id, versionId).then(this.reload);
  }

  saveRelease(name) {
    const {id} = this.props;
    ReleaseActions.updateRelease(id, name).then(this.reload);
  }

  onPublish(envId) {
    const {id} = this.props;
    ReleaseActions.publishRelease(id, envId).then(() => {
      this.setState({
        publishing: false,
        dialogMsg: 'The release is successfully published.'
      });
    });
  }

  removeVersions() {
    const {id} = this.props;
    const {release, selectedRows} = this.state;
    const versionIds = _.map(selectedRows, row => release.versions[row].id);
    ReleaseActions.removeVersions(id, versionIds).then(this.reload);
  }

  render() {
    const {release, selectedRows, confirmRemove, editing, adding, publishing, dialogMsg} = this.state;
    return (
      <div className="PageContent" style={{ height: 'calc(100vh - 68px)', overflow: 'auto' }}>
        <h2 className="DetailsItem" style={{ paddingTop: 0 }}>Release Summary</h2>
        <div className="hBoxContainer">
          {_.map(this._getDetails(), detail => (
            <ReadOnlyField
              key={detail.key}
              className="DetailsItem"
              floatingLabelText={detail.label}
              defaultValue={(detail.value || '-') + ''}
            />
          ))}
        </div>
        <h2 className="DetailsItem" style={{ marginTop: 0, paddingTop: 0 }}>Release Items</h2>
        <Table multiSelectable onRowSelection={(rows) =>  this.setState({ selectedRows: rows })}>
          <TableHeader enableSelectAll={false}>
            <TableRow>
              <TableHeaderColumn>Type</TableHeaderColumn>
              <TableHeaderColumn>Name</TableHeaderColumn>
              <TableHeaderColumn>Version</TableHeaderColumn>
              <TableHeaderColumn>Last Updated By</TableHeaderColumn>
              <TableHeaderColumn>Created By</TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody deselectOnClickaway={false}>
            {_.map(release.versions, (version, index) => (
              <TableRow key={version.id} selected={selectedRows.indexOf(index) > -1}>
                <TableRowColumn onTouchTap={() => this.selectVersion(version.id)}>{version.summary.module === 'product' ? 'Product': 'PDF'}</TableRowColumn>
                <TableRowColumn onTouchTap={() => this.selectVersion(version.id)}>{version.summary.name}</TableRowColumn>
                <TableRowColumn onTouchTap={() => this.selectVersion(version.id)}>{version.versionNum + (version.description ? ' (' + version.description + ')' : '')}</TableRowColumn>
                <TableRowColumn onTouchTap={() => this.selectVersion(version.id)}>{version.updateBy}</TableRowColumn>
                <TableRowColumn onTouchTap={() => this.selectVersion(version.id)}>{version.createBy}</TableRowColumn>
              </TableRow>
            ))}
          </TableBody>
        </Table>
        {adding ? (
          <AddItemDialog
            onSave={versionId => this.addVersion(versionId)}
            onClose={() => this.setState({ adding: false })}
          />
        ) : null}
        {editing ? (
          <EditReleaseDialog
            onSave={name => this.saveRelease(name)}
            onClose={() => this.setState({ editing: false })}
          />
        ) : null}
        {publishing ? (
          <PublishDialog
            onPublish={envId => this.onPublish(envId)}
            onClose={() => this.setState({ publishing: false })}
          />
        ) : null}
        <ConfirmDialog
          open={confirmRemove}
          msg="Are you sure to remove the selected items from the release?"
          onConfirm={() => this.removeVersions()}
          onClose={() => this.setState({ confirmRemove: false })}
        />
        {dialogMsg ? (
          <MsgDialog
            msg={dialogMsg}
            onClose={() => this.setState({ dialogMsg: null })}
          />
        ) : null}
      </div>
    );
  }

}

import React, {Component, PropTypes} from 'react';
import {Dialog, FlatButton, TextField, List, ListItem, Tabs, Tab, Divider} from 'material-ui';
import { SelectableContainerEnhance } from 'material-ui/lib/hoc/selectable-enhance';
const SelectableList = SelectableContainerEnhance(List);

import ConfigPageActions from '../../actions/ConfigPageActions';

export default class AddItemDialog extends Component {

  static propTypes = {
    onSave: PropTypes.func,
    onClose: PropTypes.func
  }

  constructor() {
    super();
    this.state = {
      items: {},
      searchTerm: {},
      selectedItem: null,
      selectedVersion: null,
      versions: []
    };
  }

  componentDidMount() {
    this.onSelectTab('product');
  }

  onRequestClose() {
    const {onClose} = this.props;
    onClose && onClose();
  }

  onSelectTab(key) {
    if (!this.state.items[key]) {
      ConfigPageActions.getSummaries(key).then(items => {
        const newItems = {
          ...this.state.items
        };
        newItems[key] = items;
        this.setState({
          items: newItems,
          selectedItem: null,
          selectedVersion: null,
          versions: []
        });
      });
    }
  }

  getItemList(items) {
    const {selectedItem} = this.state;
    const list = [];
    _.each(items, item => {
      list.push(
        <ListItem
          key={item.id}
          value={item.id}
          primaryText={item.name + (item.description ? ' (' + item.description + ')' : '')}
        />
      );
      list.push(<Divider />)
    });
    return (
      <SelectableList
        style={{ maxHeight: 400, overflow: 'auto' }}
        valueLink={{
          value: selectedItem,
          requestChange: (e, id) => {
            ConfigPageActions.getSummary(id).then(summary => {
              this.setState({
                selectedItem: id,
                versions: summary.versions
              });
            });
          }
        }}
      >
        {list}
      </SelectableList>
    );
  }

  getTab(key, title) {
    const {items, searchTerm} = this.state;
    const filter = searchTerm[key] && searchTerm[key].toUpperCase();
    const filteredItems = _.filter(items[key], item => {
      return !filter || item.name.toUpperCase().indexOf(filter) > -1;
    });
    return (
      <Tab value={key} label={title}>
        <TextField
          fullWidth
          hintText={searchTerm[key] ? null : "Type to filter"}
          value={searchTerm[key]}
          onChange={(event) => {
            this.setState({
              searchTerm: {
                ...searchTerm,
                [key]: event.target.value
              }
            });
          }}
        />
        {this.getItemList(filteredItems)}
      </Tab>
    );
  }

  getVersionList() {
    const {selectedItem, selectedVersion, versions} = this.state;
    if (selectedItem === null) {
      return null;
    }
    const list = [];
    _.each(versions, (version) => {
      list.push(
        <ListItem
          key={version.id}
          value={version.id}
          primaryText={version.description}
        />
      );
      list.push(<Divider />);
    });
    return (
      <SelectableList
        valueLink={{
          value: selectedVersion,
          requestChange: (e, id) => this.setState({ selectedVersion: id })
        }}
      >
        {list}
      </SelectableList>
    );
  }

  render() {
    const {onSave} = this.props;
    const {items, selectedVersion} = this.state;
    const actions = [
      <FlatButton label="Cancel" onTouchTap={() => this.onRequestClose()} />,
      <FlatButton
        primary
        label="Save"
        disabled={!selectedVersion}
        onTouchTap={() => onSave && onSave(selectedVersion)}
      />
    ];
    return (
      <Dialog
        open={true}
        title="Add Item to Release"
        modal={false}
        contentStyle={{ minWidth: '80%' }}
        bodyStyle={{ minHeight: 400 }}
        onRequestClose={() => this.onRequestClose()}
        actions={actions}
      >
        <table style={{ width: '100%', tableLayout: 'fixed', borderSpacing: 8, borderCollapse: 'separate' }}>
          <tr>
            <th style={{ fontSize: 18 }}>Type</th>
            <th style={{ fontSize: 18 }}>Version</th>
          </tr>
          <tr>
            <td style={{ verticalAlign: 'top' }}>
              <Tabs tabItemContainerStyle={{ borderBottom: '2px solid #AAAAAA' }} onChange={value => this.onSelectTab(value)}>
                {this.getTab('product', 'Products')}
                {this.getTab('pdf', 'PDFs')}
              </Tabs>
            </td>
            <td style={{ verticalAlign: 'top' }}>
              {this.getVersionList()}
            </td>
          </tr>
        </table>
      </Dialog>
    );
  }
}

import React, {Component, PropTypes} from 'react';
import {Table, TableHeader, TableHeaderColumn, TableBody, TableRow, TableRowColumn} from 'material-ui';

import ConfirmDialog from '../CustomView/ConfirmDialog.jsx';
import MsgDialog from '../CustomView/MsgDialog.jsx';
import EditReleaseDialog from './EditReleaseDialog.jsx';
import PublishDialog from './PublishDialog.jsx';

import AppBarActions from '../../actions/AppBarActions';
import ReleaseActions from '../../actions/ReleaseActions';
import ConfigConstants from '../../constants/ConfigConstants';

export default class ReleaseList extends Component {

  constructor() {
    super();
    this.state = {
      releases: [],
      selectedRows: [],
      creating: false,
      publishing: false,
      confirmDelete: false,
      dialogMsg: null
    };
    this.fetch();
  }

  componentDidUpdate() {
    this.updateAppbar();
  }

  fetch() {
    ReleaseActions.getReleases().then(releases => {
      this.setState({
        releases,
        selectedRows: [],
        creating: false,
        publishing: false,
        confirmDelete: false,
        dialogMsg: null
      }, () => {
        this.updateAppbar();
      });
    });
  }

  updateAppbar() {
    const {selectedRows} = this.state;
    const actions = [];
    if (selectedRows.length) {
      if (selectedRows.length === 1) {
        actions.push({
          id: 'export',
          title: 'Export',
          type: 'customButton',
          onTouchTap: () => {
            const {selectedRows, releases} = this.state;
            ReleaseActions.exportRelease(releases[selectedRows[0]].id);
          }
        });
        actions.push({
          id: 'publish',
          title: 'Publish',
          type: 'customButton',
          onTouchTap: () => this.setState({ publishing: true })
        });
      }
      actions.push({
        id: 'delete',
        title: 'Delete',
        type: 'customButton',
        onTouchTap: () => this.setState({ confirmDelete: true })
      });
    }
    actions.push({
      id: 'new',
      title: 'Create Release',
      type: 'customButton',
      onTouchTap: () => this.setState({ creating: true })
    });
    AppBarActions.updateAppBar('Releases', actions);
  }

  onSelect(id) {
    ReleaseActions.openRelease(id);
  }

  onCreate(name) {
    ReleaseActions.createRelease(name).then(() => this.fetch());
  }

  onPublish(envId) {
    const {selectedRows, releases} = this.state;
    ReleaseActions.publishRelease(releases[selectedRows[0]].id, envId).then(() => {
      this.setState({
        publishing: false,
        dialogMsg: 'The release is successfully published.'
      });
    });
  }

  onDelete() {
    const {releases, selectedRows} = this.state;
    const ids = _.map(selectedRows, row => releases[row].id);
    ReleaseActions.deleteReleases(ids).then(() => this.fetch());
  }

  render() {
    const {releases, selectedRows, creating, publishing, confirmDelete, dialogMsg} = this.state;
    return (
      <div className="PageContent" style={{ height: 'calc(100vh - 68px)', overflow: 'auto' }}>
        <Table multiSelectable onRowSelection={(rows) =>  this.setState({ selectedRows: rows })}>
          <TableHeader enableSelectAll={false}>
            <TableRow>
              <TableHeaderColumn>Release ID</TableHeaderColumn>
              <TableHeaderColumn>Release Name</TableHeaderColumn>
              <TableHeaderColumn>Last Updated By</TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody deselectOnClickaway={false}>
            {_.map(releases, (release, index) => (
              <TableRow key={release.id} selected={selectedRows.indexOf(index) > -1}>
                <TableRowColumn onTouchTap={() => this.onSelect(release.id)}>{release.id}</TableRowColumn>
                <TableRowColumn onTouchTap={() => this.onSelect(release.id)}>{release.name}</TableRowColumn>
                <TableRowColumn onTouchTap={() => this.onSelect(release.id)}>{release.updateBy + ' / ' + new Date(release.updateDate).format(ConfigConstants.DateTimeFormat)}</TableRowColumn>
              </TableRow>
            ))}
          </TableBody>
        </Table>
        {creating ? (
          <EditReleaseDialog
            onSave={name => this.onCreate(name)}
            onClose={() => this.setState({ creating: false })}
          />
        ) : null}
        {publishing ? (
          <PublishDialog
            onPublish={envId => this.onPublish(envId)}
            onClose={() => this.setState({ publishing: false })}
          />
        ) : null}
        <ConfirmDialog
          open={confirmDelete}
          msg="Are you sure to delete the selected releases?"
          onConfirm={() => this.onDelete()}
          onClose={() => this.setState({ confirmDelete: false })}
        />
        {dialogMsg ? (
          <MsgDialog
            msg={dialogMsg}
            onClose={() => this.setState({ dialogMsg: null })}
          />
        ) : null}
      </div>
    );
  }
}

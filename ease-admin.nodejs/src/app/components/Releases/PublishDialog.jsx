import React, {Component, PropTypes} from 'react';
import {Dialog, FlatButton, TextField, RadioButtonGroup, RadioButton} from 'material-ui';

import ReleaseActions from '../../actions/ReleaseActions';

export default class PublishDialog extends Component {

  static propTypes = {
    onPublish: PropTypes.func,
    onClose: PropTypes.func
  };

  constructor() {
    super();
    this.state = {
      envs: [],
      envId: null
    };
  }

  componentDidMount() {
    ReleaseActions.getEnvs().then(envs => {
      this.setState({ envs });
    });
  }

  onRequestClose() {
    const {onClose} = this.props;
    onClose && onClose();
  }

  render() {
    const {onPublish} = this.props;
    const {envs, envId} = this.state;
    const actions = [
      <FlatButton label="Cancel" onTouchTap={() => this.onRequestClose()} />,
      <FlatButton
        primary
        label="Publish"
        disabled={!envId}
        onTouchTap={() => onPublish && onPublish(envId)}
      />
    ];
    return (
      <Dialog
        open
        title="Publish to:"
        modal={false}
        onRequestClose={() => this.onRequestClose()}
        actions={actions}
      >
        <RadioButtonGroup name="env" onChange={(e, value) => this.setState({ envId: value })}>
          {_.map(envs, env => (
            <RadioButton
              key={env.envId}
              value={env.envId}
              label={env.name}
            />
          ))}
        </RadioButtonGroup>
      </Dialog>
    );
  }
}

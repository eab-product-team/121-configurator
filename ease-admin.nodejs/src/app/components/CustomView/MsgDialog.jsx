import React, {Component, PropTypes} from 'react';
import {Dialog, FlatButton} from 'material-ui';

export default class MsgDialog extends Component {

  static propTypes = {
    msg: PropTypes.string,
    onClose: PropTypes.func
  };

  render() {
    const {msg, onClose} = this.props;
    return (
      <Dialog
        open
        modal={false}
        actions = {[
          <FlatButton label='OK' primary onTouchTap={() => onClose && onClose()} />
        ]}
      >
        {msg}
      </Dialog>
    );
  }
}

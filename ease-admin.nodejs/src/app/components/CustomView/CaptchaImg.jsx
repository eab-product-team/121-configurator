var React = require('react');

var CaptchaImg = React.createClass({

	propTypes: {
		style: React.PropTypes.object
	},

	render: function() {
		return <div style={this.props.style?this.props.style:null}><img src={'/captcha.jpg'}/></div> 
	}

});

module.exports = CaptchaImg;

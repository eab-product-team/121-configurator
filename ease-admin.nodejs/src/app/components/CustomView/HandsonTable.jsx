let React = require('react');
let ReactDOM = require('react-dom');
let mui = require('material-ui');
let RaisedButton = mui.RaisedButton;

var HandsonTable = React.createClass({

  hot: null,

  fields: [],
  cellValidation: [],
  mandatoryFields: [],

  isValidateCells: false,
  isEmptyGrid: false,

  ageFrom: undefined,
  ageTo: undefined,
  groupBy: undefined,

  errorMsg: undefined,

  propTypes:{
    template: React.PropTypes.object,
    gridData: React.PropTypes.any,
    pageData: React.PropTypes.any,
    insertable: React.PropTypes.bool,
    disabled: React.PropTypes.bool,
    onChange: React.PropTypes.func,
  },

  getData() {
    return this.trimData(cloneArray(this.hot.getSourceData()));
    // return this.hot.getSourceData();
  },

  trimData(data, validateKeys) {
    // console.debug("[Handsontable] - [trimData] start data: ", this.fields, data);

    this.isEmptyGrid = true;

    if (data instanceof Array) {
      var rowToDelete = [];
      for (let i = 0; i < data.length; i++) {
        var row = data[i];
        // console.debug("trimData row: ", row);
        var emptyRow = true;
        if (row instanceof Object) {
          var inputKeys = Object.keys(row);
          var keys = this.fields;

          // Validate keys
          if (validateKeys) {
            for (let j = 0; j < inputKeys.length; j++) {
              if (keys.indexOf(inputKeys[j]) == -1) {
                alert(getLocalizedText("HANDSONTABLE_MSG.INVALID_HEADER","Invalid column header."),
                  getLocalizedText("HANDSONTABLE_MSG.INVALID_IMPORT","Import failed"));
                return false;
              }
            }
          }

          for (let j = 0; j < keys.length; j++) {
            var cell = row[keys[j]];

            // console.debug("trimData row Type: ", typeof cell);
            if (typeof cell === "string" && cell.length > 0) {
              emptyRow = false;
              // console.debug("trimData row String: ", cell);
            } else if (typeof cell === "number") {
              emptyRow = false;
              // console.debug("trimData row Number: ", cell);
            } else {
              cell = "";
              // console.debug("trimData row ELSE: ", cell);
            }

            if (keys[j] == "age" && this.ageFrom != undefined && this.ageTo != undefined) {
              var age = parseInt(cell);
              if (age == NaN || age < this.ageFrom || age > this.ageTo) {
                rowToDelete.push(i);
                break;
              }
            }

            row[keys[j]] = cell;
          }

          // console.debug("[Handsontable] - [trimData] - [emptyRow]", emptyRow);
          if (!emptyRow) {
            this.isEmptyGrid = false;
          }
        }
      }

      for (let i = rowToDelete.length - 1; i >= 0; i--) {
        data.splice(rowToDelete[i], 1);
      }
    }
    // console.debug("trimData row end data: ", data);
    return data;
  },

  mandatoryChecking(data) {
    // console.debug("[Handsontable] - [mandatoryChecking]", data);
    if (data) {
      for (let i = 0; i < data.length; i++) {
        var row = data[i];
        if (row) {
          for (let j = 0; j < this.mandatoryFields.length; j++) {
            var key = this.mandatoryFields[j];
            var cell = row[key];
            if (!this.mandatoryValidator(cell)) {
              console.debug("[Handsontable] - [mandatoryChecking] FALSE", cell);
              this.errorMsg = getLocalizedText("HANDSONTABLE_MSG.MISSING_VALUES","Missing value in row ") + (i + 1) + ", " +  getLocalizedText("HANDSONTABLE_MSG.COLUMN","column ") + " '" + key + "'.";
              return false;
            }
          }
        }
      }
    }

    return true;
  },

  createGrid() {
    var el = this.refs["handsontable"];
    if (!el) return false;


    var height = 0;
    if (el.parentNode.parentNode.parentNode.clientHeight > 100) {
      height = el.parentNode.parentNode.parentNode.clientHeight - 90;
    } else {
      height = el.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.clientHeight - 138;
    }

    if (!this.hot) {
      this.hot = new Handsontable(el, {
        height: height,
        stretchH: 'all',
        autoWrapRow: true,
        rowHeaders: true,
        colHeaders: true,
        persistentState: true,
        allowInvalid: true,
        allowRemoveColumn: false,
        allowInsertColumn: false,
        afterValidate: this.afterValidate,
        afterChange: this.afterChange,
        afterCreateRow: this.afterCreateRow,
        afterRemoveRow: this.afterRemoveRow,
        fillHandle: {
          autoInsertRow: false,
        },
      });
    }

    return true;
  },

  updateGrid(props) {
    console.debug("[Handsontable] - [updateGrid] - [id]", props.template.id);

    var self = this;
    var currentValues = props.pageData;

    if (!this.hot && !this.createGrid()) {
      return;
    }

    this.fields = [];
    this.mandatoryFields = [];
    this.cellValidation = [];

    var columns = [];
    var colHeaders = [];
    var data;

    // Grid items to HandsonTable properties
    var gridItems = props.template.items;
    if (gridItems && gridItems instanceof Array) {
      // Fields
      for (let i = 0; i < gridItems.length; i++) {
        var gridItem = gridItems[i];
        this.fields.push(gridItem.id);
      }

      // Data
      data = props.gridData && props.gridData.length > 0 ? this.trimData(props.gridData) : props.insertable ? [{}] : [];

      for (let i = 0; i < gridItems.length; i++) {
        var gridItem = gridItems[i];

        if (gridItem.mandatory) {
          this.mandatoryFields.push(gridItem.id);
        }

        var column = {
          data: gridItem.id,
          type: gridItem.type,
          readOnly: gridItem.disabled,
          format: gridItem.presentation,
        };

        // Validator
        if (gridItem.type.toUpperCase() == "TEXT" || gridItem.type.toUpperCase() == "NUMERIC") {
          column.validator = function(value, callback) {
            var validated = true;

            var filled = self.mandatoryValidator(value);

            if (self.isValidateCells && this.mandatory) {
              validated = filled;
            }

            if (filled) {
              // Min & Max
              if (this.min != undefined || this.max != undefined) {
                validated = self.numberRangeValidator(value, this.min, this.max);
              }
            }

            callback(validated);
          }.bind(gridItem);
        }

        // Auto generate age
        if (gridItem.auto && gridItem.id == "age") {
          if (currentValues.ProdFeatures && currentValues.ProdFeatures.entryAge) {
            var staAgeFemale = parseInt(currentValues.ProdFeatures.entryAge.staAgeFemale);
            var staAgeMale = parseInt(currentValues.ProdFeatures.entryAge.staAgeMale);
            var endAgeFemale = parseInt(currentValues.ProdFeatures.entryAge.endAgeFemale);
            var endAgeMale = parseInt(currentValues.ProdFeatures.entryAge.endAgeMale);

            var min;
            var max;

            if (staAgeFemale != NaN && staAgeMale != NaN && endAgeFemale != NaN && endAgeMale != NaN) {
              min = staAgeFemale < staAgeMale ? staAgeFemale : staAgeMale;
              max = endAgeFemale > endAgeMale ? endAgeFemale : endAgeMale;
            } else if (staAgeFemale != NaN && endAgeFemale != NaN) {
              min = staAgeFemale;
              max = endAgeFemale;
            } else if (staAgeMale != NaN && endAgeMale != NaN) {
              min = staAgeMale;
              max = endAgeMale;
            }

            if (min && max) {
              this.ageFrom = min;
              this.ageTo = max;

              var _data = [];

              for (let j = min; j <= max; j++) {
                var found = false;
                for (let k = 0; k < data.length; k++) {
                  var row = data[k];
                  if (row.age && parseInt(row.age) == j) {
                    found = true;
                    _data.push(row);
                    break;
                  }
                }
                if (!found) {
                  _data.push({age: j});
                }
              }

              data = _data;
            } else {
              this.ageFrom = undefined;
              this.ageTo = undefined;
            }
          }
        }

        // Header
        colHeaders.push(gridItem.title);

        // Retrieve currency list from database
        if (gridItem.id == "ccy" && gridItem.type == "dropdown") {
          if (currentValues.ProdFeatures && currentValues.ProdFeatures.ccy && typeof currentValues.ProdFeatures.ccy == "string") {
            column.source = currentValues.ProdFeatures.ccy.split(",");
          }
        } else if (gridItem.options && gridItem.options.length > 0) {
          // Options
          var source = [];
          for (let j = 0; j < gridItem.options.length; j++) {
            var option = gridItem.options[j];
            source.push(option.title);
          }
          if(gridItem.id=='payMode'){
            this.groupBy = {id:'payMode', source: source};
          }
          column.source = source;
        }
        if(props.disabled)
          column.readOnly = true;
        columns.push(column);
      }
    }

    this.hot.updateSettings({
      // data: Handsontable.helper.createSpreadsheetData(10, 10),
      data: data,
      colHeaders: colHeaders,
      columns: columns,
      // minSpareRows: props.insertable ? 1 : 0,
      allowRemoveRow: props.insertable,
      allowInsertRow: props.insertable,
      contextMenu: false,
      fixedColumnsLeft: this.fields[0] == "age" ? 1 : 0,
    });

    if (props.insertable) {
      // Context Menu
      this.hot.updateSettings({
        contextMenu: {
          items: {
            "row_above": {
              name: getLocalizedText("HANDSONTABLE_OPT.INSERT_ABOVE","Insert Row Above")
            },
            "row_below": {
              name: getLocalizedText("HANDSONTABLE_OPT.INSERT_BELOW","Insert Row Below")
            },
            "remove_row": {
              name: getLocalizedText("HANDSONTABLE_OPT.REMOVE_ROW","Remove this row(s)?")
            }
          }
        }
      });
    }

    // Validate all cell
    // if (this.isEmptyGrid == false) {
    //   this.isValidateCells = true;
    //   this.hot.validateCells(
    //     function(valid) {
    //       this.isValidateCells = false;
    //     }.bind(this)
    //   );
    // }
    console.debug("[Handsontable] - [updateGrid] - [END]");
  },

  mandatoryValidator(value) {
    if (value === undefined) {
      return false;
    } else if (typeof value === "string" && value.length < 1) {
      return false;
    } else {
      return true;
    }
  },

  numberRangeValidator(value, min, max) {
    // console.debug("numberRangeValidator start", value);
    var fValue = parseFloat(value);
    var fMin = parseFloat(min);
    var fMax = parseFloat(max);

    if (fValue.toString() != value) {
      return false;
    }

    if (!isNaN(fValue)) {
      if (fValue < fMin) {
        return false;
      }
      if (fValue > fMax) {
        return false;
      }
    } else {
      return false;
    }

    return true;
  },

  overlapValidation(data) {
    // console.debug("[Handsontable] - [overlapValidation]", data);

    if (data) {
      var _data = [];
      if(this.groupBy){
        for(let j in this.groupBy.source){
          var groupItem = [];
          for (let i = 0; i < data.length; i++) {
            var row = data[i];
            if (row && row instanceof Object) {
              var keys = Object.keys(row);
              if(keys.indexOf(this.groupBy.id)!=-1){
                if(row.payMode == this.groupBy.source[j]){
                  row.position = parseInt(j) + 1;
                  groupItem.push(row);
                }
              }
            }
          }
          if(groupItem.length>0){
            _data.push(groupItem);
          }
        }
      }else{
        for(var z in data){
          data[z].position = parseInt(z) + 1;
        }
        _data[0]=data;
      }
      for(var k in _data){
        var periods = [];
        for (let i = 0; i < _data[k].length; i++) {
          var row = _data[k][i];
          if (row && row instanceof Object) {
            var keys = Object.keys(row);
            if (keys.indexOf("ageFr") != -1 && keys.indexOf("ageTo") != -1 && row.ageFr != undefined && row.ageTo != undefined) {
              var ageFrom = parseInt(row.ageFr);
              var ageTo = parseInt(row.ageTo);
              if (ageFrom != NaN && ageTo != NaN) {

                // ageFr must smaller than ageTo
                if (ageFrom > ageTo) {
                  console.debug("[Handsontable] - [from/to] - FALSE", ageFrom, ageTo);
                  this.errorMsg =  getLocalizedText("HANDSONTABLE_MSG.INVALID_RANGE","To value cannot be From value in row ") + row.position + ".";
                  return false;
                }

                // check overlap
                for (var j = 0; j < periods.length; j++) {
                  var period = periods[j];
                  var pAgeFrom = period.ageFrom;
                  var pAgeTo = period.ageTo;

                  if (Math.max(ageFrom, pAgeFrom) <= Math.min(ageTo, pAgeTo)) {
                    console.debug("[Handsontable] - [overlap] - FALSE", ageFrom, ageTo);
                    this.errorMsg = getLocalizedText("HANDSONTABLE_MSG.INVALID_OVERLAP","The range cannot be overlapped in row ") + row.position + ".";
                    return false;
                  }
                }

                periods.push({
                  ageFrom: ageFrom,
                  ageTo: ageTo,
                });
              }
            }
          }
        }
      }

    }

    return true;
  },

  validation() {
    console.debug("[Handsontable] - [validation]", this.cellValidation);

    this.errorMsg = undefined;

    for (let i = 0; i < this.cellValidation.length; i++) {
      var row = this.cellValidation[i];
      for (var column in row) {
        if (!row[column]) {
          this.errorMsg = getLocalizedText("HANDSONTABLE_MSG.INVALID_CELL_VALUE","Invalid cell value in row ") + (i + 1) + ".";
          return false;
        }
      }
    }

    var data = this.getData();

    if (!this.isEmptyGrid && !this.mandatoryChecking(data)) {
      // this.valiadateCells();
      this.isValidateCells = true;
      this.hot.validateCells(
        function(valid) {
          this.isValidateCells = false;
        }.bind(this)
      );
      return false;
    }
    //TODO: overlap validation for  payMode and currency
    //if (!this.overlapValidation(data)) {
    //  return false;
    //}

    return true;
  },

  afterValidate(isValid, value, row, prop, source) {
    // console.debug("[Handsontable] - [afterValidate] - [before]", source);
    // console.debug("[Handsontable] - [afterValidate] - [before]", this.cellValidation);
    setTimeout(
      function() {
        if (!this.cellValidation[this.row]) {
          this.cellValidation[this.row] = {};
        }
        this.cellValidation[this.row][this.prop] = this.isValid;
      }.bind({cellValidation: this.cellValidation, isValid: isValid, row: row, prop: prop}),
      0
    );
    // console.debug("[Handsontable] - [afterValidate] - [after]", this.cellValidation);
  },

  afterChange: function (changes, source) {
    console.debug("[Handsontable] - [afterChange]", source, changes);
    if (changes && this.props.onChange) {
      setTimeout(
        function() {
          this.onChange(this.getData());
        }.bind({onChange: this.props.onChange, getData: this.getData}),
        0
      );
    }
  },

  afterCreateRow(index, amount) {
    console.debug("[Handsontable] - [afterCreateRow]", index, amount);

    this.cellValidation.splice(index, amount, {});

    if (this.props.onChange) {
      this.props.onChange(this.getData());
    }
  },

  afterRemoveRow(index, amount) {
    console.debug("[Handsontable] - [afterRemoveRow]", index, amount);

    this.cellValidation.splice(index, amount);

    if (this.props.onChange) {
      this.props.onChange(this.getData());
    }
  },

  componentDidMount: function () {
    console.debug("[Handsontable] - [componentDidMount]", this.props);
    this.updateGrid(this.props);
  },

  componentWillReceiveProps: function (nextProps) {
    console.debug("[Handsontable] - [componentWillReceiveProps]", this.props, nextProps);
    //if (JSON.stringify(this.props.template) != JSON.stringify(nextProps.template)) {
      this.updateGrid(nextProps);
    //}
  },

  render: function () {
    console.debug("[Handsontable] - [render] - [id]", this.props);
    return (
      <div style={{margin: "0px 24px"}} key="handsontable" ref="handsontable"></div>
    );
  }
});

module.exports = HandsonTable;

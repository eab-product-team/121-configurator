let React = require('react');
let mui, {RadioButton, TextField, FontIcon, IconButton} = require('material-ui');

import EABFieldBase from './EABFieldBase.js';

let EABTextField = require('./TextField.jsx');
let EABPickerField = require('./PickerField.jsx');
let EABSwitchField = require('./SwitchField.jsx');
let EABViewOnlyField = require('./ViewOnlyField.jsx');
let EABCheckBox = require('./CheckBox.jsx');
let EABSelectList = require('./SelectList.jsx');
let EABMultipleTextField = require('./MultipleTextField.jsx');
let FunctionEditor = require('./FunctionEditor.jsx');
let EABFileUpload = require('./FileUpload.jsx');

import appTheme from '../../theme/appBaseTheme.js';

let EABTable = React.createClass({
  mixins: [EABFieldBase],

  _checkHasColumnGroup(items) {
    for (var i in items) {
      var type = items[i].type
      if (type.toUpperCase() == 'COLUMNGROUP') {
        return true;
      }
    }
    return false;
  },

  _genColumnHeader(header, items, columns, level) {
    var subheader = [];
    var hasGroup = this._checkHasColumnGroup(items);
    for (var i in items) {
      var item = items[i];
      if (item.type) {
        var ctitle = getLocalText("en", item.title)

        if (item.type.toUpperCase() == 'COLUMNGROUP') {
          this._genColumnHeader(subheader, item.items, columns, level + 1);
          header.push(<th key={level+"tg"+i}
            colSpan={ item.items.length }>
            {ctitle}
          </th>)
        } else if (item.type.toUpperCase() == 'HBOX') {
          this._genColumnHeader(header, item.items, columns, level);
        } else {
          columns.push(item);
          header.push(<th key={level+"th"+header.length+"_"+i}
            style= {{width: item.width || 'auto'}}
            rowSpan={ hasGroup && level < 1 ? 2 : 1 }>{ctitle}</th>)
        }
      }
    }
    if (this.props.deleteAble && level === 0) {
      header.push(<th key={level+'del'+i} className="delCol" rowSpan={ hasGroup ? 2 : 1 }></th>)
    }
    if (subheader.length) {
      return [<tr key={"tr0"}>{header}</tr>, <tr key={"tr1"}>{subheader}</tr>]
    } else {
      return [<tr key={"tr0"}>{header}</tr>];
    }
  },

  getInitialState() {
    var {
      template,
      width,
      handleChange,
      rootValues,
      changedValues
    } = this.props;

    var {
      items,
      subType
    } = template

    this.columns = [];
    this.headers = this._genColumnHeader([], items, this.columns, 0);
    console.debug("table", rootValues, changedValues);
    return {
      rootValues: rootValues || changedValues
    }
  },

  // get the row key and find referencing value
  _getKeyField(columns) {
    var rowKeys = [];
    if (columns) {
      for(var c in columns) {
        var col = columns[c];
        if (col.type && col.type.toUpperCase() == 'COLUMN') {
          for (var i in col.items) {
            var field = col.items[i];
            if (field.type == "rowKey") {
              field.value = getValueFmRootByRef(this.state.rootValues, field.reference || field.id, []) //fvalue;
              rowKeys.push(field);
            }
          }
        } else {
          if (col.type == "rowKey") {
            col.value = getValueFmRootByRef(this.state.rootValues, col.reference || col.id, []) //fvalue;
            rowKeys.push(col);
          }
        }
      }
    }
    return rowKeys;
  },

  _genCellField(its, item, values, changedValues, disabled, tableValues, row) {
    var self = this;
    var value = item.id && values?values[item.id]:values
    var cValue = item.id && changedValues?changedValues[item.id]:changedValues
    var lang = "en";

    var {
      rootValues
    } = this.state;

    // if it is a new change get default value if changed value is empty
    if (changedValues && cValue == undefined && item.value && item.id) {
      cValue = changedValues[item.id] = item.value
    }

    if (!showCondition(item, value, this.state.changedValues, this.showCondItems)) {
      return;
    }
    if (!item.type || item.type.toUpperCase() == 'READONLY' || item.type.toUpperCase() == 'ROWKEY') {
      its.push(
        <EABViewOnlyField
          ref = {item.id}
          key = {"ctn_"+ item.id}
          compact = {true}
          template = { item }
          rootValues = { rootValues }
          values = { changedValues }/>)
    }
    else if (item.type.toUpperCase() == 'TEXT' || item.type.toUpperCase() == 'TEXTFIELD') {
      its.push(
        <EABTextField
          ref = {item.id}
          key = {"ctn_"+ item.id}
          template = { item }
          index = {row}
          values = { values }
          disabled = { disabled }
          compact = {true}
          changedValues = { changedValues }
          rootValues = { rootValues }
          handleChange = { this.props.handleChange }
          />
      );
    }
    else if (item.type.toUpperCase() == 'MULTTEXT') {
      its.push(
        <EABMultipleTextField
          ref = {item.id}
          key = {"ctn_"+ item.id}
          template = { item }
          index = {row}
          values = { values }
          disabled = { disabled }
          changedValues = { values }
          rootValues = { rootValues }
          compact = {true}
          handleChange = { this.props.handleChange }
        />)
    }
    else if (item.type.toUpperCase() == 'PICKER') {
      its.push(
        <EABPickerField
          ref = {item.id}
          key = {"ctn_"+ item.id}
          template = { item }
          index = {row}
          values = { values }
          disabled = { disabled }
          changedValues = { changedValues }
          rootValues = { rootValues }
          compact = {true}
          width = {item.colWidth?item.colWidth:''}
          handleChange = { this.props.handleChange }
          />
      );
    }
    else if (item.type.toUpperCase() == 'SWITCH') {
      its.push(
          <EABSwitchField ref = {item.id}
            key = {"ctn_"+ item.id}
            template = { item }
            index = {row}
            values = { values }
            disabled = { disabled }
            mode = "landscape"
            changedValues = { changedValues }
            rootValues = { rootValues }
            compact = { true }
            handleChange = { this.props.handleChange }
            />
      );
    }
    else if (item.type.toUpperCase() == 'CHECKBOX') {
      its.push(
        <EABCheckBox ref = {item.id}
          key = {"ctn_"+ item.id}
          template = { item }
          index = {row}
          values = { values }
          changedValues = { changedValues }
          rootValues = { rootValues }
          disabled = { disabled }
          compact = { true }
          handleChange = {function(id, value) {
            // handle default row
            if (id == 'default' && value == 'Y') {
              for (let i in tableValues) {
                if (this.changedValues != tableValues[i]) {
                  tableValues[i].default = 'N';
                }
              }
            }
            if (typeof self.props.handleChange == 'function') {
              self.props.handleChange(id, value)
            } else {
              self.forceUpdate();
            }
          }.bind({changedValues: changedValues})}
          />
      )
    }
    else if (item.type.toUpperCase() == 'SELECTLIST'){
      its.push(<EABSelectList ref = {item.id}
        key = {"ctn_"+ item.id}
        template = { item }
        index = {row}
        compact = { true }
        values = { values }
        style = { { minHeight: '48px' }}
        changedValues = { changedValues }
        rootValues = { rootValues }
        disabled = { disabled }
        handleChange = { this.props.handleChange }
        />)
    }
    else if (item.type.toUpperCase() == 'FILEUPLOAD' || item.type.toUpperCase() == 'IMAGES') {
      its.push(
        <EABFileUpload
          ref = {item.id}
          key = {"ctn_"+ item.id}
          template = { item }
          values = { values }
          changedValues = { changedValues }
          rootValues = { rootValues }
          style = { { minHeight: '48px' }}
          disabled = { disabled }
          inline= {true}
          handleChange = { this.props.handleChange }
        />)
    }
    else if (item.type.toUpperCase() == 'JSEDITOR'){
      its.push(<FunctionEditor ref = {item.id}
        key = {"ctn_"+ item.id}
        template = { item }
        index = {row}
        values = { values }
        style = { { minHeight: '48px' }}
        changedValues = { changedValues }
        rootValues = { rootValues }
        disabled = { disabled }
        inline= {true}
        handleChange = { this.props.handleChange }
        />)
    }
    else if (item.type.toUpperCase() == 'TABLE' || item.type.toUpperCase() == 'COMPLEXLIST') {
      var cells = [];
      if (item.presentation && cValue && cValue instanceof Array) {
        for (var v in cValue) {
          var sv = cValue[v];
          var valPresent = item.presentation;
          for (var i in item.items) {
            var col = item.items[i];

            var cellItem = col.items || [col];

            for (var ci in cellItem) {
              var cell = cellItem[ci];
              var cvalue = sv[cell.id];
              if (cell.options) {
                for (var o in cell.options) {
                  if (cell.options[o].value == cvalue) {
                    cvalue = getLocalText('en', cell.options[o].title)
                  }
                }
              }
              if (cell.subType && cell.subType == 'currency') {
                cvalue = getCurrency(cvalue, "$", 2);
              }
              valPresent = valPresent.replace('['+cell.id+']', cvalue);
            }
          }
          cells.push(<div>{valPresent}</div>)
        }
      // } else {
        // if (!cValue || !cValue.length) {
        //   cells.push(<div>Add</div>)
        // } else {
        // }
      }
      cells.push(<FontIcon className="material-icons" style={{padding: '0 12px'}}>edit</FontIcon>)
      its.push(<div className={"TableInTable"} onClick={
        function(e) {
          if ( typeof self.props.handleUpdateRow == "function" ){
            self.props.handleUpdateRow(this.row);
          }
        }.bind({row: row})
      }>{cells}</div>)
    }
    // else if (item.type.toUpperCase() == 'JSEDITOR') {
    //   its.push(<FunctionEditor
    //     ref = {item.id}
    //     key = {"ctn_"+ item.id}
    //     inline={true}
    //     values = { values }
    //     changedValues = { changedValues }
    //     template={item}
    //     handleChange = { function(e) {
    //         if ( typeof self.props.handleUpdateRow == "function" ){
    //           self.props.handleUpdateRow(this.row);
    //         }
    //       }.bind({row: row})
    //     }/>)
    // }
  },

  render() {
    var self = this;
    var {
      template,
      width,
      handleChange, rootValues,
      deleteAble,
      ...others
    } = this.props;

    console.debug("table render template", template, this.props.changedValues);


    var {
      changedValues,
      muiTheme
    } = this.state;
   if(!this.props.changedValues[template.id] || this.props.changedValues[template.id] == null){
      if(template.static){
        changedValues[template.id] = template.values;
      }
    }

    var {
      id,
      type,
      title,
      placeholder,
      disabled,
      value,
      min,
      max,
      multiLine,
      interval,
      subType
    } = template;

    var cValue = self.getValue([])
    changedValues[id] = cValue;
    var errorText = (typeof(template.validation)=='function')?template.validation(changedValues):validate(template, changedValues);

    placeholder = placeholder || "No records found."

    var rows = [];

    var keyFields = this._getKeyField(this.columns);

    var newValues = [];

    var addRowRec = function(kf, dValue) {
      var keyField = keyFields[kf];
      if (keyField && keyField.value) {
        var kValues = keyField.value;
        if (typeof kValues == 'string') {
          keyField.value = kValues = kValues.split(",");
        }

        if (kValues instanceof Array) {
          for (var k in kValues) {
            if (kf == 0) {
              dValue = {};
            } else if (dValue) {
              dValue = cloneObject(dValue);
            } else {
              dValue = {};
            }
            dValue[keyField.id] = kValues[k];
            if (keyFields.length == kf + 1) {
              newValues.push(dValue);
            } else {
              addRowRec(kf + 1, dValue)
            }
          }
        }
      }
    }

    // re-create the row base on the keyfields
    if (keyFields && keyFields.length) {
      addRowRec(0, null);

      // copy exsiting value to new values if the key value match
      for (var i in newValues) {
        var j = 0;
        var found = false;
        for (; j < cValue.length; j++) {
          found = true;
          for (var k in newValues[i]) {
            if (cValue[j][k] != newValues[i][k]) {
              found = false;
              break;
            }
          }
          if (found) {
            newValues[i] = cValue[j]
            break;
          }
        }
      }

      changedValues[id] = cValue = newValues;
    }

    // render the rows
    var rows = [];
    if (cValue instanceof Array) {
      for (var i in cValue) {
        var rValue = cValue[i];
        var row = []
        for (var c in this.columns) {
          var col = this.columns[c]

          var citems = null;

          if (col.items && ("COLUMN" == col.type.toUpperCase() || "COLUMNGROUP" == col.type.toUpperCase())) {
            citems = col.items;
          } else {
            var colItem = cloneObject(col);
            colItem.title = "";
            citems = [colItem];
          }

          var colDisabled = disableTrigger(col, template, rValue, rValue, rootValues)

          if (citems && citems instanceof Array) {
            var cells = []
            for (let ci in citems) {
              var item = citems[ci];
              this._genCellField(cells, item, rValue, rValue, colDisabled, cValue, i);
            }

            row.push(<td className={"TableCell" + (citems.length > 1?" column2":" column1")}
              key={id+"i"+i+"c"+c}
              style={{
                paddingRight: '24px'
              }}>
                {cells}
              </td>)
          }
        }
        if (deleteAble) {
          row.push(<td key={"delBtn_"+i} className="delCol">
              <IconButton onClick={
                  function(e)  {
                    e.preventDefault();
                    e.stopPropagation();
                    alert('Confirm Deletion?', 'Confirm', "NO", "YES", function(e) {
                      this.cValue.splice(this.index, 1);
                      self.forceUpdate()
                    }.bind(this))
                  }.bind({index:i, cValue:cValue})
                }
                disabled={rValue.default == 'Y'}>
                <FontIcon className="material-icons"
                  style={{color:appTheme.palette.text1Color}}>delete</FontIcon>
              </IconButton>
            </td>)
        }
        rows.push(<tr key={id+"fi"+i}
          style={{
            borderBottom: '1px solid ' + appTheme.palette.borderColor
          }}
          >{row}</tr>)
      }
    }

    if (rows.length == 0) {
      rows.push(<tr key={"empty"}><td key="emptyRow" colSpan={ this.columns.length } style={{textAlign:"center"}}>{getLocalText('en', placeholder)}</td></tr>)
    }

    var title = getLocalText("en", title);
    return <div
      className={this.props.className || "DetailsItem"}>
      { title ? <span style={{
        fontSize: '20px',
        lineHeight: '40px',
        fontWeight: 300
      }}>{title}</span>:null }
      <table key={"tb"+id} className={"DynTable"}>
        <thead style={{
            borderBottom: '1px solid black'
          }}>
        {this.headers}
        </thead>
        <tbody>
        {rows}
        </tbody>
      </table>
    </div>
  }
});

module.exports = EABTable;

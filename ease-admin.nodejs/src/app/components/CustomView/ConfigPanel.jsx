let React = require('react');
let mui = require('material-ui');
let StylePropable = mui.Mixins.StylePropable;
let Transitions = mui.Styles.Transitions;
let Overlay = mui.Overlay;

import appTheme from '../../theme/appBaseTheme.js';

module.exports = React.createClass({

  mixins: [React.LinkedStateMixin, StylePropable],

  propTypes:{
    id: React.PropTypes.string.isRequired,
    open: React.PropTypes.bool.isRequired,
    children: React.PropTypes.node,
    onPanelClose: React.PropTypes.func,
    onPanelOpen: React.PropTypes.func,
    onRequestChange: React.PropTypes.func,
    height:React.PropTypes.number,
    style: React.PropTypes.object
  },

  getDefaultProps: function getDefaultProps() {
    return {
      open: null,
      height: null
    };
  },

  getInitialState: function getInitialState() {

    return {
      open: this.props.open !== null ? this.props.open : false
    };
  },

  componentWillReceiveProps: function componentWillReceiveProps(nextProps, nextContext) {
    /*var newState = { };
    if (nextProps.open !== null) newState.open = nextProps.open;

    this.setState(newState);*/
    if (nextProps.open !== null){
      if(nextProps.open){
        this.open();
      }else{
        this.close();
      }
    }
  },

  toggle: function toggle() {
    if (this.state.open) this.close();else this.open();
    return this;
  },
  close: function close() {
    let errorMsg = false;
    if (this.props.onPanelClose) errorMsg = this.props.onPanelClose();
    if(!errorMsg) this.setState({ open: false });
    return this;
  },
  open: function open() {
    let errorMsg = false;
    if (this.props.onPanelOpen) errorMsg = this.props.onPanelOpen();
    if(!errorMsg) this.setState({ open: true });
    return this;
  },

  _shouldShow: function _shouldShow() {
    return this.state.open
  },
  _close: function _close(reason) {
    if (this.props.open === null) this.setState({ open: false });
    if (this.props.onRequestChange) this.props.onRequestChange(false, reason);
    return this;
  },
  _open: function _open(reason) {
    if (this.props.open === null) this.setState({ open: true });
    if (this.props.onRequestChange) this.props.onRequestChange(true, reason);
    return this;
  },

  getStyles(){
    var _height= (this._shouldShow())?(this.props.height?this.props.height+'px':'300px'):'0px';

    var styles= {
      layout:{
        height: (this._shouldShow())? 'calc(100vh - 56px)': '0px',
        width: '100%',
        left: '0px',
        top: '0px',
        overflow: 'hidden',
        position: 'absolute',
        backgroundColor: "rgba(255,255,255, 0)"
      },
      configPanel:{
        transition: Transitions.easeOut(),
        boxShadow: 'rgba(0, 0, 0, 0.156863) 0px 0px 10px, rgba(0, 0, 0, 0.227451) 0px 0px 10px',
        height: _height,
        width: '100%',
        position: 'absolute',
        zIndex: '1300',
        left: '0px',
        bottom: '0px',
        overflow: 'hidden',
        transform: 'translate3d(0px, 0px, 0px)',
        backgroundColor: 'rgb(255, 255, 255)'
      },
      overlay: {
        zIndex: '1200',
        left: '0px',
        top: '0px',
        width: '100%',
        height: '100vh',
        position: 'relative',
        backgroundColor: "rgba(255,255,255, 0)"
      }
    }
    return styles;
  },

  render() {
    var self = this;
    var styles = this.getStyles();
    var {
      id,
      style,
      height,
      children,
      ...others
    } = this.props;


    return (
      <div key={"cp-"+id} style={styles.layout}>
        <Overlay
          show={open}
          style={styles.overlay}
          onClick={function(){self.close("Click Overlay")}}/>
        <div key={"cp-panel-"+id} style={self.mergeAndPrefix(styles.configPanel, style)}>
          {children}
        </div>
      </div>
    )
  }
});

let React = require('react');
var json2csv = require('json2csv');

module.exports = {

  hot: null,
  colHeadersId: [],

  tableStyle:{
    margin: "0px 24px",
    overflow: "auto"
  },

  propTypes:{
    template: React.PropTypes.object,
    gridData: React.PropTypes.any,
    onChange: React.PropTypes.func,
  },

  _export(err, csv){
  //  if (err) console.log(err);

    var uri = 'data:text/csv;charset=utf-8,' + escape(csv);

    var downloadLink = document.createElement("a");
    downloadLink.href = uri;
    downloadLink.download = "export.csv";

    document.body.appendChild(downloadLink);
    downloadLink.click();
    document.body.removeChild(downloadLink);
  },

  exportCsv(options){
    json2csv(options, this._export );
  },

  createGrid() {
    var self = this;
    var el = this.refs["handsontable"];
    if (!el) return false;

    // var height = this.props.height;
    // if (!height) {
    //   if (el.parentNode.parentNode.parentNode.clientHeight > 100) {
    //     height = el.parentNode.parentNode.parentNode.clientHeight - 90;
    //   } else {
    //     height = el.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.clientHeight - 138;
    //   }
    // }
    //
    // var {
    //   maxHeight
    // } = this.props;
    //
    // if (height < 140) height = 140;
    // if (maxHeight && height > maxHeight) height = maxHeight;

    var height = 0;

      if (el.parentNode.parentNode.parentNode.clientHeight > 100) {
        height = el.parentNode.parentNode.parentNode.clientHeight - 90;
      } else {
        height = el.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.clientHeight - 138;
      }
    

    if (!this.hot) {
      this.hot = new Handsontable(el, {
        height: height < 140 ? 140:height,
        stretchH: 'all',
        autoWrapRow: true,
        rowHeaders: true,
        colHeaders: true,
        persistentState: true,
        allowInvalid: true,
        autoColumnSize:  {useHeaders: true},
        allowRemoveColumn: false,
        allowInsertColumn: false,
        fillHandle: {
          autoInsertRow: false,
        },
      });
    }

    return true;
  },

  initData(props){
    if (!this.hot && !this.createGrid()) {
      return false;
    }

    var colHeadersId = this.colHeadersId = [];

    var columns = [];
    var colHeaders = [];

    // Grid items to HandsonTable properties
    var gridItems = props.template.items;
    if (gridItems && gridItems instanceof Array) {
      // Fields
      var giLength = gridItems.length;
      for (let i=0; i < giLength; i++) {
        var gridItem = gridItems[i];
        var {
          id,
          title,
          mandatory,
          disabled,
          presentation,
          width,
          options,
          type
        } = gridItem;

        var column = {
          data: id,
          type: type,
          readOnly: disabled,
          format: presentation,
          width: (width || 120)
        };

        colHeaders.push(title);
        colHeadersId.push(id);

        if(!isEmpty(options)){
          var source = [];
          var optsLength = options.length
          for (let j = 0; j < optsLength; j++) {
            source.push(options[j].title);
          }

          column.source = source;
        }

        if(props.disabled)
          column.readOnly = true;
        columns.push(column);
      }
    }

    return {
      colHeaders: colHeaders,
      colHeadersId: colHeadersId,
      columns: columns
    }
  },

  disabledFunc(){
    return this.hot.getSelected()[0] === 0;
  },

  initContextMenu(startIndex){
    let self = this;

    this.hot.updateSettings({
      contextMenu: {
        items: {
          "row_above": {
            name: getLocalizedText("HANDSONTABLE_OPT.INSERT_ABOVE","Insert Row Above"),
            disabled: (startIndex?self.disabledFunc:null)
          },
          "row_below": {
            name: getLocalizedText("HANDSONTABLE_OPT.INSERT_BELOW","Insert Row Below"),
            disabled: (startIndex?self.disabledFunc:null)
          },
          "remove_row": {
            name: getLocalizedText("HANDSONTABLE_OPT.REMOVE_ROW","Remove this row(s)?"),
            disabled: (startIndex?self.disabledFunc:null)
          }
        }
      }
    });
  }

};

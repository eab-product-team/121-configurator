let React = require('react');
let mui, {RadioButton, TextField, IconButton, FontIcon, FlatButton, Paper} = require('material-ui');

import EABFieldBase from './EABFieldBase.js';

let EABTextField = require('./TextField.jsx');
let EABPickerField = require('./PickerField.jsx');
let EABSwitchField = require('./SwitchField.jsx');
let EABViewOnlyField = require('./ViewOnlyField.jsx');
let EABCheckBox = require('./CheckBox.jsx');
let EABSelectList = require('./SelectList.jsx');
let EABTable = require('./Table.jsx');
let EABMultipleTextField = require('./MultipleTextField.jsx');
let EABComplexList = require('./ComplexList.jsx');
let EABGenericGrid = require('./GenerateGrid.jsx');
let EABRateGrid = require('./RateGrid.jsx');
let FunctionEditor = require('./FunctionEditor.jsx');
let JavascriptField = require('./JavascriptField.jsx');
let EABHtmlEditor = require('./HtmlEditor.jsx');
let EABCssEditor = require('./CssEditor.jsx');
var beautify = require('js-beautify').js_beautify;

import appTheme from '../../theme/appBaseTheme.js';

let EABDetail = React.createClass({
  mixins: [EABFieldBase],

  getInitialState() {
    var {
      template,
      width,
      handleChange,
      rootValues,
      changedValues,
      collapsed
    } = this.props;

    var {
      items,
      id,
      subType
    } = template

    return {
      collapsed: !!collapsed,
      detailValue: rootValues[id] || changedValues
    }
  },

  _genCellField(its, item, values, changedValues, disabled, tableValues, row) {
    var self = this;
    var subType = item.subType;
    var value = item.id && values?values[item.id]:values
    var cValue = item.id && changedValues?changedValues[item.id]:changedValues;
    var rootValues = self.props.rootValues || this.props.changedValues;
    var lang = "en";
    console.log('render cell:', item);

    // if it is a new change get default value if changed value is empty
    if (changedValues && cValue == undefined && item.value && item.id) {
      cValue = changedValues[item.id] = item.value
    }

    if (!showCondition(item, value, this.state.changedValues, this.showCondItems)) {
      return;
    }
    if (!item.type || item.type.toUpperCase() == 'READONLY' || item.type.toUpperCase() == 'ROWKEY') {
      its.push(
        <EABViewOnlyField
          ref = {"ctn_"+self.props.index+"_"+item.id}
          key = {"ctn_"+ self.props.index+"_"+item.id}
          compact = { subType == 'inline'}
          template = { item }
          index = {row}
          values = { changedValues }
          rootValues = {rootValues}
          />)
    }
    else if (item.type.toUpperCase() == 'CSSEDITOR'){
      its.push(
        <EABCssEditor
          ref = {"ctn_"+self.props.index+"_"+item.id}
          key = {"ctn_"+ self.props.index+"_"+item.id}
          compact = { subType == 'inline'}
          template = { item }
          index = {row}
          values = { values }
          changedValues = { changedValues }
          rootValues = {rootValues}
          />
      )
    }
    else if (item.type.toUpperCase() == 'HTMLEDITOR'){
      its.push(
        <EABHtmlEditor
          ref = {"ctn_"+self.props.index+"_"+item.id}
          key = {"ctn_"+ self.props.index+"_"+item.id}
          compact = { subType == 'inline'}
          template = { item }
          index = {row}
          values = { values }
          changedValues = { changedValues }
          rootValues = {rootValues}
          />
      )
    }
    else if (item.type.toUpperCase() == 'GENERICGRID'){
      its.push(
        <EABGenericGrid
          ref = {"ctn_"+self.props.index+"_"+item.id}
          key = {"ctn_"+ self.props.index+"_"+item.id}
          template = { item }
          index = {row}
          values = { values }
          disabled = { disabled }
          height = {300}
          compact = { subType == 'inline'}
          changedValues = { changedValues }
          rootValues = {rootValues}
          handleChange = { this.props.handleChange }
          />
      );
    }
    else if (item.type.toUpperCase() == 'RATEGRID'){
      its.push(
        <EABRateGrid
          ref = {"ctn_"+self.props.index+"_"+item.id}
          key = {"ctn_"+ self.props.index+"_"+item.id}
          template = { item }
          index = {row}
          values = { values }
          height = {300}
          disabled = { disabled }
          compact = { subType == 'inline'}
          changedValues = { changedValues }
          rootValues = {rootValues}
          handleChange = { this.props.handleChange }
          />
      );
    }
    else if (item.type.toUpperCase() == 'TEXT' || item.type.toUpperCase() == 'TEXTFIELD') {
      its.push(
        <EABTextField
          ref = {"ctn_"+self.props.index+"_"+item.id}
          key = {"ctn_"+ self.props.index+"_"+item.id}
          template = { item }
          index = {row}
          values = { values }
          disabled = { disabled }
          compact = { subType == 'inline'}
          changedValues = { changedValues }
          rootValues = {rootValues}
          handleChange = { this.props.handleChange }
          />
      );
    }
    else if (item.type.toUpperCase() == 'MULTTEXT') {
      console.debug("mul Text template", item);
      its.push(
        <EABMultipleTextField
          ref = {"ctn_"+self.props.index+"_"+item.id}
          key = {"ctn_"+ self.props.index+"_"+item.id}
          template = { item }
          index = {row}
          values = { values }
          disabled = { disabled }
          changedValues = { values }
          rootValues = {rootValues}
          compact = { subType == 'inline'}
          handleChange = { this.props.handleChange }
        />)
    }
    else if (item.type.toUpperCase() == 'PICKER') {
      its.push(
        <EABPickerField
          ref = {"ctn_"+self.props.index+"_"+item.id}
          key = {"ctn_"+ self.props.index+"_"+item.id}
          template = { item }
          index = {row}
          values = { values }
          disabled = { disabled }
          changedValues = { changedValues }
          rootValues = {rootValues}
          compact = { subType == 'inline'}
          width = {item.colWidth?item.colWidth:''}
          handleChange = { this.props.handleChange }
          />
      );
    }
    else if (item.type.toUpperCase() == 'SWITCH') {
      its.push(
          <EABSwitchField ref = {"ctn_"+self.props.index+"_"+item.id}
            key = {"ctn_"+ self.props.index+"_"+item.id}
            template = { item }
            index = {row}
            values = { values }
            disabled = { disabled }
            mode = "landscape"
            changedValues = { changedValues }
            rootValues = {rootValues}
            compact = { subType == 'inline'}
            handleChange = { this.props.handleChange }
            />
      );
    }
    else if (item.type.toUpperCase() == 'CHECKBOX') {
      its.push(
        <EABCheckBox ref = {"ctn_"+self.props.index+"_"+item.id}
          key = {"ctn_"+ self.props.index+"_"+item.id}
          template = { item }
          index = {row}
          values = { values }
          changedValues = { changedValues }
          rootValues = {rootValues}
          disabled = { disabled }
          compact = { subType == 'inline'}
          handleChange = {function(id, value) {
            // handle default row
            if (id == 'default' && value == 'Y') {
              for (let i in tableValues) {
                if (this.changedValues != tableValues[i]) {
                  tableValues[i].default = 'N';
                }
              }
            }
            if (typeof self.props.handleChange == 'function') {
              self.props.handleChange(id, value)
            } else {
              self.forceUpdate();
            }
          }.bind({changedValues: changedValues})}
          />
      )
    }
    else if (item.type.toUpperCase() == 'SELECTLIST'){
      its.push(<EABSelectList ref = {"ctn_"+self.props.index+"_"+item.id}
        key = {"ctn_"+ self.props.index+"_"+item.id}
        template = { item }
        index = {row}
        values = { values }
        changedValues = { changedValues }
        rootValues = {rootValues}
        disabled = { disabled }
        compact = { subType == 'inline'}
        handleChange = { this.props.handleChange }
        />)
    }
    else if (item.type.toUpperCase() == 'COMPLEXLIST') {
      its.push(<EABComplexList
        ref = {"ctn_"+self.props.index+"_"+item.id}
        key = {"ctn_"+ self.props.index+"_"+item.id}
        template = { item }
        index = {row}
        values = { values }
        compact = { subType == 'inline'}
        changedValues = { changedValues }
        rootValues = {rootValues}
        handleChange = { this.props.handleChange }
        />)
    }
    else if (item.type.toUpperCase() == 'JSEDITOR') {

      its.push(<FunctionEditor
        ref = {"ctn_"+self.props.index+"_"+item.id}
        key = {"ctn_"+ self.props.index+"_"+item.id}
        values = { values }
        index = {row}
        changedValues = { changedValues }
        rootValues = { rootValues }
        compact = { subType == 'inline'}
        template={item}
        handleChange = { this.props.handleChange }/>)
    }
    else if (item.type.toUpperCase() == 'JSFIELD') {
      var cVal = cValue
      if (cValue && typeof cValue == 'object') {
        cVal = JSON.stringify(cValue);
      }
      if (cVal) {
        try {
          cVal = beautify(cVal, { indent_size: 4});
        } catch (e) {
          // for save
        }
      }

      if (changedValues.disabled) {
        its.push(<div className={"FullScript"} style={{margin: '12px 24px', width: 'auto'}}>{cVal}</div>);
      } else {
        its.push(<div className={"DetailsItem"}>
          <JavascriptField
            id = {self.props.index+"_"+item.id}
            key = {"ctn_"+self.props.index+"_"+item.id}
            ref = {"ctn_"+self.props.index+"_"+item.id}
            className = { "ColumnField" }
            disabled = {disabled}
            style = { {
              height: "300px", display: 'block',
              border: '1px solid ' + appTheme.palette.borderColor
            } }
            onBlur = {
              function(value) {
                this.item.errorMsg = self.refs["ctn_"+self.props.index+"_"+this.id].validate();
                changedValues[this.id] = value;
                self.forceUpdate();
              }.bind({id: item.id, item: item, values: values})
            }
            defaultValue = { cVal }
            />
        </div>)
      }
    }
    else if (item.type.toUpperCase() == 'HBOX' || item.type.toUpperCase() == 'SECTION') {

      // either column2, column3, column4
      var additionalPresentClass = item.presentation?" "+item.presentation:"";
      if (!additionalPresentClass && item.type.toUpperCase() == 'SECTION') {
        additionalPresentClass = 'column1';
      }
      if (item.items.length) {
        var titleNode = null;
        if (item.title) {
          if (item.parentType && item.parentType.toUpperCase() == 'LIST') {
            titleNode = (<label key="label" className="Label" style={styles.floatingLabel}>{title}</label>)
          } else {
            titleNode = (<label key="label" className="Title" style={styles.titleStyle}>{title}</label>)
          }
        }

        if (!cValue && item.id) {
          if (!changedValues) {
            cValue = this.state.changedValues[item.id] = {};
          } else {
            cValue = changedValues[item.id] = {};
          }
        }
        var subItems = [];
        for (let si in item.items) {
          var _item = item.items[si];
          this._genCellField(subItems, _item, cValue, cValue, disabled, changedValues, row);
        }

        its.push(
          <div key= {"hbox"+(this.runningId++)} className={"hBoxContainer"+additionalPresentClass}>
            {titleNode}
            {subItems}
          </div>)
      }
    }
  },

  render() {
    var self = this;
    var {
      template,
      width,
      handleChange,
      handleDeleteRow,
      handleUpdateRow,
      deleteAble,
      rootValues,
      index,
      discollapsible,
      isNoCard,
      ...others
    } = this.props;

    var {
      changedValues,
      detailValue,
      collapsed,
      muiTheme
    } = this.state;

    var {
      id,
      type,
      title,
      placeholder,
      disabled,
      value,
      min,
      max,
      multiLine,
      items,
      interval,
      subType
    } = template;

    var cValue = changedValues;
    var errorText = (typeof(template.validation)=='function')?template.validation(changedValues):validate(template, changedValues);
    //console.log('render detail:', template);
    var rows = [];

    if (subType == 'inline') {
      for (var c in items) {
        var item = items[c];
        var colDisabled = disableTrigger(item, template, detailValue, detailValue, rootValues)
        this._genCellField(rows, item, cValue, cValue, colDisabled, detailValue, index);
      }
      return <div className="inlineContainer">
        <span className="DetailsItem">{cValue["id"]}:</span>
        {rows}
        {cValue.default?<IconButton
          key="delBtn"
          onClick={
            function(e) {
              e.preventDefault();
              e.stopPropagation();
              alert('Confirm Deletion?', 'Confirm', "NO", "YES", function(e) {
                if (typeof this.handle == 'function') {
                  this.handle(this.index);
                }
              }.bind({handle: handleDeleteRow, index:this.index}))
            }.bind({index:index})
          }><FontIcon className="material-icons">delete</FontIcon>
        </IconButton>:null}
      </div>
    } else {
      var titleControls = [];
      var subTitle = cValue['title'] || cValue['desc'];
      subTitle = (!isEmpty(subTitle) && (getLocalText('en', subTitle)+ " (id:"+cValue['id']+")")) || cValue['id']

      titleControls.push(<div key="titleDiv" className="title"
        style={{
          maxWidth: changedValues.default?"calc(100% - 50px)":"calc(100% - 220px)"
        }}
        onClick={cValue.default?null:handleUpdateRow}>
          <div style={{
          whiteSpace: "nowrap",
          overflow: "hidden",
          textOverflow: "ellipsis",
          display: "inline-block",
          width: "100%"
        }}>{subTitle}</div>

      </div>)
      if (!cValue.default) {
        titleControls.push(<FontIcon className="material-icons" style={{padding: '0 12px'}}>edit</FontIcon>);
      }

      if(!discollapsible){
        if (collapsed) {
          titleControls.push(<IconButton key="colBtn" style={{padding: '12px', float: 'right'}} onClick={
              function(e) {
                self.setState({
                  collapsed: !collapsed
                })
              }
            }><FontIcon className="material-icons">keyboard_arrow_down</FontIcon></IconButton>)
        } else {
          titleControls.push(<IconButton key="expBtn" style={{padding: '12px', float: 'right'}} onClick={
            function(e) {
              self.setState({
                collapsed: !collapsed
              })
            }
          }><FontIcon className="material-icons">keyboard_arrow_up</FontIcon></IconButton>)
        }
      }else{
        collapsed = false;
      }


      if (deleteAble && !changedValues.disabled && !changedValues.default) {
        titleControls.push(<FlatButton
          key="delBtn"
          label="Delete"
          style={{
            padding: '6px',
            float: 'right',
            marginRight: '24px'
          }}
          onClick={
            function(e) {
              e.preventDefault();
              e.stopPropagation();
              alert('Confirm Deletion?', 'Confirm', "NO", "YES", function(e) {
                if (typeof this.handle == 'function') {
                  this.handle(this.index);
                }
              }.bind({handle: handleDeleteRow, index:this.index}))
            }.bind({index:index})
          }>
        </FlatButton>)
      }

      rows.push(<div key={"dt_"+index} className="detailTitle">{titleControls}</div>)

      if (!collapsed) {
        for (var c in items) {
          var item = items[c];
          var colDisabled = disableTrigger(item, template, detailValue, detailValue, rootValues)
          this._genCellField(rows, item, cValue, cValue, colDisabled, detailValue, index);
        }
      }
      if(isNoCard){
        return(
          <div style={{
              transition: 'all 500ms cubic-bezier(0.445, 0.05, 0.55, 0.95) 0ms',
              margin: '8px 24px',
              paddingBottom: collapsed?'0px':'24px',
              maxHeight: collapsed?'48px':'10000px'
            }}>
            {rows}
          </div>
        )
      }else{
        return (
          <Paper key={'p'+index} rounded={false} zDepth={2} style={{
              transition: 'all 500ms cubic-bezier(0.445, 0.05, 0.55, 0.95) 0ms',
              margin: '8px 24px',
              paddingBottom: collapsed?'0px':'24px',
              maxHeight: collapsed?'48px':'10000px'
            }}>
            {rows}
          </Paper>
        )
      }

    }

    return null;
  }
});

module.exports = EABDetail;

let React = require('react');
let mui = require('material-ui');

import EABFieldBase from './EABFieldBase.js';
let DynConfigPanelActions = require('../../actions/DynConfigPanelActions.js');
let DialogActions = require('../../actions/DialogActions.js');
let NeedDetailStore = require('../../stores/NeedDetailStore.js');
let ContentStore = require('../../stores/ContentStore.js');
let IconBox = require('./IconBox.jsx');
let SortableList = require('./SortableList.jsx');

let _isMount=false;

let OtherTitle = React.createClass({

  onUpdate(){
    this.forceUpdate();
  },

  render(){
    let self = this;
    let {
      changedValues
    } = this.props;

    //if not other selection
    if(!changedValues.otherSelection)
      return null;

    //other option should be the last index of options
    let otherOption = changedValues.options[changedValues.options.length-1];
    if(isEmpty(otherOption)){
      otherOption={
        "title": NeedDetailStore.genLangCodesObj(),
        "otherTitle": NeedDetailStore.genLangCodesObj(),
      }
      otherOption.title.en = "Others";
    }

    if(isEmpty(otherOption.title)){
      otherOption.title = NeedDetailStore.genLangCodesObj();
      otherOption.title.en = "Others";
    }

    if(isEmpty(otherOption.otherTitle)){
      otherOption.otherTitle = cloneObject(otherOption.title);
    }

    var _othInputTypeOption = [{title:'Text', value: 'text'}, {title:'Currency', value: 'currency'}];
    var _othInputType = {id: 'otherInputType', placeholder: 'Format', type:'PICKER', options: _othInputTypeOption};

    let openPanel = function(){
      DynConfigPanelActions.openPanel(otherOption, 'otherSelection', {onUpdate: self.onUpdate, section: {items:[_othInputType]}});
    }

    return(
      <div  className="configurable" onClick={openPanel}>
        {isEmpty(otherOption.otherTitle.en)?isEmpty(otherOption.title.en)?"Others":otherOption.title.en:otherOption.otherTitle.en}:
      </div>
    );
  }
});


module.exports = React.createClass({

  mixins: [EABFieldBase],

  propTypes:{

  },

  componentWillUnmount(){
    _isMount=false;
  },

  componentDidMount(){
    _isMount=true;
  },

  _genItem(changedValues){
    let items = [];
    let self = this;

    let {
      disabled
    } = this.props;

    if(!changedValues.options){
      changedValues.options =[];
    }

    let cValue = ContentStore.getPageContent().changedValues;
    if(!cValue){
      cValue = ContentStore.getPageContent().changedValues = {};
    }
    if(!cValue.Attachments){
      cValue.Attachments = {};
    }

    let options = changedValues.options;
    let attachments = cValue.Attachments;

    let size = (checkExist(changedValues, 'boxRegularWidth'))? Number(changedValues.boxRegularWidth): 150;

    for(let i in options){
      let id = this.props.id + '-ic-' + i;

      let imageId = options[i].image;

      if(!attachments[imageId]){
        attachments[imageId]="";
      }
      attachments[imageId];

      let changeImage = function(url){
        if(!imageId || imageId == ""){
          imageId = options[i].image = getUniqueId();
        }
        attachments[imageId] = url;
      }

      items.push(
				<IconBox
          id={id}
          key={id}
          ref={id}
          disabled={disabled}
          image={attachments[imageId]}
          title={options[i].title}
          size={size}
          type={changedValues.type}
          isOther={options[i].isOther}
          onClickBody={function(){self.openPanel(changedValues, i);}}
          onDrop={changeImage}/>
      )
    }
    if(!disabled){
      items.push(
        <IconBox key={this.props.id + '-addBtn'} size={size} type={changedValues.type} isAdd={true} onDrop={this.addIconSelection}/>
      )
    }

    return items;
  },

  openPanel(changedValues, index){
    let self = this;
    let options = changedValues.options;
    let _options = this.props.options;
    let sections = undefined;

    if(changedValues.isRelationNeedsAly && _options && _options.needsAlyOpts){
      var _items = [];
      var _needsAlyItem = {id:'needAlyRelationIds', title:getLocalizedText("CONFIG_PANEL.NEED_ALY_RELATION","Relational Needs Options") , type:'CHECKBOXGROUP', options: _options.needsAlyOpts, presentation:'column1'};
      _items.push(_needsAlyItem);
      sections={items: _items};
    }

    let onUpdate = function(){
      self.refs[self.props.id + '-ic-' + index].onUpdate();
    };

    let delAction = function(){
      let removeDialog = {
        id: "delete",
        title: getLocalizedText("DELETE","DELETE"),
        message: getLocalizedText("DELETE_ITEM","Delete this item?"),
        negative: {
          id: "delete_cancel",
          title: getLocalizedText("BUTTON.CANCEL","CANCEL"),
          type: "button"
        },
        positive:{
          id: "delete_confirm",
          title: getLocalizedText("BUTTON.OK","OK"),
          type: "customButton",
          handleTap: function(){
            if(options[index].isOther){
              changedValues.otherSelection=false;
            }
            options.splice(Number(index),1);
            for(let j in options){
              options[j].seq = (Number(j)+1)*100;
            }
            DynConfigPanelActions.closePanel();
            self.onUpdate();
          }
        }
      }

      DialogActions.showDialog(removeDialog);

    }
    if(options[index].isOther){
      if(!sections){
        sections = {items: []};
      }
      var _items = sections.items;
      var _othInputTypeOption = [{title:'Text', value: 'text'}, {title:'Currency', value: 'currency'}];
      var _othInputType = {id: 'otherInputType', placeholder: 'Format', type:'PICKER', options: _othInputTypeOption};
      var _textInput = {id:'textInput', title:getLocalizedText("CONFIG_PANEL.TEXT_INPUT","Text Input"), type:'SWITCH'};
      _items.push(_othInputType);
      _items.push(_textInput);
    }


    //parent.setState({cfValues: options[i], deleteAction: delAction});
    DynConfigPanelActions.openPanel(options[index], 'question', {deleteAction: delAction, onUpdate: onUpdate, section: sections});
  },

  addIconSelection(url){
    let self = this;
    let {
      changedValues
    } = this.state;

    if(changedValues && !changedValues.options){
      changedValues.options =[];
    }
    var {
      id,
      options
    } = changedValues;


    let _id = this.props.id + '-ic-' + (options.length+1);

    let cValue = ContentStore.getPageContent().changedValues;
    //let imageId = 'image_'+id + '_'+options.length;
    let imageId = getUniqueId();

    cValue["Attachments"][imageId]=url;
    //check other selection exist or not
    let _i = 0;
    if(changedValues.otherSelection){
      _i = 1;
    }
    options.splice(options.length-_i, 0, {title:NeedDetailStore.genLangCodesObj(), id: _id + '-ic', image:imageId});

    self.onUpdate();

    self.openPanel(changedValues, options.length-1-_i);
  },

  onUpdate(){
    if(_isMount)
      this.forceUpdate();
  },

  render() {
    var self = this;
    let {
      changedValues
    } = this.state;

    let {
      isSortable
    } = this.props;

    let items = this._genItem(changedValues?changedValues:[]);

    //index of items which cannot sort
    let disabledItems = [];
    if(changedValues.otherSelection){
      disabledItems.push(items.length-2);
    }
    disabledItems.push(items.length-1);

    let sortableItems = (
      <SortableList
        id={this.props.id}
        items={items}
        changedValues={changedValues.options}
        horizontal={true}
        disabledItems={disabledItems}
        handleSort={this.onUpdate}/>
    )
    return (
      <div>
        {isSortable?sortableItems:items}
        <OtherTitle changedValues = {changedValues}/>
      </div>
    );
  }
});

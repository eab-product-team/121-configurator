let React = require('react');
let mui = require('material-ui');

import EABFieldBase from './EABFieldBase.js';
let DynConfigPanelActions = require('../../actions/DynConfigPanelActions.js');
let DialogActions = require('../../actions/DialogActions.js');
let NeedDetailStore = require('../../stores/NeedDetailStore.js');
let ContentStore = require('../../stores/ContentStore.js');
let IconBox = require('./SelectionBox.jsx');
let SortableList = require('./SortableList.jsx');
let IconButton = mui.IconButton;
import AddCircleOutline from 'material-ui/lib/svg-icons/content/add-circle-outline';
import Add from 'material-ui/lib/svg-icons/content/add';
import Info from 'material-ui/lib/svg-icons/action/info';

module.exports = React.createClass({

  mixins: [EABFieldBase],

  propTypes:{

  },

  getIcBoxStyles(sectionId, type, size){


    let style={width:261, height:260,margin:'8px'};
    let paperStyle={background: '#FAFAFA' , textAlign: 'center' , boxShadow: '0px 2px 4px #000000'};
    let gridZoneStyle={style, paperStyle};

    let viewDImageStyle={'background':'#9B9B9B'};
    let dzStyle={height:'196px', width: '100%', margin: 'auto'};
    let dragZoneStyle={viewDImageStyle, dzStyle};

    let imgZoneStyle={ cursor: 'pointer', width:'261px', height:'196px' };

    let addBtnStyle={width:'261px', height:'196px' };
    let icBtnStyle={width: 48, height: 48,  WebkitFilter:'invert(100%)'};
    let icBtnIconStyle={ width: imgZoneStyle.width, height: imgZoneStyle.height };
    let addZoneStyle={icBtnStyle, icBtnIconStyle, addBtnStyle};

    let descOuterStyle={height:'64px', width:'100%', textAlign: 'left'};
    let descInnerStyle={ cursor: 'pointer', paddingLeft:14, paddingTop:12 , fontSize:'14px'};
    let descZoneStyle= {descOuterStyle,descInnerStyle};

    return {
      gridZoneStyle,
      dragZoneStyle,
      imgZoneStyle,
      addZoneStyle,
      descZoneStyle,

    };

  },
  getIcBoxBtns(sectionId){
    let addIcBtn=[];
    let infoIcBtn=[];

   if(sectionId=="NeedsSelection")

    addIcBtn.push(
      <div  style={{display:'table', height:'100%'}}>
         <div  style={{display:'table-cell', verticalAlign:'middle'}}>
          <IconButton
              iconStyle={{width: 48, height: 48,  WebkitFilter:'invert(100%)'}}
              style={{width:'261px', height:'196px'}}
              >
              <Add/>
          </IconButton>
        </div>
      </div>
    );

    infoIcBtn.push(
        <IconButton  style={{float:'right'}}  iconStyle={{fill:"rgba(0, 0, 0, 0.54)", width:24, height:24}}>
          <Info  />
        </IconButton>
      );



    return {
      addIcBtn,
      infoIcBtn,
    }
  },



  _genItem(changedValues, pickerArray){
    let items = [];
    let self=this;

    let {
      id,
      disabled,
      sectionId
    } = this.props;

    if(isEmpty(id)){
      id='sbs';
    }


    if(changedValues && !changedValues.options){
      changedValues.options =[];
    }
    let options = changedValues.options;
    let size = (checkExist(changedValues, 'boxRegularWidth'))? Number(changedValues.boxRegularWidth): 150;
    let type=(changedValues.type)?changedValues.type:'imageSelection';


    let {
      gridZoneStyle,
      dragZoneStyle,
      imgZoneStyle,
      addZoneStyle,
      descZoneStyle,
    }=this.getIcBoxStyles(sectionId, type, size);



    let {
      addIcBtn,
      infoIcBtn,
    }=this.getIcBoxBtns(sectionId);

    /*let isDragable=false;
    let txtItem="title";
    let defaultTxt=getLocalizedText("ICON_BOX.ADD_TITLE", "Add Title") ;
    */
    let txtItem="desc";
    let isDragable=true;
    let defaultTxt=getLocalizedText("ICON_BOX.ADD_NEED", "Add Need");

    for(let i in options){
      let openPanel = function(){
        let delAction = function(){
          let removeDialog = {
      			id: "delete",
      			title: getLocalizedText("DELETE","DELETE"),
      			message: getLocalizedText("DELETE_ITEM","Delete this item?"),
      			negative: {
      				id: "delete_cancel",
      				title: getLocalizedText("BUTTON.CANCEL","CANCEL"),
      				type: "button"
      			},
      			positive:{
      				id: "delete_confirm",
      				title: getLocalizedText("BUTTON.OK","OK"),
      				type: "customButton",
      				handleTap: function(){
                options.splice(Number(i),1);
      					for(let j in options){
      						options[j].seq = (Number(j)+1)*100;
      					}
      					DynConfigPanelActions.closePanel();
                self.onUpdate();
      				}
      			}
      		}

      		DialogActions.showDialog(removeDialog);

				}
        let changeImgIdAction = function(newFileName){
          let fileName = options[i].image;
          cValue["Attachments"][fileName] = cValue["Attachments"][newFileName];
          cValue["Attachments"][newFileName] = undefined;
        }
        //parent.setState({cfValues: options[i], deleteAction: delAction});

        let onUpdate = function(){
          //self.refs[id+'-ic-'+i].onUpdate();
          self.onUpdate();
        }
        var template = {id:'conceptSelection', title:getLocalizedText("CONFIG_PANEL.CONCEPT_SELLING_SELECT_LABEL",'Concept Selling') , type:'PICKER', options: pickerArray ,value: (pickerArray[0])?pickerArray[0].value:'', disabled:disabled};


        let configItem=options[i];
    		let validation = function(){
    			let errorMsg = false;

    			if(!(configItem.desc.en&&configItem.title.en))
    				errorMsg= "Some mandatory data do not input yet, if you click OK button, this item would be deleted.";
    			if(!errorMsg){
    				console.log("validate");
    			}else{
    				console.log(errorMsg);
    				let removeDialog = {
    					id: "delete",
    					title: getLocalizedText("DELETE","DELETE"),
    					message: errorMsg,
    					negative: {
    						id: "delete_cancel",
    						title: getLocalizedText("BUTTON.CANCEL","CANCEL"),
    						type: "button"
    					},
    					positive:{
    						id: "delete_confirm",
    						title: getLocalizedText("BUTTON.OK","OK"),
    						type: "customButton",
    						handleTap: function(){
                  options.splice(options.length-1, 1);
                  self.onUpdate();
    							DynConfigPanelActions.closePanel(true);
    						}
    					}
    				}
    				DialogActions.showDialog(removeDialog);
    			}

    			return errorMsg;
    		};

        DynConfigPanelActions.openPanel(options[i], 'question', {  deleteAction: delAction, changeImgIdAction: changeImgIdAction, onUpdate: onUpdate, section:{items:[template]}} );
      }

      let option=options[i];
      let txtContent= checkExist(option, txtItem+'.en') && !isEmpty(option[txtItem].en)? multilineText(option[txtItem].en) :getLocalizedText("ICON_BOX.ADD_DESC", "Add Description");
      let isShowInfoIc=false;
      if(checkExist(option, 'conceptSelection')&&option.conceptSelection!='pleaseSelect'){
         isShowInfoIc=true;
      };
      items.push(
				<IconBox key={id + '-ic-' + i} ref={id + '-ic-' + i} changedValues={option} size={size} type={type} onClickBody={openPanel} disabled={disabled}

          forceRefresh={this.onUpdate}
          isDragable={isDragable}
          txtContent={txtContent}

          gridZoneStyle={gridZoneStyle}
          dragZoneStyle={dragZoneStyle}
          imgZoneStyle={imgZoneStyle}
          addZoneStyle={addZoneStyle}
          descZoneStyle={descZoneStyle}

          addIcBtn={addIcBtn}
          infoIcBtn={isShowInfoIc?infoIcBtn:null}
          sectionId={this.props.sectionId}
          />
      )
    }
    if(!disabled){
      items.push(
        <IconBox size={size} type={type} isAdd={true} onDrop={this.addIconSelectionn}


          forceRefresh={this.onUpdate}
          isDragable={false}
          txtContent={defaultTxt}

          gridZoneStyle={gridZoneStyle}
          dragZoneStyle={dragZoneStyle}
          imgZoneStyle={imgZoneStyle}
          addZoneStyle={addZoneStyle}
          descZoneStyle={descZoneStyle}

          addIcBtn={addIcBtn}
          />
      )
    }
    return items;
  },

  getNewId(options){
    let maxId=0;
    for(let i in options){
      let _id = options[i].id;
      if(maxId<Number(_id.substring(1, _id.length))){
        maxId=Number(_id.substring(1, _id.length));
      }
    }
    return 'r'+(maxId+1);

  },

  addIconSelectionn(url){
    let self=this;
    let {
      changedValues
    } = this.state;

    let {
      disabled,
      sectionId
    } = this.props;

    if(changedValues && !changedValues.options){
      changedValues.options =[];
    }
    var {
      id,
      options
    } = changedValues;

    let langCodesItems = NeedDetailStore.genLangCodesObj();
    let cValue = ContentStore.getPageContent().changedValues;
    let newId= this.getNewId(options);
    let imageId = getUniqueId();

    cValue["Attachments"][imageId]=url;

    options.push({title:NeedDetailStore.genLangCodesObj(), desc:NeedDetailStore.genLangCodesObj(), id: newId, image:imageId, boxRegularWidth:261/*, pickerArray:this.props.pickerArray*/   });


    let delAction = function(){
      options.splice(options.length-1,1);
      cValue["Attachments"][imageId]=undefined;
      DynConfigPanelActions.closePanel();
      self.onUpdate();
    }

    let changeImgIdAction = function(newFileName){
      let fileName = options[options.length-1].image;
      cValue["Attachments"][fileName] = cValue["Attachments"][newFileName];
      cValue["Attachments"][newFileName] = undefined;
    }

    self.onUpdate();

    //concept selling options
    let csOption = self.props.pickerArray;
    var template = {id:'conceptSelection', title:getLocalizedText("CONFIG_PANEL.CONCEPT_SELLING_SELECT_LABEL",'Concept Selling') , type:'PICKER', options: csOption ,value:(csOption[0])?csOption[0].value:'', disabled:disabled};

    let _id = this.props.id;
    if(isEmpty(_id)){
      _id='sbs';
    }
    let onUpdate = function(){
      //self.refs[_id + '-ic-' + (options.length-1)].onUpdate();
      self.onUpdate();
    }
    //parent.setState({cfCValues: options[options.length-1]});
    let configItem=options[options.length-1];
    let validation = function(){
      let errorMsg = false;

      if(!(configItem.desc.en&&configItem.title.en))
        errorMsg= "Some mandatory data do not input yet, if you click OK button, this item would be deleted.";
      if(!errorMsg){
        console.log("validate");
      }else{
        console.log(errorMsg);
        let removeDialog = {
          id: "delete",
          title: getLocalizedText("DELETE","DELETE"),
          message: errorMsg,
          negative: {
            id: "delete_cancel",
            title: getLocalizedText("BUTTON.CANCEL","CANCEL"),
            type: "button"
          },
          positive:{
            id: "delete_confirm",
            title: getLocalizedText("BUTTON.OK","OK"),
            type: "customButton",
            handleTap: function(){
              options.splice(options.length-1, 1);
							self.onUpdate();
              DynConfigPanelActions.closePanel(true);
            }
          }
        }
        DialogActions.showDialog(removeDialog);
      }

      return errorMsg;
    };
    DynConfigPanelActions.openPanel(options[options.length-1], 'question', { deleteAction: delAction, changeImgIdAction: changeImgIdAction, onUpdate: onUpdate, section:{items:[template]}});
  },

  onUpdate(){
    this.forceUpdate();
  },

  render() {
    var self = this;
    let {
      changedValues
    } = this.state;

    let {
      id,
      sectionId,
      isSortable,
      disabled
    } = this.props;

    if(isEmpty(id)){
      id = 'sbs';
    }

    let items = this._genItem(changedValues,self.props.pickerArray);
    let disabledItems = [];
    disabledItems.push(items.length-1);

    let sortableItems = (
      <SortableList
        disabled={disabled}
        id={id}
        items={items}
        disabledItems={disabledItems}
        changedValues={changedValues.options}
        horizontal={true}
        hasAddButton={true}
        handleSort={function(e){self.onUpdate();}}
		    presentation={3}/>
    )
    return (
      <div>
        {isSortable?sortableItems:items}
      </div>
    );
  }
});

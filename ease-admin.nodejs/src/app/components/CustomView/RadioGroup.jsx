let React = require('react');
let mui = require('material-ui');

let RadioButton = mui.RadioButton;
let RadioButtonGroup = mui.RadioButtonGroup;

import EABFieldBase from './EABFieldBase.js';

let RadioGroup = React.createClass({
  mixins: [EABFieldBase],

  styles: {
    checkBoxLabelStyle: {
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      whiteSpace: 'nowrap',
      width: 'calc(100% - 40px)',
      fontSize: '16px'
    }
  },

  render: function() {
    var self = this;
    var {
      template,
      changedValues,
      handleChange,
      disabled,
      ...others
    } = this.props;

    var {
      id,
      title,
      value,
      placeholder,
      presentation
    } = template;

    var {
      options
    } = this.state

    var cValue = (id && changedValues && changedValues[id])?changedValues[id]:(value?value:"");

    var cbGroup = [];

    for (let i = 0; i < options.length; i++) {
      var option = options[i];

      if (optionSkipTrigger(template, option, changedValues)) {
        continue;
      }
      var otitle = getLocalText("en", option.title);
      cbGroup.push(<RadioButton
        key={ option.value }
        className = {"CheckBox"}
        label = {otitle}
        disabled = {disabled}
        labelStyle = {this.styles.checkBoxLabelStyle}
        value = { option.value }
        />);
    }

    if (cbGroup.length == 0) {
      cbGroup.push(<label
        key={"placeholder"}
        className="CheckBox">
        {placeholder?placeholder:"No Option"}
      </label>)
    }

    var errorMsg = validate(template, changedValues);
    errorMsg = errorMsg?<div className="ErrorLabel" style={{
      color: this.state.muiTheme.textField.errorColor,
      height: '16px',
      fontSize: '12px',
      position: 'absolute',
      left: '24px',
      top: null,
      bottom: '4px'
    }}>{errorMsg}</div>:null;

    return (<div
        className={"CheckboxGroup"+ (presentation?" "+presentation:"") + this.checkDiff()}>
        {this.getFloatingTitle()}
        <RadioButtonGroup
           key= {id}
           name = {id}
           onChange={
              function(e, value){
                self.requestChange(value);
              }
           }
           valueSelected={cValue}
           {...others}
           >
           {cbGroup}
        </RadioButtonGroup>
        {errorMsg}
    </div>);
  }
});
module.exports = RadioGroup;

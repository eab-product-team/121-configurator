let React = require('react');
let mui = require('material-ui');
let StylePropable = mui.Mixins.StylePropable;

let DynActions = require('../../actions/DynActions.js');
import appTheme from '../../theme/appBaseTheme.js';
let ConfigConstants = require('../../constants/ConfigConstants.js')
let ContentStore = require('../../stores/ContentStore.js');

let numeral = require('numeral'); // refs: http://numeraljs.com/

module.exports = {
  mixins: [React.LinkedStateMixin, StylePropable],

  PropTypes: {
    template: React.PropTypes.object.isRequired,
    values: React.PropTypes.object.isRequired,
    changedValues: React.PropTypes.object.isRequired,
    changeActionIndex: React.PropTypes.func,
    floatingTitleStyle: React.PropTypes.object,
    titleStyle: React.PropTypes.object,
    resetValueItems: React.PropTypes.object,
    headerTitleStyle: React.PropTypes.object,
    handleChange: React.PropTypes.func,
    requireShowDiff: React.PropTypes.bool
  },

  getDefaultProps() {
    return {
      requireShowDiff: false
    }
  },

  componentWillUnmount() {

  },

  componentDidMount() {

  },

  componentWillReceiveProps(newProps, newState) {
    // console.debug("componentWillReceiveProps :", this.props, newProps);
    this.setState({
      changedValues: newProps.changedValues
    });
  },

  contextTypes: {
    muiTheme: React.PropTypes.object
  },

  childContextTypes: {
    muiTheme: React.PropTypes.object
  },

  getChildContext() {
    return {
      muiTheme: this.state.muiTheme
    }
  },

  _getOptionsByReference(field) {
    var {
      reference
    } = field;
    var options = field.options;
    if (reference && !options) {
      var {
        template
      } = ContentStore.getPageContent();

      var ref = reference.replace("@", "/")
      var path = ref.split("/");
      var refField = getFieldFmTemplate(template, path, 0, "options");
      if (refField && refField.options) {
        options = refField.options;
      } else if (this.props.rootValues){
        var refValues = getValueFmRootByRef(this.props.rootValues, reference, []);
        if (refValues && refValues.length) {
          options = [];
          for (var r in refValues) {
            options.push({
              value: refValues[r],
              title: refValues[r]
            })
          }
        }
      }
    }
    return options;
  },

  getInitialState() {
    // copy options by reference
    return {
      options: this._getOptionsByReference(this.props.template),
      changedValues: this.props.changedValues,
      muiTheme: this.context.muiTheme ? this.context.muiTheme : appTheme.getTheme()
    };
  },

  checkDiff() {

    if (!this.props.requireShowDiff) return "";

    var {
      id,
      type
    } = this.props.template;
    var value = this.props.values[id];
    var cValue = this.state.changedValues[id];

    if (type && 'checkbox'.toUpperCase() == type.toUpperCase()) {
        value = (value == 'Y' || value == 'y');
        cValue = (cValue == 'Y' || cValue == 'y');
    }
    return checkChanges(value, cValue) ? " diff": "";
  },

  getValue(dValue) {
    var {
      changedValues
    } = this.state;

    var {
      id,
      value
    } = this.props.template;

    if (id) {
      var cValue = changedValues && changedValues[id];
      cValue = performValueUpdateTrigger(this.props.template, cValue, changedValues);
      if (cValue == undefined) {
        if (typeof changedValues == 'object') {
          cValue = changedValues[id] = !isEmpty(value)?value:(dValue != undefined?dValue:null);
        }
      }
      return cValue;
    } else {
      return value || changedValues;
    }
  },

  _getDisplayValue(item, value, values) {
    var options = item.options;
    if (!options && value && item.reference) {
      options = this._getOptionsByReference(item);
    }
    if (options && value) {

      if (value == '*') {
        return getLocalizedText('OPTION.ALL');
      }

      for (let idx in options) {
        var op = options[idx];

        if (optionSkipTrigger(item, op, values, null, this.props.rootValues)) {
          continue;
        }
        if (op.value.toUpperCase() == value.toUpperCase()) {
          return op.title;
        }
      }
      // exception handle
      if (value == '0') {
        return "-";
      } else if (value == '*'){
        return "ALL";
      }
    } else if (item.type == 'datepicker' && value instanceof Date) {
      return value.format(ConfigConstants.DateFormat);
    } else if (item.type && 'CHECKBOX' == item.type.toUpperCase()) {
      if (item.id == "default") {
        if (value == "Y") {
          return " (Default)";
        } else {
          return "";
        }
      }
    }else if(item.type && 'MULTTEXT' == item.type.toUpperCase()){
      var combValue = '';
      for (var key in value) {
        var valueChild = value[key];
        if(combValue == '')
          combValue += valueChild;
        else
          combValue += "/" + valueChild;
      }
      value = combValue;
    }
    return value;
  },

  requestChange(value) {
    var item = this.props.template;
    //TO DO....
    if(!item){
      return;
    }
    if (item.type.toUpperCase() == 'TEXT' || item.type.toUpperCase() == 'TEXTFIELD') {
      // if (value && item.isKey) {
      //   value = value.toUpperCase();
      // }

      // validation
      var error = null;
      if (item.subType && item.subType.toLowerCase() == 'number' && value != "") {

      }
      else {
        if (item.max && value.length > item.max) {
          return
        }
      }
    } else
    if (item.type.toUpperCase() == 'DATEPICKER') {
      value = value.getTime();
    } else
    if (item.type.toUpperCase() == 'CHECKBOX') {
      value = value?"Y":"N"
    }

    // // do trigger
    // // updDfValue should replace with defaultValueOverride, trigger by target control
    // if(item.trigger && item.trigger.type=='updDfValue'){
    //    var _key = item.trigger.id;
    //    var _value = changedValues[_key];
    //    if(_value == undefined || Number(_value) < Number(val)){
    //      changedValues[_key] = val;
    //    }
    // }

    // reset value if resetValueItems exists
    if (this.props.resetValueItems) {
      let related = this.props.resetValueItems[item.id];
      if (related) {
        for (let i = 0; i < related.length; i++) {
          if (related[i] && this.state.changedValues[related[i]]) {
            this.state.changedValues[related[i]] = "";
          }
        }
      }
    }

    // bubble up the changeds
    if (typeof this.props.requestChange == 'function') {
      this.props.requestChange(value, item.id);
    } else {
      if (this.state.changedValues) {
        this.state.changedValues[item.id] = value;
      }
      this.handleChange(this.state.changedValues);
    }
  },

  handleBlur(value) {
    var item = this.props.template;

    if (item.type.toUpperCase() == 'TEXT' || item.type.toUpperCase() == 'TEXTFIELD') {
      if (item.subType && item.subType == 'number') {
        if (value != undefined && !isNaN(parseFloat(value)) && item.presentation) {
          value = numeral(value).format(item.presentation);
        } else {
          value = !isNaN(parseFloat(value))?parseFloat(value):0;
        }

        if (item.min != undefined && value < item.min) {
          // error = langMap["INPUT_ERROR.AMOUNT_LESS_THAN"]?"Amount less than {0}":null
          value = item.min;
        }
        if (item.max != undefined && value > item.max) {
          // error = langMap["INPUT_ERROR.AMOUNT_GREATER_THAN"]?"Amount greater than {0}":null
          value = item.max;
        }

        if (typeof this.props.handleBlur == 'function') {
          this.props.handleBlur(value);
        } else {
          if (this.state.changedValues) {
            this.state.changedValues[item.id] = value;
          }
          this.handleChange(this.state.changedValues)
        }
      }
    }
  },

  handleChange(values) {
    var item = this.props.template;
    this.forceUpdate();

    if (typeof this.props.handleChange == 'function') {
      this.props.handleChange(item.id, values[item.id]);
    } else {
      if (typeof this.props.changeActionIndex == 'function') {
        DynActions.changeValues(null, this.props.changeActionIndex(), item.id);
      } else {
        DynActions.changeValues(null, 0, item.id);
      }
    }
  },

  getStyles() {
    let theme = this.state.muiTheme;

    return {
      floatingTitleStyle : {
        lineHeight: '22px',
        top: '24px',
        bottom: 'none',
        fontSize: '16px',
        opacity: 1,
        color: theme.textField.fixedLabelColor,
        transform: 'perspective(1px) scale(0.875) translate3d(0px, -28px, 0)',
        transformOrigin: 'left top',
        position: 'absolute',
        width: '115%',
        zIndex: 100
      },
      titleStyle: {
        whiteSpace: 'nowrap',
        overflow: 'hidden',
        textOverflow: 'ellipsis'
      },
      headerTitleStyle : {
        display: 'block',
        fontSize: '20px',
        lineHeight: '64px',
      },
      disabledUnderline: {
        borderBottom: '1px solid',
        borderColor: appTheme.palette.borderColor
      }
    }
  },

  getTitle(_title, styles) {
    var {
      title
    } = this.props.template;
    if (!_title) _title = title;
    if (!_title) return null;

    _title = getLocalizedText(getLocalText('en', _title));

    var t = [];
    t.push(<span key="t" style={ this.mergeAndPrefix(this.getStyles().titleStyle, this.props.titleStyle, styles || {}) }>{_title}</span>)
    // if (mandatory) {
    //   t.push(<span key="s" style={ {color:'red'} }>&nbsp;*</span>)
    // }
    return t;
  },

  getFloatingTitle(_title) {
    if (!_title) _title = this.props.template.title;
    if (!_title) return null;
    return <label key="l" className="Label"
      style={ this.mergeAndPrefix(this.getStyles().floatingTitleStyle, this.props.floatingTitleStyle) }>{ this.getTitle(_title) }</label>
  },

  getHeaderTitle(_title) {
    if (!_title) _title = this.props.template.title;
    if (!_title) return null;
    return <label key="l" className="Title"
      style={ this.mergeAndPrefix(this.getStyles().headerTitleStyle, this.props.headerTitleStyle) }>{ this.getTitle(_title) }</label>
  }
};

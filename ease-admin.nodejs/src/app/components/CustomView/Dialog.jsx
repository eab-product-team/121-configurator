let React = require('react');
let mui = require('material-ui');
let Dialog_ = mui.Dialog;
let RaisedButton = mui.RaisedButton;
let MenuItem = mui.MenuItem;
let SelectField = mui.SelectField;
let FlatButton = mui.FlatButton;
let Dialog = React.createClass({
  PropTypes: {
    template: React.PropTypes.object.isRequired,
    open: React.PropTypes.bool,
  },
  getInitialState(){
    return {open : false, values: this.props.template}
  },
  handleOpen: function() {
    this.setState({open: true});
  },

  handleClose: function() {
    this.setState({open: false});
  },

  handleChange: function(_values){
    this.setState({values: _values});
  },
  render: function() {
    var self = this;
    var template = this.props.template;
    var dialog = template.dialog;
    var pickers = [];
    var values = dialog.values;
    for (let i in dialog.items){
      var item = dialog.items[i];
      var dfValue = {
        id: item.id,
        value: values[item.id],
        requestChange: function(value) {
          values[this.id] = value;
          self.handleChange(values);
        },
      };
      var oItems = [];
      for (let j in item.options){
        var option = item.options[j];
        var otitle = getLocalText('en', option.title)
        var oItem = <MenuItem
          value={option.value}
          key={option.value}
          primaryText={otitle} />
        oItems.push(oItem);
      }
      var title = getLocalText('en', item.title)
      var label = <div>
          <span>{title}</span>{item.mandatory?<span style={ {color:'red'} }> * </span>:null}
        </div>
      var picker = <SelectField
          key = {item.id}
          value = {values[item.id]}
          className = { "ColumnField" }
          labelStyle = { {top: 5} }
          floatingLabelText = {label}
          selectFieldRoot = { { height: '54px' } }
          style= {{width:'100%'}}
          children = {oItems}
          onChange = { function(e, index, value) {
            this.requestChange(value);
          }.bind(dfValue)}>
        </SelectField>
      pickers.push(picker);

    }
    var  actions = [
      <FlatButton
        label="Delete"
        float="left"
        className="left"
        secondary={true}
        onTouchTap={this.handleDelete} />,
      <FlatButton
        label="Cancel"
        secondary={true}
        onTouchTap={this.handleClose} />,
      <FlatButton
        label="Submit"
        primary={true}
        keyboardFocused={true}
        onTouchTap={this.handleClose} />,
    ];
    var customContentStyle = {
        maxHeight: '500px',
      };
    var cell = <div>
      <RaisedButton label="Dialog With Actions" onTouchTap={this.handleOpen} />
      <Dialog_
        autoDetectWindowHeight={true}
        title="Dialog With Actions"
        actions={actions}
        modal={false}
        open={this.state.open}
        contentStyle={customContentStyle}
        onRequestClose={this.handleClose}
        children = {pickers}>
      </Dialog_>
    </div>
    return(
      <div>{cell}</div>
      );
  },
  getValue: function(){
    return this.refs[this.props.template.id].getValue();
  },

});

module.exports = Dialog;

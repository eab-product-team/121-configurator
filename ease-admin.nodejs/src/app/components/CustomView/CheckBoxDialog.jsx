let React = require('react');
let mui = require('material-ui');

import EABFieldBase from './EABFieldBase.js';
import ReadOnlyField from './ReadOnlyField.jsx';
import DialogActions from '../../actions/DialogActions.js';

let CheckBoxDialog = React.createClass({
  mixins: [EABFieldBase],

  render() {
    var self = this;

    var {
      template,
      width,
      handleChange,
      ...others
    } = this.props;

    var {
      changedValues,
      muiTheme
    } = this.state;

    var {
      id,
      title,
      value
    } = template;

    var cValue = self.getValue("");

    // generate sub field title
    var valueStr = "";
    var cbValues = cValue.split(','); //this.handleCbValues(item.id, changedValues);
    for (let cbi in cbValues) {
      var cbl = this._getDisplayValue(template, cbValues[cbi]);
      valueStr += (valueStr?", ":"") + cbl;
    }

    var checkboxItem = cloneObject(template);
    checkboxItem.type = 'checkboxgroup';
    checkboxItem.title = null;

    var errorMsg = validate(template, changedValues);
    errorMsg = errorMsg?<div style={{
      color: this.state.muiTheme.textField.errorColor,
      height: '16px',
      fontSize: '12px',
      position: 'absolute',
      left: '24px',
      bottom: '6px'
    }}>{errorMsg}</div>:null;

    return (
        <div
          className={"DetailsItem" + this.checkDiff() }>
          <ReadOnlyField
            key = {id}
            ref = {id}
            className = { "ColumnField" }
            style = { {width:width?width:'100%', backgroundColor:null } }
            floatingLabelText = { this.getTitle() }
            mandatory = {template.mandatory}
            underline = {true}
            defaultValue = { valueStr }
            onClick = {
              function() {
                DialogActions.showDialog({
                  title: this.title,
                  message: this.message,
                  items: this.items,
                  values: this.values,
                  actions: [{
                    type: 'cancel',
                    title: getLocalizedText('BUTTON.CANCEL', "Cancel"),
                    secondary: true,
                  }, {
                    type: 'customButton',
                    title: getLocalizedText('BUTTON.SAVE', "Save"),
                    primary: true,
                    handleTap: function( values ) {
                      self.requestChange(values[id]);
                    }
                  }]
                });
              }.bind({items: [checkboxItem], values: changedValues, title: this.getTitle() })
            }
            {...others}
            />
          { errorMsg }
      </div>
    );
  }
});

module.exports = CheckBoxDialog;

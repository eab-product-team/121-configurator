let React = require('react');
let mui = require('material-ui');

let FontIcon = mui.FontIcon;
let Slider = mui.Slider;

import EABFieldBase from './EABFieldBase.js';
import appTheme from '../../theme/appBaseTheme.js';

module.exports = React.createClass({
  mixins: [EABFieldBase],

  styles: {
    titleStyle:{
      color: '#0066FF',
      fontSize: '24px'
    },
    containerStyle:{
      margin: 'auto',
    },
    sliderStyle:{
      display: 'inline-block',
      width:'70%',
      verticalAlign: 'middle',
      marginLeft: '15%',
      position: 'relative'
    },
    percentageStyle:{
      backgroundImage:'url(web/img/tracking_box.png)',
      backgroundSize:'contain',
      position: 'absolute',
      textAlign: 'center',
      width: '90px',
      height: '45px',
      lineHeight: '40px',
      color: '#FFFFFF',
      fontSize: '16px'
    }
  },

    getInitialState: function() {
      let {
        id
      } = this.props;


      return {
        id: id
      }
    },

    componentWillReceiveProps(nextProps){
      let {
        id
      } = nextProps;

      this.setState({
        id: id
      });
    },

  render() {
    var self = this;
    var {
      disabled,
      ...others
    } = this.props;

    var {
      changedValues,
      tempValue
    } = this.state;

    var {
      maxValue,
      minValue,
      interval,
      defaultValue
    } = changedValues;

    let cValue = tempValue||defaultValue;
    if(!maxValue) maxValue = 100;
    if(!minValue) minValue = 0;

    let percentage = (new Number((cValue-minValue)/(maxValue-minValue)*100)).toFixed(2) + '%';

    return (
      <div style={{width:'100%'}}>
        <div style={this.styles.containerStyle}>
          <div style={this.styles.sliderStyle}>
            <div style={this.mergeAndPrefix(this.styles.percentageStyle, {left: 'calc('+percentage+' - 45px)'})}>
              {Number(cValue) + '%'}
            </div>
            <Slider
              style={{marginTop:'42px'}}
              name="trackingSlider"
              min={Number(minValue||0)}
              step={Number(interval||1)}
              max={Number(maxValue||100)}
              value={Number(cValue)}
              onChange={function(e, value){
                self.state.tempValue = value;
                self.forceUpdate();
              }}
              />
          </div>
        </div>
      </div>
    )
  }
});

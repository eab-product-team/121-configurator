import React, {Component, PropTypes} from 'react';
import {Dialog, FlatButton, TextField} from 'material-ui';

export default class ConfirmDialog extends Component {

  static propTypes = {
    open: PropTypes.bool,
    msg: PropTypes.string,
    onConfirm: PropTypes.func,
    onClose: PropTypes.func
  };

  onRequestClose() {
    const {onClose} = this.props;
    onClose && onClose();
  }

  render() {
    const {open, msg, onConfirm} = this.props;
    const actions = [
      <FlatButton label="Cancel" onTouchTap={() => this.onRequestClose()} />,
      <FlatButton primary label="Confirm" onTouchTap={() => onConfirm && onConfirm()} />
    ];
    return (
      <Dialog
        open={open}
        title="Save Version"
        modal={false}
        onRequestClose={() => this.onRequestClose()}
        actions={actions}
      >
        {msg}
      </Dialog>
    );
  }
}

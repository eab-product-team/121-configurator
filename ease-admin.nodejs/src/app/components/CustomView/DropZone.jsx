const React = require('react');
let mui = require('material-ui');
let StylePropable = mui.Mixins.StylePropable;
let _ = require('lodash');

    var Dropzone = React.createClass({

      mixins: [React.LinkedStateMixin, StylePropable],

      getInitialState: function() {
        return {
          isDragActive: false
        }
      },

      propTypes: {
        onDrop: React.PropTypes.func.isRequired,
        accept: React.PropTypes.string,
        require: React.PropTypes.object, // {width, height}
      },

      onDragLeave: function(e) {
        this.setState({
          isDragActive: false
        });
      },

      onDragOver: function(e) {
        e.preventDefault();
        e.dataTransfer.dropEffect = 'copy';

        this.setState({
          isDragActive: true
        });
      },

      onDrop: function(e) {
        e.preventDefault();
        this.setState({
          isDragActive: false
        });

        var files;
        if (e.dataTransfer) {
          files = e.dataTransfer.files;
        } else if (e.target) {
          files = e.target.files;
        }

        _.each(files, this._createPreview);
      },

      onClick: function () {
        this.refs.fileInput.getDOMNode().click();
      },

      _createPreview: function(file){
        var self = this
          , newFile
          , reader = new FileReader();

        reader.onloadend = function(e){
          newFile = {
            file: file,
            imageUrl: e.target.result,
          };
          if (self.props.onDrop) {
            self.props.onDrop(newFile);
          }
        };

        reader.readAsDataURL(file);
      },

      render: function() {
        var className = 'dropzone';
        if (this.state.isDragActive) {
          className += ' active';
        };

        var {
          style
        } = this.props;

        // var style = {
        //   width: this.props.size || 100,
        //   height: this.props.size || 100,
        //   borderStyle: this.state.isDragActive ? 'solid' : 'dashed'
        // };

        return (
          <div
            className={className}
            onClick={this.onClick}
            onDragLeave={this.onDragLeave}
            onDragOver={this.onDragOver}
            onDrop={this.onDrop}
            style={this.mergeAndPrefix({cursor: 'pointer'}, style)}>
            <input
              style={{display: 'none'}}
              type='file'
              ref='fileInput'
              onChange={this.onDrop}
              value=""
              accept={this.props.accept} />
            {this.props.children}
          </div>
        );
      }

    });

    module.exports = Dropzone

let React = require('react');
let mui = require('material-ui');
let StylePropable = mui.Mixins.StylePropable;
let Dialog = mui.Dialog;

let RightDialog = React.createClass({
  mixins: [React.LinkedStateMixin, StylePropable],

  propTypes: {
    show: React.PropTypes.bool,
    children: React.PropTypes.node,
    handleClose: React.PropTypes.func,
  },

  getInitialState() {

    return {
    open: true
    };
  },
  getStyles:function(){
    let styles={
      contentStyle:{
        position:"absolute",
        width:"320px",
        right:"0px",
        top:"-66px"
      },
      contentStyleHide:{
        position:"absolute",
        width:"0px",
        right:"0px",
        top:"-66px"
      },
      bodyStyle:{
        margin:"0px",
        height:'100vh',
        yOverflow:'auto'
      },
    }
    return styles;
  },
  render() {
    var {
      show,
      title,
      style,
      children,
      ...others,
    } = this.props;
    var styles = this.getStyles();
    var width = '0px';
    if (!show)

    return (
      <Dialog
        title={this.props.title}
        ref="dialog"
        id="rightDialog"
        open={true}
        style = {{zIndex:-1, }}
        overlayStyle = {{width:'0px',}}
        contentStyle={ styles.contentStyleHide }
        autoDetectWindowHeight={false}
        {...others}
        >
        <div style={ styles.bodyStyle }>
          {children}
        </div>
      </Dialog>
    )

    return (
      <Dialog
        title={this.props.title}
        ref="dialog"
        id="rightDialog"
        onRequestClose={this.props.handleClose}
        open={this.props.show}
        contentStyle={ styles.contentStyle }
        autoDetectWindowHeight={false}
        {...others}
        >
        <div style={ styles.bodyStyle }>
          {children}
        </div>
      </Dialog>
    );

  }

});

module.exports = RightDialog;

import React from 'react';

import ConfigSummaryList from '../Config/ConfigSummaryList.jsx';
import NewProductDialog from './NewProductDialog.jsx';

import ConfigPageActions from '../../actions/ConfigPageActions';
import ConfigConstants from '../../constants/ConfigConstants';

export default class ProductSummaryList extends ConfigSummaryList {

  static defaultProps = {
    module: 'product',
    title: 'Products',
    createBtnLabel: 'New Product',
    deleteMsg: 'Are you sure to delete the selected products?'
  };

  _getColumnHeaders() {
    return [
      'Product Name',
      'Product Code',
      'Type',
      'Last Updated By'
    ];
  }

  _getColumnValues(row) {
    return [
      row.name,
      row.description,
      row.type === 'B' ? 'Basic Plan' : 'Rider',
      row.updateBy + ' / ' + new Date(row.updateDate).format(ConfigConstants.DateTimeFormat)
    ];
  }

  _showRow(searchTerm, row) {
    const term = searchTerm.toUpperCase();
    return (row.name && row.name.toUpperCase().indexOf(term) > -1) ||
        (row.description && row.description.toUpperCase().indexOf(term) > -1) ||
        (row.updateBy.toUpperCase().indexOf(term) > -1);
  }

  onCreate(type, name, description) {
    const {module} = this.props;
    ConfigPageActions.createSummary({
      module,
      type,
      name,
      description
    }).then(() => this.fetch());
  }

  _getCreateDialog(open) {
    return (
      <NewProductDialog
        open={open}
        onSave={(type, name, covCode) => this.onCreate(type, name, covCode)}
        onClose={() => this.setState({ creating: null })}
      />
    );
  }

}

import React, {Component, PropTypes} from 'react';
import {Dialog, FlatButton, TextField, RadioButtonGroup, RadioButton} from 'material-ui';

export default class NewProductDialog extends Component {

  static propTypes = {
    open: PropTypes.bool,
    onSave: PropTypes.func,
    onClose: PropTypes.func
  };

  constructor() {
    super();
    this.state = {
      type: null,
      name: null,
      covCode: null
    };
  }

  onRequestClose() {
    const {onClose} = this.props;
    onClose && onClose();
  }

  render() {
    const {open, onSave} = this.props;
    const {type, name, covCode} = this.state;
    const actions = [
      <FlatButton label="Cancel" onTouchTap={() => this.onRequestClose()} />,
      <FlatButton
        primary
        label="Save"
        disabled={!type || !name || !covCode}
        onTouchTap={() => onSave && onSave(type, name, covCode)}
      />
    ];
    return (
      <Dialog
        open={open}
        title="Create Product"
        modal={false}
        onRequestClose={() => this.onRequestClose()}
        actions={actions}
      >
        <RadioButtonGroup name="prodType" onChange={(e, value) => this.setState({ type: value })}>
          <RadioButton style={{ width: '50%', display: 'inline-block'}} value="B" label="Basic Plan" />
          <RadioButton style={{ width: '50%', display: 'inline-block'}} value="R" label="Rider" />
        </RadioButtonGroup>
        <TextField
          fullWidth
          floatingLabelText="Name"
          value={name}
          onChange={(event) => this.setState({ name: event.target.value })}
        />
        <TextField
          fullWidth
          floatingLabelText="Product Code"
          value={covCode}
          onChange={(event) => this.setState({ covCode: event.target.value })}
        />
      </Dialog>
    );
  }

}

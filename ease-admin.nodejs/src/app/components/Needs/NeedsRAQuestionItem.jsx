let React = require('react');
let mui = require('material-ui');

let ContentStore = require('../../stores/ContentStore.js');
let NeedDetailStore = require('../../stores/NeedDetailStore.js');

let DialogActions = require('../../actions/DialogActions.js');
let DynConfigPanelActions = require('../../actions/DynConfigPanelActions.js');

let ItemMinix = require('../../mixins/ItemMinix.js');

let SortableList = require('../CustomView/SortableList.jsx');
let RaisedButton = mui.RaisedButton;
let Toolbar = mui.Toolbar;
let ToolbarGroup = mui.ToolbarGroup;
let ToolbarTitle = mui.ToolbarTitle;
let FontIcon = mui.FontIcon;
let Paper = mui.Paper;
let Transitions = mui.Styles.Transitions;
let TextField = mui.TextField;

let bgcolor = '#FAFAFA';

let TopBar = React.createClass({
	mixins: [ItemMinix],

	onUpdate(){
    this.forceUpdate();
  },


	render(){
		let self = this;
		let {
			index,
			changedValues,
			onRemove
		} = this.props;

		if(this.isDebug()){
			console.log("render - nraq - topbar", changedValues);
		}

		let title = changedValues[index]['title'].en;

		let changeCPanel = function(){
			let delAction = function(){
				self._remove();
				DynConfigPanelActions.closePanel();
			}
			DynConfigPanelActions.openPanel(changedValues[index], 'question', {deleteAction:delAction, onUpdate:self.onUpdate});
		};

		let isLock = NeedDetailStore.isLock();

		return (
			<div style={{height: '58px', display: 'block', overflow:'hidden'}}>
				<Toolbar style={{padding:"0px", position:'relative', background: bgcolor}} >
					<ToolbarGroup style={{width:'calc(100% - 178px)', display:'flex'}}>
						<ToolbarTitle  className="configurable"  onClick={changeCPanel} text={title?title:"Add Title"} style={{color:'rgb(38, 150, 204)', paddingLeft: '24px', width:'100%', cursor:'pointer'}}/>
					</ToolbarGroup>
					<ToolbarGroup float="right">
						{isLock?null:
							<RaisedButton
								style={{display:'inline-block', float: 'right', marginRight: '24px'}}
								backgroundColor={bgcolor}
								onClick={onRemove}
								label={getLocalizedText("BUTTON.DELETE","DELETE")}
								labelColor='#FF0000'
							/>
						}
					</ToolbarGroup>
				</Toolbar>
			</div>
		);
	}
})

let ItemRow = React.createClass({
	mixins: [ItemMinix],

	onUpdate(){
    this.forceUpdate();
  },

	openPanel(){
		let self = this;
		DynConfigPanelActions.openPanel(this.props.option.text, 'question', {onUpdate: self.onUpdate});
	},

	requestChange(value){
		this.props.option.code = value;
		this.onUpdate();
	},

	removeItem(){
		let self = this;
		let {
			index,
			changedValues,
			onItemUpdate
		} = this.props;

		let removeDialog = {
			id: "delete",
			title: getLocalizedText("DELETE","DELETE"),
			message: getLocalizedText("DELETE_ITEM","Delete this item?"),
			negative: {
				id: "delete_cancel",
				title: getLocalizedText("BUTTON.CANCEL","CANCEL"),
				type: "button"
			},
			positive:{
				id: "delete_confirm",
				title: getLocalizedText("BUTTON.OK","OK"),
				type: "customButton",
				handleTap: function(){
					changedValues.options.splice(index, 1);
					for(let j in changedValues.options){
						changedValues.options[j].seq = Number(j+1)*100;
					}
					onItemUpdate();
				}
			}
		}

		DialogActions.showDialog(removeDialog);

	},

	render: function(){
		let self = this;
		let {
			option,
			removeItem
		} = this.props;

		if(this.isDebug()){
				console.log("NRAQItemRow-render", option);
		}

		let isLock = NeedDetailStore.isLock();


		return(
			<div style={{transition: Transitions.easeOut(), padding: '12px'}}>
				<FontIcon
					style = {{padding: '0px 12px 0 12px', verticalAlign: 'middle', cursor:'pointer'}}
					color = {"#000000"}
					hoverColor={"#000000"}
					className="material-icons drag-area">
						reorder
				</FontIcon>
				<span className="configurable" style={{verticalAlign: 'middle', cursor:'pointer'}}  onClick={this.openPanel}>
					{(option.text.en)?option.text.en:"Add Title"}
				</span>
				{
					isLock?null:
					<FontIcon
						style = {{padding: '0px 12px 0 12px', verticalAlign: 'middle', float: 'right', cursor:'pointer'}}
						color = {"#AAAAAA"}
						hoverColor={"#AAAAAA"}
						className="material-icons drag-area"
						onClick={this.removeItem}>
							delete
					</FontIcon>
				}
				<TextField
					style={{float:'right', width:'48px', height:'32px'}}
					inputStyle={{height:'initial'}}
					hintText="Score"
					type="number"
					disabled={isLock}
					value={option.code}
					onChange={function(e){self.requestChange(e.target.value)}}/>
			</div>
		);
	}
});

let NeedsRAQuestionItem = React.createClass({
	mixins: [ItemMinix],

	onUpdate(){
    this.forceUpdate();
  },

	propTypes:{
		index: React.PropTypes.number.isRequired,
		changedValues: React.PropTypes.array.isRequired,
		onClick: React.PropTypes.func
	},

	_remove(){

		let {
			index,
			changedValues,
			onListUpdate
		} = this.props;

		let removeDialog = {
			id: "delete",
			title: getLocalizedText("DELETE","DELETE"),
			message: getLocalizedText("DELETE_QUESTION","Delete this question?"),
			negative: {
				id: "delete_cancel",
				title: getLocalizedText("BUTTON.CANCEL","CANCEL"),
				type: "button"
			},
			positive:{
				id: "delete_confirm",
				title: getLocalizedText("BUTTON.OK","OK"),
				type: "customButton",
				handleTap: function(){
					changedValues.splice(index, 1);
					for(let i in changedValues){
						changedValues[i].seq = (i+1)*100;
					}
					onListUpdate();
				}
			}
		}

		DialogActions.showDialog(removeDialog);

	},

  _genItems(changedValues){
    let items = [];
    let self = this;
		let isLock = NeedDetailStore.isLock();

    for(let i in changedValues.options){
      let option = changedValues.options[i];
      items.push(
        <ItemRow key={'naq-i'+i} ref={'naq-i'+i} option={option} index={i} changedValues={changedValues} onItemUpdate={self.onUpdate}/>
      );

    }

    let addFunc = function(){
      if(!changedValues.options){
        changedValues.options = [];
      }

      changedValues.options.push({
        text:NeedDetailStore.genLangCodesObj(),
        seq:(changedValues.options.length+1)*100,
				code:""
      });

			self.onUpdate();
			let onItemUpdate = function(){
				self.refs['naq-i'+(changedValues.options.length-1)].onUpdate();
			}
			DynConfigPanelActions.openPanel(changedValues.options[changedValues.options.length-1].text, 'question', {onUpdate: onItemUpdate});
    };


		if(!isLock){
			let addBtn = (
	      <div style={this.mergeAndPrefix({padding: '12px', cursor:'pointer'})} onClick={addFunc}>
	        <FontIcon
	          style = {{padding: '0px 12px 0 12px', verticalAlign: 'middle'}}
	          color = {"#000000"}
	          hoverColor={"#000000"}
	          className="material-icons">
	          add
	        </FontIcon>
	        <span style={{verticalAlign: 'middle'}}>
	          Add Item
	        </span>
	      </div>
	    )

			items.push(
	      addBtn
	    )
		}


    return items;
  },

	render: function() {
		if(this.isDebug()){
				console.log("NRAQItem-render");
		}

		var self = this;

		let {
			index,
			disabled,
      changedValues,
			onListUpdate,
			...others
		} = this.props;

    let items = this._genItems(changedValues[index]);



		return (
			<Paper style={{margin:'12px', background: bgcolor}} {...others}>
				<TopBar index={index} changedValues={changedValues} onListUpdate={onListUpdate} onRemove={self._remove}/>
				<div key={"nrqi - " + index}>
					<SortableList
		        id={"nrqi-sl"}
		        items={items}
						disabled={disabled}
		        changedValues={changedValues[index].options}
						disabledItems={[items.length-1]}
		        handleSort={this.onUpdate}/>
				</div>
			</Paper>
		);
	},
});

module.exports = NeedsRAQuestionItem;

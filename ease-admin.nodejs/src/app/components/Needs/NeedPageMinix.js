let React = require('react');
let ContentStore = require('../../stores/ContentStore.js');
let NeedDetailStore = require('../../stores/NeedDetailStore.js');
let ItemMinix = require('../../mixins/ItemMinix.js');

module.exports = {
  mixins: [ItemMinix],

  componentDidMount: function() {
		ContentStore.addChangeListener(this.onContentChange);
    NeedDetailStore.addChangeListener(this.onPageChange);
  },

  componentWillUnmount: function() {
		ContentStore.removeChangeListener(this.onContentChange);
    NeedDetailStore.removeChangeListener(this.onPageChange);

  },

  onPageChange(){
		var content = ContentStore.getPageContent();
    if(content){
  		var self = this;
  		var {
  			template
  		} = content;

  		var {
  			validation,
  			sectionId
  		} = this.state;

  		let foundTemplate = false;

  		if(!template.items){
  			template.items = [];
  		}



  		for(let j in template.items){
  			if(template.items[j].id == sectionId){
  				template.items[j].validate = validation;
  				let foundTemplate = true;
  			}
  		}

  		if(!foundTemplate){
  			template.items.push({id:sectionId, validate:validation});
  		}
  		this.onContentChange();
    }
	},

	onContentChange() {
		var content = ContentStore.getPageContent();
		var {
			values,
			changedValues
		} = content;

    var currSid = NeedDetailStore.getCurrentPage();

    

		if(changedValues && currSid && changedValues[currSid])
			this.setState({
        sectionId: currSid,
				values: values[currSid],
				changedValues: changedValues[currSid]
			});
	},

  shouldComponentUpdate: function(nextProps, nextState) {
      if(NeedDetailStore.isReflesh()){
        return true;
      }
			if(nextState.currentPage != this.state.currentPage){
				return true;
			}
			if(nextState.sectionId != this.state.sectionId){
				return true;
			}
			return false;
	},


}

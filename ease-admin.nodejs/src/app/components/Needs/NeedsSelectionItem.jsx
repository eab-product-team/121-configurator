let React = require('react');
let mui = require('material-ui');

let ContentStore = require('../../stores/ContentStore.js');
let NeedDetailStore = require('../../stores/NeedDetailStore.js');

let NeedActions = require('../../actions/NeedActions.js');
let DynConfigPanelActions = require('../../actions/DynConfigPanelActions.js');

let ItemMinix = require('../../mixins/ItemMinix.js');

let TextSelection = require('../CustomView/TextSelection.jsx');
let IconBoxSection = require('../CustomView/SelectionBoxSection.jsx');

let Toolbar = mui.Toolbar;
let ToolbarGroup = mui.ToolbarGroup;
let ToolbarTitle = mui.ToolbarTitle;
let FontIcon = mui.FontIcon;

let NeedsSelectionItem = React.createClass({
	mixins: [ItemMinix],

	getInitialState() {
		let {
			index,
			changedValues
		} = this.props;
		return {
			index: index,
			changedValues: changedValues
		}
	},

	componentWillReceiveProps(nextProps){
		if(this.isMount())
			this.setState({
				index: nextProps.index,
				changedValues: nextProps.changedValues
			})
	},

	propTypes:{
		index: React.PropTypes.number.isRequired,
		changedValues: React.PropTypes.array.isRequired,
		onClick: React.PropTypes.func
	},

	getStyles(){
		return(
				{
					topBarStyle:{
		        height: '58px',
		        display: 'block',
		        overflow:'hidden'
		      },
					imageStyle:{
						maxWidth:'100%'
					}
				}
		)

	},

	_genItem(inputType){
		let {
			index,
			changedValues
		} = this.state;

		let {
			parent
		} = this.props;
		let styles = this.getStyles();

		let cValue = changedValues[index];
		let imagePath = "web/img/input_type/";

		switch(inputType){
			case 'iconSelection':
			case 'imageSelection':
				return <IconBoxSection parent={parent} changedValues={changedValues[index]} pickerArray={changedValues[index].pickerArray} isSortable={true} sectionId={this.props.sectionId}/>
			case 'textSelection':
				return <TextSelection parent={parent} changedValues={changedValues[index]}/>
			default:
				return <img style={styles.imageStyle} src={imagePath+inputType+'.png'}/>

		}

	},

	render: function() {

		var self = this;
		let styles = this.getStyles();

    let {
			index,
      changedValues
    } = this.state;

		let {
			onClick,
			...others
		} = this.props;

		let item = (changedValues[index] && changedValues[index]['type'])? this._genItem(changedValues[index]['type']):null;

		return (
			<div {...others}>
				<div  style={{textAlign:'center'  }}>
					<div  style={{textAlign:'left',width:831 ,margin:'0 auto'}} >
						{item}

					</div>
				</div>
			</div>
		);
	},
});

module.exports = NeedsSelectionItem;

/** In this file, we create a React component which incorporates components provided by material-ui */

let React = require('react');
let mui = require('material-ui');
let StylePropable = mui.Mixins.StylePropable;
let NeedDetailStore = require('../../stores/NeedDetailStore.js');
let ContentStore = require('../../stores/ContentStore.js');
let DynMenu = require('../DynamicUI/DynMenu.jsx');
let InputConfigPanel = require('./InputConfigPanel.jsx');

let FlatButton = mui.FlatButton;

let NeedDetail = React.createClass({
  mixins: [React.LinkedStateMixin, StylePropable],

  componentDidMount() {
    // console.debug('homepage did mount');
    ContentStore.addChangeListener(this.onContentChange );
    NeedDetailStore.addChangeListener(this.onPageChange);

  },

  componentWillUnmount: function() {
    ContentStore.removeChangeListener(this.onContentChange);
    NeedDetailStore.removeChangeListener(this.onPageChange);
  },

  shouldComponentUpdate: function(nextProps, nextState) {
    if(checkChanges(this.state.currentPage, nextState.currentPage)){
        return true;
    }
    if(checkChanges(this.state.menuList, nextState.menuList)){
      return true;
    }
    return false;
  },
  onContentChange(){
    var content = ContentStore.getPageContent();
    var {
      template,
      values
    } = content;

    this.setState({
      menuList: template.menu
    });
  },


  onPageChange(){
    let pageId = NeedDetailStore.getCurrentPage();
    let uiPage = NeedDetailStore.getUiPage();
    let currentPage = {id:pageId, uid: uiPage};
    this.setState({currentPage: currentPage});
  },

  getInitialState() {
    var content = ContentStore.getPageContent();
    var {
      template,
      values
    } = content;

    let currentPage = values['selectPage'];
    return {
      menuList: template.menu,
      currentPage: currentPage
    };
  },

  getStyle(){
    return ({
      containerStyle:{
        position: 'relative',
        display: 'inline-block',
        width: 'calc(100% - 280px)',
        height: 'calc(100vh - 58px)',
        overflow: 'hidden'
      },
      pageStyle:{
        width: '100%',
        height: 'calc(100vh - 58px)',
        overflowY: 'auto'
      }
    });
  },

  render: function() {
    var self = this;
    let styles = this.getStyle();

    let {
      menuList,
      currentPage
    } = this.state;
    let uiPage = null;

    if (currentPage.uid){
      uiPage = {id: currentPage.uid};
    }
    let page = getPageDom(uiPage?uiPage:currentPage);

    return (
      <div className="PageContent">
        <DynMenu
          key={"needDynMenu"}
          ref={"needDynMenu"}
          show={true}
          module={"/Needs/Detail"}
          items={menuList}
          style={{width: "280px"}}/>
        <div style={styles.containerStyle}>
            <div style={styles.pageStyle}>
              {page}
            </div>
            <InputConfigPanel
    					id="detail"
    					height={500} />
        </div>
      </div>
    );
  }
});

module.exports = NeedDetail;

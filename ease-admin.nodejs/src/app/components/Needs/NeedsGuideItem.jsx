let React = require('react');
let mui = require('material-ui');
let PDF = require('react-pdf');

let ContentStore = require('../../stores/ContentStore.js');
let NeedDetailStore = require('../../stores/NeedDetailStore.js');

let DialogActions = require('../../actions/DialogActions.js');
let DynConfigPanelActions = require('../../actions/DynConfigPanelActions.js');

let RaisedButton = mui.RaisedButton;
let Toolbar = mui.Toolbar;
let ToolbarGroup = mui.ToolbarGroup;
let ToolbarTitle = mui.ToolbarTitle;
let FontIcon = mui.FontIcon;
let Divider = mui.Divider;

let Dropzone = require('../CustomView/DropZone.jsx');
let Dialog = require('../CustomView/Dialog.jsx');

let ItemMinix = require('../../mixins/ItemMinix.js');

let Topbar = React.createClass({
	mixins: [ItemMinix],

	onUpdate(){
    this.forceUpdate();
  },

	render(){
		if(this.isDebug()){
			console.log("render - ngi - topbar");
		}

		let self = this;
		let {
			index,
      changedValues,
			changeCPanel,
			onRemove
    } = this.props;

		let title = changedValues[index]['title'].en;
		let isLock = NeedDetailStore.isLock();

		return(
			<div style={{height: '58px', display: 'block', overflow:'hidden'}}>
				<Toolbar style={{padding:"0px", position:'relative', background: '#FFFFFF'}} >
					<ToolbarGroup style={{width:'calc(100% - 140px)', display:'flex'}}>
						<FontIcon
							style = {{padding: '0px 15px 0 24px'}}
							color = {"#000000"}
							hoverColor={"#000000"}
							className="material-icons drag-area">
							reorder
						</FontIcon>
						<ToolbarTitle onClick={changeCPanel} text={title} style={{cursor:'pointer', color:'#000000'}}/>
					</ToolbarGroup>
					<ToolbarGroup float="right">
						{isLock?null:<RaisedButton
			        key={'cp-btn-delete'}
			        style={{display:'inline-block'}}
			        onClick={onRemove}
			        labelColor="#FF0000"
			        label={getLocalizedText("BUTTON.DELETE","Delete")}
			      />}

					</ToolbarGroup>
				</Toolbar>
			</div>
		);
	}

});

let Description = React.createClass({
	mixins: [ItemMinix],

	onUpdate(){
    this.forceUpdate();
  },

	openHFPanel(){
		let {
			index,
			changedValues
		} = this.props;


		let self = this;

		//self.setState({cfCValues:changedValues[type]});
		DynConfigPanelActions.openPanel(changedValues[index]['content'], 'question', {onUpdate: self.onUpdate});
	},


	render(){
		if(this.isDebug()){
			console.log("render - ngi- title");
		}

		let self = this;
		let {
			index,
      changedValues
    } = this.props;

		let title = (changedValues[index] && checkExist(changedValues[index], 'content.en') && !isEmpty(changedValues[index]['content'].en))?
				changedValues[index]['content'].en:
				<span style={{color:'(0, 0, 0, 0.298039)'}}>
					{getLocalizedText("Description","Please click here to add description")}
				</span>;



		return(
			<div style={{cursor:'pointer', width:'100%', height:'58px', paddingTop:'12px'}} onClick={function(){self.openHFPanel()}}>
				{title}
			</div>
		);
	}
})

let NeedsGuideItem = React.createClass({
	mixins: [ItemMinix],

	onUpdate(){
    this.forceUpdate();
  },

	getInitialState() {
		let {
			index,
			changedValues
		} = this.props;

		let content = ContentStore.getPageContent();
		if(!content.changedValues["Attachments"]){
			content.changedValues["Attachments"] = {};
		}

		let attachments = content.changedValues["Attachments"];

		return {
			index: index,
			changedValues: changedValues,
			image: attachments[changedValues[index].image]
		}
	},

	componentWillReceiveProps(nextProps){
		if(this.isMount())
			this.setState({
				index: nextProps.index,
				changedValues: nextProps.changedValues
			})
	},

	propTypes:{
		index: React.PropTypes.number.isRequired,
		changedValues: React.PropTypes.array.isRequired,
		onClick: React.PropTypes.func
	},

	getStyles(){
		return(
				{
					imageStyle:{
						maxWidth:'100%'
					}
				}
		)

	},

	_remove(closePanelFunc){
		let {
			index,
			changedValues
		} = this.state;

		let {
			onListUpdate
		} = this.props;

    let removeDialog = {
			id: "delete",
			title: getLocalizedText("DELETE","DELETE"),
			message: getLocalizedText("DELETE_QUESTION","Delete this question?"),
			negative: {
				id: "delete_cancel",
				title: getLocalizedText("BUTTON.CANCEL","CANCEL"),
				type: "button"
			},
			positive:{
				id: "delete_confirm",
				title: getLocalizedText("BUTTON.OK","OK"),
				type: "customButton",
				handleTap: function(){
					changedValues.splice(index, 1);
					for(let i in changedValues){
						changedValues[i].seq = (i+1)*100;
					}
					onListUpdate();
					if(closePanelFunc && typeof(closePanelFunc) == 'function'){
						closePanelFunc(true);
					}
				}
			}
		}

		DialogActions.showDialog(removeDialog);

	},

	changeCPanel(){
		let self = this;
		let {
			changedValues,
			index
		} = this.state;

		let delAction = function(){
			self._remove(DynConfigPanelActions.closePanel);
		}

		//self.setState({cfCValues:question, deleteAction:delAction});
		DynConfigPanelActions.openPanel(changedValues[index], 'question', {deleteAction:delAction, onUpdate: self.onUpdate});
	},

	setImg(newFile) {
		let {
			changedValues,
			index
		} = this.state;

		let _content = ContentStore.getPageContent();
		if(!checkExist(_content, 'changedValues.Attachments')){
			_content.changedValues.Attachments = {};
		}
		let attachments = _content.changedValues["Attachments"];

		attachments[changedValues[index].image] = newFile.url;

    this.setState({image: newFile.url});

    this.onUpdate();
  },

	removeFile() {

		let {
			index,
			changedValues
		} = this.props;
		let _content = ContentStore.getPageContent();

		let attachments = _content.changedValues["Attachments"];
		attachments[changedValues[index].image] = "";
		this.setState({image: ""});

    this.onUpdate();
  },

	render: function() {

		var self = this;
		let styles = this.getStyles();

    let {
			index,
      changedValues,
			image
    } = this.state;

		if(this.isDebug()){
			console.log("render - ngi - item - ", index);
		}

		let type="image";
		var encodedStr = "";
		if(image && image.indexOf("application/pdf")>-1){
			type="pdf";
			encodedStr = image.substring(image.search(",") + 1, image.length);
		}

		let isLock = NeedDetailStore.isLock();

		let {
			onClick,
			...others
		} = this.props;
		let item = (type=='image'?
			<img src={image} style={{width:'auto', height: 'auto', maxHeight: '261px', maxWidth: '261px'}}/>:
				<PDF content={encodedStr} page={1} scale={0.4} loading={(<span>Loading...</span>)}/>
		)

		return (
			<div {...others}>
				<Topbar changedValues={changedValues} index={index} changeCPanel={this.changeCPanel} onRemove={this._remove}/>
				<div  style={{paddingLeft:'64px', paddingRight:'64px', paddingBottom: '24px'}}>
					<Description changedValues={changedValues} index={index}/>
					<div>
							{isLock?
								<div style={this.mergeAndPrefix({width: '100%', margin: 'auto', display: 'block'})}>{item}</div>:
									<Dropzone
						        onDrop={function(res) {
						          var invalid = [];

						          //File type checking
						          var actualFileType = res.file.type;
						          if (actualFileType == 'image/png' || actualFileType == 'image/jpg' || actualFileType =='application/pdf') {
						            if ( res.file.size > 2*1048576) {
						               invalid.push("Your file size exceed " +  2*1048576/(1024*1024) + " MB.");
						            }
						          }else{
						              invalid.push("Your file is not .png or .pdf type.");
						          }


						          if (invalid.length == 0) {
						            this.updateImg({
						              url: res.imageUrl,
						            });
						          } else {
						            var errorMsg = "";
						            for (var i = 0; i < invalid.length; i++) {
						              errorMsg += invalid[i] + "\n";
						            }
						            alert(errorMsg, "Import failed");
						          }
						        }.bind({updateImg: this.setImg})}
						        className="dropzone">
						        {image? item:
											<RaisedButton
								        key={'nag-btn-add-image-'+index}
												labelColor="#20A84C"
								        label={getLocalizedText("BUTTON.ADD_FILE","ADD FILE")}
								      />}
						      </Dropzone>
							}

						</div>
						{
							(!isLock && image)?
							<RaisedButton
				        key={'nag-btn-del-image-'+index}
				        label={getLocalizedText("BUTTON.DELETE","DELETE")}
								labelColor="#FF0000"
								style={{marginTop:'24px'}}
								onClick={this.removeFile}
				      />:null
						}
				</div>
				<Divider/>
			</div>
		);
	},
});

module.exports = NeedsGuideItem;

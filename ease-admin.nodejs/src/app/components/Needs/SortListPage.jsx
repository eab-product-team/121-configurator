let React = require('react');
let mui = require('material-ui');
let ReactCSSTransitionGroup = require('react-addons-css-transition-group');

let SortableList = require('../CustomView/SortableList.jsx');
let StylePropable = mui.Mixins.StylePropable;
let Transitions = mui.Styles.Transitions;
let FontIcon = mui.FontIcon;
let Toolbar = mui.Toolbar;
let ToolbarGroup = mui.ToolbarGroup;
let ToolbarTitle = mui.ToolbarTitle;
let Divider = mui.Divider;


let SortListPage = React.createClass({

	show(){
		this.setState({show:true});
	},
	hide(){
		this.setState({show:false});
	},
	getInitialState() {
    return {
      show:false
    };
  },

	onUpdate(){
		this.forceUpdate();
	},

	genItems(changedValues){
		let items = [];
		for(let i=0; i<changedValues.length; i++){
			let option = changedValues[i];
			let row = (
				<div style={{height:'56px', lineHeight:'56px'}}>
					<FontIcon
						style = {{padding: '0px 12px 0 12px', verticalAlign: 'middle', cursor:'pointer'}}
						color = {"#000000"}
						hoverColor={"#000000"}
						className="material-icons drag-area">
							reorder
					</FontIcon>
					<span style={{fontSize: '20px', display: 'inline-block', overflowX: 'hidden', whiteSpace: 'nowrap', width:'calc(100% - 64px)', textOverflow: 'ellipsis', verticalAlign: 'middle'}}>{(i+1) + '. ' + option.title.en}</span>
				</div>
			);
			items.push(row)
		}
		return items;
	},

	render(){
		let self = this;

		let {
			show
		} = this.state;

		let {
			changedValues,
			onListUpdate,
			disabled
		} = this.props;

		var slPageStyle = {
			height: show?'calc(100vh - 56px)':'0vh',
			transition: Transitions.easeOut(),
			position: 'absolute',
			background: '#FFFFFF',
			transform: 'translate3d(0px, 0px, 0px)',
			width: '100%',
			zIndex: '1300',
			left: '0px',
			bottom: '0px'
		}
		console.log("render sortListPage - ", changedValues);

		let items = this.genItems(changedValues);
		if(!show) return (<div key="slPage" style={slPageStyle}/>)
		return (
			<div key="slPage" style={slPageStyle}>
				<div key={'nfe-slp-header'} style={{height: '58px', display: 'block', overflow:'hidden'}}>
	        <Toolbar key="nfe-slp-hToolbar" style={{padding:"0px", position:'relative', background: '#FFFFFF'}} >
	          <ToolbarGroup key={'nfe-slp-hToolbar-left'} style={{display:'flex'}}>
							<span style={{cursor: 'pointer'}}>
								<FontIcon
				          key="backBtn"
				          tooltip="Back"
				          style= {{padding: '0px 12px', margin: '4px', lineHeight: '48px'}}
				          className="material-icons"
				          onClick={function(){
										onListUpdate();
										self.hide();
									}}>
				          arrow_back
				        </FontIcon>
							</span>
	            <ToolbarTitle text={"Sort"} style={{color:'#000000'}}/>
	          </ToolbarGroup>
	        </Toolbar>
					<hr/>
	      </div>
				<SortableList
					id={"nfe-sl"}
					items={items}
					disabled={disabled}
					changedValues={changedValues}
					handleSort={self.onUpdate}/>
			</div>
		);
	}
});
module.exports = SortListPage;

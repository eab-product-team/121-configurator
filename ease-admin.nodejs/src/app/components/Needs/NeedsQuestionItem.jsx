let React = require('react');
let mui = require('material-ui');

let ContentStore = require('../../stores/ContentStore.js');
let NeedDetailStore = require('../../stores/NeedDetailStore.js');

let DialogActions = require('../../actions/DialogActions.js');
let DynConfigPanelActions = require('../../actions/DynConfigPanelActions.js');

let ItemMinix = require('../../mixins/ItemMinix.js');

let TextSelection = require('../CustomView/TextSelection.jsx');
let IconBoxSection = require('../CustomView/IconBoxSection.jsx');
let NormalSlider = require('../CustomView/NormalSlider.jsx');
let TrackingSlider = require('../CustomView/TrackingSlider.jsx');
let AttachmentBox = require('../CustomView/AttachmentBox.jsx');
let Stepper = require('../CustomView/Stepper.jsx');

let Toolbar = mui.Toolbar;
let ToolbarGroup = mui.ToolbarGroup;
let ToolbarTitle = mui.ToolbarTitle;
let FontIcon = mui.FontIcon;
let Divider = mui.Divider;

let Dialog = require('../CustomView/Dialog.jsx');

let Topbar = React.createClass({

	mixins: [ItemMinix],

	onUpdate(){
    this.forceUpdate();
  },

	render(){
		if(this.isDebug()){
			console.log("render - topbar");
		}

		let self = this;
		let {
			index,
      changedValues,
			changeCPanel,
			onRemove
    } = this.props;

		let isLock = NeedDetailStore.isLock();

		let title = (changedValues && changedValues[index] && changedValues[index].title )?changedValues[index]['title'].en:'';

		let toggleMadatory = isLock?null:function(){
	    changedValues[index]['isMandatory'] = !changedValues[index]['isMandatory'];
			self.forceUpdate();
	  };

		return(
			<div style={{height: '58px', display: 'block', overflow:'hidden'}}>
				<Toolbar style={{padding:"0px", position:'relative', background: '#FFFFFF'}} >
					<ToolbarGroup style={{width:'calc(100% - 130px)', display:'flex'}}>
						<span style={{color:'#000000', height:'56px', lineHeight:'56px', padding:'0px 15px 0px 24px', width:'64px', fontSize: '20px'}}>{(index+1) + '. '}</span>
						<ToolbarTitle className="configurable" onClick={changeCPanel} text={title} style={{cursor:'pointer', color:'#000000'}}/>
					</ToolbarGroup>
					<ToolbarGroup float="right">
						<FontIcon
							style = {{padding: '0px 15px 0 24px'}}
							color = {(changedValues.length>index && changedValues[index]['isMandatory'])?"#FF0000":"#AAAAAA"}
							hoverColor={"#000000"}
							className="material-icons"
							onClick={toggleMadatory}>
							star
						</FontIcon>
						{isLock?null:<FontIcon
							style = {{padding: '0px 15px 0 24px'}}
							color = {"#AAAAAA"}
							hoverColor={"#000000"}
							className="material-icons"
							onClick={onRemove}>
							delete
						</FontIcon>}

					</ToolbarGroup>
				</Toolbar>
			</div>
		);
	}

});

let HFTitle = React.createClass({
	mixins: [ItemMinix],

	onUpdate(){
    this.forceUpdate();
  },

	openHFPanel(titleId){
		let {
			index,
			changedValues
		} = this.props;

		if(!changedValues[index]){
			changedValues[index] = {};
		}

		let self = this;


		if(!checkExist(changedValues[index], titleId+'.en')){
			changedValues[index][titleId]=NeedDetailStore.genLangCodesObj();
		}

		//self.setState({cfCValues:changedValues[type]});
		DynConfigPanelActions.openPanel(changedValues[index][titleId], 'question', {onUpdate: self.onUpdate});
	},

	getTitleId(type){
		return (type=='header')?'headerMsg':'footerMsg';
	},

	render(){
		if(this.isDebug()){
			console.log("render - title");
		}

		let self = this;
		let {
			index,
      changedValues,
			type
    } = this.props;

		let titleId = this.getTitleId(type.toLowerCase());
		let title = (changedValues[index] && checkExist(changedValues[index], titleId+'.en') && !isEmpty(changedValues[index][titleId].en))?
				changedValues[index][titleId].en:
				<span style={{color:'(0, 0, 0, 0.298039)'}}>
					{getLocalizedText(type.toUpperCase(),"Please click here to add " + type.toLowerCase())}
				</span>;



		return(
			<div className="configurable" style={{cursor:'pointer', width:'100%', height:'58px', paddingTop:'12px'}} onClick={function(){self.openHFPanel(titleId)}}>
				{title}
			</div>
		);
	}
})

let NeedsQuestionItem = React.createClass({
	mixins: [ItemMinix],

	onUpdate(){
    this.forceUpdate();
  },


	propTypes:{
		index: React.PropTypes.number.isRequired,
		changedValues: React.PropTypes.array.isRequired,
		onClick: React.PropTypes.func
	},

	getStyles(){
		return(
				{
					imageStyle:{
						maxWidth:'100%'
					}
				}
		)

	},

	_genItem(inputType){
		let {
			id,
			index,
			changedValues
		} = this.props;

		let styles = this.getStyles();

		let cValue = changedValues[index];
		let imagePath = "web/img/input_type/";

		let isLock = NeedDetailStore.isLock();

		switch(inputType){
			case 'iconSelection':
			case 'imageSelection':
				var needsAlyOpts = ContentStore.getPageContent().values.needsAlyOpts;
				var options = {needsAlyOpts: needsAlyOpts};
				return <IconBoxSection id={id} changedValues={cValue} isSortable={true} sectionId={this.props.sectionId} disabled={isLock} options={options}/>
			case 'textSelection':
				return <TextSelection id={id} isSortable={true} changedValues={cValue} disabled={isLock}/>
			case 'attachment':
				return <AttachmentBox id={id} changedValues={cValue.attachmentSetting} disabled={isLock}/>
			case 'normalSlider':
				return <NormalSlider id={id} changedValues={cValue} disabled={isLock}/>
			case 'trackingSlider':
				return <TrackingSlider id={id} changedValues={cValue} disabled={isLock}/>
			case 'stepper':
				return <Stepper id={id} defaultValue={cValue.defaultValue} changedValues={cValue.defaultValue} minValue={cValue.minValue} maxValue={cValue.maxValue} interval={cValue.increment} isDemo={true} disabled={isLock}/>
			default:
				return <img style={styles.imageStyle} src={imagePath+inputType+'.png'}/>
		}

	},

	_remove(callback){


		let {
			index,
			changedValues,
			onListUpdate
		} = this.props;

    let removeDialog = {
			id: "delete",
			title: getLocalizedText("DELETE","DELETE"),
			message: getLocalizedText("DELETE_QUESTION","Delete this question?"),
			negative: {
				id: "delete_cancel",
				title: getLocalizedText("BUTTON.CANCEL","CANCEL"),
				type: "button"
			},
			positive:{
				id: "delete_confirm",
				title: getLocalizedText("BUTTON.OK","OK"),
				type: "customButton",
				handleTap: function(){
					changedValues.splice(index, 1);
					for(let i in changedValues){
						changedValues[i].seq = (i+1)*100;
					}
					onListUpdate();
					if(callback && typeof(callback) == 'function'){
						callback();
					}
				}
			}
		}

		DialogActions.showDialog(removeDialog);

	},

	changeCPanel(){
		let self = this;
		let {
			changedValues,
			index
		} = this.props;

		let validation = function(){
			//assume changedValues[index] is deleted if changedValues[index] not exist
			if(!changedValues[index]) return null;
			
			let type = changedValues[index].type;
			let errorMsg = false;
			if(!checkExist(changedValues[index], 'title.en')){
				errorMsg = "Some mandatory data do not input yet.";
			}else if(type){
				let section = NeedDetailStore.getInputTypeSections(type);
				for(let i in section.items){
					let _error = validate(section.items[i], changedValues[index]);

					if(_error){
						errorMsg = "Some mandatory data do not input yet.";
						break;
					}
				}
			}
			if(!errorMsg){
				console.log("validate");
			}else{
				console.log(errorMsg);
				let removeDialog = {
					id: "delete",
					title: getLocalizedText("ERROR","Error"),
					message: errorMsg,
					negative: {
						id: "OkBtn",
						title: getLocalizedText("BUTTON.OK","OK"),
						type: "button"
					}
				}

				DialogActions.showDialog(removeDialog);
			}

			return errorMsg;
		};

		let delAction = function(){
			self._remove(DynConfigPanelActions.closePanel);
		}

		let imgOtherAction = function(value){
			let options = changedValues[index].options;
			if(value){
				if(!options){
					options = [];
				}
				let langCodesItems = NeedDetailStore.genLangCodesObj();
				options.push({title:langCodesItems, id: 'others', isOther: true, otherInputType: 'text', image:changedValues[index].id+'-other'});
			}else{
				for(let i in options){
					if(options[i].id == 'others'){
						options.splice(Number(i),1);
					}
				}
			}

		};

		//self.setState({cfCValues:question, deleteAction:delAction});
		DynConfigPanelActions.openPanel(changedValues[index], 'question', {deleteAction:delAction, otherAction: imgOtherAction, onUpdate: self.onUpdate, validation: validation});
	},

	render: function() {


		var self = this;
		let styles = this.getStyles();

		let {
			index,
      changedValues,
			onClick,
			...others
		} = this.props;

		if(this.isDebug()){
			console.log("render - item", index);
		}


		let item = (changedValues[index] && changedValues[index]['type'])? this._genItem(changedValues[index]['type']):null;

		return (
			<div {...others}>
				<Topbar changedValues={changedValues} index={index} changeCPanel={this.changeCPanel} onRemove={this._remove}/>
				<div  style={{paddingLeft:'64px', paddingRight:'64px', paddingBottom: '24px'}}>
					<HFTitle changedValues={changedValues} index={index} type="header"/>
					{item}
					<HFTitle changedValues={changedValues} index={index} type="footer"/>
				</div>
				<Divider/>
			</div>
		);
	},
});

module.exports = NeedsQuestionItem;

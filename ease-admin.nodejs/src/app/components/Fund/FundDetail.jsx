import React, { Component, PropTypes } from 'react';

const mui = require('material-ui');
const {SelectField, Checkbox, TextField, MenuItem, Dialog, FlatButton} = mui;
const FundActions = require('../../actions/FundActions.js');
const AppBarActions = require('../../actions/AppBarActions.js');
const EABFileUpload = require('../CustomView/FileUpload.jsx');
const ReadOnlyField = require('../CustomView/ReadOnlyField.jsx');
const FUNDCONST = require('./FundConst.jsx');

const ITEMSTYLE = {
  width : '100%',
};

const PAGESTYLE = {
  overflowY : 'auto',
  height: "calc(100vh - 58px)",
  paddingLeft : '20px',
  paddingRight : '20px'
};

const FIELDKEY = {
  fundCode : 'fundCode',
  fundName : 'fundName',
  annualManageFee : 'annualManageFee',
  updateBy : 'updateBy'
};

const DIALOGOPTION = {
  createNotNull : 'createNotNull',
  updateSuccess : 'updateSuccess',
  createSuccess : 'createSuccess',
  deleteQuery : 'deleteQuery'
};

var ATTACHMENT = {
  subType : 'attachment',
  presentation : 'application/pdf||10',
  version : '1',
  title : 'Product Highlight Sheet'
};

const FundDetail = React.createClass({

  // React Life Cycle
  getInitialState : function() {
    return {
      assetClass : null,
      fundCode : null,
      openDialog : false,
      dialogInfo : {}
    };
  },

  componentWillMount() {
    this.load();
  },

  load : function() {
    if (this.props.id) {
      FundActions.getFund(this.props.id).then(fund => {
        this.loadEditModePage(fund);
      });
    } else {
      this.initPaymentOptionsCheck('');
      this.appbarCreateMode();
    }
  },

  // CRUD
  save : function() {
    var paymentMethod = this.paymentMethodToSend();
    if (!this.checkMandatoryFields(paymentMethod)) {
      return;
    }
    var result = _.assign({}, this.state.fund, {
      fundCode : this.state.fundCode,
      fundName : this.state.fundName,
      annualManageFee : this.state.annualManageFee,
      riskRating : this.state.riskRating,
      ccy : this.state.ccy,
      isMixedAsset : this.state.isMixedAsset,
      assetClass : this.state.assetClass,
      paymentMethod : paymentMethod
    });
    FundActions.updateFund(result).then(() => {
      this.setState({
        openDialog : true,
        dialogInfo : this.getDialogInfo(DIALOGOPTION.updateSuccess)
      });
    });
  },

  add : function() {
    var paymentMethod = this.paymentMethodToSend();
    if (!this.checkMandatoryFields(paymentMethod)) {
      return;
    }
    var result = {
      compCode : '08',
      fundCode : this.state.fundCode,
      fundName : this.state.fundName,
      assetClass : this.state.assetClass,
      annualManageFee : this.state.annualManageFee,
      riskRating : this.state.riskRating,
      ccy : this.state.ccy,
      isMixedAsset : this.state.isMixedAsset,
      paymentMethod : paymentMethod
    };
    FundActions.createFund(result).then((fund) => {
      this.setState({
        fund : fund,
        openDialog : true,
        dialogInfo : this.getDialogInfo(DIALOGOPTION.createSuccess)
      });
    });
  },

  delete : function() {
    this.setState({
      openDialog : true,
      dialogInfo : this.getDialogInfo(DIALOGOPTION.deleteQuery)
    });
  },

  confirmDelete : function() {
    FundActions.deleteFund(this.props.id).then(() => {
      this.closeDialog(() => {
        FundActions.openFundList();
      });
    })
  },

  // UI
  loadEditModePage : function (fund) {
    this.initPaymentOptionsCheck(fund.paymentMethod);
    this.setState({
      fund,
      ...fund,
      startGetAttachment : true
    });
    this.appbarEditMode();
  },

  // Others
  initPaymentOptionsCheck : function(paymentMethod) {
    var obj = {};
    _.each(FUNDCONST.PAYMENTOPTIONSNAME, (name, key) => {
      obj[key] = paymentMethod.indexOf(key) > -1;
    });
    this.setState(obj);
  },

  checkMandatoryFields : function(paymentMethod) {
    const {fundCode, fundName, assetClass, annualManageFee, riskRating,
      ccy} = this.state;
    var pass = fundCode && fundName && assetClass && !_.isUndefined(annualManageFee) && !_.isNull(annualManageFee) &&
      riskRating && ccy && paymentMethod;
    if (!!pass) {
      return true;
    } else {
      this.setState({
        openDialog : true,
        dialogInfo : this.getDialogInfo(DIALOGOPTION.createNotNull)
      });
      return false;
    }
  },

  paymentMethodToSend : function() {
    var result = '';
    _.each(FUNDCONST.PAYMENTOPTIONSNAME, (name, key) => {
       result += this.state[key] ? key + ',' : '';
    });
    if (result.length > 0) {
      result = result.slice(0, -1);
    }
    return result;
  },

  getPaymentOptions : function() {
    var result = _.map(FUNDCONST.PAYMENTOPTIONSNAME, (name, key) => {
      return (
        <Checkbox
          key={key}
          id={key}
          label={name}
          checked={this.state[key]}
          onCheck={(event, isInputChecked) => this.setStateFunc(isInputChecked, key)}
        />
      );
    });
    return result;
  },

  getRiskRatingOptions : function() {
    var result = _.map(FUNDCONST.RISKRATINGOPTIONS, option => {
      return (
        <MenuItem
          key={'riskrating_' + option.riskRating}
          value={option.riskRating}
          primaryText={option.label}
        />
      );
    });
    return result;
  },

  getCcyOptions : function() {
    var result = _.map(FUNDCONST.CCYOPTIONS, (option, optionKey) => {
      return (
        <MenuItem
          key={'ccy_' + optionKey}
          value={optionKey}
          primaryText={option}
        />
      );
    });
    return result;
  },

  getAssetOptions : function() {
    var result = _.map(FUNDCONST.ASSETOPTIONS, (option) => {
      return (<MenuItem key={'asset_' + option.id} value={option.id} primaryText={option.label} />);
    });
    return result;
  },

  // onChange
  riskRatingHandleChange : function(event, index, value) {
    this.setState({
      riskRating : value
    });
  },

  ccyHandleChange : function(event, index, value) {
    this.setState({
      ccy : value
    });
  },

  isMixedAssetHandleChange : function(event, checked) {
    this.setState({
      isMixedAsset : checked
    });
  },

  annualManageFeeHandleChange : function(e) {
    var newValue = e.target.value;
    if (!window.isNaN(newValue)) {
      this.setState({annualManageFee : newValue});
    }
  },

  assetHandleChange : function(event, index, value) {
    this.setState({
      assetClass : value
    });
  },

  setStateFunc : function(value, key) {
    var obj = {};
    obj[key] = value;
    this.setState(obj);
  },

  // dialog
  getDialogInfo : function(option) {
    var result = {};
    const closeBtn = (
      <FlatButton
        label='OK'
        primary={true}
        onTouchTap={this.closeDialog}
      />
    );
    const cancelBtn = (
      <FlatButton
        label='Cancel'
        primary={true}
        onTouchTap={this.closeDialog}
      />
    );
    const createSuccessBtn = (
      <FlatButton
        label='OK'
        primary={true}
        onTouchTap={() => {
          this.closeDialog();
          this.loadEditModePage(this.state.fund);
        }}
      />
    );
    const deleteConfirmBtn = (
      <FlatButton
        label='Delete'
        primary={true}
        onTouchTap={this.confirmDelete}
      />
    );
    const toFundListBtn = (
      <FlatButton
        label='OK'
        primary={true}
        onTouchTap={this.closeDialog}
      />
    );

    switch (option) {
      case DIALOGOPTION.createNotNull:
        result = {
          title : 'Notice',
          message : 'Please fill in all fields',
          actions : [closeBtn]
        }
        break;
      case DIALOGOPTION.createSuccess:
        result = {
          title : 'Notice',
          message : 'New fund has been created',
          actions : [createSuccessBtn]
        }
        break;
      case DIALOGOPTION.updateSuccess:
        result = {
          title : 'Notice',
          message : 'New data has been updated',
          actions : [closeBtn]
        }
        break;
      case DIALOGOPTION.deleteQuery:
        result = {
          title : 'Notice',
          message : 'Confirmed to delete this fund?',
          actions : [cancelBtn, deleteConfirmBtn]
        }
        break;
      default:
        break;
    }
    return result;
  },

  openDialog : function() {
    const {openDialog, dialogInfo} = this.state;
    const {title, actions, message} = dialogInfo;

    return (
      <Dialog
        open = {openDialog}
        title = {title}
        actions = {actions}
      >
        {message}
      </Dialog>
    );
  },

  closeDialog : function(callback) {
    this.setState({
      openDialog : false
    }, () => {
      callback && callback();
    });
  },

  // appBar
  appbarCreateMode : function() {
    AppBarActions.updateAppBar('Funds', [
      {
        id : 'add',
        title : 'Add',
        type : 'customButton',
        onTouchTap : this.add
      },
    ]);
  },

  appbarEditMode : function() {
    AppBarActions.updateAppBar('Funds', [
      {
        id : 'save',
        title : 'Save',
        type : 'customButton',
        onTouchTap : this.save
      },
      {
        id : 'delete',
        title : 'Delete',
        type : 'customButton',
        onTouchTap : this.delete
      }
    ]);
  },

  render() {
    var {id, fundCode, fundName, assetClass, annualManageFee,
      riskRating, updateBy, updateDate, startGetAttachment,
      ccy, isMixedAsset} = this.state;

    return (
      <div style={PAGESTYLE}>
        <TextField
          id= {FIELDKEY.fundCode}
          style={ITEMSTYLE}
          value={fundCode}
          floatingLabelText = {FUNDCONST.FIELDNAME.FUNDCODE}
          onChange = {(e) => {this.setStateFunc(e.target.value, FIELDKEY.fundCode)}}
        />

        <TextField
          id= {FIELDKEY.fundName}
          style={ITEMSTYLE}
          value={fundName}
          floatingLabelText = {FUNDCONST.FIELDNAME.FUNDNAME}
          onChange = {(e) => {this.setStateFunc(e.target.value, FIELDKEY.fundName)}}
        />

        <SelectField
          id='assetClass'
          style={ITEMSTYLE}
          floatingLabelText={FUNDCONST.FIELDNAME.ASSETCLASS}
          value={assetClass}
          onChange={this.assetHandleChange}
          autoWidth={true}
        >
          {this.getAssetOptions()}
        </SelectField>

        <Checkbox
          name="isMixedAssetGroup"
          checked={isMixedAsset}
          label={FUNDCONST.FIELDNAME.ISMIXEDASSET}
          onCheck={this.isMixedAssetHandleChange}
        />

        <TextField
          id= {FIELDKEY.annualManageFee}
          style={ITEMSTYLE}
          value={annualManageFee || ''}
          floatingLabelText = {FUNDCONST.FIELDNAME.ANNAULMANAGEFEE}
          onChange = {this.annualManageFeeHandleChange}
        />

        <div id='paymentMethod' style={ITEMSTYLE}>
          <label>{FUNDCONST.FIELDNAME.PAYMENTMETHOD}</label>
          {this.getPaymentOptions()}
        </div>

        <SelectField
          id='riskRating'
          style={ITEMSTYLE}
          floatingLabelText={FUNDCONST.FIELDNAME.RISKRATING}
          value={riskRating}
          onChange={this.riskRatingHandleChange}
          autoWidth={true}
        >
          {this.getRiskRatingOptions()}
        </SelectField>

        <SelectField
          id='ccy'
          style={ITEMSTYLE}
          floatingLabelText={FUNDCONST.FIELDNAME.CURRENCY}
          value={ccy}
          onChange={this.ccyHandleChange}
          autoWidth={true}
        >
          {this.getCcyOptions()}
        </SelectField>

        {updateBy && updateDate ?
          <ReadOnlyField
            key = {FIELDKEY.updateBy}
            id = {FIELDKEY.updateBy}
            floatingLabelText = {FUNDCONST.FIELDNAME.UPDATEBY}
            defaultValue = {(updateBy + ' / ' + new Date(updateDate).format(FUNDCONST.DATEFORMAT))}
          />
          :
          null
        }

        {startGetAttachment ?
          <EABFileUpload
            changedValues={{}}
            template={{
              id : 'phs',
              subType : ATTACHMENT.subType,
              presentation : ATTACHMENT.presentation,
              title : ATTACHMENT.title
            }}
            rootValues={{
              id : 'fund_' + id,
              version : ATTACHMENT.version
            }}
          />
          :
          null
        }

        {this.openDialog()}
      </div>
    );
  }
});

module.exports = FundDetail;

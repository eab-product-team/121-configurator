import React, { Component, PropTypes } from 'react';
import Table from 'material-ui/lib/table/table';
import TableBody from 'material-ui/lib/table/table-body';
import TableHeader from 'material-ui/lib/table/table-header';
import TableHeaderColumn from 'material-ui/lib/table/table-header-column';
import TableRow from 'material-ui/lib/table/table-row';
import TableRowColumn from 'material-ui/lib/table/table-row-column';

const FUNDCONST = require('./FundConst.jsx');
const FundActions = require('../../actions/FundActions.js');
const AppBarActions = require('../../actions/AppBarActions.js');

const Fund = React.createClass({

  // react life cycle
  getInitialState : function() {
    return {
      fundList : null,
      selectedId : null,
      selectedRows : []
    };
  },

  componentWillMount() {
    this.load();
  },

  componentDidUpdate() {
    if (!this.state.openDetail) {
      this.updateAppBar();
    }
  },

  // init
  load : function() {
    FundActions.listFunds().then(list => {
      this.setState({fundList: list});
    });
    this.updateAppBar();
  },

  // appBar
  updateAppBar : function() {
    const {selectedRows} = this.state;
    var buttons = [];
    if (selectedRows && selectedRows.length >= 1) {
      buttons.push({
        id : 'export',
        title : 'Export',
        type : 'customButton',
        onTouchTap : this.exportOnClick
      });
    }
    buttons.push({
      id : 'add',
      title : 'Add',
      type : 'customButton',
      onTouchTap : this.addOnClick
    });

    AppBarActions.updateAppBar('Funds', buttons);
  },

  exportOnClick: function() {
    const {fundList, selectedRows} = this.state;
    const ids = _.map(selectedRows, row => fundList[row].id);
    FundActions.exportFund(ids);
  },

  addOnClick: function() {
    FundActions.openFundDetail(null);
  },

  // fund
  fundOnClick : function(id) {
    FundActions.openFundDetail(id);
  },

  // UI
  getTable : function() {
    var tableBody = this.getTableBody();

    return (
      <div key="content" className="PageContent" style={{ height: 'calc(100vh - 68px)', overflow: 'auto' }}>
        <Table
          selectable
          multiSelectable
          onRowSelection={(rows) => this.setState({ selectedRows: rows})}
        >
          <TableHeader enableSelectAll={false}>
            <TableRow>
              <TableHeaderColumn style={{width : '10%'}}>{FUNDCONST.FIELDNAME.FUNDCODE}</TableHeaderColumn>
              <TableHeaderColumn style={{width : '30%'}}>{FUNDCONST.FIELDNAME.FUNDNAME}</TableHeaderColumn>
              <TableHeaderColumn style={{width : '25%'}}>{FUNDCONST.FIELDNAME.ASSETCLASS}</TableHeaderColumn>
              <TableHeaderColumn style={{width : '20%'}}>{FUNDCONST.FIELDNAME.RISKRATING}</TableHeaderColumn>
              <TableHeaderColumn style={{width : '25%'}}>{FUNDCONST.FIELDNAME.UPDATEBY}</TableHeaderColumn>
            </TableRow>
          </TableHeader>

          <TableBody>
            {tableBody}
          </TableBody>
        </Table>
      </div>
    );
  },

  getTableBody : function() {
    const {fundList, selectedRows} = this.state;
    var records = [];

    _.each(fundList, (fund, index) => {
      records.push(
        <TableRow key={'fund' + fund.id} selected={selectedRows.indexOf(index) > -1}>
          <TableRowColumn style={{width : '10%'}} onTouchTap={() => this.fundOnClick(fund.id)}>{fund.fundCode}</TableRowColumn>
          <TableRowColumn style={{width : '30%'}} onTouchTap={() => this.fundOnClick(fund.id)}>{fund.fundName}</TableRowColumn>
          <TableRowColumn style={{width : '25%'}} onTouchTap={() => this.fundOnClick(fund.id)}>{(_.find(FUNDCONST.ASSETOPTIONS, opt => opt.id === fund.assetClass) || {}).label}</TableRowColumn>
          <TableRowColumn style={{width : '20%'}} onTouchTap={() => this.fundOnClick(fund.id)}>{(_.find(FUNDCONST.RISKRATINGOPTIONS, opt => opt.riskRating === fund.riskRating) || {}).label}</TableRowColumn>
          <TableRowColumn style={{width : '25%'}} onTouchTap={() => this.fundOnClick(fund.id)}>{fund.updateBy + ' / ' + new Date(fund.updateDate).format(FUNDCONST.DATEFORMAT)}</TableRowColumn>
        </TableRow>
      );
    });

    return records;
  },

  // Others
  onDeleteCb : function() {
    this.load();
    this.setState({openDetail : false});
  },

  paymentMethodName : function(inputStr) {
    var nameList = [];
    _.each(FUNDCONST.PAYMENTOPTIONSNAME, (name, key) => {
      if (inputStr.indexOf(key) > -1) {
        nameList.push(name);
      }
    });
    return nameList.join(',');
  },

  render() {
    return this.getTable();
  }
});

module.exports = Fund;

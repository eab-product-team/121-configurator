import React, {Component, PropTypes} from 'react';
import {Table, TableHeader, TableHeaderColumn, TableBody, TableRow, TableRowColumn} from 'material-ui';

import ConfirmDialog from '../CustomView/ConfirmDialog.jsx';
import ReadOnlyField from '../CustomView/ReadOnlyField.jsx';
import EditVersionDialog from './EditVersionDialog.jsx';
import EditSummaryDialog from './EditSummaryDialog.jsx';
import CloneConfigDialog from './CloneConfigDialog.jsx';

import AppBarActions from '../../actions/AppBarActions';
import ConfigPageActions from '../../actions/ConfigPageActions';
import ConfigConstants from '../../constants/ConfigConstants';

export default class ConfigSummary extends Component {

  static propTypes = {
    id: PropTypes.number
  };

  constructor(props) {
    super(props);
    this.state = {
      summary: {},
      editSummary: false,
      editVersion: null,
      cloneOther: false,
      cloneVersion: false,
      selectedRows: [],
      confirmDelete: false
    };
    this.fetch(this.props.id);
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.id !== nextProps.id) {
      this.fetch(nextProps.id);
    }
  }

  componentDidUpdate() {
    this.updateAppbar();
  }

  fetch(id) {
    ConfigPageActions.getSummary(id).then(summary => {
      this.setState({
        summary,
        selectedRows: [],
        editSummary: false,
        editVersion: null,
        cloneOther: false,
        cloneVersion: false,
        confirmDelete: false
      }, () => this.updateAppbar());
    });
  }

  updateAppbar() {
    const {id} = this.props;
    const {summary, selectedRows} = this.state;
    const actions = [];
    if (selectedRows.length) {
      if (selectedRows.length == 1) {
        actions.push({
          id: 'editVersion',
          title: 'Edit Version',
          type: 'customButton',
          onTouchTap: () => {
            const selectedVersion = summary.versions[selectedRows[0]];
            this.setState({ editVersion: selectedVersion });
          }
        });
      }
      actions.push({
        id: 'deleteVersion',
        title: 'Delete',
        type: 'customButton',
        onTouchTap: () => this.setState({ confirmDelete: true })
      });
    } else {
      // actions.push({
      //   id: 'edit',
      //   title: 'Edit',
      //   type: 'customButton',
      //   onTouchTap: () => this.setState({ editSummary: true })
      // });
      actions.push({
        id: 'newVersion',
        title: 'New Version',
        type: 'customButton',
        onTouchTap: () => this.setState({ editVersion: {} })
      });
      actions.push({
        id: 'cloneOther',
        title: 'Clone From Other Item',
        type: 'customButton',
        onTouchTap: () => this.setState({ cloneOther: true })
      });
      if (summary.versions.length) {
        actions.push({
          id: 'cloneLatestVersion',
          title: 'Clone latest version',
          type: 'customButton',
          onTouchTap: () => this.setState({ cloneVersion: true })
        });
      }
    }
    AppBarActions.updateAppBar(summary.name, actions);
  }

  selectVersion(versionId) {
    ConfigPageActions.initConfigPage(versionId);
  }

  saveSummary(name, description) {
    const {summary} = this.state;
    ConfigPageActions.updateSummary(Object.assign({}, summary, { name, description })).then(summary => {
      this.setState({
        editSummary: null,
        summary
      });
    });
  }

  saveVersion(description) {
    const {id} = this.props;
    const {editVersion} = this.state;
    if (!editVersion.id) {
      ConfigPageActions.createVersion(id, description).then(version => {
        this.selectVersion(version.id);
      });
    } else {
      ConfigPageActions.updateVersion(id, Object.assign({}, editVersion, { description })).then(() => this.fetch(id));
    }
  }

  cloneVersion(description) {
    const {id} = this.props;
    const {summary} = this.state;
    let latestVersion = null;
    _.each(summary.versions, version => {
      if (!latestVersion || version.versionNum > latestVersion.versionNum) {
        latestVersion = version;
      }
    });
    ConfigPageActions.cloneVersion(id, latestVersion.id, description).then((version) => {
      this.selectVersion(version.id);
    });
  }

  cloneOther(description, summaryId, versionId) {
    const {id} = this.props;
    ConfigPageActions.cloneVersion(summaryId, versionId, description, id).then((version) => {
      this.selectVersion(version.id);
    });
  }

  deleteVersions() {
    const {id} = this.props;
    const {summary, selectedRows} = this.state;
    const versionIds = _.map(selectedRows, row => summary.versions[row].id);
    ConfigPageActions.deleteVersions(id, versionIds).then(() => this.fetch(id));
  }

  _getDetails() {
    const {id} = this.props;
    const {name, description, updateBy, updateDate, createBy, createDate} = this.state.summary;
    return [
      { key: 'id', label: 'ID', value: id },
      { key: 'name', label: 'Name', value: name },
      { key: 'description', label: 'Description', value: description },
      { key: 'updateBy', label: 'Last Updated By', value: updateDate && (updateBy + ' / ' + new Date(updateDate).format(ConfigConstants.DateTimeFormat)) },
      { key: 'createBy', label: 'Created By', value: createDate && (createBy + ' / ' + new Date(createDate).format(ConfigConstants.DateTimeFormat)) }
    ];
  }

  render() {
    const {id} = this.props;
    const {summary, editSummary, editVersion, cloneOther, cloneVersion, selectedRows, confirmDelete} = this.state;
    const {versions, module, type} = summary;
    return (
      <div className="PageContent" style={{ height: 'calc(100vh - 68px)', overflow: 'auto' }}>
        <h2 className="DetailsItem" style={{ paddingTop: 0 }}>Configuration Summary</h2>
        <div className="hBoxContainer">
          {_.map(this._getDetails(), detail => (
            <ReadOnlyField
              key={detail.key}
              className="DetailsItem"
              floatingLabelText={detail.label}
              defaultValue={detail.value}
            />
          ))}
        </div>
        <h2 className="DetailsItem" style={{ marginTop: 0, paddingTop: 0 }}>Versions</h2>
        <Table multiSelectable onRowSelection={(rows) =>  this.setState({ selectedRows: rows })}>
          <TableHeader enableSelectAll={false}>
            <TableRow>
              <TableHeaderColumn>Version</TableHeaderColumn>
              <TableHeaderColumn>Name</TableHeaderColumn>
              <TableHeaderColumn>Last Updated By</TableHeaderColumn>
              <TableHeaderColumn>Created By</TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody deselectOnClickaway={false}>
            {_.map(versions, (version, index) => (
              <TableRow key={version.id} selected={selectedRows.indexOf(index) > -1}>
                <TableRowColumn onTouchTap={() => this.selectVersion(version.id)}>{version.versionNum}</TableRowColumn>
                <TableRowColumn onTouchTap={() => this.selectVersion(version.id)}>{version.description}</TableRowColumn>
                <TableRowColumn onTouchTap={() => this.selectVersion(version.id)}>{version.updateBy}</TableRowColumn>
                <TableRowColumn onTouchTap={() => this.selectVersion(version.id)}>{version.createBy}</TableRowColumn>
              </TableRow>
            ))}
          </TableBody>
        </Table>
        {editSummary ? (
          <EditSummaryDialog
            open
            onSave={(name, description) => this.saveSummary(name, description)}
            onClose={() => this.setState({ editSummary: false })}
          />
        ) : null}
        {editVersion ? (
          <EditVersionDialog
            open
            onSave={description => this.saveVersion(description)}
            onClose={() => this.setState({ editVersion: null })}
          />
        ) : null}
        {cloneOther ? (
          <CloneConfigDialog
            module={module}
            type={type}
            onSave={(description, id, versionId) => this.cloneOther(description, id, versionId)}
            onClose={() => this.setState({ cloneOther: false })}
          />
        ) : null}
        {cloneVersion ? (
          <EditVersionDialog
            open
            onSave={description => this.cloneVersion(description)}
            onClose={() => this.setState({ cloneVersion: false })}
          />
        ) : null}
        {confirmDelete ? (
          <ConfirmDialog
            open
            msg="Are you sure to delete the selected versions?"
            onConfirm={() => this.deleteVersions()}
            onClose={() => this.setState({ confirmDelete: false })}
          />
        ) : null}
      </div>
    );
  }
}

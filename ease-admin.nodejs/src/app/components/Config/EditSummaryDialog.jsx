import React, {Component, PropTypes} from 'react';
import {Dialog, FlatButton, TextField} from 'material-ui';

export default class EditSummaryDialog extends Component {

  static propTypes = {
    open: PropTypes.bool,
    onSave: PropTypes.func,
    onClose: PropTypes.func
  };

  constructor() {
    super();
    this.state = {
      name: null,
      description: null
    };
  }

  onRequestClose() {
    const {onClose} = this.props;
    onClose && onClose();
  }

  render() {
    const {open, onSave} = this.props;
    const {name, description} = this.state;
    const actions = [
      <FlatButton label="Cancel" onTouchTap={() => this.onRequestClose()} />,
      <FlatButton
        primary
        label="Save"
        disabled={!name}
        onTouchTap={() => onSave && onSave(name, description)}
      />
    ];
    return (
      <Dialog
        open={open}
        title="Save Summary"
        modal={false}
        onRequestClose={() => this.onRequestClose()}
        actions={actions}
      >
        <TextField
          fullWidth
          floatingLabelText="Name"
          value={name}
          onChange={(event) => this.setState({ name: event.target.value })}
        />
        <TextField
          fullWidth
          floatingLabelText="Description"
          value={description}
          onChange={(event) => this.setState({ description: event.target.value })}
        />
      </Dialog>
    );
  }

};

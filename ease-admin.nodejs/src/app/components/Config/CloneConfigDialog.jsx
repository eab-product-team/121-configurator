import React, {Component, PropTypes} from 'react';
import {Dialog, FlatButton, TextField, List, ListItem, Tabs, Tab, Divider} from 'material-ui';
import { SelectableContainerEnhance } from 'material-ui/lib/hoc/selectable-enhance';
const SelectableList = SelectableContainerEnhance(List);

import ConfigPageActions from '../../actions/ConfigPageActions';

export default class AddItemDialog extends Component {

  static propTypes = {
    module: PropTypes.string,
    type: PropTypes.string,
    onSave: PropTypes.func,
    onClose: PropTypes.func
  }

  constructor(props) {
    super(props);
    this.state = {
      description: null,
      items: [],
      versions: [],
      selectedItem: null,
      selectedVersion: null,
      searchTerm: null
    };
  }

  componentDidMount() {
    const {module, type} = this.props;
    ConfigPageActions.getSummaries(module, type).then(items => {
      this.setState({
        items,
        versions: [],
        selectedItem: null,
        selectedVersion: null,
        searchTerm: null
      });
    });
  }

  onRequestClose() {
    const {onClose} = this.props;
    onClose && onClose();
  }

  getItemList() {
    const {selectedItem, items, searchTerm} = this.state;
    const list = [];
    const filter = searchTerm && searchTerm.toUpperCase();
    _.each(items, item => {
      if (filter && (item.name.toUpperCase().indexOf(filter) === -1 && (!item.description || item.description.toUpperCase().indexOf(filter) === -1))) {
        return;
      }
      list.push(
        <ListItem
          key={item.id}
          value={item.id}
          primaryText={item.name + (item.description ? ' (' + item.description + ')' : '')}
        />
      );
      list.push(<Divider />);
    });
    return (
      <SelectableList
        style={{ maxHeight: 400, overflow: 'auto' }}
        valueLink={{
          value: selectedItem,
          requestChange: (e, id) => {
            ConfigPageActions.getSummary(id).then(summary => {
              this.setState({
                selectedItem: id,
                versions: summary.versions
              });
            });
          }
        }}
      >
        {list}
      </SelectableList>
    );
  }

  getSummaryList() {
    const {searchTerm} = this.state;
    return (
      <div>
        <TextField
          fullWidth
          hintText={searchTerm ? null : "Type to filter"}
          value={searchTerm}
          onChange={(event) => this.setState({ searchTerm: event.target.value })}
        />
        {this.getItemList()}
      </div>
    );
  }

  getVersionList() {
    const {selectedItem, selectedVersion, versions} = this.state;
    if (selectedItem === null) {
      return null;
    }
    const list = [];
    _.each(versions, (version) => {
      list.push(
        <ListItem
          key={version.id}
          value={version.id}
          primaryText={version.description}
        />
      );
      list.push(<Divider />);
    });
    return (
      <SelectableList
        valueLink={{
          value: selectedVersion,
          requestChange: (e, id) => this.setState({ selectedVersion: id })
        }}
      >
        {list}
      </SelectableList>
    );
  }

  render() {
    const {onSave} = this.props;
    const {description, selectedItem, selectedVersion} = this.state;
    const actions = [
      <FlatButton label="Cancel" onTouchTap={() => this.onRequestClose()} />,
      <FlatButton
        primary
        label="Save"
        disabled={!selectedVersion || !description}
        onTouchTap={() => onSave && onSave(description, selectedItem, selectedVersion)}
      />
    ];
    return (
      <Dialog
        open={true}
        title="Clone Configuration"
        modal={false}
        contentStyle={{ minWidth: '80%' }}
        bodyStyle={{ minHeight: 400 }}
        onRequestClose={() => this.onRequestClose()}
        actions={actions}
      >
        <TextField
          floatingLabelText="New Description"
          value={description}
          onChange={(event) => this.setState({ description: event.target.value })}
        />
        <table style={{ width: '100%', tableLayout: 'fixed', borderSpacing: 8, borderCollapse: 'separate' }}>
          <tr>
            <th style={{ fontSize: 18 }}>Configurations</th>
            <th style={{ fontSize: 18 }}>Version</th>
          </tr>
          <tr>
            <td style={{ verticalAlign: 'top' }}>
              {this.getSummaryList()}
            </td>
            <td style={{ verticalAlign: 'top' }}>
              {this.getVersionList()}
            </td>
          </tr>
        </table>
      </Dialog>
    );
  }
}

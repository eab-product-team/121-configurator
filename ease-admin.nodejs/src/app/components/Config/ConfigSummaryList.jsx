import React, {Component, PropTypes} from 'react';
import {Table, TableHeader, TableHeaderColumn, TableBody, TableRow, TableRowColumn, TextField} from 'material-ui';

import ConfirmDialog from '../CustomView/ConfirmDialog.jsx';

import AppBarActions from '../../actions/AppBarActions';
import ConfigPageActions from '../../actions/ConfigPageActions';

export default class ConfigSummaryList extends Component {

  static propTypes = {
    module: PropTypes.string,
    title: PropTypes.string,
    createBtnLabel: PropTypes.string,
    deleteMsg: PropTypes.string
  };

  constructor(props) {
    super(props);
    this.state = {
      summaries: [],
      selectedIds: [],
      creating: false,
      confirmDelete: false,
      searchTerm: null
    };
    this.fetch();
  }

  componentDidUpdate() {
    this.updateAppbar();
  }

  fetch() {
    const {module} = this.props;
    ConfigPageActions.getSummaries(module).then(summaries => {
      this.setState({
        summaries,
        selectedIds: [],
        creating: false,
        confirmDelete: false,
        searchTerm: null
      });
    });
  }

  updateAppbar() {
    const {title, createBtnLabel} = this.props;
    const {summaries, selectedIds} = this.state;
    const actions = [];
    if (selectedIds.length) {
      actions.push({
        id: 'delete',
        title: 'Delete',
        type: 'customButton',
        onTouchTap: () => this.setState({ confirmDelete: true })
      });
    }
    actions.push({
      id: 'new',
      title: createBtnLabel,
      type: 'customButton',
      onTouchTap: () => this.setState({ creating: true })
    });
    AppBarActions.updateAppBar(title, actions);
  }

  selectSummary(id) {
    ConfigPageActions.openConfig(id);
  }

  onDelete() {
    const {summaries, selectedIds} = this.state;
    ConfigPageActions.deleteSummaries(selectedIds).then(() => this.fetch());
  }

  _getColumnHeaders() {
    return []; // override this function
  }

  _getColumnValues() {
    return []; // override this function
  }

  _getCreateDialog() {
    return null; // override this function
  }

  _showRow(searchTerm, row) {
    return false; // override this function
  }

  getRows(items) {
    const {summaries, selectedIds} = this.state;
    return _.map(items, (item, index) => {
      const values = this._getColumnValues(item);
      return (
        <TableRow key={item.id} selected={selectedIds.indexOf(item.id) > -1}>
          {_.map(values, (label, colIndex) => (
            <TableRowColumn
              key={index + '-' + colIndex}
              onTouchTap={() => this.selectSummary(item.id)}
            >
              {label}
            </TableRowColumn>
          ))}
        </TableRow>
      );
    });
  }

  getItems() {
    const {summaries, searchTerm} = this.state;
    return _.filter(summaries, (summary, index) => !searchTerm || this._showRow(searchTerm, summary));
  }

  render() {
    const {deleteMsg} = this.props;
    const {creating, confirmDelete, searchTerm} = this.state;
    const items = this.getItems();
    return (
      <div className="PageContent" style={{ height: 'calc(100vh - 68px)', overflow: 'auto' }}>
        <TextField
          fullWidth
          hintText={searchTerm ? null : "Type to filter"}
          value={searchTerm}
          onChange={(event) =>  this.setState({ searchTerm: event.target.value })}
        />
        <Table
          multiSelectable
          onRowSelection={(rows) => {
            this.setState({
              selectedIds: _.map(rows, row => items[row].id)
            });
          }}
        >
          <TableHeader enableSelectAll={false}>
            <TableRow>
              {_.map(this._getColumnHeaders(), (label, index) => (
                <TableHeaderColumn key={index}>{label}</TableHeaderColumn>
              ))}
            </TableRow>
          </TableHeader>
          <TableBody deselectOnClickaway={false}>
            {this.getRows(items)}
          </TableBody>
        </Table>
        {this._getCreateDialog(!!creating)}
        <ConfirmDialog
          open={confirmDelete}
          msg={deleteMsg}
          onConfirm={() => this.onDelete()}
          onClose={() => this.setState({ confirmDelete: false })}
        />
      </div>
    );
  }

}

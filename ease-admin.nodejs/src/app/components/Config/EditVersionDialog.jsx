import React, {Component, PropTypes} from 'react';
import {Dialog, FlatButton, TextField} from 'material-ui';

export default class EditVersionDialog extends Component {

  static propTypes = {
    open: PropTypes.bool,
    onSave: PropTypes.func,
    onClose: PropTypes.func
  };

  constructor() {
    super();
    this.state = {
      description: null
    };
  }

  onRequestClose() {
    const {onClose} = this.props;
    onClose && onClose();
  }

  render() {
    const {open, onSave} = this.props;
    const {description} = this.state;
    const actions = [
      <FlatButton label="Cancel" onTouchTap={() => this.onRequestClose()} />,
      <FlatButton primary label="Save" disabled={!description} onTouchTap={() => onSave && onSave(description)} />
    ];
    return (
      <Dialog
        open={open}
        title="Save Version"
        modal={false}
        onRequestClose={() => this.onRequestClose()}
        actions={actions}
      >
        <TextField
          fullWidth
          floatingLabelText="Description"
          value={description}
          onChange={(event) => this.setState({ description: event.target.value })}
        />
      </Dialog>
    );
  }

};

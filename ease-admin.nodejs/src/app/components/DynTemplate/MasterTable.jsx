let React = require("react");
let mui = require("material-ui");
let StylePropable = mui.Mixins.StylePropable;

import Table from "material-ui/lib/table/table";
import TableBody from "material-ui/lib/table/table-body";
import TableFooter from "material-ui/lib/table/table-footer";
import TableHeader from "material-ui/lib/table/table-header";
import TableHeaderColumn from "material-ui/lib/table/table-header-column";
import TableRow from "material-ui/lib/table/table-row";
import TableRowColumn from "material-ui/lib/table/table-row-column";

let DynMasterTable = require("../DynamicUI/DynMasterTable.jsx");
let RightDialog = require("../CustomView/RightDialog.jsx");

let AppBarActions = require('../../actions/AppBarActions.js');
let DynActions = require('../../actions/DynActions.js');

let ContentStore = require('../../stores/ContentStore.js');
let HomeStore = require('../../stores/HomeStore.js');

let MasterTable = React.createClass({
  mixins: [React.LinkedStateMixin, StylePropable],

  getInitialState() {
    return {
      content: ContentStore.getPageContent(),
    };
  },

  componentDidMount() {
    ContentStore.addChangeListener(this.onChange);

    this.shouldComponentUpdate();
  },

  componentWillUnmount() {
    ContentStore.removeChangeListener(this.onChange);
  },

  gettingContent: false,

  shouldComponentUpdate: function(nextProps, nextState) {
    var content = ContentStore.getPageContent();
    if (!content || !content.template) {
      if (!this.gettingContent) {
        this.gettingContent = true;
        DynActions.reflesh();
      }
      return false;
    }
    return true;
  },

  onChange() {
    this.gettingContent = false;
    this.setState({
      content: ContentStore.getPageContent(),
    });
  },

  handleTapRow(e, row) {
    console.debug("[ProductList] - [handleTapRow]", e.target, row);
    // TODO: goto product page
    let content = this.state.content;
    let backPage = HomeStore.getCurrentPage();
    DynActions.goDynDetail(content.values.list[row], backPage);
  },

  handleRowSelection(rows) {
    console.debug("[ProductList] - [handleRowSelection]", rows);

    let content = this.state.content;

    if (rows instanceof Array && rows.length > 0 && rows != 'none') {
      var isNewVersion = true;
      var isClone = true;

      for (let i = 0; i < rows.length; i++) {
        var row = content.values.list[rows[i]];
        // Handle "Create New Version" button
        if (typeof row.isNewVersion != 'undefined') {
          if (!row.isNewVersion) {
            isNewVersion = false;
          }
        }
        // Handle "Clone" button
        if (typeof row.isClone != 'undefined') {
          if (!row.isClone) {
            isClone = false;
          }
        }
      }

      if (rows.length != 1) {
        isNewVersion = false;
      }

      return isNewVersion && isClone ? 1 : isNewVersion ? 3 : isClone ? 2 : 0;
    }

    return 0;
  },

  render() {
    var windowHeight = window.innerHeight;
    var content = this.state.content;
    console.debug('productlist: render : ', content);

    if (content == null || content.template == null) {
      return <div key="content"></div>;
    } else {
      console.debug("dynList content", content);
      return(
        <div key="content" className="PageContent">
          <DynMasterTable
            id = "productList"
            list = {content.values.list}
            total = {content.values.total}
            template = {content.template}
            show = {true}
            multiSelectable = {true}
            height = { (windowHeight - 170) + "px"}
            handleTapRow = {this.handleTapRow}
            handleRowSelection = {this.handleRowSelection} />

          <RightDialog
            show={false} />
        </div>
      );
    }
  }

});

module.exports = MasterTable;

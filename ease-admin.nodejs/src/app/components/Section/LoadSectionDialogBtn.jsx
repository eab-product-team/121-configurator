import React, {Component, PropTypes} from 'react';
import { Dialog, FlatButton, RadioButtonGroup, RadioButton } from 'material-ui';
import {SelectableContainerEnhance} from 'material-ui/lib/hoc/selectable-enhance';

import ConfigItemActions from '../../actions/ConfigItemActions';

export default class LoadSectionDialogBtn extends Component {

  static propTypes = {
    sectionType: PropTypes.string,
    onLoad: PropTypes.func,
    style: PropTypes.object
  };

  constructor() {
    super();
    this.state = {
      open: false,
      sectionList: [],
      selected: null
    };
  }

  open() {
    this.setState({
      open: true,
      sectionList: [],
      selected: null
    }, () => {
      const {sectionType} = this.props;
      ConfigItemActions.listConfigs(sectionType).then(list => {
        this.setState({
          sectionList: list
        });
      });
    });
  }

  close() {
    this.setState({ open: false });
  }

  render() {
    const {onLoad, style} = this.props;
    const {open, sectionList, selected} = this.state;
    const actions = [
      <FlatButton
        label="Cancel"
        onTouchTap={() => this.close()}
      />,
      <FlatButton
        primary
        label="Load"
        disabled={!selected}
        onTouchTap={() => {
          var item = sectionList.find(item => item.id === selected);
          if (item && item.value) {
            onLoad && onLoad(JSON.parse(item.value));
            this.close();
          }
        }}
      />
    ];
    return (
      <div style={{ display: 'inline-block' }}>
        <FlatButton
          style={style}
          label="Load Section"
          onTouchTap={() => this.open()}
        />
        <Dialog
          open={open}
          title="Load Section from Repository"
          modal={false}
          onRequestClose={() => this.close()}
          actions={actions}
        >
          <RadioButtonGroup name="sections" onChange={(e, value) => this.setState({ selected: value })}>
            {_.map(sectionList, item => (
              <RadioButton
                key={item.id}
                value={item.id}
                label={item.name + (item.description ? ' (' + item.description + ')' : '')}
                style={{ height: 40 }}
              />
            ))}
          </RadioButtonGroup>
        </Dialog>
      </div>
    );
  }

};

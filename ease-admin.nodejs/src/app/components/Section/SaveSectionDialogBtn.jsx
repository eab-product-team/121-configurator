import React, {Component, PropTypes} from 'react';
import { TextField, Dialog, FlatButton } from 'material-ui';

import ConfigItemActions from '../../actions/ConfigItemActions';

export default class SaveSectionDialogBtn extends Component {

  static propTypes = {
    sectionType: PropTypes.string,
    value: PropTypes.object,
    style: PropTypes.object
  };

  constructor() {
    super();
    this.state = {
      open: false,
      name: '',
      description: ''
    };
  }

  open() {
    this.setState({
      open: true,
      name: '',
      description: ''
    });
  }

  close() {
    this.setState({ open: false });
  }

  render() {
    const {sectionType, value, style} = this.props;
    const {open, name, description} = this.state;
    const actions = [
      <FlatButton
        label="Cancel"
        onTouchTap={() => this.close()}
      />,
      <FlatButton
        primary
        label="Save"
        disabled={!name}
        onTouchTap={() => {
          ConfigItemActions.createConfig(sectionType, name, description, value).then(() => this.close());
        }}
      />
    ];
    return (
      <div style={{ display: 'inline-block' }}>
        <FlatButton
          style={style}
          label="Save Section"
          onTouchTap={() => this.open()}
        />
        <Dialog
          open={open}
          title="Save Section to Repository"
          modal={false}
          onRequestClose={() => this.close()}
          actions={actions}
        >
          <TextField
            fullWidth
            floatingLabelText="Name"
            value={name}
            onChange={(event) => this.setState({ name: event.target.value })}
          />
          <TextField
            fullWidth
            floatingLabelText="Description"
            value={description}
            onChange={(event) => this.setState({ description: event.target.value })}
          />
        </Dialog>
      </div>
    );
  }

};

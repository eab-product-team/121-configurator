import React from 'react';

import ConfigSumamryList from '../Config/ConfigSummaryList.jsx';
import NewTemplateDialog from './NewTemplateDialog.jsx';

import ConfigPageActions from '../../actions/ConfigPageActions';
import ConfigConstants from '../../constants/ConfigConstants';

export default class PDFSummary extends ConfigSumamryList {

  static defaultProps = {
    module: 'pdf',
    title: 'PDFs',
    createBtnLabel: 'New Template',
    deleteMsg: 'Are you sure to delete the selected templates?'
  };

  _getColumnHeaders() {
    return [
      'Template Name',
      'Description',
      'Last Updated By'
    ]
  };

  _getColumnValues(row) {
    return [
      row.name,
      row.description,
      row.updateBy + ' / ' + new Date(row.updateDate).format(ConfigConstants.DateTimeFormat)
    ]
  };

  _showRow(searchTerm, row) {
    const term = searchTerm.toUpperCase();
    return (row.name && row.name.toUpperCase().indexOf(term) > -1) ||
        (row.description && row.description.toUpperCase().indexOf(term) > -1) ||
        (row.updateBy && row.updateBy.toUpperCase().indexOf(term) > -1);
  }

  onCreate(name, description) {
    const {module} = this.props;
    ConfigPageActions.createSummary({
      module,
      type: 'template',
      name,
      description
    }).then(() => this.fetch());
  }

  _getCreateDialog(open) {
    return (
      <NewTemplateDialog
        open={open}
        onSave={(name, description) => this.onCreate(name, description)}
        onClose={() => this.setState({ creating: null })}
      />
    );
  }

}

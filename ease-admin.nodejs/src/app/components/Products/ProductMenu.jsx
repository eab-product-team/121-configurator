let React = require('react');
let mui = require('material-ui');
let StylePropable = mui.Mixins.StylePropable;

import Divider from 'material-ui/lib/divider';
import List from 'material-ui/lib/lists/list';
import ListItem from 'material-ui/lib/lists/list-item';
import { SelectableContainerEnhance } from 'material-ui/lib/hoc/selectable-enhance';
let SelectableList = SelectableContainerEnhance(List);
import appTheme from '../../theme/appBaseTheme.js';

var ProductActions = require('../../actions/ProductActions.js');
let ProductMenuStore = require('../../stores/ProductMenuStore.js');
let ProductStore = require('../../stores/ProductStore.js');
let ContentStore = require('../../stores/ContentStore.js');
let AppStore = require('../../stores/AppStore.js');
let AppBarActions = require('../../actions/AppBarActions.js');

let ProductMenu = React.createClass({
  mixins: [React.LinkedStateMixin, StylePropable],

  childContextTypes: {
    muiTheme: React.PropTypes.object,
  },

  getChildContext() {
    return {
      muiTheme: appTheme.getTheme(),
    };
  },

  getStyles() {
    console.debug("getStyles:", appTheme);
    return {
      // Styles
      headerStyle: {
        fontSize: '20px',
        paddingLeft: '16px'
      },
      subheaderStyle: {
        paddingLeft: '16px',
        color: "gray"
      },
      dividerStyle: {
        marginTop: '4px',
        marginBottom: '4px'
      },
      selectionListStyle: {

        backgroundColor:'transparent'
      },
      selectedItemStyle: {
        backgroundColor: 'transparent',
        color: appTheme.palette.primary1Color
      },
      body:{
        height: "calc(100vh - 56px)",
        position:"relative",
        width:"280px",
        overflowY: 'auto'

      },
      border:{
        position:"absolute",
        right:"0px",
        top:"0px",
        width:"2px",
        height:"calc(100vh - 56px)",
        backgroundColor:"#DDDDDD"
      },
    }
  },

  getInitialState() {
    var rows = this.genMenu(ProductMenuStore.getMainMenu());
    var index = 1;
    return {
      selectedIndex: index,
      rows: rows,
      overlay: this.props.overlay
    };
  },

  menuMap: [],

  genMenu(mainMenu) {
      var valueIndex = 1;
      var rows = [];
      var curSection = '1';
      var styles = this.getStyles();
      if (mainMenu) {
        for (let i = 0; i < mainMenu.length; i++) {
          var item = mainMenu[i];
          var id = item.id;
          var type = item.type;
          var title = item.title;
          var child = item.subItems;
          var disabled = item.disabled;
          var section = (''+item.seq)[0];

          if (type == 'header') {
            rows.push(<h1 key={'header'+j} style={styles.headerStyle}>{title}</h1>);
          } else if (type == 'subheader') {
            rows.push(<p key={'subheader'+j} style={styles.subheaderStyle}>{title}</p>);
          } else {
            var childRow = [];

            if (child) {
              for (let c = 0; c < child.length; c++) {
                var subRow = child[c];
                if (subRow) {
                  var subRowId = subRow.id;
                  var subRowType = subRow.type;
                  var subRowTitle = subRow.title;
                  childRow.push(<ListItem key={subRowId} value={valueIndex} primaryText={subRowTitle} disabled={disabled}  />);

                  this.menuMap[valueIndex] = subRow;
                  valueIndex++;
                }
              }
              rows.push(<ListItem key={id} primaryText={title} primaryTogglesNestedList={true} nestedItems={childRow}  disabled={disabled} style={{color:(disabled)?"#AAAAAA":"#000000"}} />);
            } else {
              rows.push(<ListItem key={id} value={valueIndex} primaryText={title}  disabled={disabled} style={{color:(disabled)?"#AAAAAA":"#000000"}} />);

              this.menuMap[valueIndex] = item;
              valueIndex++;
            }
          }
        }
      }
      return rows;
  },


  componentDidMount() {
    ProductMenuStore.addChangeListener(this.onMenuChange);
  },

  componentWillUnmount: function() {
    ProductMenuStore.removeChangeListener(this.onMenuChange);
  },

  onMenuChange() {
    var menuMap = this.menuMap;
    var selectPage = ProductStore.getCurrentPage();
    var index = 1;
    for (let i in menuMap){
      if(selectPage.id == menuMap[i].id)
        index = parseInt(i);
    }
    var rows = this.genMenu(ProductMenuStore.getMainMenu());
    this.setState({selectedIndex: index, rows: rows});
  },

  handleUpdateSelectedIndex(e,index) {
    var menu = this.menuMap;
    var selectedIndex = this.state.selectedIndex;
    if(index != this.state.selectedIndex){
      if(menu[selectedIndex].id != 'ProdBrochureNCover'){
          var content = ContentStore.getPageContent();
          if(content.values.isNew){
            this.setState({selectedIndex: index});
          }
          ProductActions.productChangePage(content, menu[index].id);
      }
      else{
        //upload attachment
        //check attachment diff
        var orgBNC= ContentStore.getPageContent().values.ProdBrochureNCover;
        var changedBNC = ContentStore.getPageContent().changedValues.ProdBrochureNCover;
        var upload = false;
        for(var i in orgBNC){
          var langValueO = orgBNC[i];
          var langValueC = changedBNC[i];
          for(var j in langValueO){
            if(langValueO[j] != langValueC[j])
              upload = true;
          }
        }
        if(upload){
          AppBarActions.submitProdBNC();
        }
        ProductActions.changePage(menu[index]);
      }
  }
  },

  propTypes:{
    overlay: React.PropTypes.bool
  },

  getDefaultProps() {
    return {
      overlay: false
    }
  },

  render() {
    var {
      style,
      ...others,
    } = this.props;
    let styles = this.getStyles();
    return (
      <div key="productMenu" style={this.mergeAndPrefix(styles.body, style)} {...others}>
        <SelectableList key="prodMenuList"
          valueLink={{value: this.state.selectedIndex, requestChange: this.handleUpdateSelectedIndex}}
          selectedItemStyle={ styles.selectedItemStyle }
          style={ styles.selectionListStyle }>
          {this.state.rows}
        </SelectableList>
        <div style={styles.border}>
        </div>
      </div>
    );

  }

});

module.exports = ProductMenu;

/** In this file, we create a React component which incorporates components provided by material-ui */

let React = require('react');
let mui = require('material-ui');
let StylePropable = mui.Mixins.StylePropable;
let ProductStore = require('../../stores/ProductStore.js');
let ContentStore = require('../../stores/ContentStore.js');
let DynMenu = require('../DynamicUI/DynMenu.jsx');
let CurrentColumn = require('../DynamicUI/DynEditableColumn.jsx');
let BNC = require('./BNC.jsx');

let Products = React.createClass({
  mixins: [React.LinkedStateMixin, StylePropable],

  componentDidMount() {
    // console.debug('homepage did mount');
    ContentStore.addChangeListener(this.onContentChange);
    ProductStore.addChangeListener(this.onPageChange);
  },

  componentWillUnmount: function() {
    ProductStore.removeChangeListener(this.onPageChange);
    ContentStore.removeChangeListener(this.onContentChange);
  },

  shouldComponentUpdate: function(nextProps, nextState) {
    if (!nextState.template) {
      if (!this.gettingContent) {
        this.gettingContent = true;
        DynActions.reflesh();
      }
      return false;
    } else {
      var s1 = nextState.section ? nextState.section.id : "";
      var s2 = this.state.section ? this.state.section.id : "";
      if (s1 != s2) {
        return true;
      }

      if (checkChanges(this.state.values.id, nextState.values.id) ||
            checkChanges(this.state.values.version, nextState.values.version) ){
        return true;
      }

      if(this.state.values.taskId && nextState.values.taskId
        && checkChanges(this.state.values.taskId[0], nextState.values.taskId[0])){
          return true;
        }

      if(checkExist(nextState, 'changedValues.TaskInfo.statusCode')
        && checkChanges(nextState.taskStatusCode,nextState.changedValues.TaskInfo.statusCode)){
          nextState.taskStatusCode = nextState.changedValues.TaskInfo.statusCode;
          return true;
        }

      if (this.refs.current) {

        var showCondItems = this.refs.current.showCondItems;
        var changedId = ContentStore.getChangedFieldId();
        ContentStore.resetChangeFieldId(null);
        if (showCondItems && changedId && showCondItems[changedId]) {
          return true;
        }
      }
    }
    return checkChanges(this.state.template, nextState.template);
  },

  onPageChange() {
    var section = this.getSection(ProductStore.getCurrentPage(), this.state.template);
    var currPage = ProductStore.getCurrentPage();
    if(currPage.id == 'ProdBrochureNCover'){
      ContentStore.isBNC = true;
    }else {
      ContentStore.isBNC = false;
    }
    this.setState({currentPage: currPage, section: section});
  },

  onContentChange() {
    var {
      template,
      values,
      changedValues
    } = ContentStore.getPageContent();
    var section = this.getSection(ProductStore.getCurrentPage(), template);
    template.validate = this._validate;
    template.checkChanges = this._checkChanges;
    var orgValues = this.state.values;
    var newState = {template: template, menuList: template.menu, section: section, changedValues: changedValues};

    if (checkChanges(values.id, orgValues.id) || checkChanges(values.version, orgValues.version)){
      newState.values = ContentStore.getPageContent().values = cloneObject(values);
      newState.changedValues = ContentStore.getPageContent().changedValues = cloneObject(values);
    }

    if(!changedValues){
      newState.changedValues = ContentStore.getPageContent().changedValues = cloneObject(values);
    }
    this.setState(newState);
  },

  _validate(values) {
    var content = ContentStore.getPageContent();
    var templateItems = content.template.items;
    var sectionId = ProductStore.getCurrentPage().id;
    var isNew = content.values.isNew;
    var isChanged = false;
    if(isNew && values && content.values){
      isChanged = checkChanges(values, content.values);
    }else if(!isNew && values && content.values){
      if(!isEmpty(values[sectionId]) && isEmpty(content.values[sectionId]))
        isChanged = true;
      else if(values[sectionId] && content.values[sectionId])
        isChanged = checkChanges(values[sectionId], content.values[sectionId]);
    }
    var self = this;

    if(isNew && !values){
      return {
        error: true,
        errorMsg: content.values.errorMsg
      }
    } else if (!isChanged && !isNew) {
      return {
        error: false,
        skipCheck: true
      };
    }

    var checkValidItem = function (items, values) {
      for (let i in items) {
        var item = items[i];
        if(item.id=='brochure'){
          console.debug(item);
        }
        if (item.items) {
          if (item.type == 'action') {
            //do nothing...
          } else if (item.type == 'section') {
            var result = checkValidItem(item.items, values);
            if (result.error) {
              return result;
            }
          } else if (item.mandatory) {
            if (item.id == undefined) {
              var result = checkValidItem(item.items, values);
              if (result.error) {
                return result;
              }
            } else if (values[item.id] == undefined || values[item.id] == '') {
              var errorMsg = item.title + ' is missing';
              return {
                error: true,
                errorMsg: errorMsg
              };
            } else if (checkValidItem(item.items, values[item.id])) {
              var result = checkValidItem(item.items, values[item.id]);
              if (result.error) {
                return result;
              }
            }
          }
          if (item.type == 'grid') {
            if (self.refs["current"] && self.refs["current"].refs[item.id]) {
              if (!self.refs["current"].refs[item.id].validation()) {
                var grid = self.refs["current"].refs[item.id].refs[item.id];
                return {
                  error: true,
                  errorMsg: grid.errorMsg,
                }
              }
            }
          }
        } else {
          if (item.mandatory && (isEmpty(values) || (values && isEmpty(values[item.id])))) {
            var errorMsg = item.title + ' is missing';
            return {
              error: true,
              errorMsg: errorMsg
            };
          }
        }
      }

      return {
        error: false
      };
    }

    if (isNew) {
      var error = (checkValidItem(templateItems[0].items, values[templateItems[0].id]).error || checkValidItem(templateItems[1].items, values[templateItems[1].id]).error);
      return {
        error: error,
        errorMsg: content.values.errorMsg
      }
    } else {
      for (let i in templateItems){
        var item = templateItems[i];
        if( sectionId == item.id ){
          return checkValidItem(item.items, values[item.id]);
        }
      }
    }
  },

  _checkChanges(orgValues, values) {
    var items = ContentStore.getPageContent().template.items;
    var change = false;
    var changedValues = {};
    if (values && typeof values == 'object') {
      if (orgValues && typeof orgValues == 'object') {
        for (let i in items) {
          var item = items[i];
          if (item.id && item.type && item.type.toUpperCase() == 'SECTION'
            && checkChanges(orgValues[item.id], values[item.id])) {
              change = true;
              changedValues[item.id] = values[item.id];
          }
        }
      } else {
        changedValues = values;
      }
    }

    let _orgValues = ContentStore.getPageContent().values;
    changedValues['id'] = _orgValues.id;
    changedValues['version'] = _orgValues.version;
    changedValues['selectPage'] = values['selectPage'];
    changedValues['selectPageId'] = values['selectPageId'];
    if(_orgValues.taskId && _orgValues.taskId instanceof Array)
      changedValues['taskId'] = _orgValues.taskId[0];
    if(_orgValues.isNew){
      if(values['selectPage'].id == 'TaskInfo'){
        changedValues['ProdDetail'] = _orgValues['ProdDetail'];
      }else{
        changedValues['TaskInfo'] = _orgValues['TaskInfo'];
      }
    }
    return {
      change: change,
      changedValues: changedValues
    };
  },

  getInitialState() {
    var {
      template,
      values
    } = ContentStore.getPageContent();
    var section = template.items[0];
    var currPage = ProductStore.getCurrentPage();
    var changedValues = ContentStore.getPageContent().changedValues = cloneObject(values);
    ContentStore.getPageContent().valuesBackup = values;
    var taskStatusCode = (checkExist(changedValues, 'TaskInfo.statusCode'))?changedValues.TaskInfo.statusCode:undefined;

    // validate
    template.validate = this._validate;
    // checkChanges
    template.checkChanges = this._checkChanges;

    return {
      id: values.id?values.id:"",
      version: values.version?values.version:1,
      currentPage: currPage,
      template: template,
      menuList: template.menu,
      values: values,
      changedValues: changedValues,
      section: section,
      taskStatusCode: taskStatusCode
    };
  },

  getSection(page, template){
    var templateItems = template.items;
    for (let i in templateItems) {
      var section = templateItems[i];
      if (section.id == page.id) {
        return section;
      }
    }
  },

  render: function() {
    console.debug('[Products] - [render]', this.state.section);
    var section = this.state.section;
    if (this.state.section) {
      let containerStyle = {
        flex: 1
      };
      let dataContainerStyle = {
        width:'100%',
        height:"calc(100vh - 58px)",
        overflowY:"auto",
      };
      var id = section.id;
      var content = '';

      if(id == 'ProdBrochureNCover'){
        content =
        <BNC template = {section}/>
        ;
      }else{
        if (!this.state.changedValues[id]) {
          this.state.changedValues[id] = {}
        }
        content =(
          <CurrentColumn
            style={dataContainerStyle}
            show = {true}
            template = {section}
            values = { this.state.values[id] }
            changedValues = { this.state.changedValues[id] }
            id = "current"
            ref = "current" />
        );
      }
      return (
        <div className="PageContent">
          <DynMenu
            key={"prodDynMenu"}
            ref={"prodDynMenu"}
            show={true}
            module={"/Products/Detail"}
            items={this.state.menuList}
            style={{width: "280px"}}/>
          <div style={containerStyle}>
            {content}
          </div>
        </div>
      );
    } else {
      return null;
    }
  }
});

module.exports = Products;

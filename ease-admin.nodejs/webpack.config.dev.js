const webpack = require('webpack');

module.exports = {
    entry: {
        bundle: './src/app/app.jsx'
    },
    output: {
        filename: 'app.js',
    },
    devServer: {
        hot: true,
        port: 8081
    },
    module: {
        loaders: [{
          test: /\.jsx?$/,
          loader: 'babel-loader',
          exclude: /node_modules/,
          query: {
            presets: ['react', 'es2015', 'stage-0']
          }
        }]
    }
}
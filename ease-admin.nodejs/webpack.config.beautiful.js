const webpack = require('webpack');

module.exports = {
    entry: {
        bundle: './src/app/app.jsx'
    },
    output: {
        filename: '../src/main/webapp/web/app.beautiful.js'
    },
    module: {
        loaders: [{
          test: /\.jsx?$/,
          loader: 'babel-loader',
          exclude: /node_modules/,
          query: {
            presets: ['react', 'es2015', 'stage-0']
          }
        }]
    }
}

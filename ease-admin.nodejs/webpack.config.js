const webpack = require('webpack');

module.exports = {
    entry: {
        bundle: './src/app/app.jsx'
    },
    output: {
        filename: '../src/main/webapp/web/app.js'
    },
    module: {
        loaders: [{
          test: /\.jsx?$/,
          loader: 'babel-loader',
          exclude: /node_modules/,
          query: {
            presets: ['react', 'es2015', 'stage-0']
          }
        }]
    },
    plugins: [
      new webpack.optimize.UglifyJsPlugin({
        comments: false,
        // compressor: {
        //   warnings: false,
        //   pure_getters: true,
        //   unsafe: true,
        //   unsafe_comps: true,
        //   screw_ie8: true,
        //   conditionals: true,
        //   unused: true,
        //   comparisons: true,
        //   sequences: true,
        //   dead_code: true,
        //   evaluate: true,
        //   if_return: true,
        //   join_vars: true
        // },
      //   exclude: [/\.min\.js$/gi]
      })
    ]
}

# Prerequisites

* Node.js 7+ (https://nodejs.org/en/download/releases/)
* JDK 1.8
* Maven 3.5+ (https://maven.apache.org/download.cgi)
* JBoss EAP 7.0+


# Build

## Initial Setup
1. Install java libraries
 * `mvn install`
2. Install node dependencies
 * `cd ease-admin.nodejs`
 * `npm install`
3. JBoss setup
 * Unzip clean jboss zip file
 * Add Oracle driver as jboss module (JBOSS_HOME/modules/oracle)
 * Add data source <b>java:jboss/EASEADMIN-DS</b>
 * Add symbolic link to WAR output folder (for non-Eclipse IDE)

## Build client-side source
Compile and minify client-side code to a single JS file in the webapp folder.
* Build app.js only
 * `npm run build`
* Build app.beautiful.js (for debug in production environment)
 * `npm run build-beautiful`
* Build both files
 * `npm run build-all`

## Build Java source
Build java source and output in the target folder.
* `mvn install`


# Development
1. Start jboss server
2. Start hot server
 * `npm run dev`
3. Access configurator with <b>dev</b> flag
 * http://localhost:8080/ease-admin/?dev


# Deployment (Openshift)
1. Copy the WAR folder (target/ease-admin-1.0-SNAPSHOT) to `deployments/ease-admin.war` folder, commit and push
2. Trigger the build in Openshift
